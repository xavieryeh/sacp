<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\Password;

class MemberController extends Controller
{
    public function change_password()
    {
        return view('member.change_password');
    }

    public function change_password_save(Request $request)
    {
        $validated = $request->validate([
            'current_password' => 'required|current_password',
            'password' => ['required','confirmed',Password::default()],
        ]);

        $user = Auth::user();
        // 檢查最近三次密碼
        // $user->password
    }
}
