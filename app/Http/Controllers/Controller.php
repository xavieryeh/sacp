<?php
namespace  App\Http\Controllers;
//use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
//use App\Http\Controllers\Controller;
use App\Models\AppLog as AppLog;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;
use PDO;

use App\Models\User;
use Illuminate\Http\Request;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    //顯示所有資料
    public function connect(){

        $posts=DB::table('TOOLLIST')->where('id', '=', "1")->get();
        //$posts = $db->select('select * from [MACHINE_LIST]');
        //return view('layouts/index', ['articles' => $posts]);
         echo json_encode($posts);
    }
    public function phpinfo(){
        return view('phpinfo');
    }
    public function getcsrf(){
        return csrf_token();
    }
    public function showbanndePage(Request $request){
        return view('bannerPage',['id'=>$request->id]);
    }
    public function showbanndePageforYear(Request $request){
        return view('bannerPageforYear',['id'=>$request->id]);
    }
    public function Log(Request $request){
        AppLog::Log($request->func,$request->action);
    }
}


