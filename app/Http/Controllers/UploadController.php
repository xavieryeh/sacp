<?php

namespace App\Http\Controllers;

use App\Models\Upload as Upload;
use App\Jobs\ProcessCaseJob as ProcessCaseJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class UploadController extends Controller
{
    // Load 檔案管理-國內外情勢分析頁面
    public function getFileAnalyze(Request $request)
    {
        $SQLComm = sprintf("SELECT [File_System]
                                ,[File_type]
                                ,[File_Date]
                                ,convert(varchar,[UpLoad_Date],120) [UpLoad_Date]
                                ,[UserName]
                                ,[File_Source]
                            FROM [UpLoad_Log]
                            where [File_System]='國內外情勢分析'");
        $Datas = DB::select($SQLComm);
        return view('fileAnalyze', compact('Datas'));
    }

    // Load 檔案管理-國內外情勢分析-檔案上傳頁面
    public function getFileAnalyzeUpload(Request $request)
    {
        $yearlist = array();
        for ($i = date("Y") - 10; $i <= date("Y") + 10; $i++) {
            array_push($yearlist, $i - 1911);
        }
        $monthlist = array();
        for ($i = 1; $i <= 12; $i++) {
            array_push($monthlist, str_pad($i, 2, "0", STR_PAD_LEFT));
        }
        return view("fileAnalyzeUpload", compact('yearlist', 'monthlist'));
    }

    // 檔案上傳-國內外情勢分析-刪除資料
    public function uploadLogDelAnalyzeDate(Request $request)
    {
        $keyValue = $request->keyValue;
        DB::table('UpLoad_Log')->select(DB::raw('File_System,convert(varchar,UpLoad_Date,120) as UpLoad_Date'))->where('File_System', '國內外情勢分析')->where('UpLoad_Date', $keyValue)->delete();
        $SQLComm = sprintf("SELECT [File_System]
                                ,[File_type]
                                ,[File_Date]
                                ,convert(varchar,[UpLoad_Date],120) [UpLoad_Date]
                                ,[UserName]
                                ,[File_Source]
                            FROM [UpLoad_Log]
                            where [File_System]='國內外情勢分析'");
        $Datas = DB::select($SQLComm);
        return $Datas;
    }

    // 檔案上傳-國內外情勢分析-新增資料
    public function uploadLogAnalyzeDate(Request $request){
        $Type = $request->type;
        $Date1 = $request->Date1;
        $Date2 = $request->Date2;
        $Message = $request->message;

        $posts= DB::table('Abroad_Analyze')->insert([
            'UpLoad_Date' => date('Y/m/d H:i:s'),
            'Message' => $Message,
            'UserName' => '若薇',
        ]); 

        if ($posts == 1) {
            $posts2= DB::table('UpLoad_Log')->insert([
                'File_System' => '國內外情勢分析',
                'File_Source' => '',
                'File_type' => $Type,
                'File_Date'=> $Date1.'/'.$Date2,
                'UpLoad_Date'=> date('Y/m/d H:i:s'),
                'UserName'=> '若薇'
            ]);
            DB::commit();
            return "Success";
        } else {
            DB::rollBack();
            return "Error";
        }
    }
    // Load 檔案管理-檔案上傳-檔案列表
    public function getUploadPage(Request $request)
    {
        $Type = $request->Type;
        $Info = Upload::getTypeInfo($Type);
        $Title = $Info['Title'];
        $Datas = DB::select($Info['SQLComm']);
        return view($Info['Page'], compact('Datas', 'Type', 'Title'));
    }

    // Load 檔案管理-檔案上傳-檔案列表-檔案上傳頁面
    public function getUploadPageUploadFile(Request $request)
    {
        $yearlist = array();
        for ($i = date("Y") - 10; $i <= date("Y") + 10; $i++) {
            array_push($yearlist, $i - 1911);
        }
        $monthlist = array();
        for ($i = 1; $i <= 12; $i++) {
            array_push($monthlist, str_pad($i, 2, "0", STR_PAD_LEFT));
        }
        $Type = $request->Type;
        $Info = Upload::getTypeInfo($Type);
        $Title = $Info['Title'];
        return view("fileUploadPageUploadFile", compact('Type', 'Title', 'yearlist', 'monthlist'));
    }

    // 檔案上傳-非訪視記錄-刪除資料
    public function uploadLogDelDate(Request $request)
    {
        $keyValue = $request->keyValue;
        $Params = explode("_", $keyValue);//[File_System]_[File_Source]_[UpLoad_Date]_[File_Date]
        DB::table('UpLoad_Log')->select(DB::raw('File_System,File_Source,convert(varchar,UpLoad_Date,120) as UpLoad_Date'))->where('File_System', $Params[0])->where('File_Source', $Params[1])->where('UpLoad_Date', $Params[2])->delete();

        $CHINADate=str_replace('/','',$Params[3]);
        $ParamsDate=explode("/", $Params[3]);
        $yearmonth = strval((intval($ParamsDate[0]) + 1911)) . '-' . $ParamsDate[1] . '-01';
        if($Params[0]=='地方網絡資料' )
        {
            if($Params[1]=='警察局' )
            {
            
             DB::table('SEIZED')->where('CHINA_Date', $CHINADate)->delete();
             DB::table('FIRST_OFFENDER')->where('CHINA_Date', $CHINADate)->delete();
            }
            else if($Params[1]=='少年及家事法院')
            {
             DB::table('Juvenile_Court')->where('Date', date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')))->delete();
            }
            else if($Params[1]=='衛生局')
            {
             DB::table('Medical_Treatment')->where('Date', date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')))->delete();
            }
            else if($Params[1]=='民政局')
            {
             DB::table('CIVIL_AFFAIRS_DEPT')->where('Year', intval($ParamsDate[0]) + 1911)->where('Month', $ParamsDate[1])->delete();
            }
        }
        else if($Params[0]=='中央資料' )
        {
            if($Params[1]=='衛福部-其他' )
            {
             DB::table('DRUG_RECIDIVISM_RATE')->where('Date',  date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')))->delete();
             DB::table('DRUG_RECIDIVISM_RATE_Halfyear')->where('Date',  date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')))->delete();
             DB::table('DRUG_RECIDIVISM_RATE_Oneyear')->where('Date',  date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')))->delete();
             DB::table('DRUG_RECIDIVISM_RATE_Twoyear')->where('Date',  date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')))->delete();
            }
            else if($Params[1]=='衛福部-全國藥癮' )
            {
             DB::table('DRUG_RECIDIVISM_RATE')->where('Date',  date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')))->delete();
            }
            else if($Params[1]=='戶政司' )
            {
             DB::table('LOCATIONE_POPULATION_Num')->where('Date',  date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')))->delete();
            }
        }
        else if($Params[0]=='兒少保案管系統' )
        {
            DB::table('DRUGS_LIST_TEENAGER_Bak')->where('File_UpLoadDate',  date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')))->delete();
        }
        else if($Params[0]=='三四級講習' )
        {
            DB::table('3_4_Lecture_Bak')->where('File_UpLoadDate',  date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')))->delete();
        }
        else if($Params[0]=='監所銜接輔導' )
        {
            DB::table('Coupling_Counseling')->where('Date',  date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')))->delete();
        }
        else if($Params[0]=='特定營業場所' )
        {
            DB::table('Specific_Business_demanagement')->where('File_Date',  date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')))->delete();
            DB::table('Specific_Business_Registered_Companies')->where('Date',  date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')))->delete();
            DB::table('Specific_Business_Unsolicited')->where('Date',  date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')))->delete();
        }
  
        $Info = Upload::getTypeInfo($request->type);
        $Datas = DB::select($Info['SQLComm']);
        return $Datas;
    }

    // 檔案上傳-訪視記錄-刪除資料
    public function uploadLogDelVRDate(Request $request)
    {
        $keyValue = $request->keyValue;
        DB::table('UpLoad_Log')->where('File_System', '訪視記錄')->where('Case_ID', $keyValue)->delete();
        DB::table('訪視記錄測試')->where('案號',  $keyValue)->delete();
        $Info = Upload::getTypeInfo($request->type);
        $Datas = DB::select($Info['SQLComm']);
        return $Datas;
    }

    public function HealthOther(Request $request)
    {
        if (UploadController::Upload_CatchListFiles($request) == 'Success' && UploadController::Upload_ServiceListFiles($request) == 'Success'
            && UploadController::Upload_24HoursFiles($request) == 'Success' && UploadController::importExecl($request) == 'Success') {
            $year10 = $request->year + 1911;
            $month11 = $request->month;

            $posts4 = DB::table('UpLoad_Log')->insert([
                'File_System' => '中央資料',
                'File_Source' => '衛福部-其他',
                'File_type' => '月度資料',
                'File_Date' => $request->year . '/' . $month11,
                'UpLoad_Date' => date('Y/m/d H:i:s'),
                'UserName' => '若薇',
            ]);
            return 'Success';
        } else {
            return 'Error';
        }
    }

    #region 再犯率分析excel匯入
    public function importExecl(Request $request)
    {
        try {
            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 0;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            for ($_row = 2; $_row <= $rowCnt; $_row++) {
                $isNull = true;
                $index = 0;
                for ($_column = 2; $_column <= 13; $_column++) {

                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;
                    $cell = $currSheet->getCell($cellId);

                    if (isset($options['format'])) {
                        /* 獲取格式 */
                        $format = $cell->getStyle()->getNumberFormat()->getFormatCode();
                        /* 記錄格式 */
                        $options['format'][$_row][$cellName] = $format;
                    }

                    if (isset($options['formula'])) {
                        /* 獲取公式，公式均為=號開頭資料 */
                        $formula = $currSheet->getCell($cellId)->getValue();

                        if (0 === strpos($formula, '=')) {
                            $options['formula'][$cellName . $_row] = $formula;
                        }
                    }

                    if (isset($format) && 'm/d/yyyy' == $format) {
                        /* 日期格式翻轉處理 */
                        $cell->getStyle()->getNumberFormat()->setFormatCode('yyyy/mm/dd');
                    }

                    $data[$_row - 2][$index] = trim($currSheet->getCell($cellId)->getFormattedValue());
                    if ($data[$_row - 2][$index] == "") {
                        unset($data[$_row - 2][$index]);
                    }
                    if ($_row == 2 || $_row == 5 || $_row == 8 || $_row == 11) {
                        if (trim($currSheet->getCell($cellName . ($_row + 1))->getFormattedValue()) == "" || trim($currSheet->getCell($cellName . ($_row + 2))->getFormattedValue()) == "") {
                            unset($data[$_row - 2][$index]);
                        }
                    }
                    if (!empty($data[$_row - 2][$index])) {
                        $isNull = false;
                    }
                    $index++;
                }

                /* 判斷是否整行資料為空，是的話刪除該行資料 */
                if ($isNull) {
                    unset($data[$_row - 2]);
                }
            }
            return UploadController::Updatedatabase($data);
            //return $data;
        } catch (\Exception$e) {
            throw $e;
        }
    }
    #endregion
    #region 再犯率分析整理上傳資料庫
    public static function Updatedatabase($array)
    {

        $data1 = [];
        $data2 = [];
        $data3 = [];
        $data4 = [];
        $sdata1 = [];
        $sdata2 = [];
        $sdata3 = [];
        $sdata4 = [];

        for ($_row = 0; $_row <= 2; $_row++) {
            array_push($data1, $array[$_row]);
        }
        for ($_row = 3; $_row <= 5; $_row++) {
            array_push($data2, $array[$_row]);
        }
        for ($_row = 6; $_row <= 8; $_row++) {
            array_push($data3, $array[$_row]);
        }
        for ($_row = 9; $_row <= 11; $_row++) {
            array_push($data4, $array[$_row]);
        }
        $index = 0;
        foreach ($data1[0] as $value) {
            $year = explode("年", $value);
            $month = explode("月", $year[1]);
            $yearmonth = strval((intval($year[0]) + 1911)) . '-' . $month[0] . '-01';
            array_push($sdata1, (object) ["Date" => date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')), "CHINA_Date" => $value, "Location" => "高雄市", "Recidivism_Rate" => round(floatval($data1[1][$index]) * 100, 2)]);
            array_push($sdata1, (object) ["Date" => date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')), "CHINA_Date" => $value, "Location" => "全國", "Recidivism_Rate" => round(floatval($data1[2][$index]) * 100, 2)]);
            $index++;
        }

        $index = 0;
        foreach ($data2[0] as $value) {

            $year = explode("年", $value);
            $month = explode("月", $year[1]);
            $yearmonth = strval((intval($year[0]) + 1911)) . '-' . $month[0] . '-01';
            array_push($sdata2, (object) ["Date" => date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')), "CHINA_Date" => $value, "Location" => "高雄市", "Recidivism_Rate" => round(floatval($data2[1][$index]) * 100, 2)]);
            array_push($sdata2, (object) ["Date" => date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')), "CHINA_Date" => $value, "Location" => "全國", "Recidivism_Rate" => round(floatval($data2[2][$index]) * 100, 2)]);
            $index++;
        }

        $index = 0;
        foreach ($data3[0] as $value) {
            $year = explode("年", $value);
            $month = explode("月", $year[1]);
            $yearmonth = strval((intval($year[0]) + 1911)) . '-' . $month[0] . '-01';
            array_push($sdata3, (object) ["Date" => date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')), "CHINA_Date" => $value, "Location" => "高雄市", "Recidivism_Rate" => round(floatval($data3[1][$index]) * 100, 2)]);
            array_push($sdata3, (object) ["Date" => date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')), "CHINA_Date" => $value, "Location" => "全國", "Recidivism_Rate" => round(floatval($data3[2][$index]) * 100, 2)]);
            $index++;
        }
        $index = 0;
        foreach ($data4[0] as $value) {
            $year = explode("年", $value);
            $month = explode("月", $year[1]);
            $yearmonth = strval((intval($year[0]) + 1911)) . '-' . $month[0] . '-01';
            array_push($sdata4, (object) ["Date" => date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')), "CHINA_Date" => $value, "Location" => "高雄市", "Recidivism_Rate" => round(floatval($data4[1][$index]) * 100, 2)]);
            array_push($sdata4, (object) ["Date" => date(date_format(date_create($yearmonth), 'Y-m-d H:i:s')), "CHINA_Date" => $value, "Location" => "全國", "Recidivism_Rate" => round(floatval($data4[2][$index]) * 100, 2)]);
            $index++;
        }
        try {
            DB::beginTransaction();
            foreach ($sdata1 as $all) {

                $posts5 = DB::table('DRUG_RECIDIVISM_RATE')->where('Date', $all->Date)->where('Location', $all->Location)->delete();

                $posts = DB::table('DRUG_RECIDIVISM_RATE')->insert([
                    'Date' => $all->Date,
                    'CHINA_Date' => $all->CHINA_Date,
                    'Location' => $all->Location,
                    'Recidivism_Rate' => $all->Recidivism_Rate,
                ]);
            }
            foreach ($sdata2 as $all) {
                $posts5 = DB::table('DRUG_RECIDIVISM_RATE_Halfyear')->where('Date', $all->Date)->where('Location', $all->Location)->delete();
                $posts2 = DB::table('DRUG_RECIDIVISM_RATE_Halfyear')->insert([
                    'Date' => $all->Date,
                    'CHINA_Date' => $all->CHINA_Date,
                    'Location' => $all->Location,
                    'Recidivism_Rate' => $all->Recidivism_Rate,
                ]);
            }
            foreach ($sdata3 as $all) {
                $posts5 = DB::table('DRUG_RECIDIVISM_RATE_Oneyear')->where('Date', $all->Date)->where('Location', $all->Location)->delete();
                $posts3 = DB::table('DRUG_RECIDIVISM_RATE_Oneyear')->insert([
                    'Date' => $all->Date,
                    'CHINA_Date' => $all->CHINA_Date,
                    'Location' => $all->Location,
                    'Recidivism_Rate' => $all->Recidivism_Rate,
                ]);
            }
            foreach ($sdata4 as $all) {
                $posts5 = DB::table('DRUG_RECIDIVISM_RATE_Twoyear')->where('Date', $all->Date)->where('Location', $all->Location)->delete();
                $posts4 = DB::table('DRUG_RECIDIVISM_RATE_Twoyear')->insert([
                    'Date' => $all->Date,
                    'CHINA_Date' => $all->CHINA_Date,
                    'Location' => $all->Location,
                    'Recidivism_Rate' => $all->Recidivism_Rate,
                ]);
            }
            if ($posts == 1 && $posts2 == 1 && $posts3 == 1 && $posts4 == 1) {
                DB::commit();
                return "Success";
            } else {
                DB::rollBack();
                return "Error";
            }

        } catch (\Exception$e) {
            throw $e;
        }

        // return  $posts;
        // return  json_encode($data1_1[0]->Date);

    }
    #endregion
    #region 全國各縣市藥癮個案excel匯入
    public function importDrugcaseFilesExecl(Request $request)
    {

        try {
            $year = $request->year + 1911;
            $month = $request->month;

            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 0;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            for ($_row = 7; $_row <= 28; $_row++) {
                $cellName = Coordinate::stringFromColumnIndex(1);
                $cellId = $cellName . $_row;
                $data[0][$index] = trim($currSheet->getCell($cellId)->getFormattedValue());

                $index++;
            }
            $index = 0;
            for ($_row = 7; $_row <= 28; $_row++) {
                $cellName = Coordinate::stringFromColumnIndex(5);
                $cellId = $cellName . $_row;
                $data[1][$index] = trim($currSheet->getCell($cellId)->getFormattedValue());

                $index++;
            }
            $result = [];
            $index = 0;
            foreach ($data[0] as $value) {
                array_push($result, (object) ["Date" => date_format(date_create($year . '-' . $month . '-1'), 'Y-m-d'), "CHINA_Date" => '110', "Location" => $value, "Num" => intval($data[1][$index])]);
                $index++;
            }
            try {
                DB::beginTransaction();
                foreach ($result as $data) {
                    $posts5 = DB::table('LOCATIONE_TUBE_Num')->where('Date', $data->Date)->where('Location', $data->Location)->delete();

                    $posts = DB::table('LOCATIONE_TUBE_Num')->insert([
                        'Date' => $data->Date,
                        'CHINA_Date' => $data->CHINA_Date,
                        'Location' => $data->Location,
                        'Num' => $data->Num,
                    ]);
                }
                if ($posts == 1) {
                    $year10 = $request->year + 1911;
                    $month11 = $request->month;

                    $posts4 = DB::table('UpLoad_Log')->insert([
                        'File_System' => '中央資料',
                        'File_Source' => '衛福部-全國藥癮',
                        'File_type' => '月度資料',
                        'File_Date' => $request->year . '/' . $month11,
                        'UpLoad_Date' => date('Y/m/d H:i:s'),
                        'UserName' => '若薇',
                    ]);
                    DB::commit();
                    return "Success";
                } else {
                    DB::rollBack();
                    return "Error";
                }
            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
    #endregion
    #region 全國人口excel匯入
    public function importAllPeopleFilesExecl(Request $request)
    {
        try {
            $year = $request->year + 1911;
            $month = $request->month;

            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 0;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            for ($_row = 8; $_row <= 29; $_row++) {
                $cellName = Coordinate::stringFromColumnIndex(1);
                $cellName2 = Coordinate::stringFromColumnIndex(3);
                $cellId = $cellName . $_row;
                $data[0][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                $cellId = $cellName2 . $_row;
                $data[1][$index] = trim($currSheet->getCell($cellId)->getFormattedValue());
                $index++;
            }

            $result = [];
            $index = 0;
            foreach ($data[0] as $value) {
                array_push($result, (object) ["Date" => date_format(date_create($year . '-' . $month . '-1'), 'Y-m-d'), "CHINA_Date" => '110', "Location" => $value, "Num" => intval($data[1][$index])]);
                $index++;
            }
            try {
                DB::beginTransaction();
                foreach ($result as $data) {
                    $posts5 = DB::table('LOCATIONE_POPULATION_Num')->where('Date', $data->Date)->where('Location', $data->Location)->delete();

                    $posts = DB::table('LOCATIONE_POPULATION_Num')->insert([
                        'Date' => $data->Date,
                        'CHINA_Date' => $data->CHINA_Date,
                        'Location' => $data->Location,
                        'Num' => $data->Num,
                    ]);
                }
                if ($posts == 1) {
                    $year10 = $request->year + 1911;
                    $month11 = $request->month;

                    $posts4 = DB::table('UpLoad_Log')->insert([
                        'File_System' => '中央資料',
                        'File_Source' => '戶政司',
                        'File_type' => '月度資料',
                        'File_Date' => $request->year . '/' . $month11,
                        'UpLoad_Date' => date('Y/m/d H:i:s'),
                        'UserName' => '若薇',
                    ]);
                    DB::commit();
                    return "Success";
                } else {
                    DB::rollBack();
                    return "Error";
                }

            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
    #endregion
    #region 追輔數excel匯入
    public function Upload_CatchListFiles(Request $request)
    {
        try {
            $year = $request->year + 1911;
            $month = $request->month;

            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 1;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            for ($_row = 2; $_row <= 5; $_row++) {
                $cellName = Coordinate::stringFromColumnIndex(1);
                $cellId = $cellName . $_row;
                $data[0][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                $index++;
            }
            $index = 0;
            for ($_row = 2; $_row <= 6; $_row++) {
                $cellName = Coordinate::stringFromColumnIndex(2);
                $cellId = $cellName . $_row;
                $data[1][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                $index++;
            }
            $result = [];
            $index = 0;
            $during = $data[1][0];
            foreach ($data[0] as $value) {
                array_push($result, (object) ["Date" => date_format(date_create($year . '-' . $month . '-01'), 'Y-m-d'), "Date_Merk" => $during, "Item" => $value, "Count" => intval($data[1][$index])]);
                $index++;
            }
            try {
                DB::beginTransaction();

                foreach ($result as $data) {
                    $posts5 = DB::table('Coach_List')->where('Date', $data->Date)->where('Item', $data->Item)->delete();

                    $posts = DB::table('Coach_List')->insert([
                        'Date' => $data->Date,
                        'Date_Merk' => $data->Date_Merk,
                        'Item' => $data->Item,
                        'Count' => $data->Count,
                    ]);
                }
                if ($posts == 1) {
                    DB::commit();
                    return "Success";
                } else {
                    DB::rollBack();
                    return "Error";
                }

            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
    #endregion
    #region 轉介服務數
    public function Upload_ServiceListFiles(Request $request)
    {
        try {
            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 2;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            for ($_row = 2; $_row <= 7; $_row++) {
                $cellName = Coordinate::stringFromColumnIndex(1);
                $cellId = $cellName . $_row;
                $data[0][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                $index++;
            }
            $index = 0;
            for ($_row = 2; $_row <= 8; $_row++) {
                $cellName = Coordinate::stringFromColumnIndex(2);
                $cellId = $cellName . $_row;
                $data[1][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                $index++;
            }
            $result = [];
            $index = 0;
            $during = array_shift($data[1]);
            foreach ($data[0] as $value) {
                array_push($result, (object) ["Date" => date_format(date_create('2021-09-01'), 'Y-m-d H:i:s'), "Date_Merk" => $during, "Item" => $value, "Count" => intval($data[1][$index])]);
                $index++;
            }
            try {
                DB::beginTransaction();
                foreach ($result as $data) {
                    $posts5 = DB::table('Service_List')->where('Date', $data->Date)->where('Item', $data->Item)->delete();

                    $posts = DB::table('Service_List')->insert([
                        'Date' => $data->Date,
                        'Date_Merk' => $data->Date_Merk,
                        'Item' => $data->Item,
                        'Count' => $data->Count,
                    ]);
                }
                if ($posts == 1) {
                    DB::commit();
                    return "Success";
                } else {
                    DB::rollBack();
                    return "Error";
                }

            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
    #endregion
    #region 24小時專線服務excel匯入
    public function Upload_24HoursFiles(Request $request)
    {
        try {
            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 3;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            for ($_column = 2; $_column <= 13; $_column++) {
                $cellName = Coordinate::stringFromColumnIndex($_column);
                $cellId = $cellName . 2;

                $data[0][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                if (str_replace(" ", "", trim($currSheet->getCell($cellName . 2)->getFormattedValue())) == "") {
                    unset($data[0][$index]);
                }
                $index++;
            }
            $index = 0;
            for ($_row = 3; $_row <= 13; $_row++) {
                $cellName = Coordinate::stringFromColumnIndex(1);
                $cellId = $cellName . $_row;
                $data[1][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                $index++;
            };
            $index = 0;
            for ($_row = 3; $_row <= 13; $_row++) {
                $index = 0;
                for ($_column = 1; $_column <= 13; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;
                    $data[$_row][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                    if ($data[$_row][$index] == "") {
                        unset($data[$_row][$index]);
                    }

                    $index++;
                }
                array_shift($data[$_row]);
            }

            $result = [];

            // return $data;
            // $during = array_shift($data[1]);
            $num = 0;
            foreach ($data[0] as $value) {
                $index = 3;
                $year = explode("年", $value);
                $month = explode("月", $year[1]);
                $yearmonth = strval((intval($year[0]) + 1911)) . '-' . $month[0] . '-01';

                foreach ($data[1] as $title) {
                    // foreach($data[$index] as $each){
                    if ($num < count($data[$index])) {
                        array_push($result, (object) ["Date" => date_format(date_create($yearmonth), 'Y-m-d H:i:s'), "Date_Merk" => $value, "Item" => $title, "Count" => intval($data[$index][$num])]);
                    } else {
                        array_push($result, (object) ["Date" => date_format(date_create($yearmonth), 'Y-m-d H:i:s'), "Date_Merk" => $value, "Item" => $title, "Count" => 0]);
                    }
                    // }
                    $index++;
                }
                $num++;
            }
            try {
                DB::beginTransaction();
                foreach ($result as $data) {
                    $posts5 = DB::table('24Hours_List')->where('Date', $data->Date)->where('Item', $data->Item)->delete();

                    $posts = DB::table('24Hours_List')->insert([
                        'Date' => $data->Date,
                        'Date_Merk' => $data->Date_Merk,
                        'Item' => $data->Item,
                        'Count' => $data->Count,
                    ]);
                }
                if ($posts == 1) {
                    DB::commit();
                    return "Success";
                } else {
                    DB::rollBack();
                    return "Error";
                }
            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
    #endregion
    #region Upload_DrugFiles
    public function Upload_DrugFiles(Request $request)
    {
        try {

            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 0;
            $currSheet = $obj->getSheet($sheet);
            $currSheet2 = $obj->getSheet(1);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            $during = str_replace(" ", "", trim($currSheet->getCell(Coordinate::stringFromColumnIndex(1) . 1)->getFormattedValue()));
            //$year = substr($during, 0, 3);
            //$month = substr($during, 3, 2);
            $year = $request->year + 1911;
            $month = $request->month;
            $yearmonth = strval((intval($year))) . '-' . $month . '-01';
            for ($_column = 1; $_column <= 11; $_column++) {
                $cellName = Coordinate::stringFromColumnIndex($_column);
                $cellId = $cellName . 2;

                $data[0][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                if ($data[0][$index] == "") {
                    unset($data[0][$index]);
                }

                $index++;
            }
            array_shift($data[0]);
            for ($_column = 1; $_column <= 11; $_column++) {
                $cellName = Coordinate::stringFromColumnIndex($_column);
                $cellId = $cellName . 3;

                $data[1][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                if ($data[1][$index] == "") {
                    unset($data[1][$index]);
                }

                $index++;
            }
            array_shift($data[1]);
            for ($_column = 1; $_column <= 11; $_column++) {
                $cellName = Coordinate::stringFromColumnIndex($_column);
                $cellId = $cellName . 9;

                $data[2][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                if ($data[2][$index] == "") {
                    unset($data[2][$index]);
                }

                $index++;
            }
            array_shift($data[2]);
            $data[3][0]=0;
            for ($_column = 12; $_column <= 13; $_column++) {
                $cellName = Coordinate::stringFromColumnIndex($_column);
                $cellId = $cellName . 8;
                $data[3][0]+=intval(str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue())));
            }
            $index = 0;
            $result = [];
            for ($_column = 1; $_column <= 13; $_column++) {
                $cellName = Coordinate::stringFromColumnIndex($_column);
                $cellId = $cellName . 1;
                $data2[0][$index] = str_replace(" ", "", trim($currSheet2->getCell($cellId)->getFormattedValue()));
                if (str_replace(" ", "", trim($currSheet2->getCell($cellName . 3)->getFormattedValue())) == "" && str_replace(" ", "", trim($currSheet2->getCell($cellName . 4)->getFormattedValue())) == "") {
                    unset($data2[0][$index]);
                }
                $index++;
            }
            $index = 0;
            for ($_column = 1; $_column <= count($data2[0]); $_column++) {
                $cellName = Coordinate::stringFromColumnIndex($_column);
                $cellId = $cellName . 3;
                $data2[1][$index] = str_replace(" ", "", trim($currSheet2->getCell($cellId)->getFormattedValue()));
                $cellId = $cellName . 4;
                $data2[2][$index] = str_replace(" ", "", trim($currSheet2->getCell($cellId)->getFormattedValue()));
                $index++;
            }
            
           

            array_shift($data2[0]);
            array_shift($data2[1]);
            array_shift($data2[2]);
            $result = [];
            $index = 0;
            foreach ($data2[0] as $title) {
                // $year = substr($title, 0, 3);
                // $month = substr($title, 3, 2);
                $yearmonth = strval((intval($year))) . '-' . $month . '-01';
                array_push($result, (object) ["Date" => date_format(date_create($year . '-' . $month . '-01'), 'Y-m-d'), "CHINA_Date" => $title, "CRIMINAL_PENALTY" => intval($data2[1][$index]), "ADMINISTRATIVE_PENALTY" => intval($data2[2][$index])]);
                $index++;
            }
            try {
                DB::beginTransaction();
                $posts5 = DB::table('SEIZED')->where('Date', $yearmonth)->delete();

                $posts = DB::table('SEIZED')->insert([
                    'Date' => $yearmonth,
                    'CHINA_Date' => $during,
                    'Level_1_Case' => $data[0][0],
                    'Level_2_Case' => $data[0][1],
                    'Level_3_Case' => $data[0][2],
                    'Level_4_Case' => $data[0][3],
                    'Other_Case' => $data[0][4],
                    'Level_1_Num' => $data[1][0],
                    'Level_2_Num' => $data[1][1],
                    'Level_3_Num' => $data[1][2],
                    'Level_4_Num' => $data[1][3],
                    'Other_Num' => $data[1][4],
                    'Level_1_Gram' => intval($data[2][0]),
                    'Level_2_Gram' => intval($data[2][1]),
                    'Level_3_Gram' => intval($data[2][2]),
                    'Level_4_Gram' => intval($data[2][3]),
                    'Other_Gram' => intval($data[2][4]),
                    'Sale_Num'=>$data[3][0]
                ]);
                $index = 0;
                foreach ($result as $data) {

                    $DateT =substr($data->CHINA_Date , 0 , 3 )+1911;
                    $monthT =substr($data->CHINA_Date , 3 , 2 );


                    $posts5 = DB::table('FIRST_OFFENDER')->where('CHINA_Date', $data->CHINA_Date)->delete();

                    $posts2 = DB::table('FIRST_OFFENDER')->insert([
                        'Date' =>date_create($DateT. '-' . $monthT . '-01'), //$data->date_format(date_create($DateT . '-' . $monthT . '-01'), 'Y-m-d'),
                        'CHINA_Date' => $data->CHINA_Date,
                        'CRIMINAL_PENALTY' => $data->CRIMINAL_PENALTY,
                        'ADMINISTRATIVE_PENALTY' => $data->ADMINISTRATIVE_PENALTY,
                    ]);
                }

                if ($posts == 1 && $posts2 == 1) {
                    $year10 = $request->year + 1911;
                    $month11 = $request->month;

                    $posts4 = DB::table('UpLoad_Log')->insert([
                        'File_System' => '地方網絡資料',
                        'File_Source' => '警察局',
                        'File_type' => '月度資料',
                        'File_Date' => $request->year . '/' . $month11,
                        'UpLoad_Date' => date('Y/m/d H:i:s'),
                        'UserName' => '若薇',
                    ]);
                    DB::commit();
                    return "Success";
                } else {
                    DB::rollBack();
                    return "Error";
                }
            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
    #endregion
    #region 高雄各區人口數
    public function Upload_PopulationFiles(Request $request)
    {
        try {
            $file = $request->file('Files');
            $year = $request->year;
            $month = $request->month;
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 0;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            for ($_row = 3; $_row <= $rowCnt; $_row++) {
                $index = 0;
                for ($_column = 1; $_column <= $columnCnt; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;
                    $data[$_row - 3][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                    $index++;
                }

            }
            try {
                DB::beginTransaction();
                foreach ($data as $value) {
                    $posts5 = DB::table('CIVIL_AFFAIRS_DEPT')->where('Year', $request->year + 1911)->where('Month', $request->month)->where('Location', $value[0])->delete();

                    $posts = DB::table('CIVIL_AFFAIRS_DEPT')->insert([
                        'CHINA_Year' => $request->year,
                        'Year' => $request->year + 1911,
                        'Month' => $request->month,
                        'Location' => $value[0],
                        'Village' => $value[1],
                        'Neighborhood' => $value[2],
                        'Households' => $value[3],
                        'Population' => $value[4],
                        'Male' => $value[5],
                        'Female' => $value[6],
                    ]);

                }
                if ($posts == 1) {
                    $year10 = $request->year + 1911;
                    $month11 = $request->month;

                    $posts4 = DB::table('UpLoad_Log')->insert([
                        'File_System' => '地方網絡資料',
                        'File_Source' => '民政局',
                        'File_type' => '月度資料',
                        'File_Date' => $request->year . '/' . $month11,
                        'UpLoad_Date' => date('Y/m/d H:i:s'),
                        'UserName' => '若薇',
                    ]);
                    DB::commit();
                    return "Success";
                } else {
                    DB::rollBack();
                    return "Error";
                }
            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
    #endregion
    #region 醫療戒治使用人數
    public function Upload_MDTreatmentFiles(Request $request)
    {
        try {
            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 0;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            for ($_row = 2; $_row < 4; $_row++) {
                $index = 0;
                for ($_column = 2; $_column <= 13; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;
                    $data[$_row - 2][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                    $index++;
                }
            }
            $index = 0;
            $year = $request->year + 1911;
            $result = [];
            foreach ($data[0] as $value) {
                $month = explode("月", $data[0][$index]);
                array_push($result, (object) ["Date" => date_format(date_create($year . '-' . $month[0] . '-01'), 'Y-m-d'), "CHINA_Date" => $value, "Num" => intval($data[1][$index])]);
                $index++;
            }
            try {
                DB::beginTransaction();

                foreach ($result as $data) {
                    $posts5 = DB::table('Medical_Treatment')->where('Date', $data->Date)->delete();

                    $posts = DB::table('Medical_Treatment')->insert([
                        'Date' => $data->Date,
                        'CHINA_Date' => $data->CHINA_Date,
                        'Num' => $data->Num,
                    ]);

                }
                if ($posts == 1) {
                    $year10 = $request->year + 1911;
                    $month11 = $request->month;

                    $posts4 = DB::table('UpLoad_Log')->insert([
                        'File_System' => '地方網絡資料',
                        'File_Source' => '衛生局',
                        'File_type' => '月度資料',
                        'File_Date' => $request->year . '/' . $month11,
                        'UpLoad_Date' => date('Y/m/d H:i:s'),
                        'UserName' => '若薇',
                    ]);
                    DB::commit();
                    return "Success";
                } else {
                    DB::rollBack();
                    return "Error";
                }

            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
    #endregion

    #region 少家院
    public function Upload_Juvenile_CourtFiles(Request $request)
    {
        try {
            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 0;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            for ($_row = 4; $_row <= 7; $_row++) {
                $index = 0;
                for ($_column = 1; $_column <= 5; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;

                    $data[$_row - 4][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                    if ($_column < 5) {
                        if (str_replace(" ", "", trim($currSheet->getCell(Coordinate::stringFromColumnIndex($_column + 1) . $_row)->getFormattedValue())) == "") {
                            unset($data[$_row - 4]);
                        }

                    } else {
                        if ($data[$_row - 4][$index] == "") {
                            unset($data[$_row - 4]);
                        }

                    }
                    $index++;
                }
            }
            try {
                DB::beginTransaction();

                foreach ($data as $value) {

                    $year = explode("年", $value[0]);
                    $month = explode("月", explode("-", $year[1])[1])[0];
                    $yearmonth = strval((intval($year[0]) + 1911)) . '-' . $month . '-01';
                    $posts5 = DB::table('Juvenile_Court')->where('Date', date_format(date_create($yearmonth), 'Y-m-d H:i:s'))->delete();
                    $posts = DB::table('Juvenile_Court')->insert([

                        'Date' => date_format(date_create($yearmonth), 'Y-m-d H:i:s'),
                        'CHINA_Date' => $value[0],
                        'Not_Penalty_Case' => $value[1],
                        'Not_Penalty_Num' => $value[2],
                        'Penalty_Case' => $value[3],
                        'Penalty_Num' => $value[4],
                    ]);
                }
                if ($posts == 1) {
                    $year10 = $request->year + 1911;
                    $month11 = $request->month;

                    $posts4 = DB::table('UpLoad_Log')->insert([
                        'File_System' => '地方網絡資料',
                        'File_Source' => '少年及家事法院',
                        'File_type' => '月度資料',
                        'File_Date' => $request->year . '/' . $month11,
                        'UpLoad_Date' => date('Y/m/d H:i:s'),
                        'UserName' => '若薇',
                    ]);
                    DB::commit();
                    return "Success";
                } else {
                    DB::rollBack();
                    return "Error";
                }

            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
    #endregion

    #region 輔導銜接統計
    public function Upload_Coupling_Counseling(Request $request)
    {
        try {
            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 0;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            for ($_row = 2; $_row <= $rowCnt; $_row++) {
                $index = 0;
                for ($_column = 2; $_column <= 13; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;

                    $data[$_row - 2][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                    if ($_row == 2) {
                        if (str_replace(" ", "", trim($currSheet->getCell(Coordinate::stringFromColumnIndex($_column) . $_row + 1)->getFormattedValue())) == "" && str_replace(" ", "", trim($currSheet->getCell(Coordinate::stringFromColumnIndex($_column) . $_row + 2)->getFormattedValue())) == "") {
                            unset($data[$_row - 2][$index]);
                        } else if ($data[$_row - 2][$index] != "") {
                            $data[$_row - 2][$index] = preg_replace('/\s+/', '', $data[$_row - 2][$index]);
                        }
                    } else {
                        if ($data[$_row - 2][$index] == "") {
                            unset($data[$_row - 2][$index]);
                        }

                    }
                    $index++;
                }
            }
            $index = 0;
            $result = [];
            foreach ($data[0] as $value) {
                $year = substr($value, 0, 3);
                $month = str_replace($year, '', $value);
                $month = str_replace('年', '', $month);
                $month = str_replace('月', '', $month);
                $yearmonth = strval((intval($year) + 1911)) . '-' . $month . '-01';
                array_push($result, (object) ["Date" => date_format(date_create($yearmonth), 'Y-m-d H:i:s'), "CHINA_Date" => $value, "Session" => intval($data[1][$index]), "Person_Times" => intval($data[2][$index])]);
                $index++;
            }
            try {
                DB::beginTransaction();

                foreach ($result as $value) {
                    $posts5 = DB::table('Coupling_Counseling')->where('Date', $value->Date)->delete();
                    $posts = DB::table('Coupling_Counseling')->insert([
                        'Date' => $value->Date,
                        'CHINA_Date' => $value->CHINA_Date,
                        'Session' => $value->Session,
                        'Person_Times' => $value->Person_Times,
                    ]);
                }
                if ($posts == 1) {
                    $year10 = $request->year + 1911;
                    $month11 = $request->month;

                    $posts4 = DB::table('UpLoad_Log')->insert([
                        'File_System' => '監所銜接輔導',
                        'File_Source' => '',
                        'File_type' => '月度資料',
                        'File_Date' => $request->year . '/' . $month11,
                        'UpLoad_Date' => date('Y/m/d H:i:s'),
                        'UserName' => '若薇',
                    ]);
                    DB::commit();
                    return "Success";
                } else {
                    DB::rollBack();
                    return "Error";
                }

            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
    #endregion

    #region 中央案管系統
    public function Upload_DrugList(Request $request)
    {
        try {
            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 0;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }
            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            $isNull = false;
            for ($_row = 2; $_row <= $rowCnt; $_row++) {
                $index = 0;
                for ($_column = 1; $_column <= $columnCnt; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;

                    if ($_column == 7 || $_column == 14 || $_column == 16 || $_column == 30) {
                        if (str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue())) != "") {
                            $data[$_row - 2][$index] = date_format(Date::excelToDateTimeObject(str_replace(" ", "", trim($currSheet->getCell($cellId)->getValue()))), 'Y/m/d');
                        } else {
                            $data[$_row - 2][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                        }
                    } else {
                        $data[$_row - 2][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                    }
                    if ($_column == 1) {
                        $data[$_row - 2][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                        if ($data[$_row - 2][$index] == "") {
                            $isNull = true;
                        }
                    }
                    if ($isNull) {
                        unset($data[$_row - 2]);
                    }
                    $index++;
                }
            }
            //    return $data;
            $index = 0;
            $year10 = $request->year + 1911;
            $month10 = $request->month;
            DB::beginTransaction();
            try {
                $posts5 = DB::table('DRUGS_LIST')->delete();
                $posts5 = DB::table('DRUGS_LIST_Bak')->where('File_UpLoadDate', date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'))->delete();
                foreach ($data as $value) {
                    $year = explode("/", $value[6]);
                    //   $month = explode("/",$year[1]);
                    //   $date = explode("",$month[1]);
                    $yearmonth = strval((intval($year[0]) - 1911) . '-' . $year[1] . '-' . $year[2]);
                    $posts = DB::table('DRUGS_LIST')->insert([
                        'ID' => $value[0],
                        'Case_ID' => $value[1],
                        'Person_ID' => $value[2],
                        'Person_Name' => $value[3],
                        'Manager' => $value[4],
                        'Manage_Type' => $value[5]
                        , 'Counseling_Day_CHINA' => str_replace('-', '/', $yearmonth)
                        , 'Counseling_Day' => date_create($value[6])
                        , 'Drugs_Grade' => $value[7]
                        , 'Drug_Level_1' => $value[8]
                        , 'Drug_Level_2' => $value[9]
                        , 'Drug_Level_3' => $value[10]
                        , 'Drug_Level_4' => $value[11]
                        , 'Gender' => $value[12]
                        , 'Birthday_CHINA' => $value[13]
                        , 'Entry_Age' => $value[14]
                        , 'File_Date' => $value[15]
                        , 'File_Age' => $value[16]
                        , 'Marriage' => $value[17]
                        , 'Education' => $value[18]
                        , 'County_Name' => $value[19]
                        , 'Area_Name' => $value[20]
                        , 'Village_Name' => $value[21]
                        , 'Residence_Address' => $value[22]
                        , 'County_Name_Live' => $value[23]
                        , 'Area_Name_Live' => $value[24]
                        , 'Live_Address' => $value[25]
                        , 'Employment_Status' => $value[26]
                        , 'Employment_Willingness' => $value[27]
                        , 'Job_Type' => $value[28]
                        , 'Closing_Date_CHINA' => $value[29]
                        , 'Closing_Reason' => $value[30]
                        , 'Drug_Addict' => $value[31]
                        , 'Multiple' => $value[32]
                        , '來源編號' => 'A'
                        , '毒品級數' => mb_substr($value[7], 0, 1)
                        , '狀態' => '列管',
                        // ,'初案/再案'=>'1'
                    ]);

                    $posts6 = DB::table('DRUGS_LIST_Bak')->insert([
                        'ID' => $value[0],
                        'Case_ID' => $value[1],
                        'Person_ID' => $value[2],
                        'Person_Name' => $value[3],
                        'Manager' => $value[4],
                        'Manage_Type' => $value[5]
                        , 'Counseling_Day_CHINA' => str_replace('-', '/', $yearmonth)
                        , 'Counseling_Day' => date_create($value[6])
                        , 'Drugs_Grade' => $value[7]
                        , 'Drug_Level_1' => $value[8]
                        , 'Drug_Level_2' => $value[9]
                        , 'Drug_Level_3' => $value[10]
                        , 'Drug_Level_4' => $value[11]
                        , 'Gender' => $value[12]
                        , 'Birthday_CHINA' => $value[13]
                        , 'Entry_Age' => $value[14]
                        , 'File_Date' => $value[15]
                        , 'File_Age' => $value[16]
                        , 'Marriage' => $value[17]
                        , 'Education' => $value[18]
                        , 'County_Name' => $value[19]
                        , 'Area_Name' => $value[20]
                        , 'Village_Name' => $value[21]
                        , 'Residence_Address' => $value[22]
                        , 'County_Name_Live' => $value[23]
                        , 'Area_Name_Live' => $value[24]
                        , 'Live_Address' => $value[25]
                        , 'Employment_Status' => $value[26]
                        , 'Employment_Willingness' => $value[27]
                        , 'Job_Type' => $value[28]
                        , 'Closing_Date_CHINA' => $value[29]
                        , 'Closing_Reason' => $value[30]
                        , 'Drug_Addict' => $value[31]
                        , 'Multiple' => $value[32]
                        , '來源編號' => 'A'
                        , '毒品級數' => mb_substr($value[7], 0, 1)
                        , '狀態' => '列管'
                        , 'File_UpLoadDate' => date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'),
                    ]);
                }
                if ($posts == 1) {
                    $update_query1 = "UPDATE [DRUGS_LIST] SET [初案/再案]='再案' WHERE [Person_ID] in(SELECT 　[Person_ID]  FROM [DRUGS_LIST]
                                group by [Person_ID] having count(*)>1)";
                    $posts1 = DB::statement($update_query1);

                    $update_query10 = "UPDATE [DRUGS_LIST_Bak] SET [初案/再案]='再案' WHERE [Person_ID] in(SELECT 　[Person_ID]  FROM [DRUGS_LIST]
                    group by [Person_ID] having count(*)>1)";
                    $posts10 = DB::statement($update_query10);

                    if ($posts1 == 1) {
                        $update_query2 = "UPDATE [DRUGS_LIST] SET [初案/再案] = '初案' WHERE [初案/再案] is null";
                        $posts2 = DB::statement($update_query2);

                        $update_query20 = "UPDATE [DRUGS_LIST_Bak] SET [初案/再案] = '初案' WHERE [初案/再案] is null";
                        $posts20 = DB::statement($update_query20);
                        if ($posts2 == 1) {
                            $year10 = $request->year + 1911;
                            $month11 = $request->month;

                            $posts4 = DB::table('UpLoad_Log')->insert([
                                'File_System' => '中央案管系統',
                                'File_Source' => '',
                                'File_type' => '月度資料',
                                'File_Date' => $request->year . '/' . $month11,
                                'UpLoad_Date' => date('Y/m/d H:i:s'),
                                'UserName' => '若薇',
                            ]);
                            DB::commit();
                            return "Success";
                        } else {
                            DB::rollBack();
                            return "Error";
                        }

                    } else {
                        DB::rollBack();
                        return "Error";
                    }

                } else {
                    DB::rollBack();
                    return "Error";
                }
            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
    #endregion

    #region 兒少保案管系統
    public function Upload_DrugListTeenager(Request $request)
    {
        try {
            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 0;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            for ($_row = 4; $_row <= $rowCnt; $_row++) {
                $index = 0;
                for ($_column = 1; $_column <= 9; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;
                    $data[$_row - 4][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                    $index++;
                }

                for ($_column = 10; $_column <= 16; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;
                    $data[$_row - 4][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                    $index++;
                }

                for ($_column = 17; $_column <= 22; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;
                    $data[$_row - 4][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                    $index++;
                }

                for ($_column = 23; $_column <= 28; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;
                    $data[$_row - 4][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                    $index++;
                }

            }

            $index = 0;
            $year10 = $request->year + 1911;
            $month10 = $request->month;
            try {
                //DB::beginTransaction();
                $posts5 = DB::table('DRUGS_LIST_TEENAGER')->delete();
                $posts5 = DB::table('DRUGS_LIST_TEENAGER_Bak')->where('File_UpLoadDate', date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'))->delete();

                foreach ($data as $value) {

                    $posts = DB::table('DRUGS_LIST_TEENAGER')->insert([
                        'Case_ID' => $value[0],
                        'Person_Name' => $value[1],
                        'Person_ID' => $value[2],
                        'Gender' => $value[3],
                        'Birthday_CHINA' => $value[4], //生日
                        'Manage_Type' => $value[5],
                        'Area_Name' => $value[6],
                        'Live_Address' => $value[7],
                        'Job_Type' => $value[8],
                        'Education' => $value[9],
                        'Marriage' => $value[10],
                        'Employment_Status' => $value[11],
                        'Employment_Willingness' => $value[12],
                        'Drug_Addict' => $value[13],
                        'Counseling_Day_CHINA' => $value[14],
                        'Entry_Age' => $value[15],
                        'File_Age' => $value[16],
                        'Multiple' => $value[17],
                        'Manager' => $value[18],
                        'Drugs_Grade' => $value[19],
                        'Coaching_Close' => $value[20],
                        'Closing_Date_CHINA' => $value[21],
                        'Closing_Reason' => $value[22],
                        'Listed_Chunhui' => $value[23],
                        'Listed_Chunhui_Close' => $value[24],
                        'Dsacp_Tube' => $value[25],
                        'Education__Tube' => $value[26],
                        'Chunhui_Interrupt' => $value[27],
                        'File_UpLoadDate' => date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'),
                        '來源編號' => 'C',
                        '毒品級數' => mb_substr($value[19], 0, 1),
                        '狀態' => '非列管',
                    ]);

                    $postsb = DB::table('DRUGS_LIST_TEENAGER_Bak')->insert([
                        'Case_ID' => $value[0],
                        'Person_Name' => $value[1],
                        'Person_ID' => $value[2],
                        'Gender' => $value[3],
                        'Birthday_CHINA' => $value[4], //生日
                        'Manage_Type' => $value[5],
                        'Area_Name' => $value[6],
                        'Live_Address' => $value[7],
                        'Job_Type' => $value[8],
                        'Education' => $value[9],
                        'Marriage' => $value[10],
                        'Employment_Status' => $value[11],
                        'Employment_Willingness' => $value[12],
                        'Drug_Addict' => $value[13],
                        'Counseling_Day_CHINA' => $value[14],
                        'Entry_Age' => $value[15],
                        'File_Age' => $value[16],
                        'Multiple' => $value[17],
                        'Manager' => $value[18],
                        'Drugs_Grade' => $value[19],
                        'Coaching_Close' => $value[20],
                        'Closing_Date_CHINA' => $value[21],
                        'Closing_Reason' => $value[22],
                        'Listed_Chunhui' => $value[23],
                        'Listed_Chunhui_Close' => $value[24],
                        'Dsacp_Tube' => $value[25],
                        'Education__Tube' => $value[26],
                        'Chunhui_Interrupt' => $value[27],
                        'File_UpLoadDate' => date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'),
                        '來源編號' => 'C',
                        '毒品級數' => mb_substr($value[19], 0, 1),
                        '狀態' => '非列管',
                    ]);
                }
                if ($posts == 1) {
                    $update_query1 = "UPDATE [DRUGS_LIST_TEENAGER] SET [初案/再案]='再案' WHERE [Person_ID] in(SELECT 　[Person_ID]  FROM [DRUGS_LIST_TEENAGER]
                            group by [Person_ID] having count(*)>1)";
                    $posts1 = DB::statement($update_query1);

                    $update_query10 = "UPDATE [DRUGS_LIST_TEENAGER_Bak] SET [初案/再案]='再案' WHERE [Person_ID] in(SELECT 　[Person_ID]  FROM [DRUGS_LIST_TEENAGER_Bak]
                group by [Person_ID] having count(*)>1)";
                    $posts10 = DB::statement($update_query10);

                    if ($posts1 == 1) {
                        $update_query2 = "UPDATE [DRUGS_LIST_TEENAGER] SET [初案/再案] = '初案' WHERE [初案/再案] is null";
                        $posts2 = DB::statement($update_query2);

                        $update_query20 = "UPDATE [DRUGS_LIST_TEENAGER_Bak] SET [初案/再案] = '初案' WHERE [初案/再案] is null";
                        $posts20 = DB::statement($update_query20);
                        if ($posts2 == 1) {
                            $year10 = $request->year + 1911;
                            $month11 = $request->month;

                            $posts4 = DB::table('UpLoad_Log')->insert([
                                'File_System' => '兒少保案管系統',
                                'File_Source' => '',
                                'File_type' => '月度資料',
                                'File_Date' => $request->year . '/' . $month11,
                                'UpLoad_Date' => date('Y/m/d H:i:s'),
                                'UserName' => '若薇',
                            ]);
                            DB::commit();
                            return "Success";
                        } else {
                            DB::rollBack();
                            return "Error";
                        }

                    } else {
                        DB::rollBack();
                        return "Error";
                    }

                }
            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
    #endregion
    #region 三四級講習
    public function Upload_3_4_Lecture(Request $request)
    {
        try {
            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 0;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            $isNull = false;
            for ($_row = 3; $_row <= $rowCnt; $_row++) {
                $index = 0;
                for ($_column = 1; $_column <= 22; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;
                    $data[$_row - 3][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                    $index++;
                }
            }
            $index = 0;
            try {
                DB::beginTransaction();
                $year10 = $request->year + 1911;
                $month10 = $request->month;
                $posts5 = DB::table('3_4_Lecture')->delete();
                $posts5 = DB::table('3_4_Lecture_Bak')->where('File_UpLoadDate', date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'))->delete();
                foreach ($data as $value) {
                    $posts = DB::table('3_4_Lecture')->insert([
                        'Case_ID' => $value[0],
                        'Counseling_Day_CHINA' => $value[1],
                        'Drugs_Grade' => $value[2],
                        'Person_Name' => $value[3],
                        'Gender' => $value[4],
                        'Person_ID' => $value[5],
                        'Birthday_CHINA' => $value[6],
                        'Entry_Age' => $value[7],
                        'File_Age' => $value[8],
                        'Marriage' => $value[9],
                        'Education' => $value[10],
                        'Job_Type' => $value[11],
                        'Employment_Status' => $value[12],
                        'Employment_Willingness' => $value[13],
                        'Drug_Addict' => $value[14],
                        'Multiple' => $value[15],
                        'Area_Name' => $value[16],
                        'Live_Address' => $value[17],
                        'Lecture_Day_CHINA' => $value[18],
                        'Manager' => $value[19],
                        'Coaching_Close' => $value[20],
                        'Closing_Reason' => $value[21],
                        'Manage_Type' => '三四級毒品',
                        'File_UpLoadDate' => date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'),
                        '來源編號' => 'B',
                        '毒品級數' => mb_substr($value[2], 0, 1),
                        '狀態' => '非列管',
                    ]);

                    $postsb = DB::table('3_4_Lecture_bak')->insert([
                        'Case_ID' => $value[0],
                        'Counseling_Day_CHINA' => $value[1],
                        'Drugs_Grade' => $value[2],
                        'Person_Name' => $value[3],
                        'Gender' => $value[4],
                        'Person_ID' => $value[5],
                        'Birthday_CHINA' => $value[6],
                        'Entry_Age' => $value[7],
                        'File_Age' => $value[8],
                        'Marriage' => $value[9],
                        'Education' => $value[10],
                        'Job_Type' => $value[11],
                        'Employment_Status' => $value[12],
                        'Employment_Willingness' => $value[13],
                        'Drug_Addict' => $value[14],
                        'Multiple' => $value[15],
                        'Area_Name' => $value[16],
                        'Live_Address' => $value[17],
                        'Lecture_Day_CHINA' => $value[18],
                        'Manager' => $value[19],
                        'Coaching_Close' => $value[20],
                        'Closing_Reason' => $value[21],
                        'Manage_Type' => '三四級毒品',
                        'File_UpLoadDate' => date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'),
                        '來源編號' => 'B',
                        '毒品級數' => mb_substr($value[2], 0, 1),
                        '狀態' => '非列管',
                    ]);
                }
                if ($posts == 1) {
                    $update_query1 = "UPDATE [3_4_Lecture] SET [初案/再案]='再案' WHERE [Person_ID] in(SELECT 　[Person_ID]  FROM [3_4_Lecture]
                                group by [Person_ID] having count(*)>1)";
                    $posts1 = DB::statement($update_query1);

                    $update_query10 = "UPDATE [3_4_Lecture_Bak] SET [初案/再案]='再案' WHERE [Person_ID] in(SELECT 　[Person_ID]  FROM [3_4_Lecture_Bak]
                    group by [Person_ID] having count(*)>1)";
                    $posts10 = DB::statement($update_query10);

                    if ($posts1 == 1) {
                        $update_query2 = "UPDATE [3_4_Lecture] SET [初案/再案] = '初案' WHERE [初案/再案] is null";
                        $posts2 = DB::statement($update_query2);

                        $update_query20 = "UPDATE [3_4_Lecture_Bak] SET [初案/再案] = '初案' WHERE [初案/再案] is null";
                        $posts20 = DB::statement($update_query20);
                        if ($posts2 == 1) {
                            $year10 = $request->year + 1911;
                            $month11 = $request->month;

                            $posts4 = DB::table('UpLoad_Log')->insert([
                                'File_System' => '三四級講習',
                                'File_Source' => '',
                                'File_type' => '月度資料',
                                'File_Date' => $request->year . '/' . $month11,
                                'UpLoad_Date' => date('Y/m/d H:i:s'),
                                'UserName' => '若薇',
                            ]);
                            DB::commit();
                            return "Success";
                        } else {
                            DB::rollBack();
                            return "Error";
                        }

                    } else {
                        DB::rollBack();
                        return "Error";
                    }
                }

            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
#endregion

#region 春暉中斷總表
    public function Upload_DRUGS_LIST_TEENAGER_Interrupt(Request $request)
    {
        try {
            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 0;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            $isNull = false;
            for ($_row = 3; $_row <= $rowCnt; $_row++) {
                $index = 0;
                for ($_column = 1; $_column <= 8; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;
                    if ($_column == 5) {
                        if (str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue())) != "") {
                            $data[$_row - 3][$index] = date_format(Date::excelToDateTimeObject(str_replace(" ", "", trim($currSheet->getCell($cellId)->getValue()))), 'Y/m/d');
                        }
                    } else {
                        $data[$_row - 3][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                        if ($data[$_row - 3][$index] == "") {
                            $data[$_row - 3][$index] = null;
                        }
                    }
                    $index++;
                    if ($_column == 1) {
                        $data[$_row - 3][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                        if ($data[$_row - 3][$index] == "") {
                            $isNull = true;
                        }
                    }
                    if ($isNull) {
                        unset($data[$_row - 3]);
                    }
                }
            }
            $index = 0;
            $year10 = $request->year + 1911;
            $month10 = $request->month;
            DB::beginTransaction();
            $posts5 = DB::table('DRUGS_LIST_TEENAGER_Interrupt')->where('File_Date', date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'))->delete();
            try {
                foreach ($data as $value) {
                    $year = explode("/", $value[4]);
                    //   $month = explode("/",$year[1]);
                    //   $date = explode("",$month[1]);
                    $yearmonth = strval($year[0] . '-' . $year[1] . '-' . $year[2]);
                    $birth = date_create($yearmonth);
                    $today = date_create(date('Y-m-d'));
                    $intevel = date_diff($birth, $today);
                    $age = $intevel->format('%y');
                    $posts = DB::table('DRUGS_LIST_TEENAGER_Interrupt')->insert([
                        'ID' => $value[0],
                        'Month' => $value[1],
                        'Person_Name' => $value[2],
                        'Sex' => $value[3],
                        'Birth' => $value[4],
                        'Domicile' => $value[5],
                        'Entry_Age' => $age,
                        'Drug_Type' => $value[6],
                        'Coaching_Close' => $value[7],
                        'File_Date' => date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'),

                    ]);
                }
                if ($posts == 1) {
                    $year10 = $request->year + 1911;
                    $month11 = $request->month;

                    $posts4 = DB::table('UpLoad_Log')->insert([
                        'File_System' => '春暉中斷',
                        'File_Source' => '',
                        'File_type' => '月度資料',
                        'File_Date' => $request->year . '/' . $month11,
                        'UpLoad_Date' => date('Y/m/d H:i:s'),
                        'UserName' => '若薇',
                    ]);
                    DB::commit();
                    return "Success";
                } else {
                    DB::rollBack();
                    return "Error";
                }

            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
#endregion

#region 特殊營業場所
    public function Upload_Specific_Business(Request $request)
    {
        try {
            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 0;
            $currSheet = $obj->getSheet($sheet);
            $currSheet2 = $obj->getSheet(1);
            $currSheet3 = $obj->getSheet(2);
            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $isNull = false;
            for ($_row = 3; $_row <= $rowCnt; $_row++) {
                $index = 0;
                for ($_column = 1; $_column <= 10; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;
                    if ($_column == 1 || $_column == 6 || $_column == 8 || $_column == 10) {
                        $data[$_row - 3][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                        $data[$_row - 3][$index] = preg_replace('/\s+/', '', $data[$_row - 3][$index]);
                    }
                    $index++;
                    if ($_column == 1) {
                        $data[$_row - 3][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                        if ($data[$_row - 3][$index] == "") {
                            $isNull = true;
                        }
                    }
                    if ($isNull) {
                        unset($data[$_row - 3]);
                    }
                }
            }
            $data2 = [];
            $isNull = false;
            for ($_row = 3; $_row <= $rowCnt; $_row++) {
                $index = 0;
                for ($_column = 1; $_column <= $rowCnt; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;
                    if ($_column == 1) {
                        $data2[$_row - 3][$index] = str_replace(" ", "", trim($currSheet2->getCell($cellId)->getFormattedValue()));
                        if ($data2[$_row - 3][$index] == "") {
                            $isNull = true;
                        }
                    } else {
                        $data2[$_row - 3][$index] = str_replace(" ", "", trim($currSheet2->getCell($cellId)->getFormattedValue()));
                        if ($data2[$_row - 3][$index] == "") {
                            unset($data2[$_row - 3][$index]);
                        }

                    }
                    $index++;
                }
                if ($isNull) {
                    unset($data2[$_row - 3]);
                }
            }
            $data3 = [];
            $isNull = false;
            for ($_row = 2; $_row <= $rowCnt; $_row++) {
                $index = 0;
                for ($_column = 1; $_column <= $rowCnt; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;
                    if ($_column == 1) {
                        $data3[$_row - 2][$index] = str_replace(" ", "", trim($currSheet3->getCell($cellId)->getFormattedValue()));
                        if ($data3[$_row - 2][$index] == "") {
                            $isNull = true;
                        }
                    } else {
                        $data3[$_row - 2][$index] = str_replace(" ", "", trim($currSheet3->getCell($cellId)->getFormattedValue()));
                        if ($data3[$_row - 2][$index] == "") {
                            unset($data3[$_row - 2][$index]);
                        }

                    }
                    $index++;
                }
                if ($isNull) {
                    unset($data3[$_row - 2]);
                }
            }
            // return $data2;
            $year10 = $request->year + 1911;
            $month10 = $request->month;
            DB::beginTransaction();
            $posts5 = DB::table('Specific_Business_demanagement')->where('File_Date', date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'))->delete();
            $posts5 = DB::table('Specific_Business_Registered_Companies')->where('Date', date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'))->delete();
            $posts5 = DB::table('Specific_Business_Unsolicited')->where('Date', date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'))->delete();
            try {
                foreach ($data as $value) {
                    array_shift($value);
                    $startDate = str_replace('.', '-', explode("|", $value[3])[0]);
                    $startDate = intval(explode("-", $startDate)[0]) + 1911 . '-' . explode("-", $startDate)[1] . '-' . explode("-", $startDate)[2];
                    $endDate = str_replace('.', '-', explode("|", $value[3])[1]);
                    $endDate = intval(explode("-", $endDate)[0]) + 1911 . '-' . explode("-", $endDate)[1] . '-' . explode("-", $endDate)[2];
                    $posts = DB::table('Specific_Business_demanagement')->insert([
                        'ID' => $value[0],
                        'Domicile' => $value[1],
                        'Business' => $value[2],
                        'Tube_Start' => date_create($startDate),
                        'Tube_End' => date_create($endDate),
                        'File_Date' => date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'),
                    ]);
                }
                foreach ($data2 as $value) {
                    $date_Mark = '1-' . $month10 . '月';
                    $posts2 = DB::table('Specific_Business_Registered_Companies')->insert([
                        'Date' => date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'),
                        'Date_Merk' => $date_Mark,
                        'Item' => $value[0],
                        'Count' => $value[1],
                    ]);
                }
                foreach ($data3 as $value) {
                    $date_Mark = '1-' . $month10 . '月';
                    $posts3 = DB::table('Specific_Business_Unsolicited')->insert([
                        'Date' => date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'),
                        'Date_Merk' => $date_Mark,
                        'Item' => $value[0],
                        'Count' => $value[1],
                    ]);
                }
                if ($posts == 1 && $posts2 == 1 && $posts3 == 1) {
                    $year10 = $request->year + 1911;
                    $month11 = $request->month;

                    $posts4 = DB::table('UpLoad_Log')->insert([
                        'File_System' => '特定營業場所',
                        'File_Source' => '',
                        'File_type' => '月度資料',
                        'File_Date' => $request->year . '/' . $month11,
                        'UpLoad_Date' => date('Y/m/d H:i:s'),
                        'UserName' => '若薇',
                    ]);
                    DB::commit();
                    return "Success";
                } else {
                    DB::rollBack();
                    return "Error";
                }

            } catch (\Exception$e) {
                //throw $e;
                return "Error";
            }
        } catch (\Exception$e) {
            //throw $e;
            return "Error";
        }
    }
#endregion
#region 訪視紀錄
    public function Upload_View_Record(Request $request)
    {
        try {
            $file = $request->file('Files');
            /* 轉碼 */
            $file = iconv("utf-8", "gb2312", $file);
            if (empty($file) or !file_exists($file)) {
                throw new \Exception('檔案不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支援匯入Excel檔案！');
                }
            }

            /* 如果不需要獲取特殊操作，則只讀內容，可以大幅度提升讀取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel物件 */
            $obj = $objRead->load($file);
            /* 獲取指定的sheet表 */
            $sheet = 0;
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 讀取合併行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }
            $columnCnt = 0;
            if (0 == $columnCnt) {
                /* 取得最大的列號 */
                $columnH = $currSheet->getHighestColumn();
                /* 相容原邏輯，迴圈時使用的是小於等於 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 獲取總行數 */
            $rowCnt = $currSheet->getHighestRow();
            $data = [];
            /* 讀取內容 */
            $index = 0;
            $isNull = false;
            $value_row = 1;
            $test = 0;
            for ($_row = 3; $_row <= $rowCnt; $_row++) {
                if ($_row == 15) {
                    $index = 0;
                }

                if ($_row > 14) {
                    $test++;
                }
                for ($_column = 1; $_column <= 10; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId = $cellName . $_row;
                    if ($_row <= 14) {
                        if ($_row == 3) {
                            $data[$value_row - 1][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                            if ($data[$value_row - 1][$index] == "") {
                                unset($data[$value_row - 1][$index]);
                            }
                        }
                        $index = 0;
                    } else {
                        if ($_column == 1) {
                            if (str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue())) != "") {

                                $data[$value_row][$index] = date_format(Date::excelToDateTimeObject(str_replace(" ", "", trim($currSheet->getCell($cellId)->getValue()))), 'Y/m/d');
                            } else {
                                $data[$value_row][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                            }

                        } else {
                            $data[$value_row][$index] = str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue()));
                            $data[$value_row][$index] = preg_replace('/\s+/', '<br>', $data[$value_row][$index]);
                        }
                    }
                    $index++;
                    if ($_column == 2) {
                        if ($_row > 18) {
                            if (str_replace(" ", "", trim($currSheet->getCell($cellId)->getFormattedValue())) == "") {
                                $isNull = true;
                            }
                        }
                    }
                    if ($isNull) {
                        unset($data[$value_row]);
                    }
                }
                if ($test == 3) {
                    $value_row++;
                    $test = 0;
                    $index = 0;
                }
            }
            $index = 0;
            $year10 = $request->year + 1911;
            $month10 = $request->month;

            $CaseNum = array_shift($data);
            $CaseNum = str_replace(')', "", (explode("：", $CaseNum[0])[1]));
            DB::beginTransaction();
            $posts5 = DB::table('訪視記錄測試')->where('案號', $CaseNum)->delete();
            try {
                foreach ($data as $value) {
                    $posts = DB::table('訪視記錄測試')->insert([
                        '追蹤輔導日期' => $value[0]
                        , '個案管理師' => $value[1]
                        , '訪查方式' => $value[2]
                        , '受訪者' => $value[3]
                        , '受訪者_關係' => $value[4]
                        , '社會救助需求' => $value[6]
                        , '就業扶助需求' => $value[7]
                        , '戒治服務需求' => $value[8]
                        , '就學服務需求' => $value[9]
                        , '追輔建檔日期' => $value[10]
                        , '輔導內容' => $value[11]
                        , '處遇計畫' => $value[21]
                        , '案號' => $CaseNum
                        , 'File_UpLoadDate' => date_format(date_create($year10 . '-' . $month10 . '-01'), 'Y-m-d'),
                    ]);
                    // ProcessCaseJob::dispatch($CaseNum,$value[11]);
                }
                if ($posts == 1) {
                    $year10 = $request->year + 1911;
                    $month11 = $request->month;

                    $posts4 = DB::table('UpLoad_Log')->insert([
                        'File_System' => '訪視記錄',
                        'File_Source' => '',
                        'File_type' => '月度資料',
                        'File_Date' => $request->year . '/' . $month11,
                        'UpLoad_Date' => date('Y/m/d H:i:s'),
                        'UserName' => '若薇',
                        'Case_ID' => $CaseNum,
                    ]);
                    DB::commit();
                    return "Success";
                } else {
                    DB::rollBack();
                    return "Error";
                }

            } catch (\Exception$e) {
                throw $e;
            }
        } catch (\Exception$e) {
            throw $e;
        }
    }
#endregion
}
