<?php
namespace  App\Http\Controllers;
//use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
//use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PDO;

class BannerController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
/*#region ---------------這是第類---------------*/
/*#endregion ---------------這是第類---------------*/

/*#region ---------------這是第一類---------------*/
    public function SelectCrimialMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm =sprintf( " SET NOCOUNT ON
        SELECT
        [Date]
        , [CRIMINAL_PENALTY]
        INTO #tmp_FIRST_OFFENDER
        FROM [FIRST_OFFENDER]
        WHERE [Date] >= DATEADD(YEAR, -1,CAST('%s'+'/01'  AS DATE))
        AND [Date] <= DATEADD(YEAR, 0,CAST('%s'+'/01'  AS DATE))
        AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/01'  AS DATE)))

        SELECT * from (
        SELECT
        CAST(DATEPART(YY,[Date])-1911 AS VARCHAR) [YEAR]
        , CAST(DATEPART(MM,[Date]) AS VARCHAR)  [MONTH]
        , ISNULL([CRIMINAL_PENALTY],0) [Value]
        FROM #tmp_FIRST_OFFENDER

        UNION ALL

        SELECT
        CAST(DATEPART(YY,[Date])-1911 AS VARCHAR) [YEAR]
        , '合計' [MONTH]
        , SUM([CRIMINAL_PENALTY])
        FROM #tmp_FIRST_OFFENDER
        GROUP BY DATEPART(YY,[Date])
        )T order by CAST([YEAR] as INT)
        ,Case WHEN [MONTH] = '合計' THEN 13
        else [MONTH] END ASC

        DROP TABLE #tmp_FIRST_OFFENDER
                ",$startDate,$endDate,$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);

        return  json_decode( json_encode($posts), true);

    }
    public function SelectCrimialYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $sql=sprintf("SET NOCOUNT ON
        SELECT * from (
        SELECT	CAST(DATEPART(YY,[Date])-1911 AS VARCHAR) [YEAR]
        , SUM([CRIMINAL_PENALTY]) [Value]
        FROM [FIRST_OFFENDER]
        WHERE DATEPART(YY,[Date]) >=DATEPART(YY, CAST('%s'+'/01/01'  AS DATE) )
            AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        GROUP BY DATEPART(YY,[Date])
        )T
        order BY [YEAR]	",$startDate,$endDate);
        $result = DB::select($sql,[1]);
         return $result ;

    }
    public function SelectAdministrativeMonth(Request $request){

        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT
		[Date]
		, [ADMINISTRATIVE_PENALTY]
        INTO #tmp_FIRST_OFFENDER
        FROM [FIRST_OFFENDER]
        WHERE [Date] >= DATEADD(YEAR, -1,CAST('%s'+'/01'  AS DATE))
            AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/01'  AS DATE)))

        SELECT * FROM (
        SELECT
            CAST(DATEPART(YY,[Date])-1911 AS VARCHAR) [YEAR]
            , CAST(DATEPART(MM,[Date]) AS VARCHAR) [MONTH]
            , ISNULL([ADMINISTRATIVE_PENALTY],0) [Value]
        FROM #tmp_FIRST_OFFENDER

        UNION ALL

        SELECT
            CAST(DATEPART(YY,[Date])-1911 AS VARCHAR)  [YEAR]
            , '合計' [MONTH]
            , SUM([ADMINISTRATIVE_PENALTY])
        FROM #tmp_FIRST_OFFENDER
        GROUP BY DATEPART(YY,[Date])
        )T order by CAST([YEAR] as INT)
        ,Case WHEN [MONTH] = '合計' THEN 13
        else [MONTH] END ASC
        DROP TABLE #tmp_FIRST_OFFENDER
                ",$startDate,$startDate,$endDate);

        $posts = DB::select($SQLComm, [1]);

        return  $posts;

    }
    public function SelectAdministrativeYear(Request $request){

        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT * FROM(
        SELECT	CAST(DATEPART(YY,[Date])-1911 AS VARCHAR) [YEAR]
        , SUM([ADMINISTRATIVE_PENALTY]) [Value]
        FROM [FIRST_OFFENDER]
        WHERE DATEPART(YY,[Date]) >=DATEPART(YY, CAST('%s'+'/01/01'  AS DATE) )
            AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        GROUP BY DATEPART(YY,[Date])
        )T order by [YEAR]
                ",$startDate,$endDate);

        $posts = DB::select($SQLComm, [1]);

        return  $posts;

    }
    public function SelectPersonMonth(Request $request){

        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT *
        INTO #tmp_SEIZED
        FROM [SEIZED]
        WHERE [Date] >= DATEADD(YEAR, -1,CAST('%s'+'/01'  AS DATE))
            AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/01'  AS DATE)))
        SELECT * FROM(
        SELECT	CAST(DATEPART(YY,[Date])-1911 AS VARCHAR) [YEAR]
                , CAST(DATEPART(MM,[Date]) AS VARCHAR) [MONTH]
                    ,ISNULL([Sale_Num] ,0) [Value]
                    --SELECT *
            FROM #tmp_SEIZED
        UNION ALL

        SELECT	CAST(DATEPART(YY,[Date])-1911 AS VARCHAR) [YEAR]
                , '合計' [Month]
                , SUM(ISNULL([Level_1_Num],0))+SUM(ISNULL([Level_2_Num],0))+SUM(ISNULL([Level_3_Num],0))+SUM(ISNULL([Level_4_Num],0))+SUM(ISNULL([Other_Num] ,0)) [Value]
            FROM #tmp_SEIZED
        GROUP BY DATEPART(YYYY,[Date])
        )T order by CAST([YEAR] as INT)
        ,Case WHEN [MONTH] = '合計' THEN 13
        else [MONTH] END ASC
        DROP TABLE #tmp_SEIZED
                ",$startDate,$startDate,$endDate);

        $posts = DB::select($SQLComm, [1]);

        return  $posts;

    }
    public function SelectPersonYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT	DATEPART(YYYY,[Date])-1911 [YEAR]
		, SUM(ISNULL([Level_1_Num],0))+SUM(ISNULL([Level_2_Num],0))+SUM(ISNULL([Level_3_Num],0))+SUM(ISNULL([Level_4_Num],0))+SUM(ISNULL([Other_Num] ,0)) [Value]
        FROM SEIZED
        WHERE DATEPART(YY,[Date]) >=DATEPART(YY, CAST('%s'+'/01/01'  AS DATE) )
            AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        GROUP BY DATEPART(YYYY,[Date])
                ",$startDate,$endDate);

        $posts = DB::select($SQLComm, [1]);

        return  $posts;

    }
    public function SelectWeightMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT *
        INTO #tmp_SEIZED
        FROM [SEIZED]
        WHERE [Date] >=CAST('%s'+'/01'  AS DATE)
            AND [Date] <= CAST('%s'+'/01'  AS DATE)

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '一級毒品' [LEVEL]
                , ISNULL([Level_1_Gram],0) [Gram]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '二級毒品' [Level]
                , ISNULL([Level_2_Gram],0) [Gram]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '三級毒品' [Level]
                , ISNULL([Level_3_Gram],0) [Gram]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '四級毒品' [Level]
                , ISNULL([Level_4_Gram],0) [Gram]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '其他' [Level]
                , ISNULL([Other_Gram],0)  [Gram]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '總計' [Level]
                , ISNULL([Level_1_Gram],0)+ISNULL([Level_2_Gram],0)+ISNULL([Level_3_Gram],0)+ISNULL([Level_4_Gram],0)+ISNULL([Other_Gram],0)  [Gram]
        FROM #tmp_SEIZED
        order by [MONTH]
        DROP TABLE #tmp_SEIZED
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectNumMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT *
        INTO #tmp_SEIZED
        FROM [SEIZED]
        WHERE [Date] >=CAST('%s'+'/01'  AS DATE)
            AND [Date] <= CAST('%s'+'/01'  AS DATE)

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '一級毒品' [LEVEL]
                , ISNULL([Level_1_Num],0) [Num]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '二級毒品' [Level]
                , ISNULL([Level_2_Num],0) [Num]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '三級毒品' [Level]
                , ISNULL([Level_3_Num],0) [Num]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '四級毒品' [Level]
                , ISNULL([Level_4_Num],0) [Num]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [Month]
                , '其他' [Level]
                , ISNULL([Other_Num],0)  [Num]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '總計' [Level]
                , ISNULL([Level_1_Num],0)+ISNULL([Level_2_Num],0)+ISNULL([Level_3_Num],0)+ISNULL([Level_4_Num],0)+ISNULL([Other_Num],0)  [Gram]
        FROM #tmp_SEIZED
        order by [MONTH]
        DROP TABLE #tmp_SEIZED
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectCaseMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT *
        INTO #tmp_SEIZED
        FROM [SEIZED]
        WHERE [Date] >=CAST('%s'+'/01'  AS DATE)
            AND [Date] <= CAST('%s'+'/01'  AS DATE)

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '一級毒品' [LEVEL]
                , ISNULL([Level_1_CASE],0) [CASE]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '二級毒品' [Level]
                , ISNULL([Level_2_CASE],0) [CASE]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '三級毒品' [Level]
                , ISNULL([Level_3_CASE],0) [CASE]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '四級毒品' [Level]
                , ISNULL([Level_4_CASE],0) [CASE]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '其他' [Level]
                , ISNULL([Other_CASE],0)  [CASE]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [YEAR]
                , DATEPART(MM,[Date]) [MONTH]
                , '總和' [Level]
                , ISNULL([Level_1_CASE],0)+ISNULL([Level_2_CASE],0)+ISNULL([Level_3_CASE],0)+ISNULL([Level_4_CASE],0)+ISNULL([Other_CASE],0)  [CASE]
        FROM #tmp_SEIZED
        order by [LEVEL],[MONTH]
        DROP TABLE #tmp_SEIZED
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectWeightContrastMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT
        *
        INTO #tmp_SEIZED
        FROM [SEIZED]
        WHERE [Date] >= DATEADD(YEAR, -1,CAST('%s'+'/01'  AS DATE))
            AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/01'  AS DATE)))

        SELECT 	[Year],[LEVEL],SUM([Gram]) [Value]
        FROM
        (
        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '一級毒品' [Level]
                , ISNULL([Level_1_Gram],0) [Gram]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '二級毒品' [Level]
                , ISNULL([Level_2_Gram],0) [Gram]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '三級毒品' [Level]
                , ISNULL([Level_3_Gram],0) [Gram]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '四級毒品' [Level]
                , ISNULL([Level_4_Gram],0) [Gram]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '其他' [Level]
                , ISNULL([Other_Gram],0)  [Gram]
        FROM #tmp_SEIZED
        ) M
        GROUP BY [Year],[Level]

        UNION ALL

		SELECT DATEPART(YYYY,[Date])-1911 [Year]
				,'總計' [Level]
				, SUM(ISNULL([Level_1_Gram],0)+ISNULL([Level_2_Gram],0)+ISNULL([Level_3_Gram],0)+ISNULL([Level_4_Gram],0)+ISNULL([Other_Gram],0) ) [Gram]
		FROM #tmp_SEIZED
		GROUP BY DATEPART(YYYY,[Date])

        DROP TABLE #tmp_SEIZED
                ",$startDate,$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectWeightContrastYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT
		*
        INTO #tmp_SEIZED
        FROM [SEIZED]
        WHERE [Date] >= DATEADD(YEAR, -1,CAST('%s'+'/01/01'  AS DATE))
            AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/12/31'  AS DATE)))


        SELECT 	[Year],[Level],SUM([Gram]) [Gram]
        FROM
        (
        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '一級毒品' [Level]
                , ISNULL([Level_1_Gram],0) [Gram]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '二級毒品' [Level]
                , ISNULL([Level_2_Gram],0) [Gram]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '三級毒品' [Level]
                , ISNULL([Level_3_Gram],0) [Gram]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '四級毒品' [Level]
                , ISNULL([Level_4_Gram],0) [Gram]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '其他' [Level]
                , ISNULL([Other_Gram],0)  [Gram]
        FROM #tmp_SEIZED
        ) M
        GROUP BY [Year],[Level]

        DROP TABLE #tmp_SEIZED
                ",$startDate,$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectPeosonContrastMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT
		*
        INTO #tmp_SEIZED
        FROM [SEIZED]
        WHERE [Date] >= DATEADD(YEAR, -1,CAST('%s'+'/01'  AS DATE))
            AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/01'  AS DATE)))

        SELECT 	[Year],[Level],SUM([NUM]) [Value]
        FROM
        (
        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '一級毒品' [Level]
                , ISNULL([Level_1_NUM],0) [NUM]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '二級毒品' [Level]
                , ISNULL([Level_2_NUM],0) [NUM]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '三級毒品' [Level]
                , ISNULL([Level_3_NUM],0) [NUM]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '四級毒品' [Level]
                , ISNULL([Level_4_NUM],0) [NUM]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '其他' [Level]
                , ISNULL([Other_NUM],0)  [NUM]
        FROM #tmp_SEIZED
        ) M
        GROUP BY [Year],[Level]

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '總計' [Level]
                , SUM(ISNULL([Level_1_NUM],0)+ISNULL([Level_2_NUM],0)+ISNULL([Level_3_NUM],0)+ISNULL([Level_4_NUM],0)+ISNULL([Other_NUM],0) ) [NUM]
        FROM #tmp_SEIZED
        GROUP BY DATEPART(YYYY,[Date])

        DROP TABLE #tmp_SEIZED
                ",$startDate,$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectPeosonContrastYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT
		*
        INTO #tmp_SEIZED
        FROM [SEIZED]
        WHERE [Date] >= DATEADD(YEAR, -1,CAST('%s'+'/01/01'  AS DATE))
            AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/12/31'  AS DATE)))

        SELECT 	[Year],[Level],SUM([NUM]) [NUM]
        FROM
        (
        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '一級毒品' [Level]
                , ISNULL([Level_1_NUM],0) [NUM]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '二級毒品' [Level]
                , ISNULL([Level_2_NUM],0) [NUM]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '三級毒品' [Level]
                , ISNULL([Level_3_NUM],0) [NUM]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '四級毒品' [Level]
                , ISNULL([Level_4_NUM],0) [NUM]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '其他' [Level]
                , ISNULL([Other_NUM],0)  [NUM]
        FROM #tmp_SEIZED
        ) M
        GROUP BY [Year],[Level]


        DROP TABLE #tmp_SEIZED
                ",$startDate,$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectCaseContrastMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT
		*
        INTO #tmp_SEIZED
        FROM [SEIZED]
        WHERE [Date] >= DATEADD(YEAR, -1,CAST('%s'+'/01'  AS DATE))
            AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/01'  AS DATE)))


        SELECT 	[Year],[Level],SUM([CASE]) [Value]
        FROM
        (
        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '一級毒品' [Level]
                , ISNULL([Level_1_CASE],0) [CASE]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '二級毒品' [Level]
                , ISNULL([Level_2_CASE],0) [CASE]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '三級毒品' [Level]
                , ISNULL([Level_3_CASE],0) [CASE]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '四級毒品' [Level]
                , ISNULL([Level_4_CASE],0) [CASE]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '其他' [Level]
                , ISNULL([Other_CASE],0)  [CASE]
        FROM #tmp_SEIZED
        ) M
        GROUP BY [Year],[Level]

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '總計' [Level]
                , SUM(ISNULL([Level_1_CASE],0)+ISNULL([Level_2_CASE],0)+ISNULL([Level_3_CASE],0)+ISNULL([Level_4_CASE],0)+ISNULL([Other_CASE],0) ) [CASE]
        FROM #tmp_SEIZED
        GROUP BY DATEPART(YYYY,[Date])

        DROP TABLE #tmp_SEIZED
                ",$startDate,$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectCaseContrastYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT
		*
        INTO #tmp_SEIZED
        FROM [SEIZED]
        WHERE [Date] >= DATEADD(YEAR, -1,CAST('%s'+'/01/01'  AS DATE))
            AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/12/31'  AS DATE)))


        SELECT 	[Year],[Level],SUM([CASE]) [CASE]
        FROM
        (
        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '一級毒品' [Level]
                , ISNULL([Level_1_CASE],0) [CASE]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '二級毒品' [Level]
                , ISNULL([Level_2_CASE],0) [CASE]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '三級毒品' [Level]
                , ISNULL([Level_3_CASE],0) [CASE]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '四級毒品' [Level]
                , ISNULL([Level_4_CASE],0) [CASE]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , '其他' [Level]
                , ISNULL([Other_CASE],0)  [CASE]
        FROM #tmp_SEIZED
        ) M
        GROUP BY [Year],[Level]


        DROP TABLE #tmp_SEIZED
                ",$startDate,$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
/*#endregion ---------------這是第一類---------------*/

/*#region ---------------這是第二類---------------*/
    public function SelectRecidivesmRateMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[Date])-1911 [YEAR]
		, DATEPART(MM,[Date]) [MONTH]
		, [Location]
		, [Recidivism_Rate]
        FROM [DRUG_RECIDIVISM_RATE]
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/01'  AS DATE)))
            order by cast( DATEPART(MM,[Date]) as int)
                ",$startDate,$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectRecidivesmRateYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[Date])-1911 [YEAR]
		, [Location]
		, AVG([Recidivism_Rate]) Recidivism_Rate
        FROM [DRUG_RECIDIVISM_RATE]
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/12/31'  AS DATE))
        GROUP BY DATEPART(YY,[Date]), [Location] ORDER by [Location] DESC
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectRecidivesmRateHalfYearMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[Date])-1911 [YEAR]
		, DATEPART(MM,[Date]) [MONTH]
		, [Location]
		, [Recidivism_Rate] Recidivism_Rate
        FROM [DRUG_RECIDIVISM_RATE_Halfyear]
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/01'  AS DATE)))
            order by cast( DATEPART(MM,[Date]) as int)
                ",$startDate,$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectRecidivesmRateHalfYearYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[Date])-1911 [YEAR]
		, [Location]
		, AVG([Recidivism_Rate]) Recidivism_Rate
        FROM [DRUG_RECIDIVISM_RATE]
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/12/31'  AS DATE))
            GROUP BY DATEPART(YY,[Date]), [Location] ORDER by [Location] DESC
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectRecidivesmRateOneYearMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[Date])-1911 [YEAR]
		, DATEPART(MM,[Date]) [MONTH]
		, [Location]
		, [Recidivism_Rate]
        FROM [DRUG_RECIDIVISM_RATE_Oneyear]
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/01'  AS DATE)))
                ",$startDate,$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectRecidivesmRateOneYearbyYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[Date])-1911 [YEAR]
		, [Location]
		, AVG([Recidivism_Rate]) Recidivism_Rate
        FROM [DRUG_RECIDIVISM_RATE_Oneyear]
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/12/31'  AS DATE))
            GROUP BY DATEPART(YY,[Date]), [Location] ORDER by [Location] DESC
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectRecidivesmRateTwoYearMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[Date])-1911 [YEAR]
		, DATEPART(MM,[Date]) [MONTH]
		, [Location]
		, [Recidivism_Rate]
        FROM [DRUG_RECIDIVISM_RATE_TwoYear]
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/01'  AS DATE)))
                ",$startDate,$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectRecidivesmRateTwoYearbyYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[Date])-1911 [YEAR]
		, [Location]
		, AVG([Recidivism_Rate]) Recidivism_Rate
        FROM [DRUG_RECIDIVISM_RATE_TwoYear]
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/12/31'  AS DATE))
            GROUP BY DATEPART(YY,[Date]), [Location] ORDER by [Location] DESC
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
/*#endregion ---------------這是第二類---------------*/

/*#region ---------------這是第三類---------------*/

    public function SelectLocationeTubeMonth(Request $request){ //1
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT	DATEPART(YY,[Date])-1911 [Year]
		, DATEPART(MM,[Date]) [Month]
        , [Location]
        , [Num]
        INTO #tmp_LOCATIONE_TUBE_Num
        FROM [LOCATIONE_TUBE_Num]
        WHERE DATEPART(YY,[Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
		AND DATEPART(MM,[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
		AND [Location]<>''
        SELECT	M.*
                , ROW_NUMBER() OVER( ORDER BY M.[Year] ,M.[Num] DESC) [Rank]
                , M1.[Rank_6]
        FROM #tmp_LOCATIONE_TUBE_Num M
        LEFT JOIN (SELECT	[Year]
				, [Month]
                , [Location]
                , ROW_NUMBER() OVER( ORDER BY [Num] DESC) [Rank_6]
            FROM #tmp_LOCATIONE_TUBE_Num
            WHERE [Location] IN ('新北市','臺北市','桃園市','臺中市','臺南市','高雄市')
            ) M1 ON (M1.[Year] = M.[Year] AND M1.[Month] = M.[Month] AND M1.[Location] = M.[Location])
        DROP TABLE #tmp_LOCATIONE_TUBE_Num
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectLocationeTubebyYear(Request $request){ //2
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf(" SET NOCOUNT ON
        SELECT	DATEPART(YY,[Date])-1911 [Year]
			, [Location]
			, [Num]
	INTO #tmp_LOCATIONE_TUBE_Num
    FROM [LOCATIONE_TUBE_Num]
    WHERE (
        DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
		)
        AND [Location]<>''

    SELECT	M.*
			, ROW_NUMBER() OVER( ORDER BY M.[Year] ,M.[Num] DESC) [Rank]
			, M1.[Rank_6]
    FROM #tmp_LOCATIONE_TUBE_Num M
    LEFT JOIN (SELECT	[Year]
			, [Location]
			, ROW_NUMBER() OVER( ORDER BY [Num] DESC) [Rank_6]
		FROM #tmp_LOCATIONE_TUBE_Num
		WHERE [Location] IN ('新北市','臺北市','桃園市','臺中市','臺南市','高雄市')
	      ) M1 ON (M1.[Year] = M.[Year] AND M1.[Location] = M.[Location])

    DROP TABLE #tmp_LOCATIONE_TUBE_Num
                ",$startDate,$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectCorrectionLocationeTubeMonth(Request $request){ //3
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,s.[Date])-1911 [Year]
	  , DATEPART(MM,s.[Date]) [Month]
      ,s.[Location]
      ,ROUND(cast(B.[Num] as float) / cast(s.Num as float)*100000,0) [Num]
	  INTO #tmp_LOCATIONE_POPULATION_Num
    FROM [LOCATIONE_POPULATION_Num] S
    left join [dbo].[LOCATIONE_TUBE_Num] B on (s.Date=B.Date and s.Location=B.Location)
    WHERE DATEPART(YY,S.[Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
    AND DATEPART(MM,S.[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
    AND S.[Location]<>''
    ORDER BY [Num] DESC

    SELECT	M.*
			, ROW_NUMBER() OVER( ORDER BY M.[Year] ,M.[Num] DESC) [Rank]
			, M1.[Rank_6]
    FROM #tmp_LOCATIONE_POPULATION_Num M
    LEFT JOIN (SELECT	[Year]
			, [Location]
			, ROW_NUMBER() OVER( ORDER BY [Num] DESC) [Rank_6]
		FROM #tmp_LOCATIONE_POPULATION_Num
		WHERE [Location] IN ('新北市','臺北市','桃園市','臺中市','臺南市','高雄市')
	      ) M1 ON (M1.[Year] = M.[Year] AND M1.[Location] = M.[Location])

    DROP TABLE #tmp_LOCATIONE_POPULATION_Num

                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function SelectCorrectionLocationeTubebyYear(Request $request){ //4
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,s.[Date])-1911 [Year]
	  , DATEPART(MM,s.[Date]) [Month]
      ,s.[Location]
      ,ROUND(cast(B.[Num] as float) / cast(s.Num as float)*100000,0) [Num]
	  INTO #tmp_LOCATIONE_POPULATION_Num
    FROM [LOCATIONE_POPULATION_Num] S
    left join [dbo].[LOCATIONE_TUBE_Num] B on (s.Date=B.Date and s.Location=B.Location)
    WHERE (
        DATEPART(YY,S.[Date]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(YY,S.[Date]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,S.[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        )
        AND S.[Location]<>''
    ORDER BY [Num] DESC

    SELECT	M.*
			, ROW_NUMBER() OVER( ORDER BY M.[Year] ,M.[Num] DESC) [Rank]
			, M1.[Rank_6]
    FROM #tmp_LOCATIONE_POPULATION_Num M
    LEFT JOIN (SELECT	[Year]
			, [Location]
			, ROW_NUMBER() OVER( ORDER BY [Num] DESC) [Rank_6]
		FROM #tmp_LOCATIONE_POPULATION_Num
		WHERE [Location] IN ('新北市','臺北市','桃園市','臺中市','臺南市','高雄市')
	      ) M1 ON (M1.[Year] = M.[Year] AND M1.[Location] = M.[Location])

    DROP TABLE #tmp_LOCATIONE_POPULATION_Num
                ",$startDate,$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }

/*#endregion ---------------這是第三類---------------*/

/*#region ---------------這是第四類---------------*/
    public function DrugTypebyMonth(Request $request){ //1
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT * from (
         SELECT [Manage_Type]
         , Count(*) [Person]
         FROM [DRUGS_LIST_Bak]
         WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
           AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
         GROUP BY [Manage_Type]
         union all
         SELECT [Manage_Type]
         , Count(*) [Person]
         FROM [3_4_Lecture_Bak]
         WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                 AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
         GROUP BY [Manage_Type]
         union all
         SELECT '春暉' [Manage_Type]
         , Count(*) [Person]
         FROM [DRUGS_LIST_TEENAGER_Bak]
         WHERE (DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                 AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE)))
                 AND [Listed_Chunhui] ='1'
                 AND [Listed_Chunhui_Close] <>'1'
         GROUP BY [Manage_Type]
         union all
         SELECT '非春暉' [Manage_Type]
         , Count(*) [Person]
         FROM [DRUGS_LIST_TEENAGER_Bak]
         WHERE (DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                 AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE)))
                 AND [Listed_Chunhui] ='2'
                 AND [Listed_Chunhui_Close] <>'1'
         GROUP BY [Manage_Type]
         )T order by Person
                ",$endDate,$endDate,$endDate,$endDate,$endDate,$endDate,$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugTypebyYear(Request $request){ //2
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT * from (
         SELECT DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
         , [Manage_Type]
         , Count(*) [Person]
         FROM [DRUGS_LIST_Bak]
         WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                 AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                 AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
         GROUP BY [Manage_Type],DATEPART(YY,[File_UpLoadDate])
         union all
         SELECT DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
         , [Manage_Type]
         , Count(*) [Person]
         FROM [3_4_Lecture_Bak]
         WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                 AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                 AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
         GROUP BY [Manage_Type],DATEPART(YY,[File_UpLoadDate])
         union all
          SELECT DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
         ,'春暉' [Manage_Type]
         , Count(*) [Person]
         FROM [DRUGS_LIST_TEENAGER_Bak]
         WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                 AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                 AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
                 AND [Listed_Chunhui] ='1'
                 AND [Listed_Chunhui_Close] <>'2'
         GROUP BY [Manage_Type],DATEPART(YY,[File_UpLoadDate])
         )T order by YEAR,Person
                ",$startDate,$endDate,$endDate,$startDate,$endDate,$endDate,$startDate,$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
/*#endregion ---------------這是第四類---------------*/

/*#region ---------------這是第五類---------------*/
    public function SelectCommentTextByMonth(Request $request){ //1
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        DECLARE @STARTDT  VARCHAR(10)
        SET @STARTDT = '%s'+'/01'
                SELECT '案管系統' [type]
                    , Count(*) [Person]
                FROM [DRUGS_LIST_Bak]
                WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST(@STARTDT  AS DATE))
                        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST(@STARTDT  AS DATE))
        union all

        SELECT  '三四級講習' [type]
                    , Count(*) [Person]
                FROM [3_4_Lecture]
                WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST(@STARTDT  AS DATE))
                        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST(@STARTDT  AS DATE))
        union all

        select  '在案人數' [type]
                , sum([Person]) [Person] from (
        select Count([Person_ID]) [Person]
        FROM (
        select [Person_ID]
        FROM [DRUGS_LIST_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST(@STARTDT  AS DATE))
                AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST(@STARTDT  AS DATE))
                union all
        select [Person_ID]
        FROM [3_4_Lecture]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST(@STARTDT  AS DATE))
                AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST(@STARTDT  AS DATE))
            )T group by [Person_ID]
            )M
                ",$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugGenderByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT [Gender]
            , Count(*) [Person]
        FROM [DRUGS_LIST_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
                AND [Closing_Date_CHINA]=''
        GROUP BY [Gender]
        order by Gender desc
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugGenderByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
        , [Gender]
        , Count(*) [Person]
        FROM [DRUGS_LIST_Bak]
        WHERE (DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            )
        GROUP BY [Gender],DATEPART(YY,[File_UpLoadDate]) order by YEAR,Gender　desc
                ",$startDate,$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugAgeByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT [Age_Class]
        , SUM([Person]) [Person]
        FROM
        (
        SELECT [File_Age] [Age]
                , CASE WHEN [File_Age] <20 THEN '未滿20歲'
                    WHEN [File_Age] <=29 THEN '20-29歲'
                    WHEN [File_Age] <=39 THEN '30-39歲'
                    WHEN [File_Age] <=49 THEN '40-49歲'
                    WHEN [File_Age] <=59 THEN '50-59歲'
                    WHEN [File_Age] <=69 THEN '60-69歲'
                    WHEN [File_Age] >=69 THEN '70歲以上'
            END [Age_Class]
                , Count(*) [Person]
                --SELECT *
        FROM [DRUGS_LIST_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        GROUP BY [File_Age]
        ) M GROUP BY [Age_Class] order by replace(Age_Class,'未','0')
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugAgeByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT [YEAR]
        , [Age_Class]
        , SUM([Person]) [Person]
        FROM
        (
        SELECT DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
                , [File_Age] [Age]
                , CASE WHEN [File_Age] <20 THEN '未滿20歲'
                    WHEN [File_Age] <=29 THEN '20-29歲'
                    WHEN [File_Age] <=39 THEN '30-39歲'
                    WHEN [File_Age] <=49 THEN '40-49歲'
                    WHEN [File_Age] <=59 THEN '50-59歲'
                    WHEN [File_Age] <=69 THEN '60-69歲'
                    WHEN [File_Age] >=69 THEN '70歲以上'
            END [Age_Class]
                , Count(*) [Person]
                --SELECT *
        FROM [DRUGS_LIST_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        GROUP BY [File_Age],DATEPART(YY,[File_UpLoadDate])
        ) M GROUP BY [Age_Class],[YEAR]
                ",$startDate,$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugAbuseTypeByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Drugs_Grade], [Drug_Level_1], [Drug_Level_2], [Drug_Level_3], [Drug_Level_4]
        INTO #tmp_DRUGS_LIST
        FROM [DRUGS_LIST_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))

        SELECT [LEVEL], [Type], COUNT(*) [Person]
        FROM
        (

        SELECT '一級毒品' [Level]
                , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                    ELSE '單一'
                END [Type]
        FROM #tmp_DRUGS_LIST WHERE [Drug_Level_1] = '是' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '否'

        UNION ALL

        SELECT '一級毒品' [Level],'混用' [Type] FROM #tmp_DRUGS_LIST WHERE [Drug_Level_1] = '是' AND ([Drug_Level_2] = '是' OR [Drug_Level_3] = '是' OR [Drug_Level_4] = '是')

        UNION ALL

        SELECT '二級毒品' [Level]
                , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                    ELSE '單一'
                END [Type]
        FROM #tmp_DRUGS_LIST WHERE [Drug_Level_1] = '否' AND [Drug_Level_2] = '是' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '否'

        UNION ALL

        SELECT '二級毒品' [Level],'混用' [Type] FROM #tmp_DRUGS_LIST WHERE [Drug_Level_1] = '否' AND [Drug_Level_2] = '是' AND ([Drug_Level_3] = '是' OR [Drug_Level_4] = '是')

        UNION ALL

        SELECT '三級毒品' [Level]
                , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                    ELSE '單一'
                END [Type]
        FROM #tmp_DRUGS_LIST WHERE [Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '是' AND [Drug_Level_4] = '否'

        UNION ALL

        SELECT '三級毒品' [Level],'混用' [Type] FROM #tmp_DRUGS_LIST WHERE [Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '是' AND [Drug_Level_4] = '是'

        UNION ALL

        SELECT '四級毒品' [Level]
                , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                    ELSE '單一'
                END [Type]
        FROM #tmp_DRUGS_LIST WHERE [Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '是'

        UNION ALL

        SELECT CASE WHEN [Drugs_Grade] LIKE '%%一級%%' THEN'一級毒品'
                    WHEN [Drugs_Grade] LIKE '%%二級%%' THEN'二級毒品'
                    WHEN [Drugs_Grade] LIKE '%%三級%%' THEN'三級毒品'
                    WHEN [Drugs_Grade] LIKE '%%四級%%' THEN'四級毒品'
                    ELSE '其他'
                END [Level]
                , '單一' [Type]
        FROM #tmp_DRUGS_LIST WHERE [Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '否' --AND [Drugs_Grade] NOT LIKE '%%級%%'
        ) M GROUP BY [LEVEL], [Type]

        DROP TABLE #tmp_DRUGS_LIST
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);

        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function DrugAbuseTypeByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  DATEPART(YY,[File_UpLoadDate])-1911 [Year],[Drugs_Grade], [Drug_Level_1], [Drug_Level_2], [Drug_Level_3], [Drug_Level_4]
        INTO #tmp_DRUGS_LIST
        FROM [DRUGS_LIST_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        SELECT [YEAR], [LEVEL], [Type], sum([count]) [count]
        FROM
        (

            SELECT '一級毒品' [Level]
                    , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                        ELSE '單一'
                    END [Type]
                    ,case when  [Drug_Level_1] = '是' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '否' then 1 else 0 end [count]
                    , [YEAR]
            FROM #tmp_DRUGS_LIST

            UNION ALL

            SELECT '一級毒品' [Level],'混用' [Type],case when  [Drug_Level_1] = '是' AND ([Drug_Level_2] = '是' OR [Drug_Level_3] = '是' OR [Drug_Level_4] = '是') then 1 else 0 end [count]
            ,[YEAR] FROM #tmp_DRUGS_LIST

            UNION ALL

            SELECT '二級毒品' [Level]
                    , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                        ELSE '單一'
                    END [Type]
                    ,case when  [Drug_Level_1] = '否' AND [Drug_Level_2] = '是' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '否' then 1 else 0 end [count]
                    , [YEAR]
            FROM #tmp_DRUGS_LIST

            UNION ALL

            SELECT '二級毒品' [Level],'混用' [Type],case when  [Drug_Level_1] = '否' AND [Drug_Level_2] = '是' AND ([Drug_Level_3] = '是' OR [Drug_Level_4] = '是') then 1 else 0 end [count]
            , [YEAR] FROM #tmp_DRUGS_LIST

            UNION ALL

            SELECT '三級毒品' [Level]
                    , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                        ELSE '單一'
                    END [Type]
                    ,case when [Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '是' AND [Drug_Level_4] = '否' then 1 else 0 end [count]
                    , [YEAR]
            FROM #tmp_DRUGS_LIST

            UNION ALL

            SELECT '三級毒品' [Level],'混用' [Type],case when  [Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '是' AND [Drug_Level_4] = '是' then 1 else 0 end [count]
            , [YEAR] FROM #tmp_DRUGS_LIST

            UNION ALL

            SELECT '四級毒品' [Level]
                    , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                        ELSE '單一'
                    END [Type]
                    ,case when　[Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '是' then 1 else 0 end [count]
                    , [YEAR]
            FROM #tmp_DRUGS_LIST

            UNION ALL

            SELECT CASE WHEN [Drugs_Grade] LIKE '%%一級%%' THEN'一級毒品'
                        WHEN [Drugs_Grade] LIKE '%%二級%%' THEN'二級毒品'
                        WHEN [Drugs_Grade] LIKE '%%三級%%' THEN'三級毒品'
                        WHEN [Drugs_Grade] LIKE '%%四級%%' THEN'四級毒品'
                        ELSE '其他'
                    END [Level]
                    , '單一' [Type]
                    ,case when　[Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '否' then 1 else 0 end [count]
                    , [YEAR]
            FROM #tmp_DRUGS_LIST --AND [Drugs_Grade] NOT LIKE '%%級%%'
        ) M GROUP BY [LEVEL], [Type], [YEAR]　order by Level

        DROP TABLE #tmp_DRUGS_LIST
                ",$startDate,$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugAgeTypeByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Drugs_Grade], [Drug_Level_1], [Drug_Level_2], [Drug_Level_3], [Drug_Level_4]
        , CASE WHEN [File_Age] <20 THEN '未滿20歲'
            WHEN [File_Age] <=29 THEN '20-29歲'
            WHEN [File_Age] <=39 THEN '30-39歲'
            WHEN [File_Age] <=49 THEN '40-49歲'
            WHEN [File_Age] <=59 THEN '50-59歲'
            WHEN [File_Age] <=69 THEN '60-69歲'
            WHEN [File_Age] >=69 THEN '70歲以上'
        END [Age_Class]
        INTO #tmp_DRUGS_LIST
        FROM [DRUGS_LIST_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))

        SELECT [LEVEL], [Type], sum([count]) [count], [Age_Class]
        FROM
        (

        SELECT '一級毒品' [Level]
            , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                ELSE '單一'
            END [Type]
            ,case when [Drug_Level_1] = '是' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '否' then 1 else 0 end [count]
            , [Age_Class]
        FROM #tmp_DRUGS_LIST

        UNION ALL

        SELECT '一級毒品' [Level],'混用' [Type],case when [Drug_Level_1] = '是' AND ([Drug_Level_2] = '是' OR [Drug_Level_3] = '是' OR [Drug_Level_4] = '是') then 1 else 0 end [count]
        , [Age_Class] FROM #tmp_DRUGS_LIST

        UNION ALL

        SELECT '二級毒品' [Level]
            , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                ELSE '單一'
            END [Type]
            ,case when [Drug_Level_1] = '否' AND [Drug_Level_2] = '是' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '否' then 1 else 0 end [count]
            , [Age_Class]
        FROM #tmp_DRUGS_LIST

        UNION ALL

        SELECT '二級毒品' [Level],'混用' [Type],case when [Drug_Level_1] = '否' AND ([Drug_Level_2] = '是' AND [Drug_Level_3] = '是' OR [Drug_Level_4] = '是') then 1 else 0 end [count]
        , [Age_Class] FROM #tmp_DRUGS_LIST

        UNION ALL

        SELECT '三級毒品' [Level]
            , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                ELSE '單一'
            END [Type]
            ,case when [Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '是' AND [Drug_Level_4] = '否' then 1 else 0 end [count]
            , [Age_Class]
        FROM #tmp_DRUGS_LIST

        UNION ALL

        SELECT '三級毒品' [Level],'混用' [Type],case when [Drug_Level_1] = '否' AND ([Drug_Level_2] = '否' AND [Drug_Level_3] = '是' AND [Drug_Level_4] = '是') then 1 else 0 end [count]
        , [Age_Class] FROM #tmp_DRUGS_LIST

        UNION ALL

        SELECT '四級毒品' [Level]
            , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                ELSE '單一'
            END [Type]
            ,case when [Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '是' then 1 else 0 end [count]
            , [Age_Class]
        FROM #tmp_DRUGS_LIST

        UNION ALL

        SELECT CASE WHEN [Drugs_Grade] LIKE '%%一級%%' THEN'一級毒品'
                WHEN [Drugs_Grade] LIKE '%%二級%%' THEN'二級毒品'
                WHEN [Drugs_Grade] LIKE '%%三級%%' THEN'三級毒品'
                WHEN [Drugs_Grade] LIKE '%%四級%%' THEN'四級毒品'
                ELSE '其他'
            END [Level]
            , '單一' [Type]
            ,case when [Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '否' then 1 else 0 end [count]
            , [Age_Class]
        FROM #tmp_DRUGS_LIST
        ) M GROUP BY [LEVEL], [Type], [Age_Class] order by REPLACE(Age_Class,'未','0'),Level,Type desc

        DROP TABLE #tmp_DRUGS_LIST
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugAgeTypeByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Drugs_Grade], [Drug_Level_1], [Drug_Level_2], [Drug_Level_3], [Drug_Level_4]
        , CASE WHEN [File_Age] <20 THEN '未滿20歲'
            WHEN [File_Age] <=29 THEN '20-29歲'
            WHEN [File_Age] <=39 THEN '30-39歲'
            WHEN [File_Age] <=49 THEN '40-49歲'
            WHEN [File_Age] <=59 THEN '50-59歲'
            WHEN [File_Age] <=69 THEN '60-69歲'
            WHEN [File_Age] >=69 THEN '70歲以上'
        END [Age_Class]
        , DATEPART(YY,[File_UpLoadDate])-1911 [Year]
        INTO #tmp_DRUGS_LIST
        FROM [DRUGS_LIST_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))

        SELECT [Year],[LEVEL], sum([count]) [count], [Age_Class]
        FROM
        (

        SELECT '一級毒品' [Level]
            , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                ELSE '單一'
            END [Type]
            , [Age_Class]
            ,case when [Drug_Level_1] = '是' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '否' then 1 else 0 end [count]
            , [Year]
        FROM #tmp_DRUGS_LIST

        UNION ALL

        SELECT '一級毒品' [Level],'混用' [Type], [Age_Class],case when [Drug_Level_1] = '是' AND ([Drug_Level_2] = '是' OR [Drug_Level_3] = '是' OR [Drug_Level_4] = '是') then 1 else 0 end [count]
        , [Year] FROM #tmp_DRUGS_LIST

        UNION ALL

        SELECT '二級毒品' [Level]
            , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                ELSE '單一'
            END [Type]
            , [Age_Class]
            ,case when [Drug_Level_1] = '否' AND [Drug_Level_2] = '是' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '否' then 1 else 0 end [count]
            , [Year]
        FROM #tmp_DRUGS_LIST

        UNION ALL

        SELECT '二級毒品' [Level],'混用' [Type], [Age_Class],case when [Drug_Level_1] = '否' AND [Drug_Level_2] = '是' AND ([Drug_Level_3] = '是' OR [Drug_Level_4] = '是') then 1 else 0 end [count]
        , [Year] FROM #tmp_DRUGS_LIST

        UNION ALL

        SELECT '三級毒品' [Level]
            , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                ELSE '單一'
            END [Type]
            , [Age_Class]
            ,case when [Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '是' AND [Drug_Level_4] = '否' then 1 else 0 end [count]
            , [Year]
        FROM #tmp_DRUGS_LIST

        UNION ALL

        SELECT '三級毒品' [Level],'混用' [Type], [Age_Class],case when [Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '是' AND [Drug_Level_4] = '是' then 1 else 0 end [count]
        , [Year] FROM #tmp_DRUGS_LIST

        UNION ALL

        SELECT '四級毒品' [Level]
            , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
                ELSE '單一'
            END [Type]
            , [Age_Class]
            ,case when [Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '是' then 1 else 0 end [count]
            , [Year]
        FROM #tmp_DRUGS_LIST

        UNION ALL

        SELECT CASE WHEN [Drugs_Grade] LIKE '%%一級毒品%%' THEN'一級毒品'
                WHEN [Drugs_Grade] LIKE '%%二級毒品%%' THEN'二級毒品'
                WHEN [Drugs_Grade] LIKE '%%三級毒品%%' THEN'三級毒品'
                WHEN [Drugs_Grade] LIKE '%%四級毒品%%' THEN'四級毒品'
                ELSE '其他'
            END [Level]
            , '單一' [Type]
            , [Age_Class]
            ,case when [Drug_Level_1] = '否' AND [Drug_Level_2] = '否' AND [Drug_Level_3] = '否' AND [Drug_Level_4] = '否' then 1 else 0 end [count]
            , [Year]
        FROM #tmp_DRUGS_LIST  --AND [Drugs_Grade] NOT LIKE '%%級%%'
        ) M GROUP BY [Year],[LEVEL], [Age_Class] order by REPLACE(Age_Class,'未','0'),Year,Level

        DROP TABLE #tmp_DRUGS_LIST
                ",$startDate,$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugAreaByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT
        [Area_Name]
        , COUNT(*) [Person]
        FROM [DRUGS_LIST_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        GROUP BY [Area_Name] order by Person desc
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugAreaByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT
        [Area_Name]
        , DATEPART(YY,[File_UpLoadDate])-1911  [YEAR]
        , COUNT(*) [Person]
        ,ROW_NUMBER() over(partition by DATEPART(YY,[File_UpLoadDate]) order by DATEPART(YY,[File_UpLoadDate]),COUNT(*) desc) [NUM]
        FROM [DRUGS_LIST_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        GROUP BY [Area_Name],DATEPART(YY,[File_UpLoadDate])
                ",$startDate,$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugCorrectionAreaByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT	M.[Location]
        , (ISNULL(M1.[Person],0)*100000/M.[Population]) [Person]
        FROM [CIVIL_AFFAIRS_DEPT] M
        LEFT JOIN (
            SELECT
                    [Area_Name]
                    , COUNT(*) [Person]
            FROM [DRUGS_LIST_Bak]
            WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            GROUP BY [Area_Name]
        ) M1 ON (M1.[Area_Name] = M.[Location])
        WHERE M.[Year]  = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND M.[Month]  = DATEPART(MM,CAST('%s'+'/01'  AS DATE))order by Person desc
                ",$startDate,$startDate,$startDate,$startDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugCorrectionAreaByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT	M.[Location]
        , M.[CHINA_Year]
        , (ISNULL(M1.[Person],0)*100000/M.[Population]) [Person]
        ,ROW_NUMBER() over(partition by M.[CHINA_Year] order by M.[CHINA_Year],(ISNULL(M1.[Person],0)*100000/M.[Population]) desc) [NUM]
        FROM [CIVIL_AFFAIRS_DEPT] M
        LEFT JOIN (
                SELECT
                    [Area_Name]
                    , COUNT(*) [Person]
                    , DATEPART(YY,[File_UpLoadDate])-1911 [Year]
                FROM [DRUGS_LIST_Bak]
                WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
                GROUP BY [Area_Name],DATEPART(YY,[File_UpLoadDate])
        ) M1 ON (M1.[Area_Name] = M.[Location] AND M1.[Year] = M.[CHINA_Year])
        WHERE M.[Year]  >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND M.[Year]  <= DATEPART(YY,CAST('%s'+'/01'  AS DATE)) 
            AND M.[Month]  = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            order by Person desc
                ",$startDate,$endDate,$endDate,$startDate,$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugCoachByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT	[Date_Merk]
        , [Item]
        , [Count]
        INTO #tmp_Coach_List
        FROM [Coach_List]
        WHERE DATEPART(YY,[Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
      

        SELECT * FROM #tmp_Coach_List
        UNION ALL
        SELECT	[Date_Merk]
            , '全部' [Item]
            , SUM([Count]) [Count]
        FROM #tmp_Coach_List
        GROUP BY [Date_Merk]

        DROP TABLE #tmp_Coach_List
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugCoachByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT	DATEPART(YY,[Date])-1911 [Year]
            , [Item]
            , [Count]
        FROM [Coach_List]
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
          
                ",$startDate,$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugServiceByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT	[Date_Merk]
            , [Item]
            , [Count]
        INTO #tmp_Service_List
        FROM [Service_List]
        WHERE DATEPART(YY,[Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND (DATEPART(MM,[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            )

        SELECT	[Date_Merk]
                , '全部' [Item]
                , SUM([Count]) [Count]
        INTO #tmp_Service_List_ALL
        FROM #tmp_Service_List
        GROUP BY [Date_Merk]

        SELECT M.*, ROUND(CAST(M.[Count] AS float)/CAST(M1.[Count] AS float), 2) [Rate] FROM
        (
            SELECT * FROM #tmp_Service_List
            UNION ALL
            SELECT * FROM #tmp_Service_List_ALL
        ) M LEFT JOIN #tmp_Service_List_ALL M1 ON (M1.[Date_Merk] =  M.[Date_Merk])
        order by [Count] desc

        DROP TABLE #tmp_Service_List
        DROP TABLE #tmp_Service_List_ALL
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugServiceByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT [Year],[Item],sum([Count])　[Count] from (
        SELECT	DATEPART(YY,[Date])-1911 [Year]
        , [Item]
        , [Count]
        FROM [Service_List]
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
		)T group by [Year],[Item]
                ",$startDate,$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugCouplingByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(MM,[Date]) [MONTH]
        ,[Session]
        ,[Person_Times]
        FROM [Coupling_Counseling]
        WHERE DATEPART(YY,[Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/01'  AS DATE)))
                ",$startDate,$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugCouplingByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[Date])-1911 [YEAR]
        ,SUM([Session]) [Session]
        ,SUM([Person_Times]) [Person_Times]
        FROM [Coupling_Counseling]
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        GROUP BY DATEPART(YY,[Date])
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugMedicalByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(MM,[Date]) [Month]
        ,[Num]
        FROM [Medical_Treatment]
        WHERE DATEPART(YY,[Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND (DATEPART(MM,[Date]) >= DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) <= DATEPART(MM,CAST('%s'+'/01'  AS DATE)))
                ",$startDate,$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugMedicalByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[Date])-1911 [YEAR]
        ,SUM([Num]) [Num]
        FROM [Medical_Treatment]
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        GROUP BY DATEPART(YY,[Date])
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function Drug24HoursByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT [Item]
        ,[Count]
        FROM [24Hours_List]
        WHERE DATEPART(YY,[Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        ORDER BY [Count]
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function Drug24HoursByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        select [YEAR],[Item],sum([Count])[Count]　 from(
            SELECT DATEPART(YY,[Date])-1911 [YEAR]
                , [Item]
                ,[Count]
            FROM [24Hours_List]
            WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                    AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                    AND DATEPART(MM,[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
                    )T group by [YEAR],[Item]
            ORDER BY [Count]
                ",$startDate,$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
/*#endregion ---------------這是第五類---------------*/

/*#region ---------------這是第六類---------------*/
    public function TeenagerDrugGenderByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT [Gender]
            , COUNT([Gender]) [Count]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        AND [Dsacp_Tube]='1'
        GROUP BY [Gender]
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function TeenagerDrugGenderByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[File_UpLoadDate])-1911 [Year]
        , [Gender]
        , COUNT([Gender]) [Count]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND [Dsacp_Tube]='1'
        GROUP BY [Gender], DATEPART(YY,[File_UpLoadDate]) order by YEAR,Gender　desc
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function TeenagerDrugAgeByMonth(Request $request){
            $startDate = $request->startDate;
            $endDate = $request->endDate;
            $SQLComm = sprintf("SET NOCOUNT ON
            SELECT [Entry_Age]
            , COUNT([Entry_Age]) [Count]
            FROM [DRUGS_LIST_TEENAGER_Bak]
            WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
                AND [Dsacp_Tube]='1'
            GROUP BY [Entry_Age]
                    ",$endDate,$endDate);
            $posts = DB::select($SQLComm, [1]);
            return  $posts;
    }
    public function TeenagerDrugAgeByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[File_UpLoadDate])-1911 [Year]
        , [Entry_Age]
        , COUNT([Entry_Age]) [Count]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND [Dsacp_Tube]='1'
        GROUP BY [Entry_Age], DATEPART(YY,[File_UpLoadDate])
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function TeenagerDrugAbuseTypeByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
        , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
            ELSE '單一'
        END [MIX]
        , CASE WHEN [Drugs_Grade] LIKE '一、%%' THEN '一級毒品'
                WHEN [Drugs_Grade] LIKE '二、%%' THEN '二級毒品'
                WHEN [Drugs_Grade] LIKE '三、%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '一級%%' THEN '一級毒品'
            WHEN [Drugs_Grade] LIKE '二級%%' THEN '二級毒品'
            WHEN [Drugs_Grade] LIKE '三級%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '四級%%' THEN'四級毒品'
                    ELSE '其他'
        END [LEVEL]
        INTO #tmp_DRUGS_LIST_TEENAGER
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        AND [Dsacp_Tube]='1'

        SELECT [LEVEL],[MIX],COUNT(*) [Count] FROM #tmp_DRUGS_LIST_TEENAGER
        GROUP BY [MIX],[LEVEL]

        DROP TABLE #tmp_DRUGS_LIST_TEENAGER
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);

        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerDrugAbuseTypeByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
        , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
            ELSE '單一'
        END [MIX]
        , CASE WHEN [Drugs_Grade] LIKE '一、%%' THEN '一級毒品'
                WHEN [Drugs_Grade] LIKE '二、%%' THEN '二級毒品'
                WHEN [Drugs_Grade] LIKE '三、%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '一級%%' THEN '一級毒品'
            WHEN [Drugs_Grade] LIKE '二級%%' THEN '二級毒品'
            WHEN [Drugs_Grade] LIKE '三級%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '四級%%' THEN '四級毒品'
			WHEN [Drugs_Grade] LIKE '其他%%' THEN '其他'
        END [LEVEL]
        INTO #tmp_DRUGS_LIST_TEENAGER
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND [Dsacp_Tube]='1'

        SELECT [YEAR],[LEVEL],COUNT(*) [Count] FROM #tmp_DRUGS_LIST_TEENAGER
        GROUP BY [YEAR],[LEVEL]

        DROP TABLE #tmp_DRUGS_LIST_TEENAGER
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);

        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerDrugAgeTypeByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Entry_Age]
        , DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
        , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
            ELSE '單一'
        END [MIX]
        , CASE WHEN [Drugs_Grade] LIKE '一、%%' THEN '一級毒品'
                WHEN [Drugs_Grade] LIKE '二、%%' THEN '二級毒品'
                WHEN [Drugs_Grade] LIKE '三、%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '一級%%' THEN '一級毒品'
            WHEN [Drugs_Grade] LIKE '二級%%' THEN '二級毒品'
            WHEN [Drugs_Grade] LIKE '三級%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '四級%%' THEN '四級毒品'
			WHEN [Drugs_Grade] LIKE '其他%%' THEN '其他'
        END [LEVEL]
        INTO #tmp_DRUGS_LIST_TEENAGER
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE
        [Entry_Age] < 18
        AND DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        AND [Dsacp_Tube]='1'

        SELECT DISTINCT [Entry_Age],[DRUGS_TYPE],[DRUGS_LEVEL] INTO #tmp_DRUGS_MAP
        FROM #tmp_DRUGS_LIST_TEENAGER
        CROSS JOIN [DRUGS_MAP]

        SELECT T2.[Entry_Age],T2.[DRUGS_TYPE] [MIX],T2.[DRUGS_LEVEL] [LEVEL],COUNT(T1.[LEVEL]) [count] FROM #tmp_DRUGS_LIST_TEENAGER T1
        RIGHT JOIN #tmp_DRUGS_MAP T2 ON(T2.[Entry_Age]=T1.[Entry_Age] AND T2.[DRUGS_TYPE]=T1.[MIX] AND T2.[DRUGS_LEVEL]=T1.[LEVEL])
        GROUP BY T2.[Entry_Age],T2.[DRUGS_TYPE],T2.[DRUGS_LEVEL]
        ORDER BY T2.[Entry_Age],T2.[DRUGS_TYPE] desc,T2.[DRUGS_LEVEL]
        DROP TABLE #tmp_DRUGS_LIST_TEENAGER
        DROP TABLE #tmp_DRUGS_MAP
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerDrugAgeTypeByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Entry_Age]
        , DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
        , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
            ELSE '單一'
        END [MIX]
        , CASE WHEN [Drugs_Grade] LIKE '一、%%' THEN '一級毒品'
                WHEN [Drugs_Grade] LIKE '二、%%' THEN '二級毒品'
                WHEN [Drugs_Grade] LIKE '三、%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '一級%%' THEN '一級毒品'
            WHEN [Drugs_Grade] LIKE '二級%%' THEN '二級毒品'
            WHEN [Drugs_Grade] LIKE '三級%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '四級%%' THEN '四級毒品'
			WHEN [Drugs_Grade] LIKE '其他%%' THEN '其他'
        END [LEVEL]
        INTO #tmp_DRUGS_LIST_TEENAGER
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE [Entry_Age] < 18
        AND DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND [Dsacp_Tube]='1'

        SELECT [YEAR],[Entry_Age],[LEVEL],COUNT(*) [count] FROM #tmp_DRUGS_LIST_TEENAGER
        GROUP BY [YEAR],[Entry_Age],[LEVEL]

        DROP TABLE #tmp_DRUGS_LIST_TEENAGER
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerDrugAreaByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Area_Name]
        , COUNT(*) [Count]
        , ROW_NUMBER() OVER( ORDER BY COUNT(*) DESC) [Rank]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        AND [Dsacp_Tube]='1'
        GROUP BY [Area_Name]
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerDrugAreabyYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Area_Name]
                , DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
                , COUNT(*) [Count]
                , ROW_NUMBER() OVER( PARTITION BY DATEPART(YY,[File_UpLoadDate]) ORDER BY COUNT(*) DESC) [NUM]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND [Dsacp_Tube]='1'
        GROUP BY [Area_Name], DATEPART(YY,[File_UpLoadDate])
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerDrugCorrectionAreaByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SET NOCOUNT ON
        SELECT	M.[Location]
        , (ISNULL(M1.[Person],0)*100000/M.[Population]) [Person]
        , ROW_NUMBER() OVER(ORDER BY (ISNULL(M1.[Person],0)*100000/M.[Population]) DESC) [Rank]
        FROM [CIVIL_AFFAIRS_DEPT] M
        LEFT JOIN ( SELECT  [Area_Name] [Area_Name]
                , DATEPART(YY,[File_UpLoadDate])-1911 [Year]
				,DATEPART(MM,[File_UpLoadDate]) [Month]
                , COUNT(*) [Person]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            AND [Dsacp_Tube]='1'
        GROUP BY [Area_Name], DATEPART(YY,[File_UpLoadDate]),DATEPART(MM,[File_UpLoadDate])
        ) M1 ON (M1.[Area_Name] = M.[Location] AND M1.[Year] = M.[CHINA_Year] AND M.[Month]=M1.[Month])
        WHERE M.[Year]  = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
		   AND M.[Month]  = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
                ",$endDate,$endDate,$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerDrugCorrectionAreaByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT	M.[Location]
        , M.[CHINA_Year]
        , (ISNULL(M1.[Person],0)*100000/M.[Population]) [Person]
        , ROW_NUMBER() OVER(ORDER BY (ISNULL(M1.[Person],0)*100000/M.[Population]) DESC) [NUM]
        FROM [CIVIL_AFFAIRS_DEPT] M
        LEFT JOIN ( SELECT  [Area_Name] [Area_Name]
                , DATEPART(YY,[File_UpLoadDate])-1911 [Year]
                , COUNT(*) [Person]

        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND [Dsacp_Tube]='1'
        GROUP BY [Area_Name], DATEPART(YY,[File_UpLoadDate])
        ) M1 ON (M1.[Area_Name] = M.[Location] AND M1.[Year] = M.[CHINA_Year])
        WHERE M.[Year]  >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND M.[Year]  <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
                ",$startDate,$endDate,$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
/*#endregion ---------------這是第六類---------------*/


/*#region ---------------這是第七類---------------*/
    public function TeenagerChunhuiDrugGenderByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT [Gender]
		, COUNT([Gender]) [Count]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE [Education__Tube] = '1'
            AND DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            GROUP BY [Gender]
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function TeenagerChunhuiDrugGenderByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[File_UpLoadDate])-1911 [Year]
        , [Gender]
        , COUNT([Gender]) [Count]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE [Education__Tube] = '1'
        AND DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        GROUP BY [Gender], DATEPART(YY,[File_UpLoadDate]) order by YEAR,[Gender]　desc
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function TeenagerChunhuiDrugAgeByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT [Entry_Age]
		, COUNT([Entry_Age]) [Count]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE [Education__Tube] = '1'
            AND DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        GROUP BY [Entry_Age]
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function TeenagerChunhuiDrugAgeByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[File_UpLoadDate])-1911 [Year]
        , [Entry_Age]
        , COUNT([Entry_Age]) [Count]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE [Education__Tube] = '1'
        AND DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        GROUP BY [Entry_Age], DATEPART(YY,[File_UpLoadDate])
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function TeenagerChunhuiDrugAbuseTypeByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
        , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
            ELSE '單一'
        END [MIX]
        , CASE WHEN [Drugs_Grade] LIKE '一、%%' THEN '一級毒品'
                WHEN [Drugs_Grade] LIKE '二、%%' THEN '二級毒品'
                WHEN [Drugs_Grade] LIKE '三、%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '一級%%' THEN '一級毒品'
            WHEN [Drugs_Grade] LIKE '二級%%' THEN '二級毒品'
            WHEN [Drugs_Grade] LIKE '三級%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '四級%%' THEN '四級毒品'
			WHEN [Drugs_Grade] LIKE '其他%%' THEN '其他'
        END [LEVEL]
        INTO #tmp_DRUGS_LIST_TEENAGER
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE [Listed_Chunhui] = '1'
        AND DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        AND [Education__Tube] = '1'

        SELECT [LEVEL],[MIX],COUNT(*) [Count] FROM #tmp_DRUGS_LIST_TEENAGER
        GROUP BY [MIX],[LEVEL]

        DROP TABLE #tmp_DRUGS_LIST_TEENAGER
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);

        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerChunhuiDrugAbuseTypeByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
        , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
            ELSE '單一'
        END [MIX]
        , CASE WHEN [Drugs_Grade] LIKE '一、%%' THEN '一級毒品'
                WHEN [Drugs_Grade] LIKE '二、%%' THEN '二級毒品'
                WHEN [Drugs_Grade] LIKE '三、%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '一級%%' THEN '一級毒品'
            WHEN [Drugs_Grade] LIKE '二級%%' THEN '二級毒品'
            WHEN [Drugs_Grade] LIKE '三級%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '四級%%' THEN '四級毒品'
			WHEN [Drugs_Grade] LIKE '其他%%' THEN '其他'
        END [LEVEL]
        INTO #tmp_DRUGS_LIST_TEENAGER
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE [Listed_Chunhui] = '1'
        AND DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))

        SELECT [YEAR],[LEVEL],COUNT(*) [Count] FROM #tmp_DRUGS_LIST_TEENAGER
        GROUP BY [YEAR],[LEVEL]

        DROP TABLE #tmp_DRUGS_LIST_TEENAGER
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);

        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerChunhuiDrugAgeTypeByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Entry_Age]
        , DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
        , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
            ELSE '單一'
        END [MIX]
        , CASE WHEN [Drugs_Grade] LIKE '一、%%' THEN '一級毒品'
                WHEN [Drugs_Grade] LIKE '二、%%' THEN '二級毒品'
                WHEN [Drugs_Grade] LIKE '三、%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '一級%%' THEN '一級毒品'
            WHEN [Drugs_Grade] LIKE '二級%%' THEN '二級毒品'
            WHEN [Drugs_Grade] LIKE '三級%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '四級%%' THEN '四級毒品'
			WHEN [Drugs_Grade] LIKE '其他%%' THEN '其他'
        END [LEVEL]
        INTO #tmp_DRUGS_LIST_TEENAGER
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE [Listed_Chunhui] = '1'
        AND DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))

        SELECT DISTINCT [Entry_Age],[DRUGS_TYPE],[DRUGS_LEVEL] INTO #tmp_DRUGS_MAP
        FROM #tmp_DRUGS_LIST_TEENAGER
        CROSS JOIN [DRUGS_MAP]

        SELECT T2.[Entry_Age],T2.[DRUGS_TYPE] [MIX],T2.[DRUGS_LEVEL] [LEVEL],COUNT(T1.[LEVEL]) [count] FROM #tmp_DRUGS_LIST_TEENAGER T1
        RIGHT JOIN #tmp_DRUGS_MAP T2 ON(T2.[Entry_Age]=T1.[Entry_Age] AND T2.[DRUGS_TYPE]=T1.[MIX] AND T2.[DRUGS_LEVEL]=T1.[LEVEL])
        GROUP BY T2.[Entry_Age],T2.[DRUGS_TYPE],T2.[DRUGS_LEVEL]
        ORDER BY T2.[Entry_Age],T2.[DRUGS_TYPE] desc,T2.[DRUGS_LEVEL]
        DROP TABLE #tmp_DRUGS_LIST_TEENAGER
        DROP TABLE #tmp_DRUGS_MAP
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerChunhuiDrugAgeTypeByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Entry_Age]
        , DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
        , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
            ELSE '單一'
        END [MIX]
        , CASE WHEN [Drugs_Grade] LIKE '一、%%' THEN '一級毒品'
                WHEN [Drugs_Grade] LIKE '二、%%' THEN '二級毒品'
                WHEN [Drugs_Grade] LIKE '三、%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '一級%%' THEN '一級毒品'
            WHEN [Drugs_Grade] LIKE '二級%%' THEN '二級毒品'
            WHEN [Drugs_Grade] LIKE '三級%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '四級%%' THEN '四級毒品'
			WHEN [Drugs_Grade] LIKE '其他%%' THEN '其他'
        END [LEVEL]
        INTO #tmp_DRUGS_LIST_TEENAGER
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE [Listed_Chunhui] = '1'
        AND DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))


        SELECT [YEAR],[Entry_Age],[LEVEL],COUNT(*) [count] FROM #tmp_DRUGS_LIST_TEENAGER
        GROUP BY [YEAR],[Entry_Age],[LEVEL]

        DROP TABLE #tmp_DRUGS_LIST_TEENAGER
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerChunhuiDrugAreaByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Area_Name] [Area_Name]
        , COUNT(*) [Count]
        , ROW_NUMBER() OVER( ORDER BY COUNT(*) DESC) [Rank]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE [Listed_Chunhui] = '1'
        AND DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        GROUP BY [Area_Name]
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerChunhuiDrugAreabyYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Area_Name] [Area_Name]
                , DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
                , COUNT(*) [Count]
                , ROW_NUMBER() OVER( PARTITION BY DATEPART(YY,[File_UpLoadDate]) ORDER BY COUNT(*) DESC) [NUM]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE [Listed_Chunhui] = '1'
        AND DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        GROUP BY [Area_Name], DATEPART(YY,[File_UpLoadDate])
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
/*#endregion ---------------這是第七類---------------*/

/*#region ---------------這是第八類---------------*/
    public function TeenagerInterruptDrugGenderByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT [Gender]
        , COUNT([Gender]) [Count]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        AND [Chunhui_Interrupt]='1'
        GROUP BY [Gender]
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function TeenagerInterruptDrugGenderByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[File_UpLoadDate])-1911 [Year]
        , [Gender] [Sex]
        , COUNT([Gender]) [Count]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND [Chunhui_Interrupt]='1'
        GROUP BY [Gender], DATEPART(YY,[File_UpLoadDate]) order by YEAR,Gender　desc
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function TeenagerInterruptDrugAgeByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT [Entry_Age]
        , COUNT([Entry_Age]) [Count]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            AND [Chunhui_Interrupt]='1'
        GROUP BY [Entry_Age]
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function TeenagerInterruptDrugAgeByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[File_UpLoadDate])-1911 [Year]
        , [Entry_Age]
        , COUNT([Entry_Age]) [Count]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND [Chunhui_Interrupt]='1'
        GROUP BY [Entry_Age], DATEPART(YY,[File_UpLoadDate])
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function TeenagerInterruptDrugAbuseTypeByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
        , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
            ELSE '單一'
        END [MIX]
        , CASE WHEN [Drugs_Grade] LIKE '一、%%' THEN '一級毒品'
                WHEN [Drugs_Grade] LIKE '二、%%' THEN '二級毒品'
                WHEN [Drugs_Grade] LIKE '三、%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '一級%%' THEN '一級毒品'
            WHEN [Drugs_Grade] LIKE '二級%%' THEN '二級毒品'
            WHEN [Drugs_Grade] LIKE '三級%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '四級%%' THEN '四級毒品'
			WHEN [Drugs_Grade] LIKE '其他%%' THEN '其他'
        END [LEVEL]
        INTO #tmp_DRUGS_LIST_TEENAGER
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE  DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        AND [Chunhui_Interrupt]='1'

        SELECT [LEVEL],[MIX],COUNT(*) [Count] FROM #tmp_DRUGS_LIST_TEENAGER
        GROUP BY [MIX],[LEVEL]

        DROP TABLE #tmp_DRUGS_LIST_TEENAGER
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);

        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerInterruptDrugAbuseTypeByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
        , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
            ELSE '單一'
        END [MIX]
        , CASE WHEN [Drugs_Grade] LIKE '一、%%' THEN '一級毒品'
                WHEN [Drugs_Grade] LIKE '二、%%' THEN '二級毒品'
                WHEN [Drugs_Grade] LIKE '三、%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '一級%%' THEN '一級毒品'
            WHEN [Drugs_Grade] LIKE '二級%%' THEN '二級毒品'
            WHEN [Drugs_Grade] LIKE '三級%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '四級%%' THEN '四級毒品'
			WHEN [Drugs_Grade] LIKE '其他%%' THEN '其他'
        END [LEVEL]
        INTO #tmp_DRUGS_LIST_TEENAGER
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE  DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND [Chunhui_Interrupt]='1'

        SELECT [YEAR],[LEVEL],COUNT(*) [Count] FROM #tmp_DRUGS_LIST_TEENAGER
        GROUP BY [YEAR],[LEVEL]

        DROP TABLE #tmp_DRUGS_LIST_TEENAGER
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);

        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerInterruptDrugAgeTypeByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Entry_Age]
        , DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
        , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
            ELSE '單一'
        END [MIX]
        , CASE WHEN [Drugs_Grade] LIKE '一、%%' THEN '一級毒品'
                WHEN [Drugs_Grade] LIKE '二、%%' THEN '二級毒品'
                WHEN [Drugs_Grade] LIKE '三、%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '一級%%' THEN '一級毒品'
            WHEN [Drugs_Grade] LIKE '二級%%' THEN '二級毒品'
            WHEN [Drugs_Grade] LIKE '三級%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '四級%%' THEN '四級毒品'
			WHEN [Drugs_Grade] LIKE '其他%%' THEN '其他'
        END [LEVEL]
        INTO #tmp_DRUGS_LIST_TEENAGER
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        AND [Chunhui_Interrupt]='1'

        SELECT DISTINCT [Entry_Age],[DRUGS_TYPE],[DRUGS_LEVEL] INTO #tmp_DRUGS_MAP
        FROM #tmp_DRUGS_LIST_TEENAGER
        CROSS JOIN [DRUGS_MAP]

        SELECT T2.[Entry_Age],T2.[DRUGS_TYPE] [MIX],T2.[DRUGS_LEVEL] [LEVEL],COUNT(T1.[LEVEL]) [count] FROM #tmp_DRUGS_LIST_TEENAGER T1
        RIGHT JOIN #tmp_DRUGS_MAP T2 ON(T2.[Entry_Age]=T1.[Entry_Age] AND T2.[DRUGS_TYPE]=T1.[MIX] AND T2.[DRUGS_LEVEL]=T1.[LEVEL])
        GROUP BY T2.[Entry_Age],T2.[DRUGS_TYPE],T2.[DRUGS_LEVEL]
        ORDER BY T2.[Entry_Age],T2.[DRUGS_TYPE] desc,T2.[DRUGS_LEVEL]
        DROP TABLE #tmp_DRUGS_LIST_TEENAGER
        DROP TABLE #tmp_DRUGS_MAP
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerInterruptDrugAgeTypeByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Entry_Age]
        , DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
        , CASE WHEN [Drugs_Grade] LIKE '%%混用%%' THEN '混用'
            ELSE '單一'
        END [MIX]
        , CASE WHEN [Drugs_Grade] LIKE '一、%%' THEN '一級毒品'
                WHEN [Drugs_Grade] LIKE '二、%%' THEN '二級毒品'
                WHEN [Drugs_Grade] LIKE '三、%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '一級%%' THEN '一級毒品'
            WHEN [Drugs_Grade] LIKE '二級%%' THEN '二級毒品'
            WHEN [Drugs_Grade] LIKE '三級%%' THEN '三級毒品'
            WHEN [Drugs_Grade] LIKE '四級%%' THEN '四級毒品'
			WHEN [Drugs_Grade] LIKE '其他%%' THEN '其他'
        END [LEVEL]
        INTO #tmp_DRUGS_LIST_TEENAGER
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND [Chunhui_Interrupt]='1'

        SELECT [YEAR],[Entry_Age],[LEVEL],COUNT(*) [count] FROM #tmp_DRUGS_LIST_TEENAGER
        GROUP BY [YEAR],[Entry_Age],[LEVEL]

        DROP TABLE #tmp_DRUGS_LIST_TEENAGER
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerInterruptDrugAreaByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Area_Name] [Domicile]
        , COUNT(*) [Count]
        , ROW_NUMBER() OVER( ORDER BY COUNT(*) DESC) [Rank]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE DATEPART(YY,[File_UpLoadDate]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_UpLoadDate]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        AND [Chunhui_Interrupt]='1'
        GROUP BY [Area_Name]
                ",$endDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
    public function TeenagerInterruptDrugAreabyYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT  [Area_Name] [Domicile]
                , DATEPART(YY,[File_UpLoadDate])-1911 [YEAR]
                , COUNT(*) [Count]
                , ROW_NUMBER() OVER( PARTITION BY DATEPART(YY,[File_UpLoadDate]) ORDER BY COUNT(*) DESC) [NUM]
        FROM [DRUGS_LIST_TEENAGER_Bak]
        WHERE  DATEPART(YY,[File_UpLoadDate]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND DATEPART(YY,[File_UpLoadDate]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            AND [Chunhui_Interrupt]='1'
        GROUP BY [Area_Name], DATEPART(YY,[File_UpLoadDate])
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  response($posts)->withHeaders(['Connection'=>"Keep-Alive","Keep-Alive"=>"timeout=5, max=91"]);
    }
/*#endregion ---------------這是第八類---------------*/

/*#region ---------------這是第九類---------------*/
    public function DrugCourtPersonByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT [CHINA_Date]
        , [Not_Penalty_Num]
        , [Penalty_Num]
        FROM [Juvenile_Court]
        WHERE DATEPART(YY,[Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugCourtPersonByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[Date])-1911 [Year]
      , SUM([Not_Penalty_Num]) +SUM([Penalty_Num]) [Total]
        FROM [Juvenile_Court]
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
                AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        GROUP BY DATEPART(YY,[Date])
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugCourtCaseByMonth(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT [CHINA_Date]
        , [Not_Penalty_Case]
        , [Penalty_Case]
        FROM [Juvenile_Court]
        WHERE DATEPART(YY,[Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
            AND DATEPART(MM,[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
    public function DrugCourtCaseByYear(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON
        SELECT DATEPART(YY,[Date])-1911 [Year]
        ,SUM([Not_Penalty_Case])+SUM([Penalty_Case]) [Total]
        FROM [Juvenile_Court]
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
                AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        GROUP BY DATEPART(YY,[Date])
                ",$startDate,$endDate);
        $posts = DB::select($SQLComm, [1]);
        return  $posts;
    }
/*#endregion ---------------這是第九類---------------*/

/*#region ---------------這是第十類---------------*/
public function SpecificAreaBusinessIndustryCommentTextByMonth(Request $request){
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $SQLComm = sprintf("SET NOCOUNT ON
    SELECT
      [Domicile]
	  ,count([Domicile])　[count]
    FROM [DSACP_Kaohsiung].[dbo].[Specific_Business_demanagement]
    WHERE DATEPART(YY,[File_Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        group by [Domicile]
            ",$endDate,$endDate);
    $posts = DB::select($SQLComm, [1]);
    return  $posts;
}
public function SpecificAreaBusinessIndustryByMonth(Request $request){
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $SQLComm = sprintf("SET NOCOUNT ON
    SELECT [Business]
    , Count([Business]) [Count]
    INTO #tmp_Specific_Business_demanagement
    FROM [Specific_Business_demanagement]
    WHERE DATEPART(YY,[File_Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
    GROUP BY [Business]

    SELECT [Business]
        , [Count]
        , ROUND(CAST([Count] AS float)/CAST((SELECT SUM([Count]) FROM #tmp_Specific_Business_demanagement) AS float) , 2)*100 [Rate]
    FROM #tmp_Specific_Business_demanagement

    DROP TABLE #tmp_Specific_Business_demanagement
            ",$endDate,$endDate);
    $posts = DB::select($SQLComm, [1]);
    return  $posts;
}
public function SpecificAreaBusinessIndustryByYear(Request $request){
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $SQLComm = sprintf("SET NOCOUNT ON
    SELECT [Business]
    , Count([Business]) [Count]
    , DATEPART(YY,[File_Date])-1911 [YEAR]
    INTO #tmp_Specific_Business_demanagement
    FROM [Specific_Business_demanagement]
    WHERE DATEPART(YY,[File_Date]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND DATEPART(YY,[File_Date]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
    GROUP BY [Business],DATEPART(YY,[File_Date])

    SELECT [YEAR]
        , [Business]
        , [Count]
        , ROUND(CAST([Count] AS float)/CAST((SELECT SUM([Count]) FROM #tmp_Specific_Business_demanagement) AS float) , 2)*100 [Rate]
    FROM #tmp_Specific_Business_demanagement

    DROP TABLE #tmp_Specific_Business_demanagement
            ",$startDate,$endDate);
    $posts = DB::select($SQLComm, [1]);
    return  $posts;
}
public function SpecificAreaAdministrativeByMonth(Request $request){
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $SQLComm = sprintf("SET NOCOUNT ON
    SELECT  cast(DATEPART(YY,[File_Date])-1911 as varchar)+'年'+cast(DATEPART(MM,[File_Date])as varchar)+'月' [China_Date]
       ,[Domicile]
       , Count([Domicile]) [Count]
     --  INTO #tmp_Specific_Business_demanagement
       FROM [Specific_Business_demanagement]
       WHERE DATEPART(YY,[File_Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
       AND DATEPART(MM,[File_Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
       GROUP BY [File_Date],[Domicile]
       order by [Count] Desc
            ",$endDate,$endDate);
    $posts = DB::select($SQLComm, [1]);
    return  $posts;
}
public function SpecificAreaAdministrativeByYear(Request $request){
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $SQLComm = sprintf("SET NOCOUNT ON
    SELECT DATEPART(YY,[File_Date])-1911 [YEAR]
    , [Domicile]
    , Count([File_Date]) [Count]
    FROM [Specific_Business_demanagement]
    WHERE DATEPART(YY,[File_Date]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND DATEPART(YY,[File_Date]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
    GROUP BY [Domicile],  DATEPART(YY,[File_Date]) order by [Count] desc
            ",$startDate,$endDate);
    $posts = DB::select($SQLComm, [1]);
    return  $posts;
}
public function SpecificIndustryByMonth(Request $request){
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $SQLComm = sprintf("SET NOCOUNT ON
    SELECT M.[Item]
    , ISNULL(M1.[Count],0) [登記業者]
    , M.[Count] [列管家數]
    , ROUND(CAST(M.[Count] AS float)/CAST(ISNULL(M1.[Count],'1') AS float),2) [列管率]
    FROM [Specific_Business_Registered_Companies] M
    LEFT JOIN (SELECT [Business]
                    , COUNT([Business]) [Count]

                FROM [Specific_Business_demanagement]
                WHERE DATEPART(YY,[File_Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                    AND DATEPART(MM,[File_Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
                GROUP BY [Business]
				) M1 ON(M1.[Business] = M.[Item])
    WHERE DATEPART(YY,[Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
    AND DATEPART(MM,[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
            ",$endDate,$endDate,$endDate,$endDate);
    $posts = DB::select($SQLComm, [1]);
    return  $posts;
}
public function SpecificIndustryByYear(Request $request){
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $SQLComm = sprintf("SET NOCOUNT ON
    SELECT cast(DATEPART(YY,M.[Date])-1911 as varchar) [YEAR]
        ,M.[Item]
        , ISNULL(M1.[Count],0) [登記業者]
        , M.[Count] [列管家數]
        , ROUND(CAST(M.[Count] AS float)/CAST(ISNULL(M1.[Count],'1') AS float),2) [Rate]
        FROM [Specific_Business_Registered_Companies] M
        LEFT JOIN (SELECT  [File_Date]
                        ,[Business]
                        , COUNT([Business]) [Count]

                    FROM [Specific_Business_demanagement]
                    WHERE (DATEPART(YY,[File_Date]) >=DATEPART(YY, CAST('%s'+'/01/01'  AS DATE) )
                    AND DATEPART(YY,[File_Date]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
                )
                    GROUP BY [File_Date],[Business]
                    ) M1 ON(M1.[Business] = M.[Item])
        WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
            ",$startDate,$endDate,$startDate,$endDate);
    $posts = DB::select($SQLComm, [1]);
    return  $posts;
}
public function SpecificAreaBusinessByMonth(Request $request){
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $SQLComm = sprintf("SET NOCOUNT ON
    SELECT DATEPART(MM,[File_Date]) [Month]
    , [Tube_Start]
    , [Tube_End]
    , [File_Date]
    INTO #tmp_Specific_Business_demanagement
    FROM [Specific_Business_demanagement]
    WHERE DATEPART(YY,[File_Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
    AND DATEPART(MM,[File_Date]) >= DATEPART(MM,CAST('%s'+'/01'  AS DATE))
    AND DATEPART(MM,[File_Date]) <= DATEPART(MM,CAST('%s'+'/01'  AS DATE))
    SELECT *
    INTO #tmp
    FROM
    (
    SELECT M.[Month], '列管業者(不含除管)'[Item]
            ,M.[Count]-ISNULL(M1.[Count] ,0) [Count]
    FROM
    (SELECT [Month], COUNT(*) [Count]
    FROM #tmp_Specific_Business_demanagement
    GROUP BY [Month]
    ) M LEFT JOIN (
                    SELECT [Month], COUNT(*) [Count]
                    FROM #tmp_Specific_Business_demanagement
                    WHERE [File_Date] NOT BETWEEN [Tube_Start] AND [Tube_End]
                    GROUP BY [Month]
                    ) M1 ON (M1.[Month] = M.[Month])
    UNION ALL
    SELECT DATEPART(MM,[Date]) [Month]
                        , '登記業者'[Item]
                        , COUNT(*) [Count]
                    FROM [Specific_Business_Registered_Companies]
                    GROUP BY DATEPART(MM,[Date])
    ) M
    SELECT * FROM #tmp
    UNION
    SELECT [Month]
            ,'列管率' [Item]
            , isnull(ROUND(CAST((SELECT [Count] FROM #tmp WHERE [Month] = M.[Month] AND [Item] = '列管業者(不含除管)') AS float)
                /
            CAST((SELECT [Count] FROM #tmp WHERE [Month] = M.[Month] AND [Item] = '登記業者') AS float), 2),0) [Count]
    FROM #tmp M
    order by [Month],[Item] desc
    DROP TABLE #tmp
    DROP TABLE #tmp_Specific_Business_demanagement
            ",$startDate,$startDate,$endDate);
    $posts = DB::select($SQLComm, [1]);
    return  $posts;
}
public function SpecificAreaBusinessByYear(Request $request){
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $SQLComm = sprintf("SET NOCOUNT ON
    SELECT DATEPART(YY,[File_Date]) [Year]
    , [Tube_Start]
    , [Tube_End]
    , [File_Date]
    INTO #tmp_Specific_Business_demanagement
    FROM [Specific_Business_demanagement]
    WHERE DATEPART(YY,[File_Date]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
    AND DATEPART(YY,[File_Date]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))


    SELECT *
    INTO #tmp
    FROM
    (
    SELECT M.[Year], '列管業者(不含除管)'[Item]
        ,M.[Count]-ISNULL(M1.[Count] ,0) [Count]
    FROM
    (SELECT [Year], COUNT(*) [Count]
    FROM #tmp_Specific_Business_demanagement
    GROUP BY [Year]
    ) M LEFT JOIN (
                    SELECT [Year], COUNT(*) [Count]
                    FROM #tmp_Specific_Business_demanagement
                    WHERE [File_Date] NOT BETWEEN [Tube_Start] AND [Tube_End]
                    GROUP BY [Year]
                ) M1 ON (M1.[Year] = M.[Year])
    UNION ALL
    SELECT DATEPART(YY,[Date]) [Year]
                    , '登記業者'[Item]
                    , COUNT(*) [Count]
                FROM [Specific_Business_Registered_Companies]
                GROUP BY DATEPART(YY,[Date])
    ) M

    SELECT [Year]-1911 [Year]
        ,'Rate' [Item]
        , ROUND(CAST((SELECT [Count] FROM #tmp WHERE [Year] = M.[Year] AND [Item] = '列管業者(不含除管)') AS float)
            /
            CAST((SELECT [Count] FROM #tmp WHERE [Year] = M.[Year] AND [Item] = '登記業者') AS float), 2) [Count]
    FROM #tmp M
    order by [Year]
    DROP TABLE #tmp
    DROP TABLE #tmp_Specific_Business_demanagement
            ",$startDate,$endDate);
    $posts = DB::select($SQLComm, [1]);
    return  $posts;
}
public function SixBusinessByMonth(Request $request){
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $SQLComm = sprintf("SET NOCOUNT ON
    SELECT *
    INTO #tmp
    FROM
    (
    SELECT 1 [NUM]
        , DATEPART(YY, [Date]) [YEAR]
        ,'列管' [Type]
        ,  '列管家數(含除管)'[Item]
        ,SUM([Count]) [Count]
    FROM [Specific_Business_Registered_Companies]
    WHERE DATEPART(YY,[Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
    GROUP BY [Date]
    UNION ALL
    SELECT 1 [NUM]
          , DATEPART(YY, [File_Date]) [YEAR]
            ,'非列管' [Type]
            ,  '非列管家數'[Item]
            , COUNT([Business]) [Count]
    FROM [Specific_Business_demanagement]
    WHERE DATEPART(YY,[File_Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[File_Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
        AND [File_Date] BETWEEN [Tube_Start] AND [Tube_End]
    GROUP BY DATEPART(YY, [File_Date])
    UNION ALL
    SELECT 2 [NUM]
        , DATEPART(YY, [Date]) [YEAR]
        ,CASE WHEN CHARINDEX('非列管',[Item])>0 THEN '非列管' ELSE '列管' END [Type]
        ,CASE WHEN [Item] = '列管業者主動通報' THEN '主動通報家數'
        WHEN [Item] = '非列管業者主動通報' THEN '主動通報家數'
        END [Item]
        ,[Count]
    FROM [Specific_Business_Unsolicited]
    WHERE DATEPART(YY,[Date]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
        AND DATEPART(MM,[Date]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
    ) M

    SELECT * FROM #tmp
    UNION ALL
    SELECT	3 [NUM]
        , '' [YEAR]
        , '列管' [Type]
        , '通報率' [Item]
        , ROUND(CAST((SELECT [Count] FROM #tmp WHERE [Item] = '主動通報家數' and [Type]='列管') AS float)/CAST((SELECT [Count] FROM #tmp WHERE [Item] = '列管家數(含除管)') AS float)*100,2) [Count]
    UNION ALL
    SELECT 3 [NUM]
        ,'' [YEAR]
        , '非列管' [Type]
        , '通報率' [Item]
        ,ROUND(CAST((SELECT [Count] FROM #tmp WHERE [Item] = '主動通報家數' and [Type]='非列管') AS float)/CAST((SELECT [Count] FROM #tmp WHERE [Item] = '非列管家數') AS float)*100,2) [Count]

    DROP TABLE #tmp
            ",$startDate,$endDate,$startDate,$endDate,$startDate,$endDate);
    $posts = DB::select($SQLComm, [1]);
    return  $posts;
}
public function SixBusinessByYear(Request $request){
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $SQLComm = sprintf("SET NOCOUNT ON
    SELECT *
    INTO #tmp
    FROM
    (
    SELECT DATEPART(YY, [Date]) [YEAR]
        ,  '列管家數(含除管)'[Item]
        ,SUM([Count]) [Count]
        --SELECT *
    FROM [Specific_Business_Registered_Companies]
    WHERE DATEPART(YY,[Date]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND DATEPART(YY,[Date]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
    GROUP BY DATEPART(YY,[Date])
    UNION ALL
    SELECT  DATEPART(YY, [File_Date]) [YEAR]
            ,  '非列管家數'[Item]
            , COUNT([Business]) [Count]
            --SELECT *
    FROM [Specific_Business_demanagement]
    WHERE DATEPART(YY,[File_Date]) >= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND DATEPART(YY,[File_Date]) <= DATEPART(YY,CAST('%s'+'/01/01'  AS DATE))
        AND [File_Date] BETWEEN [Tube_Start] AND [Tube_End]
    GROUP BY DATEPART(YY, [File_Date])
    ) M

    SELECT M.[YEAR]-1911 [YEAR], M.[Item], ROUND(CAST(M.[Count] AS float)/CAST(M1.[Sum] AS float)*100, 2) [RATE]
    FROM #tmp M
    LEFT JOIN (SELECT [YEAR], SUM([Count]) [Sum] FROM #tmp GROUP BY [YEAR]) M1 ON (M1.[YEAR] = M.[YEAR])

    DROP TABLE #tmp
            ",$startDate,$endDate,$startDate,$endDate);
    $posts = DB::select($SQLComm, [1]);
    return  $posts;
}
/*#endregion ---------------這是第十類---------------*/
}


