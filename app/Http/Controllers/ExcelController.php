<?php
namespace  App\Http\Controllers;
//use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
//use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;
use PDO;
use Illuminate\Support\Env;
use App\Models\User;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class ExcelController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    //顯示所有資料
    public function connect(){

        $posts=DB::table('TOOLLIST')->where('id', '=', "1")->get();
        //$posts = $db->select('select * from [MACHINE_LIST]');
        //return view('layouts/index', ['articles' => $posts]);
         echo json_encode($posts);
    }
    public function posImport(Request $request){
        //header("content-type:text/html;charset=utf-8");
        //上传excel文件
        if ($request->hasFile('Files')){
        $file = $request->file('Files');
        //将文件保存到public/uploads目录下面
        //$info = $file->validate(['file'=>'required|mimes:xls,xlsx|max:2048'])->move( './uploads');
        $info = $file->move('./uploads');
        if($info){
            //获取上传到后台的文件名
            $fileName = $info->getFilename();
            //获取文件路径
            $filePath = public_path().DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.$fileName;
            //获取文件后缀
            $suffix = $info->getExtension();
            //判断哪种类型
            if($suffix=="xlsx"){
                $reader = IOFactory::createReader('Excel2007');
            }else{
                $reader = IOFactory::createReader('Excel5');
            }
        }else{
            $this->error('文件过大或格式不正确导致上传失败-_-!');
        }
        //载入excel文件
        $excel = $reader->load("$filePath",$encode = 'utf-8');
        //读取第一张表
        $sheet = $excel->getSheet(0);
        //获取总行数
        $row_num = $sheet->getHighestRow();
        //获取总列数
        $col_num = $sheet->getHighestColumn();
        $data = []; //数组形式获取表格数据
        for ($i = 2; $i <= $row_num; $i ++) {
            $data[$i]['code']  = $sheet->getCell("A".$i)->getValue();
            $data[$i]['last_code']  = substr($sheet->getCell("A".$i)->getValue(),-6);
            // $time = date('Y-m-d H:i',\PHPExcel_Shared_Date::ExcelToPHP($sheet->getCell("B".$i)->getValue()));
            $time = date('Y-m-d H:i',Date::excelToDateTimeObject($sheet->getCell("B".$i)->getValue()));
            $data[$i]['time'] = strtotime($time);
            //将数据保存到数据库
        }
        $res = Db::name('pos_code')->insertAll($data);
        if($res){
            return redirect('/admin/pos/posCodeLog');
        }else{
            $return = [
                'code'  =>  0,
                'msg'   =>  '提交失败，请刷新重试'
            ];
            return json_decode( json_encode($return), true);
        }
    }
    else
        return 'no files';
    }
    public function phpinfo(){
        return view('phpinfo');
    }
    public function showbanndePage(Request $request){
        return view('bannerPage',['id'=>$request->id]);
    }
    public function showbanndePageforYear(Request $request){
        return view('bannerPageforYear',['id'=>$request->id]);
    }
}


