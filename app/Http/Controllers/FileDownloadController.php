<?php

namespace App\Http\Controllers;

use App\Models\Kaohsiung\KaohsiungMap;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FileDownloadController extends Controller
{
    // 下載 檔案管理-檔案下載

    public function index()
    {
        // 三四及講習
        $SQLComm = sprintf("SELECT [Banner_ID]
            ,[Banner_Name]
            ,[Chart_ID]
            ,[Chart_Name]
            ,[Type]
            ,[Function_Name]
            ,[Function_ID]
            ,[Source_Name]
        FROM [Banner_Chart_Config]
        where [Function_Name] is not null AND (([Chart_ID] < 10 AND [Type] = '月報') OR ([Chart_ID] < 8  AND [Type] = '年報'  ))");
        $ReportCharts = DB::select($SQLComm);
        // dd($ReportCharts);

        $now_year = Carbon::now()->year - 1911;
        $now_month = Carbon::now()->month;
        $min_year = 107; // 最小年度
        $p = [
            'ReportCharts' => $ReportCharts,
            'downTypes' =>[
                'lecture'=>'三四級講習',
                'home'=>'主畫面',
                'radar'=>'文字雲與雷達圖',
            ],
            'now_year' => $now_year,
            'min_year' => $min_year,
            'now_month' => $now_month,
            'maps' => KaohsiungMap::query()->orderBy('ID')->get(),
            'form_action' => route('fileDownload'),
            'check_url'=> route('fileDownload.checkData')
        ];
        return view('fileDownload',$p);
    }

    public function fileDownload(Request $request)
    {
        // not use;
        return ;
        $validator = $this->myValidator($request);
        if($validator->fails()){
            return response()->json(['status'=>false,'response'=>[],'error'=>$validator->errors()]);
        }
        $requested = $validator->validated();
        switch ($requested['type_name']){
            case 'lecture': // 三四 講習
                if($requested['lecture_type']=='year'){
                    // 年報
                    $p = [
                        'selectYear'=> $requested['home_year'],
                        'selectMonth'=> $requested['home_month'],
                        'selectStartYear' => $requested['home_year'],
                        'selectEndYear' => $requested['home_year'],
                        'action' => 'home',
                    ];
                    return view('print.index_print',$p);
                }else{
                    // 月報

                }
                $p = [
                    'selectYear'=> $requested['lecture_year'],
                    'selectMonth'=> $requested['lecture_month'],
                ];
                return view('print.DrugCase_print',$p);
                break;
            case 'home': // 首頁下載
                $p = [
                    'selectYear'=> $requested['home_year'],
                    'selectMonth'=> $requested['home_month'],
                    'selectStartYear' => $requested['home_year'],
                    'selectEndYear' => $requested['home_year'],
                    'action' => 'home',
                ];
                // return view('print.index_print',$p);
                break;
            case 'radar': // 文字雲 & 雷達圖
                // dd('文字雷達',$requested);
                switch ($requested['radar_sub_type']){
                    case 'all': // 全國
                        $p = [
                            'selectYear' => $requested['home_year'],
                            'selectMonth' => $requested['home_month'],
                            'selectStartYear' => $requested['radar_start_year'],
                            'selectEndYear' => $requested['radar_end_year'],
                            'selectStartMonth' => $requested['radar_start_month'],
                            'selectEndMonth' => $requested['radar_end_month'],
                            'action' => 'radar',
                        ];
                        return view('print.index_print',$p);
                        break;
                    case 'area': // 區域

                        $areaNo = $request['area_id'];
                        $areaName = DB::table('KAOHSIUNG_Map')->where('ID', $areaNo)->select(DB::raw('[City_Name] as Location'))->first()->Location;

                        $SQLComm = "SELECT [City_Code]
                                        ,[District_Code]
                                        ,[City_Name]
                                        ,[District_Name]
                                    FROM [KAOHSIUNG_LOCALCULTURAL]
                                    where [City_Name]=:City_Name";

                        $Villages = DB::select($SQLComm, ['City_Name' => $areaName]);
                        /* 可能會用到的 時間參數*/
                        // $p = [
                        //     'selectYear' => $requested['home_year'],
                        //     'selectMonth' => $requested['home_month'],
                        //     'selectStartYear' => $requested['radar_start_year'],
                        //     'selectEndYear' => $requested['radar_end_year'],
                        //     'selectStartMonth' => $requested['radar_start_month'],
                        //     'selectEndMonth' => $requested['radar_end_month'],
                        //     'Villages' => $Villages,
                        //     'areaNo' => $areaNo,
                        //     'areaName' => $areaName
                        // ];
                        // return view('print.radarArea_print',$p);
                        break;
                    case 'people': // 個案管理
                        $caseNo = $request['case_people'];
                        /* 可能會用到的 時間參數*/
                        // $p = [
                        //     'selectYear' => $requested['home_year'],
                        //     'selectMonth' => $requested['home_month'],
                        //     'selectStartYear' => $requested['radar_start_year'],
                        //     'selectEndYear' => $requested['radar_end_year'],
                        //     'selectStartMonth' => $requested['radar_start_month'],
                        //     'selectEndMonth' => $requested['radar_end_month'],
                        //     'caseNo' => $caseNo,
                        // ];
                        $res = [
                            [
                                'Banner_ID'=> "1",
                                'Banner_Name'=> "檔案下載", //本市查獲毒品概況分析
                                'Chart_ID'=> "1",
                                'Chart_Name'=> "文字雲與雷達圖-個案",
                                'Function_ID'=> "SelectCrimialMonth",
                                'Function_Name'=> "getPoisonAnalysischart",
                                'Source_Name'=> "",
                                'Type'=> "文字雲與雷達圖-個案",
                            ]
                        ];
                        return response()->json([ 'status'=>true,'response'=>$res]);
                        // return view('print.radarPeople_print',$p);
                        break;
                    default:
                        abort(404);
                        break;
                }
                break;
        }
    }

    private function myValidator(Request $request)
    {
        $v = Validator::make($request->all(),[
            'type_name' => ['required','in:lecture,home,radar'],
            // 講習
            'lecture_type' => ['nullable','required_if:type_name,lecture','in:year,month'],
            'lecture_year' => ['required_if:type_name,lecture'],
            'lecture_month' => ['required_if:lecture_type,month'],
            // 主畫面
            'home_year' => ['required_if:type_name,home'],
            'home_month' => ['required_if:type_name,home'],

            // 文字雲 & 雷達圖
            'radar_sub_type' => ['required_if:type_name,radar','in:all,area,people'], // 範圍
            'area_id' => ['required_if:radar_sub_type,area'], // 行政區範圍
            'case_people' => ['required_if:radar_sub_type,people'], // 案號
            'radar_start_year' => ['required_if:type_name,radar'], // 起始年度
            'radar_start_month' => ['required_if:type_name,radar'], // 起始月份
            'radar_end_year' => ['required_if:type_name,radar'], // 結束年度
            'radar_end_month' => ['required_if:type_name,radar'], // 結束月份

        ],[],[]);
        return $v;
    }

    public function checkData(Request $request)
    {
        $validator = $this->myValidator($request);
        if($validator->fails()){
            return response()->json(['status'=>false,'response'=>[],'error'=>$validator->errors()]);
        }
        $requested = $validator->validated();
        switch ($requested['type_name']){
            case 'lecture': // 三四 講習
                if($requested['lecture_type'] == 'year'){
                    //     // 年報
                    $type = '年報';
                    //     $res = [
                    //         [
                    //             'Banner_ID'=> "1",
                    //             'Banner_Name'=> "檔案下載", //本市查獲毒品概況分析
                    //             'Chart_ID'=> "1",
                    //             'Chart_Name'=> "主畫面",
                    //             'Function_ID'=> "chart3",
                    //             'Function_Name'=> "makeWCHomeChart",
                    //             'Source_Name'=> "",
                    //             'Type'=> "年報",
                    //         ]
                    //     ];
                }else{
                    //     // 月報
                    $type = '月報';
                    //     $res = [
                    //         [
                    //             'Banner_ID'=> "1",
                    //             'Banner_Name'=> "檔案下載", //本市查獲毒品概況分析
                    //             'Chart_ID'=> "1",
                    //             'Chart_Name'=> "主畫面",
                    //             'Function_ID'=> "chart3",
                    //             'Function_Name'=> "makeWCHomeChart",
                    //             'Source_Name'=> "",
                    //             'Type'=> "月報",
                    //         ]
                    //     ];
                }
                $p =[[
                    'selectYear'=> $requested['lecture_year'],
                    'selectMonth'=> $requested['lecture_month'],
                    'Type' => $type
                ]];

                return response()->json([ 'status'=>true,'response'=>$p]);
                // return view('print.DrugCase_print',$p);
                break;
            case 'home': // 首頁下載
                $p = [
                    'selectYear'=> $requested['home_year'],
                    'selectMonth'=> $requested['home_month'],
                    'selectStartYear' => $requested['home_year'],
                    'selectEndYear' => $requested['home_year'],
                    'action' => 'home',
                ];
                $res = [
                    [
                        'Banner_ID'=> "1",
                        'Banner_Name'=> "檔案下載", //本市查獲毒品概況分析
                        'Chart_ID'=> "1",
                        'Chart_Name'=> "主畫面",
                        'Function_ID'=> "chart3",
                        'Function_Name'=> "makeWCHomeChart",
                        'Source_Name'=> "",
                        'Type'=> "主畫面",
                    ]
                ];
                return response()->json([ 'status'=>true,'response'=>$res]);
                // return view('print.index_print',$p);
                break;
            case 'radar': // 文字雲 & 雷達圖
                // dd('文字雷達',$requested);
                switch ($requested['radar_sub_type']){
                    case 'all': // 全國
                        $p = [
                            'selectYear' => $requested['home_year'],
                            'selectMonth' => $requested['home_month'],
                            'selectStartYear' => $requested['radar_start_year'],
                            'selectEndYear' => $requested['radar_end_year'],
                            'selectStartMonth' => $requested['radar_start_month'],
                            'selectEndMonth' => $requested['radar_end_month'],
                            'action' => 'radar',
                        ];
                        // return view('print.index_print',$p);
                        $res = [
                            [
                                'Banner_ID'=> "1",
                                'Banner_Name'=> "檔案下載", //本市查獲毒品概況分析
                                'Chart_ID'=> "1",
                                'Chart_Name'=> "文字雲與雷達圖-全市",
                                'Function_ID'=> "chart3",
                                'Function_Name'=> "makeWCCityChart",
                                'Source_Name'=> "",
                                'Type'=> "文字雲與雷達圖-全市",
                            ]
                        ];
                        return response()->json([ 'status'=>true,'response'=>$res]);
                        break;
                    case 'area': // 區域
                        $areaNo = $request['area_id'];
                        $areaName = DB::table('KAOHSIUNG_Map')->where('ID', $areaNo)->select(DB::raw('[City_Name] as Location'))->first()->Location;

                        $SQLComm = "SELECT [City_Code]
                                        ,[District_Code]
                                        ,[City_Name]
                                        ,[District_Name]
                                    FROM [KAOHSIUNG_LOCALCULTURAL]
                                    where [City_Name]=:City_Name";

                        $Villages = DB::select($SQLComm, ['City_Name' => $areaName]);
                        /* 可能會用到的 時間參數*/
                        $p = [
                            'selectYear' => $requested['home_year'],
                            'selectMonth' => $requested['home_month'],
                            'selectStartYear' => $requested['radar_start_year'],
                            'selectEndYear' => $requested['radar_end_year'],
                            'selectStartMonth' => $requested['radar_start_month'],
                            'selectEndMonth' => $requested['radar_end_month'],
                            'Villages' => $Villages,
                            'areaNo' => $areaNo,
                            'areaName' => $areaName
                        ];
                        $res = [
                            [
                                'Banner_ID'=> "1",
                                'Banner_Name'=> "檔案下載", //本市查獲毒品概況分析
                                'Chart_ID'=> "1",
                                'Chart_Name'=> "文字雲與雷達圖-區域",
                                'Function_ID'=> "chart3",
                                'Function_Name'=> "makeWCAreaChart",
                                'Source_Name'=> "",
                                'Type'=> "文字雲與雷達圖-區域",
                            ]
                        ];
                        return response()->json([ 'status'=>true,'response'=>$res]);

                        // return view('print.radarArea_print',$p);
                        break;
                    case 'people': // 個案管理
                        $caseNo = $request['case_people'];
                        /* 可能會用到的 時間參數*/
                        $p = [
                            'selectYear' => $requested['home_year'],
                            'selectMonth' => $requested['home_month'],
                            'selectStartYear' => $requested['radar_start_year'],
                            'selectEndYear' => $requested['radar_end_year'],
                            'selectStartMonth' => $requested['radar_start_month'],
                            'selectEndMonth' => $requested['radar_end_month'],
                            'caseNo' => $caseNo,
                        ];
                        $res = [
                            [
                                'Banner_ID'=> "1",
                                'Banner_Name'=> "檔案下載", //本市查獲毒品概況分析
                                'Chart_ID'=> "1",
                                'Chart_Name'=> "文字雲與雷達圖-個案",
                                'Function_ID'=> "chart3",
                                'Function_Name'=> "makeWCPeopleChart",
                                'Source_Name'=> "",
                                'Type'=> "文字雲與雷達圖-個案",
                            ]
                        ];
                        return response()->json([ 'status'=>true,'response'=>$res]);
                        // return view('print.radarPeople_print',$p);
                        break;
                    default:
                        abort(404);
                        break;
                }
                break;
        }
        return response()->json(['status'=>true]);
    }

}
