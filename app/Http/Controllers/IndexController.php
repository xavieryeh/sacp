<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function locationPopulation(Request $request)
    {
        // $startDate = ;
        $SQLComm = "SELECT DATEPART(YY,s.[Date])-1911 [Year]
        , DATEPART(MM,s.[Date]) [Month]
        ,s.[Location] [category]
        ,ROUND(cast(B.[Num] as float) / cast(s.Num as float)*100000,0) [count]
        ,ROW_NUMBER() over(partition by
        (case when s.[Location]='臺北市' or s.[Location]='新北市' or s.[Location]='桃園市' or s.[Location]='臺中市' or s.[Location]='臺南市' or s.[Location]='高雄市' then 1 else 0 end) order by ROUND(cast(B.[Num] as float) / cast(s.Num as float)*100000,0) desc) [Municipality]
        ,ROW_NUMBER() over(order by ROUND(cast(B.[Num] as float) / cast(s.Num as float)*100000,0) desc)[All]
        FROM [LOCATIONE_POPULATION_Num] s
        left join [dbo].[LOCATIONE_TUBE_Num] B on (s.Date=B.Date and s.Location=B.Location)
         WHERE DATEPART(YY,s.[Date]) = DATEPART(YY,CAST(:startDate+'/01/01'  AS DATE))
        ORDER BY [count] DESC";

        return DB::select($SQLComm, ['startDate'=>$request->startDate]);
    }

    public function wordCloud(Request $request)
    {
        $SQLComm = "SELECT Top 10
                    [文字] [category]
                    ,round(sqrt(sum([詞頻]))*10,0) [count]
                  FROM [CASE_WORD_TAG]
                  group by [文字]
                  order by [count] desc";

        $SQLComm_sub1 = "SELECT 
                        [ITEM] [category]
                        ,SUM([VAL]) [count]
                       FROM [CASE_RADAR]
                       where [TYPE]='危險因子'
                       group by [ITEM]";

        $SQLComm_sub2 = "SELECT 
                        [ITEM] [category]
                         ,SUM([VAL]) [count]
                        FROM [CASE_RADAR]
                        where [TYPE]='保護因子'
                        group by [ITEM]";

        // $posts =
        return array('wordCloud' => DB::select($SQLComm, [1]),'wordCloudSub1' =>  DB::select($SQLComm_sub1, [1]), 'wordCloudSub2' =>  DB::select($SQLComm_sub2, [1]));
    }

    public function ageGroupAnalyze(Request $request)
    {
        $SQLComm = "SELECT [年度] [Year]
                            ,[年齡] [category]
                            ,[數量] [count]
                        FROM [首頁測試_左2]";

        // $posts = DB::select($SQLComm, [1]);

        // $posts = DB::table('首頁測試_左2')
        // ->select(DB::raw('[年度] as Year,[年齡] as category,[數量] as count'))
        // ->get();
        // $posts =
        return DB::select($SQLComm);
    }

    public function caseSourceAnalyze(Request $request)
    {
        $startDate = $request->startDate;
        $SQLComm = sprintf("SELECT [Manage_Type] [category]
                            , Count(*) [count]
                    FROM [DRUGS_LIST]
                    WHERE DATEPART(YY,[Counseling_Day]) = DATEPART(YY,CAST('%s'+'/01'  AS DATE))
                            AND DATEPART(MM,[Counseling_Day]) = DATEPART(MM,CAST('%s'+'/01'  AS DATE))
                            AND [Closing_Date_CHINA] =''
                    GROUP BY [Manage_Type]",$startDate,$startDate);

        // $posts =
        return DB::select($SQLComm);
    }

    public function tubeSpecificAnalyze(Request $request)
    {
        $SQLComm = "SELECT [營業場所] [category]
                            ,[數量] [count]
                            ,[比例] [rate]
                        FROM [首頁測試_左3]";

        // $posts =
        return DB::select($SQLComm);
    }

    public function levelSeizedPeopleAnalyze(Request $request)
    {
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $SQLComm = sprintf("SET NOCOUNT ON SELECT *
        INTO #tmp_SEIZED
        FROM [SEIZED]
        WHERE [Date] >=CAST('%s'+'/01'  AS DATE)
              AND [Date] <= CAST('%s'+'/01'  AS DATE)

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , DATEPART(MM,[Date])[category]
                , '一級毒品' [Level]
                , ISNULL([Level_1_Num],0) [count]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , DATEPART(MM,[Date]) [category]
                , '二級毒品' [Level]
                , ISNULL([Level_2_Num],0) [count]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , DATEPART(MM,[Date]) [category]
                , '三級毒品' [Level]
                , ISNULL([Level_3_Num],0) [count]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , DATEPART(MM,[Date]) [category]
                , '四級毒品' [Level]
                , ISNULL([Level_4_Num],0) [count]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , DATEPART(MM,[Date]) [category]
                , '其他' [Level]
                , ISNULL([Other_Num],0) [count]
        FROM #tmp_SEIZED

        UNION ALL

        SELECT DATEPART(YYYY,[Date])-1911 [Year]
                , DATEPART(MM,[Date]) [category]
                , '合計' [Level]
                , ISNULL([Level_1_Num],0)+ISNULL([Level_2_Num],0)+ISNULL([Level_3_Num],0)+ISNULL([Level_4_Num],0)+ISNULL([Other_Num],0) [count]
        FROM #tmp_SEIZED

        DROP TABLE #tmp_SEIZED", $startDate, $endDate);

        // $posts =
        return DB::select($SQLComm);
    }

    public function mapDistribution(Request $request)
    {
        $SQLComm = "SET NOCOUNT ON SELECT *
                    INTO #tmp_MAP
                    FROM (SELECT [City_Name]
                        ,[ID]
                        ,isnull([人數],0) [人數]
                    FROM [KAOHSIUNG_Map] A
                    left join ( 
                    select [Area_Name_Live],count([Area_Name_Live]) [人數] from (
                    SELECT [Person_ID]
                        ,[Area_Name_Live] 
                    FROM [DRUGS_LIST] 
                    where [Closing_Date_CHINA]='' and [Area_Name_Live] <>''
                    group by [Person_ID],[Area_Name_Live]
                    )T group by [Area_Name_Live]
                    ) B on (A.[City_Name]=B.Area_Name_Live)
                    )#tmp_MAP
                    ---------------------------------------------------------------------
                    SELECT * 
                    INTO #tmp_Month_num
                    from (
                    select [Area_Name_Live],count([Area_Name_Live]) [本月新增人數] from [DRUGS_LIST] where DATEPART(YYYY,[Counseling_Day])=DATEPART(YYYY,GETDATE()) and DATEPART(MM,[Counseling_Day])=DATEPART(MM,GETDATE()) group by [Area_Name_Live]
                    )#tmp_Month_num
                    
                    -----------------------------------------------------------------------
                    
                    SELECT A.[City_Name] [Location]
                            ,A.[人數] [count]
                            ,case when isnull(B.本月新增人數,0)>0 then 'V' else '' end [new]
                            ,case when (100*[人數]/(select MAX([人數]) from #tmp_MAP))=0 then 'KSareaQuantity-0'
                                    when (100*[人數]/(select MAX([人數]) from #tmp_MAP))>0 and (100*[人數]/(select MAX([人數]) from #tmp_MAP))<20 then 'KSareaQuantity-1'
                                    when (100*[人數]/(select MAX([人數]) from #tmp_MAP))>19 and (100*[人數]/(select MAX([人數]) from #tmp_MAP))<40 then 'KSareaQuantity-2'
                                    when (100*[人數]/(select MAX([人數]) from #tmp_MAP))>39 and (100*[人數]/(select MAX([人數]) from #tmp_MAP))<60 then 'KSareaQuantity-3'
                                    when (100*[人數]/(select MAX([人數]) from #tmp_MAP))>59 and (100*[人數]/(select MAX([人數]) from #tmp_MAP))<80 then 'KSareaQuantity-4'
                                    when (100*[人數]/(select MAX([人數]) from #tmp_MAP))>69 and (100*[人數]/(select MAX([人數]) from #tmp_MAP))<101 then 'KSareaQuantity-5'
                                    end [Class]
                            ,A.[ID]
                            ,isnull(B.本月新增人數,0) [new_Count]
                    from #tmp_MAP A
                    left join #tmp_Month_num B on (A.[City_Name]=B.Area_Name_Live)
                    order by A.[人數] desc
                    
                    DROP TABLE #tmp_MAP
                    DROP TABLE #tmp_Month_num";

        $posts =DB::select($SQLComm);
        return $posts;
    }

    public function mainChartsMaker(Request $request)
    {
        $SQLComm = "SELECT [Banner_ID]
                        ,[Banner_Name]
                        ,[Chart_ID]
                        ,[Chart_Name] [Title]
                        ,[Type]
                        ,[Function_Name]
                        ,[Function_ID]
                        ,[Source_Name]
                        ,T1.[Main]
                        ,T2.[ChartName]
                        ,T2.[Num]
                    FROM [Banner_Chart_Config] T1
                    left join [MainChartList] T2 on(T1.[Main]=T2.[Position])
                    where T2.[ChartName] is not null";

        $posts = DB::select($SQLComm);
        return $posts;
    }
}
