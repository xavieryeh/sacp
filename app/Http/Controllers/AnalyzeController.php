<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnalyzeController extends Controller
{
    // 國內外情勢分析 view
    public function index()
    {
        // $SQLComm_month = "SELECT [Message],A.UpLoad_Date
        //             FROM [Abroad_Analyze] A
        //             left join [UpLoad_Log] B ON (A.UpLoad_Date = B.UpLoad_Date)
        //             Where DATEPART(YY,A.[UpLoad_Date]) = DATEPART(YY,CAST ('2021/12'+'/01' AS DATE))
        //             AND DATEPART(MM,A.[UpLoad_Date]) = DATEPART(MM,CAST ('2021/12'+'/01' AS DATE))
        //             AND B.[File_type]='月度資料'
        //             ORDER BY A.[UpLoad_Date] DESC";
        $SQLComm_month = "SELECT [Message],A.UpLoad_Date
                    FROM [Abroad_Analyze] A
                    left join [UpLoad_Log] B ON (A.UpLoad_Date = B.UpLoad_Date)
                    AND B.[File_type]='月度資料'
                    AND B.[File_System] = '國內外情勢分析'
                    ORDER BY B.[File_Date] DESC";

        $last_month = DB::select($SQLComm_month);

        // $SQLComm_year = "SELECT [Message],A.UpLoad_Date
        //             FROM [Abroad_Analyze] A
        //             left join [UpLoad_Log] B ON (A.UpLoad_Date = B.UpLoad_Date)
        //             Where DATEPART(YY,A.[UpLoad_Date]) = DATEPART(YY,CAST ('2021/12'+'/01' AS DATE))
        //             AND B.[File_type]='年度資料'
        //             ORDER BY A.[UpLoad_Date] DESC";
        $SQLComm_year = "SELECT [Message],A.UpLoad_Date
                    FROM [Abroad_Analyze] A
                    left join [UpLoad_Log] B ON (A.UpLoad_Date = B.UpLoad_Date)
                    WHERE B.[File_type]='年度資料'
                    AND B.[File_System] = '國內外情勢分析'
                    ORDER BY B.[File_Date] DESC";
        // where [City_Name]=:City_Name";
        $last_year = DB::select($SQLComm_year);

        //  國內外情勢分析 主頁
        $p =[
            'last_year' => $last_year[0]->UpLoad_Date??'年報 標題', // 年報 標題
            'year_comment' =>  $last_year[0]->Message??'年報內容', // 年報內容
            'last_month' => $last_month[0]->UpLoad_Date??'月報 標題', // 月報 標題
            'month_comment' =>  $last_month[0]->Message??'月報內容', // 月報內容
        ];
        return view('analyze.index',$p);
    }

    public function page($type,Request $request)
    {
        // 國內外情勢分析 內頁/ 月報 OR 年報
        if(!in_array($type,['month','year'])){
            abort(404);
        }
        switch (($request->method()??false)){
            case 'GET':
                $now = Carbon::now(); // 當前年度
                $max_year = $now->year - 1911; // 取得資料內最大年度
                $min_year = $max_year - 10; // 取得系統內最小年度
                $p = [
                    'type' => $type, // year or month
                    'title' => $type=='year'?'年報':'月報',
                    'max_year' => $max_year,
                    'min_year' => $min_year,
                    'now_year' => $now->year -1911, // 進入頁面預選年度
                    'now_month' => $now->month , // 進入頁面預選月份
                    'data_url' => route('Analyze.page',['type'=>$type])
                ];
                return view('analyze.page',$p);
                break;
            case 'POST':
                // 取的資料
                $requested = $request->validate([
                    'type'=>'required|in:year,month',
                    'year'=>'required',
                    'month'=>'required_if:type,month'
                ]);

                $type = $requested['type'];
                $year = $requested['year'];
                $month = $requested['month']??false;

                try{
                    // 只會回傳一筆資料
                    switch ($type){
                        case 'year':
                            $SQLComm = "SELECT [Message],A.UpLoad_Date
                                FROM [Abroad_Analyze] A
                                left join [UpLoad_Log] B ON (A.UpLoad_Date = B.UpLoad_Date)
                                Where B.[File_Date] = :File_Date
                                AND B.[File_type]='年度資料'
                                AND B.[File_System] = '國內外情勢分析'
                                ORDER BY A.[UpLoad_Date] DESC";
                            $File_Date = "$year/$year";
                            $data = DB::select($SQLComm,['File_Date'=>$File_Date]);
                            break;
                        case 'month':
                            $SQLComm = "SELECT [Message],A.UpLoad_Date
                                FROM [Abroad_Analyze] A
                                left join [UpLoad_Log] B ON (A.UpLoad_Date = B.UpLoad_Date)
                                Where B.[File_Date] =:File_Date
                                AND B.[File_type]='月度資料'
                                AND B.[File_System] = '國內外情勢分析'
                                ORDER BY A.[UpLoad_Date] DESC";
                            $File_Date = "$year/$month";
                            $data = DB::select($SQLComm,['File_Date'=>$File_Date]);
                            break;
                        default:
                            // dd('100 default',$type);
                            break;
                    }
                    // dd($year+1911,(int)$month,$SQLComm);

                    return response()->json([
                        'status'=>true,
                        'response'=>[
                            // 'content'=>"測試內容 {$type} / {$year} /{$month}"
                            'content'=>$data[0]->Message??'沒有資料'
                        ]
                    ]);
                }catch (\Exception $exception){
                    // dd(789,$exception->getMessage());
                    return response()->json([
                        'status'=>false,
                        'response'=>[
                            'content'=>$exception->getMessage()
                        ]
                    ]);
                }
                break;
            default:
                abort(404);
                break;
        }
    }
}
