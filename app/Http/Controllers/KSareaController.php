<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KSareaController extends Controller
{
    // Load 地圖第二階畫面-各里列表
    public function getKSarea(Request $request)
    {
        $areaNo = $request->areaNo;
        $areaName = DB::table('KAOHSIUNG_Map')->where('ID', $areaNo)->select(DB::raw('[City_Name] as Location'))->first()->Location;

        $SQLComm = "SELECT [City_Code]
                        ,[District_Code]
                        ,[City_Name]
                        ,[District_Name]
                    FROM [KAOHSIUNG_LOCALCULTURAL]
                    where [City_Name]=:City_Name";

        $Villages = DB::select($SQLComm, ['City_Name' => $areaName]);
        return view('KSarea', compact('areaNo', 'Villages', 'areaName'));
    }

    // 地圖第二階-雷達圖、文字雲
    public function areaProtectionFactor(Request $request)
    {
        $areaName = $request->areaName;
        $SQLCommChart1 = sprintf("SELECT B.[ITEM] [category],SUM(B.[VAL]) [count]
                            FROM [DRUGS_LIST] A
                            left join [CASE_RADAR] B on (A.[Case_ID]=B.[個案案號])
                            where A.[Area_Name]='%s' and B.[TYPE]='危險因子' and A.[Closing_Date_CHINA]=''
                            group by B.[ITEM]", $areaName);

        $SQLCommChart2 = sprintf("SELECT B.[ITEM] [category],SUM(B.[VAL]) [count]
                            FROM [DRUGS_LIST] A
                            left join [CASE_RADAR] B on (A.[Case_ID]=B.[個案案號])
                            where A.[Area_Name]='%s' and B.[TYPE]='保護因子' and A.[Closing_Date_CHINA]=''
                            group by B.[ITEM]", $areaName);

        $SQLCommChart3 = sprintf("SELECT Top 50 B.[文字] [category],SUM(B.[詞頻]) [count]
                            FROM [DRUGS_LIST] A
                            left join [CASE_WORD_TAG] B on (A.[Case_ID]=B.[個案案號])
                            where A.[Area_Name]='%s'  and A.[Closing_Date_CHINA]=''
                            group by B.[文字]
                            order by SUM(B.[詞頻]) desc", $areaName);

        $posts = array('chart1' => DB::select($SQLCommChart1, [1]), 'chart2' => DB::select($SQLCommChart2, [1]), 'chart3' => DB::select($SQLCommChart3, [1]));
        return $posts;
    }

    // public function areaPagetest(Request $request)
    // {
    //     return $this->getKSareaPage($request);
    // }

    // Load 地圖第三階畫面-個案管理資料
    public function getKSareaPage(Request $request)
    {
        $areaNo = $request->areaNo;
        $Params = explode("_", $areaNo);
        $areaName = DB::table('KAOHSIUNG_Map')->where('ID', $Params[0])->select(DB::raw('[City_Name] as Location'))->first()->Location;
        $villageName = DB::table('KAOHSIUNG_LOCALCULTURAL')->where('City_Name', $areaName)->where('District_Code', $Params[1])->first()->District_Name;

        // $showCount = $request->showCount ?? "5";
        // $pageSelect = $request->pageSelect ?? "1";
        $SQLComm = sprintf("SELECT ROW_NUMBER() over(order by [Case_ID]) [NUM]
                        ,[Case_ID]
                        ,[來源編號] as 'SourceNo'
                        ,[Gender]
                        ,[File_Age]
                        ,[毒品級數] as 'Level'
                        ,(CASE [狀態] WHEN '列管' THEN 'Y' ELSE 'N' END ) as 'States'
                        ,(CASE [初案/再案] WHEN '初案' THEN 1 WHEN '再案' THEN 2 ELSE '' END) as 'CaseType'
                        ,[Manage_Type]
                    FROM [DRUGS_LIST]
                    where [Area_Name]='%s'
                    and [Village_Name]='%s'
                    and [Closing_Date_CHINA]=''
                    order by [Case_ID]", $areaName, $villageName);
        $Cases = DB::select($SQLComm);
        // $Total = DB::table("DRUGS_LIST")->where("Area_Name_Live",$areaName)->where("Village_Name",$villageName)->count();
        return view('KSareaPage', compact('areaNo', 'Cases', 'villageName', 'areaName'));
    }

    //地圖第三階畫面-該里個案年齡分佈
    public function caseAgeDistributed(Request $request)
    {
        $Year = date("Y") - 1911;
        $Month = date("n");
        $villageName = $request->villageName;
        $SQLComm = sprintf("SET NOCOUNT ON
                            SELECT [File_Age] [Age]
                                    , CASE WHEN [File_Age] <20 THEN '未滿20歲'
                                        WHEN [File_Age] <=29 THEN '20-29歲'
                                        WHEN [File_Age] <=39 THEN '30-39歲'
                                        WHEN [File_Age] <=49 THEN '40-49歲'
                                        WHEN [File_Age] <=59 THEN '50-59歲'
                                        WHEN [File_Age] <=69 THEN '60-69歲'
                                        WHEN [File_Age] >=69 THEN '70歲以上'
                                END [Age_Class]
                                    , Count(*) [Person]
                            INTO #tmp_DRUGS
                            FROM [DRUGS_LIST]
                            WHERE  [Closing_Date_CHINA]=''
                                AND [Village_Name]='%s'
                            GROUP BY [File_Age]
                            select '%s年1-%s月' [Year],[Age_Class] [category],[Person] [count] from(
                            SELECT [Age_Class],SUM([Person]) [Person] FROM #tmp_DRUGS GROUP BY [Age_Class]
                            union all
                            SELECT '合計' [Age_Class],SUM([Person]) [Person] FROM #tmp_DRUGS)T
                            order by replace([Age_Class],'未','0')

                            DROP TABLE #tmp_DRUGS", $villageName, $Year, $Month);

        $posts = DB::select($SQLComm);
        return $posts;
    }

    //地圖第三階畫面-各級毒品人數統計
    public function drugStatistics(Request $request)
    {
        $Year = date("Y");
        $Month = date("m");
        $villageName = $request->villageName;
        $SQLComm = sprintf("SET NOCOUNT ON
                            select cast([月份] as varchar)+'月' [Year]
                                ,case when [毒品級數]='一' or [毒品級數]='二' or [毒品級數]='三' or [毒品級數]='四' then [毒品級數]+'級毒品' else [毒品級數] end [category]
                                ,count([月份]) [count]
                                from (
                            SELECT DATEPART(MM,[Counseling_Day]) [月份]
                                ,[毒品級數]
                            FROM [DRUGS_LIST]
                            where [Village_Name]='%s'
                            and  [Closing_Date_CHINA]=''
                            )T group by [月份],[毒品級數]
                            order by [毒品級數] desc", $villageName, $Year, $Year, $Month);

        $posts = DB::select($SQLComm);
        return $posts;
    }

    //地圖第三階畫面-年齡與類型交叉分析
    public function ageTypeCrossAnalysis(Request $request)
    {
        $Year = date("Y");
        $Month = date("m");
        $villageName = $request->villageName;
        $SQLComm = sprintf("SET NOCOUNT ON
                            SELECT *
                            INTO #tmp_SEIZED
                            FROM [SEIZED]
                            WHERE [Date] >=CAST('%s/01'+'/01'  AS DATE)
                                AND [Date] <= CAST('%s/%s'+'/01'  AS DATE)

                            SELECT DATEPART(YYYY,[Date])-1911 [Year_1]
                                    , cast(DATEPART(MM,[Date]) as varchar)+'月' [category]
                                    , '一級毒品' [Year]
                                    , ISNULL([Level_1_Num],0) [count]
                            FROM #tmp_SEIZED

                            UNION ALL

                            SELECT DATEPART(YYYY,[Date])-1911 [Year_1]
                                    , cast(DATEPART(MM,[Date]) as varchar)+'月' [category]
                                    , '二級毒品' [Year]
                                    , ISNULL([Level_2_Num],0) [count]
                            FROM #tmp_SEIZED

                            UNION ALL

                            SELECT DATEPART(YYYY,[Date])-1911 [Year_1]
                                    , cast(DATEPART(MM,[Date]) as varchar)+'月' [category]
                                    , '三級毒品' [Year]
                                    , ISNULL([Level_3_Num],0) [count]
                            FROM #tmp_SEIZED

                            UNION ALL

                            SELECT DATEPART(YYYY,[Date])-1911 [Year_1]
                                    , cast(DATEPART(MM,[Date]) as varchar)+'月' [category]
                                    , '四級毒品' [Year]
                                    , ISNULL([Level_4_Num],0) [count]
                            FROM #tmp_SEIZED

                            UNION ALL

                            SELECT DATEPART(YYYY,[Date])-1911 [Year_1]
                                    , cast(DATEPART(MM,[Date]) as varchar)+'月' [category]
                                    , '其他' [Year]
                                    , ISNULL([Other_Num],0)  [count]
                            FROM #tmp_SEIZED

                            UNION ALL

                            SELECT DATEPART(YYYY,[Date])-1911 [Year_1]
                                    , cast(DATEPART(MM,[Date]) as varchar)+'月' [category]
                                    , '合計' [Year]
                                    , ISNULL([Level_1_Num],0)+ISNULL([Level_2_Num],0)+ISNULL([Level_3_Num],0)+ISNULL([Level_4_Num],0)+ISNULL([Other_Num],0)  [count]
                            FROM #tmp_SEIZED

                            DROP TABLE #tmp_SEIZED", $Year, $Year, $Month);

        $posts = DB::select($SQLComm);
        return $posts;
    }

    //地圖第三階畫面-性別統計分析
    public function genderStatisticalAnalysis(Request $request)
    {
        $startDate = $request->startDate;
        $Year = date("Y");
        $Month = date("m");
        $Month_n = date("n");
        $villageName = $request->villageName;
        $SQLComm = sprintf("SET NOCOUNT ON
                            SELECT CAST(DATEPART(YY,[Counseling_Day])-1911 AS VARCHAR) [YEAR]
                                    ,[Gender]
                            INTO #tmp_FIRST_OFFENDER
                                FROM [DRUGS_LIST]
                                where [Village_Name]='%s'
                                and [Closing_Date_CHINA]=''
                                and (([Counseling_Day]>'%s/01/01' and [Counseling_Day]<'%s/%s/01')
                                or ([Counseling_Day]>'%s/01/01'  and [Counseling_Day]<'%s/%s/01'))
                            
                            SELECT cast([YEAR] as varchar)+'年1-%s月' [category]
                                ,[Gender]
                                ,count([Gender]) [count]
                            FROM #tmp_FIRST_OFFENDER
                            group by  [YEAR],[Gender]

                            UNION ALL

                            SELECT cast([YEAR] as varchar)+'年1-%s月' [category]
                                , '合計' [Gender]
                                , count([Gender]) [count]
                            FROM #tmp_FIRST_OFFENDER
                            GROUP BY [YEAR]
                            DROP TABLE #tmp_FIRST_OFFENDER", $villageName, $startDate, $startDate, $Month,$Year,$Year, $Month,$Month_n,$Month_n);

        $posts = DB::select($SQLComm);
        return $posts;
    }

    //地圖第三階畫面-收案來源分析
    public function sourceAcceptanceAnalysis(Request $request)
    {
        $villageName = $request->villageName;
        $SQLComm = sprintf("SET NOCOUNT ON
                            SELECT [Manage_Type] [category]
                                    , Count(*) [count]
                            FROM [DRUGS_LIST]
                            WHERE [Village_Name]='%s'
                            and [Closing_Date_CHINA]=''
                            GROUP BY [Manage_Type]", $villageName);

        $posts = DB::select($SQLComm);
        return $posts;
    }

    //地圖第三階畫面-初案再案統計
    public function caseStatistics(Request $request)
    {
        $villageName = $request->villageName;
        $SQLComm = sprintf("SET NOCOUNT ON
                            SELECT case when COUNT([Person_ID])='1' then '初犯'
                                        else '再案' end [初案/再案]
                            INTO #tmp_FIRST_OFFENDER
                            FROM [DRUGS_LIST]
                                where  [Closing_Date_CHINA]=''
                                    AND [Village_Name]='%s'
                                    group by [Person_ID]

                            SELECT
                                [初案/再案] [category]
                                ,COUNT([初案/再案]) [count]
                                ,cast(ROUND(cast(COUNT([初案/再案]) as float)/cast((select count([初案/再案]) from #tmp_FIRST_OFFENDER) as float)*100 ,1) as varchar) [Rate]
                            FROM #tmp_FIRST_OFFENDER
                            group by  [初案/再案]


                            DROP TABLE #tmp_FIRST_OFFENDER", $villageName);

        $posts = DB::select($SQLComm);
        return $posts;
    }

    // Load 地圖第四階主管畫面
    public function getKSareaPageOwnTeacher(Request $request)
    {
        $areaNo = $request->areaNo;
        $Params = explode("_", $areaNo);
        $areaName = DB::table('KAOHSIUNG_Map')->where('ID', $Params[0])->select(DB::raw('[City_Name] as Location'))->first()->Location;
        $villageName = DB::table('KAOHSIUNG_LOCALCULTURAL')->where('City_Name', $areaName)->where('District_Code', $Params[1])->first()->District_Name;
        $caseNo = $Params[2];

        $SQLComm = sprintf("SELECT [來源編號] as 'SourceNo'
            ,[Case_ID]-- as '個案案號'
            ,[Person_ID]-- as '身分編號'
            ,[Person_Name]-- as '姓名'
            ,[Manager]-- as '個管師/社工'
            ,[Manage_Type]-- as '類型'
            ,[Counseling_Day_CHINA]-- as '接收輔導日'
            ,[毒品級數] [Level]-- as '毒品級數'
            ,[Gender]-- as '性別'
            ,[Birthday_CHINA]-- as '出生年月日'
            ,[Entry_Age]-- as '進案年齡'
            ,[File_Age]-- as '實際年齡'
            ,[Marriage]-- as '婚姻狀況'
            ,[Education]-- as '學歷'
            ,[County_Name]+[Area_Name]+[Village_Name]+[Residence_Address] [Residence_Address]-- as '戶籍地址'
            ,[County_Name_Live]+[Area_Name_Live]+[Live_Address] [Live_Address]-- as '居住地'
            ,[Employment_Status]-- as '就業'
            ,'' [Learning]-- as '在學'
            ,'' [WorkType]-- as '工作類型'
            ,[Closing_Date_CHINA]-- as '結案日期'
            ,[Closing_Reason]-- as '結案原因'
            ,'' [Family_Drug_Addict]-- as '家戶有吸毒者'
            ,'' [Multiple]-- as '多次施用'
        FROM [DRUGS_LIST] where [Case_ID]='%s'
        order by [Case_ID]", $caseNo);
        $Cases = DB::select($SQLComm)[0];

        $SQLCommVisit = sprintf("SELECT [追蹤輔導日期] [Track_Counseling_Date]
                    ,[個案管理師] [Case_Manager]
                    ,[訪查方式] [Interview]
                    ,[受訪者] [Respondents]
                    ,[受訪者_關係] [Interviewee_Relationship]
                    ,[社會救助需求] [Social_Assistance]
                    ,[就業扶助需求] [Employment_Assistance]
                    ,[戒治服務需求] [Abstinence_Service]
                    ,[就學服務需求] [School_Service]
                    ,[追輔建檔日期] [Follow_File_Creation_Date]
                    ,[輔導內容] [Counseling_Content]
                    ,[處遇計畫] [Treatment_Plan]
                    ,[案號] [CaseNo]
                FROM [訪視記錄測試]
                where [案號]='%s'
                order by [追蹤輔導日期] desc", $caseNo);
        $Visits = DB::select($SQLCommVisit);
        return view('KSareaPageOwnTeacher', compact('areaNo', 'caseNo', 'Cases', 'villageName', 'areaName', 'Visits'));
    }

    // 地圖第四階主管-雷達圖、文字雲
    public function areaPageOwnTeacherProtectionFactor(Request $request)
    {
        $areaName = $request->areaName;
        $caseNo = $request->caseNo;
        $SQLCommChart1 = sprintf("SELECT B.[ITEM] [category],COUNT(B.[ITEM]) [count]
                            FROM [DRUGS_LIST] A
                            left join [CASE_RADAR] B on (A.[Case_ID]=B.[個案案號])
                            where A.[Area_Name]='%s' and B.[TYPE]='危險因子' and B.個案案號 = '%s'
                            group by B.[ITEM]", $areaName, $caseNo);

        $SQLCommChart2 = sprintf("SELECT B.[ITEM] [category],COUNT(B.[ITEM]) [count]
                            FROM [DRUGS_LIST] A
                            left join [CASE_RADAR] B on (A.[Case_ID]=B.[個案案號])
                            where A.[Area_Name]='%s' and B.[TYPE]='保護因子' and B.個案案號 = '%s'
                            group by B.[ITEM]", $areaName, $caseNo);

        $SQLCommChart3 = sprintf("SELECT Top 50 B.[文字] [category],SUM(B.[詞頻]) [count]
                            FROM [DRUGS_LIST] A
                            left join [CASE_WORD_TAG] B on (A.[Case_ID]=B.[個案案號])
                            where A.[Area_Name]='%s' and B.個案案號 = '%s'
                            group by B.[文字]
                            order by SUM(B.[詞頻]) desc", $areaName, $caseNo);

        $posts = array('chart1' => DB::select($SQLCommChart1, [1]), 'chart2' => DB::select($SQLCommChart2, [1]), 'chart3' => DB::select($SQLCommChart3, [1]));
        return $posts;
    }

    // 地圖第四階主管-家系圖
    public function getGenogram(Request $request)
    {
        $caseNo = $request->caseNo;
        $SQLCommBind = sprintf("SET NOCOUNT ON SELECT [ID]
                            ,[Message]
                            ,[Color]
                            ,[Position]
                        FROM [家族圖測試_Config]");

        $SQLCommGeno = sprintf("SET NOCOUNT ON select case when [是否在世]='否' then 0-[Order] else [Order] end [Key],* from(
                            SELECT ROW_NUMBER() over(partition by [是否在世] order by T2.[Row]) [Order]
                                ,[親等] [Type]
                                ,[稱謂] [Title]
                                ,[姓名] [Name]
                                ,case when [性別]='男' then 'M' else 'F' end [Gender]
                                ,[出生年月日] [Birth]
                                ,[身份證字號] [ID]
                                ,[職業] [Profession]
                                ,[是否涉毒] [DrugRelated]
                                ,[涉毒紀錄] [DrugRecoder]
                                ,case when [是否同住]='是' then 1 else 0 end [Cohabit]
                                ,[備註] [Remark]
                                ,[案號] [Case]
                                ,case when [是否親屬]='是' then 0 else 1 end [Relatives]
                                ,[是否在世]
                                ,isnull(T4.[ID],'')+isnull(T5.[ID],'') +isnull(T6.[ID],'')+isnull(T7.[ID],'')+isnull(T8.[ID],'') [Drug]
                            FROM [家族圖測試] T1
                            left join [家族圖測試_Relation] T2 on(T1.[稱謂]=T2.[Title])
                            left join [DRUGS_LIST] T3 on(T1.[案號]=T3.[Case_ID] and T1.[稱謂]='個案')
                            left join [家族圖測試_Config] T4 on(T4.[Message]=T3.[初案/再案])
                            left join [家族圖測試_Config] T5 on(T5.[Message]=(case when T1.[是否同住]='是' then '同住' else '非同住' end))
                            left join [家族圖測試_Config] T6 on(T6.[Color]=T1.[家庭問題])
                            left join [家族圖測試_Config] T7 on(T7.[Color]=T1.[家庭結構])
                            left join [家族圖測試_Config] T8 on(T8.[Message]=(case when T1.[是否在世]='是' then '在世' else '無在世' end))
                            where [案號]='%s')M", $caseNo);

        $SQLCommRelation = sprintf("SET NOCOUNT ON SELECT [Title]
                            ,case when [Title]='兒子' or [Title]='女兒' then (select [稱謂] from [家族圖測試] where [案號]='%s' and [性別]='男' and ([稱謂]='個案' or [稱謂]='配偶')) else [f] end [f]
                            ,case when [Title]='兒子' or [Title]='女兒' then (select [稱謂] from [家族圖測試] where [案號]='%s' and [性別]='女' and ([稱謂]='個案' or [稱謂]='配偶')) else [m] end [m]
                            ,[ux]
                            ,[vir]
                            ,[Level]
                        FROM [家族圖測試_Relation]", $caseNo, $caseNo);

        $posts = array('bindingData' => DB::select($SQLCommBind, [1]), 'Data' => DB::select($SQLCommGeno, [1]), 'Relation' => DB::select($SQLCommRelation, [1]));
        return $posts;
    }
}
