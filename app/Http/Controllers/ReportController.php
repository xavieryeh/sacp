<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpWord\PhpWord;


class ReportController extends Controller
{
    // Load 產出報表頁面
    public function getReport(Request $request)
    {
        $yearlist = array();
        for($i=date("Y")-10;$i<=date("Y")+10;$i++){
            array_push($yearlist,$i-1911);
        }
        $monthlist = array();
        for($i=1;$i<=12;$i++){
            array_push($monthlist,str_pad($i,2,"0",STR_PAD_LEFT));
        }
        $SQLComm = sprintf("SELECT [Banner_ID]
            ,[Banner_Name]
            ,[Chart_ID]
            ,[Chart_Name]
            ,[Type]
            ,[Function_Name]
            ,[Function_ID]
            ,[Source_Name]
        FROM [Banner_Chart_Config]
        where [Function_Name] is not null");
        $ReportCharts = DB::select($SQLComm);
        return view('Report', compact('ReportCharts','yearlist','monthlist'));
    }

    // use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function exportWord(Request $request)
    {
        $Reports = json_decode($request->chartInfo);

        // $source ="Data/Temp.docx";
        // $phpWord = \PhpOffice\PhpWord\IOFactory::load($source, 'Word2007');
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        $bannerName = "";
        foreach($Reports as $Report) { // Loop through rows
            if($bannerName!=$Report->banner){
                $bannerName=$Report->banner;
                $section->addText($Report->banner, array('bold' => true, "size"=>18));
            }
            $section->addText("  ".$Report->title, array("size"=>18));
            if($request->files->get($Report->downID)!=null){
                $section->addImage($request->files->get($Report->downID), array(
                    'width' => 500,
                    'height' => 285,
                ));
            }
            // $section->addText($Report->subText);
            // $section->addText($Report->source, null, array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::RIGHT));
        }

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

        $filename = "Report";
        $filename = iconv("UTF-8", "Big5", $filename);
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $objWriter->save($temp_file);
        header("Content-Disposition: attachment; filename={$filename}.doc'");
        // header("Content-Disposition: attachment; filename={$filename}.odt'");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file); // remove temp file

        // $objWriter->save('Month Report.odt');
        // return response()->download(public_path('Month Report.odt'));
    }
}
