<?php

namespace App\Http\Controllers;

use App\Models\AppLog;
use App\Models\FamilyTree;
use App\Models\FamilyTreeRelation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CaseController extends Controller
{
    // 個案管理第一階-雷達圖、文字雲
    public function caseProtectionFactor(Request $request)
    {
        // $areaName = $request->areaName;
        // $Manager = $request->Manager;
        $Manager = Auth::user()->name;
        // 風險因子、保護因子
        $SQLCommChart_radar = sprintf("select C.main,C.因子名稱 [category], isnull(詞頻, 0) [count]
                from (select main, 因子名稱, sqrt(sum(num))*10 詞頻
                      from CASE_RADAR2 R
                      join CASE_WORD_CATEGORY C on R.word_id = C.id
                      join (
                          select Case_ID,Manager from DRUGS_LIST where [Closing_Date_CHINA] ='' --未結案
                          union 
                          select Case_ID,Manager from [3_4_Lecture] where [Coaching_Close] ='' --未結案
                          union 
                          select Case_ID,Manager from [DRUGS_LIST_TEENAGER] where [Coaching_Close] <>'2' --未結案
                          ) D on D.Case_ID=R.個案案號
                      where D.Manager='%s'
                      group by main, 因子名稱
                     ) A
                         right join (
                    select distinct main, 因子名稱, sort, null num
                    from CASE_WORD_CATEGORY
                    where sort is not null
                ) C on A.main = C.main and A.因子名稱 = C.因子名稱
                order by C.sort", $Manager); // right join C: 沒有的因子要補0
        $radar_data = collect(DB::select($SQLCommChart_radar));
        $radar_max = $radar_data->max('count');
        $radar_data_group = $radar_data->groupBy('main');
        $radar_risk = $radar_data_group['風險'];
        $radar_protection = $radar_data_group['保護'];
        // dd($radar_max,$radar_risk);

        //文字雲
        $SQLCommChart3 = sprintf("SELECT 
                            Top 50 B.[文字] [category],round(sqrt(SUM(B.[詞頻]))*10,0) [count]
                            FROM [DRUGS_LIST] A
                            left join [CASE_WORD_TAG] B on (A.[Case_ID]=B.[個案案號])
                            where A.[Manager]='%s'
                            and [Closing_Date_CHINA] ='' --未結案
                            group by B.[文字]
                            order by SUM(B.[詞頻]) desc", $Manager);

        $posts = array(
            'radar_max' => (int)$radar_max,
            'chart1' => $radar_risk,
            'chart2' => $radar_protection,
            'chart3' => DB::select($SQLCommChart3, [1]));
        return $posts;
    }

    // Load 個案管理-個案管理資料
    public function getCase(Request $request)
    {
        AppLog::Log('個案管理', '檢視個案清單');
        // $Manager = $request->Manager;
        $Manager = Auth::user()->name;
        $SQLComm = sprintf("select ROW_NUMBER() over(order by [Case_ID]) [NUM],*
        from (
        SELECT 
                                    [Case_ID]
                                    ,[來源編號] as 'SourceNo'
                                    ,[Gender]
                                    ,[File_Age]
                                    ,[毒品級數] as 'Level'
                                    ,(CASE [狀態] WHEN '列管' THEN 'Y' ELSE 'N' END ) as 'States'
                                    ,(CASE [初案/再案] WHEN '初案' THEN 1 WHEN '再案' THEN 2 ELSE '' END) as 'CaseType'
                                    ,[Manage_Type]
                                FROM [DRUGS_LIST]
                                where [Manager]='%s'
                                and [Closing_Date_CHINA] ='' --未結案
        union all 
        SELECT 
                                     [Case_ID]
                                    ,[來源編號] as 'SourceNo'
                                    ,[Gender]
                                    ,[File_Age]
                                    ,[毒品級數] as 'Level'
                                    ,(CASE [狀態] WHEN '列管' THEN 'Y' ELSE 'N' END ) as 'States'
                                    ,(CASE [初案/再案] WHEN '初案' THEN 1 WHEN '再案' THEN 2 ELSE '' END) as 'CaseType'
                                    ,[Manage_Type]
                                FROM [3_4_Lecture]
                                where [Manager]='%s'
                                and [Coaching_Close] ='' --未結案
        union all 
        SELECT
                                     [Case_ID]
                                    ,[來源編號] as 'SourceNo'
                                    ,[Gender]
                                    ,[File_Age]
                                    ,[毒品級數] as 'Level'
                                    ,(CASE [狀態] WHEN '列管' THEN 'Y' ELSE 'N' END ) as 'States'
                                    ,(CASE [初案/再案] WHEN '初案' THEN 1 WHEN '再案' THEN 2 ELSE '' END) as 'CaseType'
                                    ,[Manage_Type]
                                FROM DRUGS_LIST_TEENAGER
                                where [Manager]='%s'
                                and [Coaching_Close] <>'1' --未結案
                                )T 
                        order by [Case_ID]", $Manager, $Manager, $Manager);
        $Cases = DB::select($SQLComm);
        return view('Case', compact('Cases','Manager'));
    }

    // Load 地圖第二階主管畫面
    public function getCasePage(Request $request)
    {
        $caseNo = $request->caseNo;

        $SQLComm = sprintf("select TOP 1 * from (
            SELECT [來源編號] as 'SourceNo'
                           ,[Case_ID]-- as '個案案號'
                           ,[Person_ID]-- as '身分編號'
                           ,[Person_Name]-- as '姓名'
                           ,[Manager]-- as '個管師/社工'
                           ,[Manage_Type]-- as '類型'
                           ,[Counseling_Day_CHINA]-- as '接收輔導日'
                           ,[毒品級數] [Level]-- as '毒品級數'
                           ,[Gender]-- as '性別'
                           ,[Birthday_CHINA]-- as '出生年月日'
                           ,[Entry_Age]-- as '進案年齡'
                           ,[File_Age]-- as '實際年齡'
                           ,[Marriage]-- as '婚姻狀況'
                           ,[Education]-- as '學歷'
                           ,[County_Name]+[Area_Name]+[Village_Name]+[Residence_Address] [Residence_Address]-- as '戶籍地址'
                           ,[County_Name_Live]+[Area_Name_Live]+[Live_Address] [Live_Address]-- as '居住地'
                           ,[Employment_Status]-- as '就業'
                           ,'' [Learning]-- as '在學'
                           ,'' [WorkType]-- as '工作類型'
                           ,[Closing_Date_CHINA]-- as '結案日期'
                           ,[Closing_Reason]-- as '結案原因'
                           ,'' [Family_Drug_Addict]-- as '家戶有吸毒者'
                           ,'' [Multiple]-- as '多次施用'
                       FROM [DRUGS_LIST] where [Case_ID]='%s'
                       union all
            　　　　　　　　SELECT [來源編號] as 'SourceNo'
                           ,[Case_ID]-- as '個案案號'
                           ,[Person_ID]-- as '身分編號'
                           ,[Person_Name]-- as '姓名'
                           ,[Manager]-- as '個管師/社工'
                           ,[Manage_Type]-- as '類型'
                           ,[Counseling_Day_CHINA]-- as '接收輔導日'
                           ,[毒品級數] [Level]-- as '毒品級數'
                           ,[Gender]-- as '性別'
                           ,[Birthday_CHINA]-- as '出生年月日'
                           ,[Entry_Age]-- as '進案年齡'
                           ,[File_Age]-- as '實際年齡'
                           ,[Marriage]-- as '婚姻狀況'
                           ,[Education]-- as '學歷'
                           ,[Area_Name] [Residence_Address]-- as '戶籍地址'
                           ,[Live_Address] [Live_Address]-- as '居住地'
                           ,[Employment_Status]-- as '就業'
                           ,'' [Learning]-- as '在學'
                           ,'' [WorkType]-- as '工作類型'
                           ,[Coaching_Close]-- as '結案日期'
                           ,[Closing_Reason]-- as '結案原因'
                           ,'' [Family_Drug_Addict]-- as '家戶有吸毒者'
                           ,'' [Multiple]-- as '多次施用'
                       FROM [3_4_Lecture] where [Case_ID]='%s'
                       union all
            　　　　　　　　SELECT [來源編號] as 'SourceNo'
                           ,[Case_ID]-- as '個案案號'
                           ,[Person_ID]-- as '身分編號'
                           ,[Person_Name]-- as '姓名'
                           ,[Manager]-- as '個管師/社工'
                           ,[Manage_Type]-- as '類型'
                           ,[Counseling_Day_CHINA]-- as '接收輔導日'
                           ,[毒品級數] [Level]-- as '毒品級數'
                           ,[Gender]-- as '性別'
                           ,[Birthday_CHINA]-- as '出生年月日'
                           ,[Entry_Age]-- as '進案年齡'
                           ,[File_Age]-- as '實際年齡'
                           ,[Marriage]-- as '婚姻狀況'
                           ,[Education]-- as '學歷'
                           ,[Area_Name] [Residence_Address]-- as '戶籍地址'
                           ,[Live_Address] [Live_Address]-- as '居住地'
                           ,[Employment_Status]-- as '就業'
                           ,'' [Learning]-- as '在學'
                           ,'' [WorkType]-- as '工作類型'
                           ,[Coaching_Close]-- as '結案日期'
                           ,[Closing_Reason]-- as '結案原因'
                           ,'' [Family_Drug_Addict]-- as '家戶有吸毒者'
                           ,'' [Multiple]-- as '多次施用'
                       FROM [DRUGS_LIST_TEENAGER] where [Case_ID]='%s'
                       )T", $caseNo, $caseNo, $caseNo);
        $Cases = DB::select($SQLComm)[0];

        $SQLCommVisit = sprintf("SELECT [追蹤輔導日期] [Track_Counseling_Date]
                       ,[個案管理師] [Case_Manager]
                       ,[訪查方式] [Interview]
                       ,[受訪者] [Respondents]
                       ,[受訪者_關係] [Interviewee_Relationship]
                       ,[社會救助需求] [Social_Assistance]
                       ,[就業扶助需求] [Employment_Assistance]
                       ,[戒治服務需求] [Abstinence_Service]
                       ,[就學服務需求] [School_Service]
                       ,[追輔建檔日期] [Follow_File_Creation_Date]
                       ,[輔導內容] [Counseling_Content]
                       ,[處遇計畫] [Treatment_Plan]
                       ,[案號] [CaseNo]
                   FROM [訪視記錄測試]
                   where [案號]='%s'
                   order by [追蹤輔導日期] desc", $caseNo);
        $Visits = DB::select($SQLCommVisit);
        foreach($Visits as &$v) {
            $v->Counseling_Content = trim(str_replace('<:br>', '', $v->Counseling_Content));
        }
        return view('CasePage', compact('caseNo', 'Cases', 'Visits'));
    }

    // 地圖第二階主管-雷達圖、文字雲
    public function casePageOwnTeacherProtectionFactor(Request $request)
    {
        $caseNo = $request->caseNo;
        // 危險因子
        // $SQLCommChart1 = sprintf("SELECT B.[ITEM] [category],COUNT(B.[ITEM]) [count]
        //                        FROM [DRUGS_LIST] A
        //                        left join [CASE_RADAR] B on (A.[Case_ID]=B.[個案案號])
        //                        where B.[TYPE]='危險因子' and B.[個案案號] = '%s'
        //                        group by B.[ITEM]", $caseNo);
        // 保護因子
        // $SQLCommChart2 = sprintf("SELECT B.[ITEM] [category],COUNT(B.[ITEM]) [count]
        //                        FROM [DRUGS_LIST] A
        //                        left join [CASE_RADAR] B on (A.[Case_ID]=B.[個案案號])
        //                        where B.[TYPE]='保護因子' and B.[個案案號] = '%s'
        //                        group by B.[ITEM]", $caseNo);
        $SQLCommChart_radar = sprintf("select C.main,C.因子名稱 [category], isnull(詞頻, 0) [count]
                from (select main, 因子名稱, sqrt(sum(num))*10 詞頻
                      from CASE_RADAR2 R
                      join CASE_WORD_CATEGORY C on R.word_id = C.id
                      where R.個案案號='%s'
                      group by main, 因子名稱
                     ) A
                         right join (
                    select distinct main, 因子名稱, sort, null num
                    from CASE_WORD_CATEGORY
                    where sort is not null
                ) C on A.main = C.main and A.因子名稱 = C.因子名稱
                order by C.sort", $caseNo); // right join C: 沒有的因子要補0
        $radar_data = collect(DB::select($SQLCommChart_radar));
        $radar_max = $radar_data->max('count');
        $radar_data_group = $radar_data->groupBy('main');
        $radar_risk = $radar_data_group['風險'];
        $radar_protection = $radar_data_group['保護'];
        // dd($radar_max,$radar_risk);
        // 文字雲
        $SQLCommChart3 = sprintf("SELECT Top 30 B.[文字] [category],SUM(B.[詞頻]) [count]
                               FROM  [CASE_WORD_TAG] B
                               where B.個案案號 = '%s'
                               group by B.[文字]
                               order by SUM(B.[詞頻]) desc", $caseNo);

        $posts = array(
            'radar_max' => (int)$radar_max,
            'chart1' => $radar_risk,
            'chart2' => $radar_protection,
            'chart3' => DB::select($SQLCommChart3, [1]));
        return $posts;
    }

    // 地圖第二階主管-家系圖
    public function getGenogram(Request $request)
    {
        $caseNo = $request->caseNo;

        $relation = FamilyTreeRelation::all()->keyBy('Title');
        $data = FamilyTree::query()->where('案號', $caseNo)
            ->get()
            ->sortBy(function(FamilyTree $item) {
                return $item->稱謂==='同居人' ? 0:1;
            });
        $data_key = $data->keyBy('稱謂');
        $result = $data->map(function (FamilyTree $item)use($relation,$data_key) {
            $a = []; // 屬性 (定義在Genogram2.js內)
            if($item->初犯再犯==='初犯') {
                $a[] = 'E';
            }
            if($item->初犯再犯==='再犯') {
                $a[] = 'C';
            }
            if($item->是否同住==='是') {
                $a[] = 'A';
            } else {
                $a[] = 'B';
            }
            if($item->是否在世==='否') {
                $a[] = 'S';
            }
            $d = [
                'key' => $item->ID,
                'n' => $item->姓名,
                's' => $item->性別==='男'?'M':'F',
                'm' => $data_key->get($relation->get($item->稱謂)->m)->ID ?? null, // 母
                'f' => $data_key->get($relation->get($item->稱謂)->f)->ID ?? null, // 父
                'ux' => $data_key->get($relation->get($item->稱謂)->ux)->ID ?? null, // 妻
                'vir' => $data_key->get($relation->get($item->稱謂)->vir)->ID ?? null, // 夫
                'a' => $a,
                // 'tr' => Str::contains($item->家庭問題,',') ? 'P': $item->家庭問題, // P: 多重問題
                // 'bl' => Str::contains($item->家庭結構,',') ? '8': $item->家庭結構, // P: 多重結構
                'tr' => $item->getProblemTag(),
                'bl' => $item->getStructureTag(),
                'r' => $item->稱謂==='同居人' ? '同住':'', // js 定義 '同住' 要虛線
            ];
            return array_filter($d); // 過濾掉 null 項目
        })->values();

        return [
            'data' => $result,
            'focus_id' => $data_key->get('個案')->ID ?? 0,
        ];

    }

    // Load 地圖第三階個案管理家系圖編輯畫面
    public function getCasePageFamily(Request $request)
    {
        $caseNo = $request->input('caseNo');

        $families = FamilyTree::query()->where('案號', $caseNo)->get();

        $RelativeList = DB::table('家族圖測試_Relation')->select('Relative')->distinct()->get();
        $TitleList = DB::table('家族圖測試_Relation')->select('Title')->distinct()->get();
        $ProblemList = DB::table('家族圖測試_Config')->where('Position', 'TR')->select('Color','Message')->get();
        $StructureList = DB::table('家族圖測試_Config')->where('Position', 'BL')->select('Color','Message')->get();
        return view('CasePageFamily', compact('caseNo',
            'families','RelativeList','TitleList','ProblemList','StructureList'));
    }

    // Load 地圖第三階個案管理家系圖編輯畫面-連動稱謂
    public function casePageFamilyGetTitle(Request $request)
    {
        $relative = $request->relative;
        if($relative=="") {
            $TitleList = DB::table('家族圖測試_Relation')->select('Title')->distinct()->get();
        }
        else{
            $TitleList = DB::table('家族圖測試_Relation')->where('Relative',$relative)->select('Title')->distinct()->get();
        }
        return $TitleList;
    }

    // Load 地圖第三階個案管理家系圖編輯畫面-新增/更新資料
    public function casePageFamilyAddDate(Request $request): string
    {
        // $ip = $request->ip();
        $item = FamilyTree::query()
            ->where('ID', $request->input('db_id'))
            ->firstOrNew();

        $item->fill([
            '親等' => $request->relative,
            '稱謂' => $request->title,
            '姓名' => $request->name,
            '性別' => $request->gender,
            '出生年月日' => $request->birth,
            '身份證字號' => $request->id,
            '職業' => $request->profession,
            '涉毒紀錄' => $request->drugRecoder,
            '是否同住' => $request->cohabit,
            '備註' => $request->remark,
            '案號' => $request->caseNo,
            '是否親屬' => $request->relatives,
            '是否在世' => $request->alive,
            '家庭問題' => implode(',', $request->input('problem',[])),
            '家庭結構' => implode(',', $request->input('structure', [])),
            '初犯再犯' => $request->input('初犯再犯'),
        ])->save();

        return '';
    }

    // Load 地圖第三階個案管理家系圖編輯畫面-刪除資料
    public function casePageFamilyDelDate(Request $request)
    {
        $item = FamilyTree::find($request->input('id'));
        if(!$item) {
            abort(404);
        }
        $item->delete();
        return "";
    }
}
