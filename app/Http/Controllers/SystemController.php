<?php

namespace App\Http\Controllers;

use App\Models\AppLog;
use App\Models\User;
use App\Rules\LastPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class SystemController extends Controller
{
    public function index()
    {
        return view('system.index');
    }

    public function account()
    {
        AppLog::Log('帳號管理', '帳號清單檢視');

        $users = User::all();
        $d = [
            'users' => $users,
        ];
        return view('system.account', $d);
    }

    public function account_add()
    {
        $d = [
            'user' => new User,
        ];
        return view('system.account_edit', $d);
    }

    public function account_edit(User $user)
    {
        // TODO 檢查管理者權限才可編輯
        $d = [
            'user' => $user,
        ];
        return view('system.account_edit', $d);
    }

    public function account_save(Request $request, User $user)
    {
        // TODO 檢查管理者權限才可編輯
        $rules = [];

        $new = !$user->exists; // 新增
        if($new) {
            $user = new User;
            $user->email = $request->input('email');
            // dd($user);
            $rules = [
                'email' => 'required|email|max:100',
            ];
        }
        $request->validate($rules + [
            'name' => 'required|max:20',
            'department' => 'required|max:20',
            'password' => [
                Rule::requiredIf(!$user->exists),
                'nullable',
                'confirmed',
                Password::default(),
                new LastPassword(),
            ],
            'roles' => 'required|array|min:1',
        ]);

        $user->fill($request->all());
        if($request->filled('password')) {
            $user->password = Hash::make($request->input('password'));
        }
        $user->roles = $request->input('roles');
        $is_enabled = $request->filled('is_enabled');
        if(!$new && $user->is_enabled!=$is_enabled) {
            AppLog::Log('帳號管理', ($is_enabled?'啟用':'停用').':'.$user->email);
        } else {
            AppLog::Log('帳號管理', ($new?'新增':'編輯').':'.$user->email);
        }
        $user->is_enabled = $request->filled('is_enabled');
        // dd($user);
        $user->save();

        return redirect()->route('system.account');
    }

    public function account_del(User $user)
    {
        // TODO 管理者才可刪除
        $user->delete();
        AppLog::Log('帳號管理', '刪除:'.$user->email);
        return redirect()->route('system.account');
    }

    public function log(Request $request)
    {
        $logs = AppLog::query()
            ->orderByDesc('created_at')
            ->with('user')
            ->get();

        $d = [
            'logs' => $logs,
        ];

        return view('system.log', $d);
    }
}
