<?php
function asset_v($path): string
{
    $timestamp = '';
    if(file_exists($path)) {
        $timestamp = '?' . filemtime($path);
    }
    return asset($path) . $timestamp;
}
