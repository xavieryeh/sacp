<?php

namespace App\Providers;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Validation\Rules\Password;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Blade::withoutDoubleEncoding();
        if(env('REDIRECT_HTTPS'))
        {
	        $this->app['request']->server->set('HTTPS', true);
            //\URL::forceScheme('https');
        }
	    Schema::defaultStringLength(191);

        Password::defaults(function() {
            return Password::min(10)
                ->symbols()
                ->numbers()
                ->mixedCase()
                ->letters();
        });
    }
}
