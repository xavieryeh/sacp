<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * @method static dispatch(string $個案案號, string $個案訪視紀錄文字) 計算個案文字雲與雷達圖因子
 */
class ProcessCaseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $case_id;
    private $content;

    /**
     * @param string $case_id 個案案號
     * @param string $content 個案訪視紀錄文字
     * @return void
     */
    public function __construct(string $case_id, string $content)
    {
        $this->case_id = $case_id;
        $this->content = $content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo "計算個案雷達圖&文字雲... $this->case_id, $this->content \n";
    }
}
