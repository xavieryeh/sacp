<?php

namespace App\Models;

use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class FamilyTree extends Model
{
    use HasFactory;
    protected $table='家族圖測試';
    protected $primaryKey='ID';
    // protected $dates=['出生年月日'];
    protected $fillable = [
        '親等','稱謂','姓名','性別','出生年月日','身份證字號','職業','涉毒紀錄','是否同住','備註',
        '案號','是否親屬','是否在世','家庭問題','家庭結構','初犯再犯',
    ];

    public function get出生年月日Attribute($value)
    {
        if(trim($value)=='') {
            return null;
        }
        // dd($value);
        $date = Carbon::parse($value);
        // return sprintf('%03d-%02d-%02d', $date->year-1911, $date->month, $date->day);
        return sprintf('%03d%02d%02d', $date->year-1911, $date->month, $date->day);
    }

    public function set出生年月日Attribute($value)
    {
        $this->attributes['出生年月日'] = null;

        //YYYMMDD
        if(preg_match('@^([01]\d{2})([01]\d)([0-3]\d)$@', $value, $m)) {
            // 輸入民國年格式正確才儲存
            $date = sprintf('%4d-%02d-%02d', $m[1]+1911, $m[2], $m[3]);
            try {
                Carbon::parse($date);
                $this->attributes['出生年月日'] = $date; // 解析成功才儲存
            } catch (InvalidFormatException $e) {

            }
        }
    }

    public function getProblemTag()
    {
        if(Str::contains($this->家庭問題, ',')) {
            return 'P'; // P: 多重問題
        }
        return $this->_getTag($this->家庭問題, 'TR');
    }

    public function getStructureTag()
    {
        if(Str::contains($this->家庭結構, ',')) {
            return '8'; // 8: 多重結構
        }
        return $this->_getTag($this->家庭結構, 'BL');
    }

    private function _getTag($txt, $type)
    {
        if(trim($txt)=='') {
            return null;
        }
        /** @var $configs \Illuminate\Support\Collection<FamilyTreeConfig> */
        $configs = Cache::remember("FamilyTree_config_$type", 60, function()use($type) {
            return FamilyTreeConfig::wherePosition($type)->get()->keyBy('Message');
        });
        // dd($this->家庭問題,$config->get($this->家庭問題)->Color);
        $config = $configs->get($txt);
        if(!$config) {
            return null;
        }
        return $config->Color; // 取得代號 A,B,C,... ,1,2,3,...
    }

    protected function performUpdate(Builder $query)
    {
        $result = parent::performUpdate($query);
        if($result) {
            AppLog::Log('家系圖',
                sprintf('[編輯] 案號:%s,稱謂:%s,姓名:%s',
                    $this->案號, $this->稱謂, $this->姓名)
            );
        }
        return $result;
    }

    protected function performInsert(Builder $query)
    {
        $result = parent::performInsert($query);
        if($result) {
            AppLog::Log('家系圖',
                sprintf('[編輯] 案號:%s,稱謂:%s,姓名:%s',
                    $this->案號, $this->稱謂, $this->姓名)
            );
        }
        return $result;
    }

    protected function performDeleteOnModel()
    {
        parent::performDeleteOnModel();
        AppLog::Log('家系圖',
            sprintf('[編輯] 案號:%s,稱謂:%s,姓名:%s',
                $this->案號, $this->稱謂, $this->姓名)
        );
    }

}
