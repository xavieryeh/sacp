<?php

namespace App\Models\Kaohsiung;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KaohsiungMap extends Model
{
    use HasFactory;
    protected $table = 'KAOHSIUNG_Map';
    public $incrementing = 'ID';
}
