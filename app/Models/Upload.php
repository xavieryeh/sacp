<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    public static function getTypeInfo(string $Type): array
    {
        $Title = "";
        $Page = "";
        $SQLComm = "";
        switch ($Type) {
            case "LNI":
                $Title = "地方網絡資料";
                $Page = "fileUploadPage1";
                break;
            case "CCMS":
                $Title = "中央資料";
                $Page = "fileUploadPage1";
                break;
            case "CCMS1":
                $Title = "中央案管系統";
                $Page = "fileUploadPage2";
                break;
            case "CPCMS":
                $Title = "兒少保案管系統";
                $Page = "fileUploadPage2";
                break;
            case "LTAFL":
                $Title = "三四級講習";
                $Page = "fileUploadPage2";
                break;
            case "VR":
                $SQLComm = sprintf("select ROW_NUMBER() over(order by [Case_ID]) [NUM],*
                                    from (
                                    SELECT 
                                        [Case_ID]
                                        ,[來源編號] as 'SourceNo'
                                        ,[Gender]
                                        ,[File_Age]
                                        ,[毒品級數] as 'Level'
                                        ,(CASE [狀態] WHEN '列管' THEN 'Y' ELSE 'N' END ) as 'States'
                                        ,(CASE [初案/再案] WHEN '初案' THEN 1 WHEN '再案' THEN 2 ELSE '' END) as 'CaseType'
                                        ,[Manage_Type]
                                    FROM [DRUGS_LIST] A
                                    WHERE EXISTS (SELECT * FROM [訪視記錄測試] WHERE Case_ID=案號)
                                    union all 
                                    SELECT 
                                        [Case_ID]
                                        ,[來源編號] as 'SourceNo'
                                        ,[Gender]
                                        ,[File_Age]
                                        ,[毒品級數] as 'Level'
                                        ,(CASE [狀態] WHEN '列管' THEN 'Y' ELSE 'N' END ) as 'States'
                                        ,(CASE [初案/再案] WHEN '初案' THEN 1 WHEN '再案' THEN 2 ELSE '' END) as 'CaseType'
                                        ,[Manage_Type]
                                    FROM [3_4_Lecture] A 
                                        WHERE EXISTS (SELECT * FROM [訪視記錄測試] WHERE Case_ID=案號)          
                                    union all 
                                    SELECT
                                            [Case_ID]
                                            ,[來源編號] as 'SourceNo'
                                            ,[Gender]
                                            ,[File_Age]
                                            ,[毒品級數] as 'Level'
                                            ,(CASE [狀態] WHEN '列管' THEN 'Y' ELSE 'N' END ) as 'States'
                                            ,(CASE [初案/再案] WHEN '初案' THEN 1 WHEN '再案' THEN 2 ELSE '' END) as 'CaseType'
                                            ,[Manage_Type]
                                        FROM DRUGS_LIST_TEENAGER A 
                                        WHERE EXISTS (SELECT * FROM [訪視記錄測試] WHERE Case_ID=案號)
                                        )T
                                        WHERE EXISTS (SELECT * FROM [UpLoad_Log] WHERE [Case_ID]=T.[Case_ID])");
                $Title = "訪視記錄";
                $Page = "fileUploadPage3";
                break;
            case "CPCMS2":
                $Title = "春暉中斷";
                $Page = "fileUploadPage2";
                break;
            case "CPCMS3":
                $Title = "監所銜接輔導";
                $Page = "fileUploadPage2";
                break;
            case "CPCMS4":
                $Title = "特定營業場所";
                $Page = "fileUploadPage2";
                break;
        }
        if($Type!='VR'){
            $SQLComm = sprintf("SELECT [File_System]
                                        ,[File_type]
                                        ,[File_Date]
                                        ,convert(varchar,[UpLoad_Date],120) [UpLoad_Date]
                                        ,[UserName]
                                        ,[File_Source]
                                    FROM [UpLoad_Log]
                                    where [File_System]='%s'",$Title);
        }
        return array('Title' => $Title, 'Page' => $Page, 'SQLComm' => $SQLComm);
    }
}
