<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class AppLog extends Model
{
    protected $table='logs';
    const UPDATED_AT = null;

    use HasFactory;

    /**
     * 紀錄使用者操作軌跡
     * @param string $func_name 功能名稱, 例如: 個案管理、檔案上傳等
     * @param string $action 做動名稱, 須包含操作對象識別碼, 例如: 新增個案案號xxxx, 新增使用者xxx, 檢視xxx圖表
     * @return void
     */
    public static function Log(string $func_name, string $action): void
    {
        $log = new self();
        $log->func_name = $func_name;
        $log->action = $action;
        $log->user_id = Auth::id();
        $log->ip = Request::ip();
        $log->save();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
