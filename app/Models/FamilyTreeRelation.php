<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyTreeRelation extends Model
{
    use HasFactory;
    protected $table='家族圖測試_Relation';
}
