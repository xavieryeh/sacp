<?php

use App\Http\Controllers\BannerController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\SystemController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/getcsrf', 'Controller@getcsrf');

Route::middleware('auth')->group(function () {
    Route::get('/', function(){
        return view('index');
    })->name('index');
    Route::get('/index', function(){
        return view('index');
    });
    Route::get('/upload', function(){
        return view('upload');
    })->name('upload');
    // Route::get('/fileAnalyze', function(){
    //     return view('fileAnalyze');
    // });
    Route::get('/fileAnalyzeUpload', function(){
        return view('fileAnalyzeUpload');
    });
    Route::get('/fileUpload', function(){
        return view('fileUpload');
    });
    // Route::get('/fileDownload', function(){
    //     return view('fileDownload');
    // });
    Route::get('/File_Download', 'FileDownloadController@index')->name('fileDownload');

    Route::get('/report', function(){
        return view('Report');
    });
    Route::get('/banner', function(){
        return view('banner');
    });
    Route::get('bannerPage', [
        'uses' => 'Controller@showbanndePage',
        'as'   => 'bannerPage'
    ]);
    Route::get('bannerPageforYear', [
        'uses' => 'Controller@showbanndePageforYear',
        'as'   => 'bannerPageforYear'
    ]);
    Route::get('/phpinfo',[Controller::class,'phpinfo']);
    Route::get('/db', [Controller::class,'connect']);
    Route::post('Log','Controller@Log')->name('Log');

    // IndexController
    Route::post('Index_locationPopulation','IndexController@locationPopulation')->name('Index.locationPopulation');
    Route::post('Index_wordCloud','IndexController@wordCloud')->name('Index.wordCloud');
    Route::post('Index_ageGroupAnalyze','IndexController@ageGroupAnalyze')->name('Index.ageGroupAnalyze');
    Route::post('Index_caseSourceAnalyze','IndexController@caseSourceAnalyze')->name('Index.caseSourceAnalyze');
    Route::post('Index_tubeSpecificAnalyze','IndexController@tubeSpecificAnalyze')->name('Index.tubeSpecificAnalyze');
    Route::post('Index_levelSeizedPeopleAnalyze','IndexController@levelSeizedPeopleAnalyze')->name('Index.levelSeizedPeopleAnalyze');
    Route::post('Index_mapDistribution','IndexController@mapDistribution')->name('Index.mapDistribution');
    Route::post('Index_mainChartsMaker','IndexController@mainChartsMaker')->name('Index.mainChartsMaker');

    // KSareaController
    Route::post('KSarea_getGenogram','KSareaController@getGenogram')->name('KSarea.getGenogram');
    Route::get('KSarea_getKSarea','KSareaController@getKSarea')->name('KSarea.getKSarea');
    Route::get('KSarea_getKSareaPage','KSareaController@getKSareaPage')->name('KSarea.getKSareaPage');
    Route::get('KSarea_getKSareaPageOwnTeacher','KSareaController@getKSareaPageOwnTeacher')->name('KSarea.getKSareaPageOwnTeacher');
    Route::post('KSarea_areaProtectionFactor','KSareaController@areaProtectionFactor')->name('KSarea.areaProtectionFactor');
    Route::post('KSarea_areaPageOwnTeacherProtectionFactor','KSareaController@areaPageOwnTeacherProtectionFactor')->name('KSarea.areaPageOwnTeacherProtectionFactor');
    Route::post('KSarea_caseAgeDistributed','KSareaController@caseAgeDistributed')->name('KSarea.caseAgeDistributed');
    Route::post('KSarea_drugStatistics','KSareaController@drugStatistics')->name('KSarea.drugStatistics');
    Route::post('KSarea_ageTypeCrossAnalysis','KSareaController@ageTypeCrossAnalysis')->name('KSarea.ageTypeCrossAnalysis');
    Route::post('KSarea_genderStatisticalAnalysis','KSareaController@genderStatisticalAnalysis')->name('KSarea.genderStatisticalAnalysis');
    Route::post('KSarea_sourceAcceptanceAnalysis','KSareaController@sourceAcceptanceAnalysis')->name('KSarea.sourceAcceptanceAnalysis');
    Route::post('KSarea_caseStatistics','KSareaController@caseStatistics')->name('KSarea.caseStatistics');

    // CaseController
    Route::get('Case_getCase','CaseController@getCase')->name('Case.getCase');
    Route::get('Case_getCasePage','CaseController@getCasePage')->name('Case.getCasePage');
    Route::get('Case_getCasePageFamily','CaseController@getCasePageFamily')->name('Case.getCasePageFamily');
    Route::post('Case_caseProtectionFactor','CaseController@caseProtectionFactor')->name('Case.caseManagerData');
    Route::post('Case_casePageOwnTeacherProtectionFactor','CaseController@casePageOwnTeacherProtectionFactor')->name('Case.casePageOwnTeacherProtectionFactor');
    Route::post('Case_getGenogram','CaseController@getGenogram')->name('Case.getGenogram');
    Route::post('Case_casePageFamilyGetTitle','CaseController@casePageFamilyGetTitle')->name('Case.casePageFamilyGetTitle');
    Route::post('Case_casePageFamilyAddDate','CaseController@casePageFamilyAddDate')->name('Case.casePageFamilyAddDate');
    Route::post('Case_casePageFamilyDelDate','CaseController@casePageFamilyDelDate')->name('Case.casePageFamilyDelDate');

    //ReportController
    Route::get('Report_getReport','ReportController@getReport')->name('Report.getReport');
    Route::post('Report_exportWord','ReportController@exportWord')->name('Report.exportWord');
    // BannerController
    /* ---------------這是第一類---------------*/
    Route::post('SelectCrimialMonth', [BannerController::class, 'SelectCrimialMonth']);
    Route::post('SelectCrimialYear', [BannerController::class, 'SelectCrimialYear']);
    Route::post('SelectAdministrativeMonth', [BannerController::class, 'SelectAdministrativeMonth']);
    Route::post('SelectAdministrativeYear', [BannerController::class, 'SelectAdministrativeYear']);
    Route::post('SelectPersonMonth', [BannerController::class, 'SelectPersonMonth']);
    Route::post('SelectPersonYear', [BannerController::class, 'SelectPersonYear']);
    Route::post('SelectWeightMonth', [BannerController::class, 'SelectWeightMonth']);
    Route::post('SelectNumMonth', [BannerController::class, 'SelectNumMonth']);
    Route::post('SelectCaseMonth', [BannerController::class, 'SelectCaseMonth']);
    Route::post('SelectWeightContrastMonth', [BannerController::class, 'SelectWeightContrastMonth']);
    Route::post('SelectPeosonContrastMonth', [BannerController::class, 'SelectPeosonContrastMonth']);
    Route::post('SelectCaseContrastMonth', [BannerController::class, 'SelectCaseContrastMonth']);
    /*end */

    /* ---------------這是第二類---------------*/
    Route::post('SelectRecidivesmRateMonth', [BannerController::class, 'SelectRecidivesmRateMonth']);
    Route::post('SelectRecidivesmRateHalfYearMonth', [BannerController::class, 'SelectRecidivesmRateHalfYearMonth']);
    Route::post('SelectRecidivesmRateOneYearMonth', [BannerController::class, 'SelectRecidivesmRateOneYearMonth']);
    Route::post('SelectRecidivesmRateTwoYearMonth', [BannerController::class, 'SelectRecidivesmRateTwoYearMonth']);
    Route::post('SelectWeightContrastYear', [BannerController::class, 'SelectWeightContrastYear']);
    Route::post('SelectPeosonContrastYear', [BannerController::class, 'SelectPeosonContrastYear']);
    Route::post('SelectCaseContrastYear', [BannerController::class, 'SelectCaseContrastYear']);
    Route::post('SelectRecidivesmRateYear', [BannerController::class, 'SelectRecidivesmRateYear']);
    Route::post('SelectRecidivesmRateHalfYearbyYear', [BannerController::class, 'SelectRecidivesmRateHalfYearYear']);
    Route::post('SelectRecidivesmRateOneYearbyYear', [BannerController::class, 'SelectRecidivesmRateOneYearbyYear']);
    Route::post('SelectRecidivesmRateTwoYearbyYear', [BannerController::class, 'SelectRecidivesmRateTwoYearbyYear']);
    /*end*/

    /* ---------------這是第三類---------------*/
    Route::post('SelectLocationeTubeMonth', [BannerController::class, 'SelectLocationeTubeMonth']);
    Route::post('SelectCorrectionLocationeTubeMonth', [BannerController::class, 'SelectCorrectionLocationeTubeMonth']);
    Route::post('SelectLocationeTubebyYear', [BannerController::class, 'SelectLocationeTubebyYear']);
    Route::post('SelectCorrectionLocationeTubebyYear', [BannerController::class, 'SelectCorrectionLocationeTubebyYear']);
    /*end*/

    /* ---------------這是第四類---------------*/
    Route::post('DrugTypebyMonth', [BannerController::class, 'DrugTypebyMonth']);
    Route::post('DrugTypebyYear', [BannerController::class, 'DrugTypebyYear']);
    /*end */

    /* ---------------這是第五類---------------*/
    Route::post('SelectCommentTextByMonth', [BannerController::class, 'SelectCommentTextByMonth']);
    Route::post('DrugGenderByMonth', [BannerController::class, 'DrugGenderByMonth']);
    Route::post('DrugGenderByYear', [BannerController::class, 'DrugGenderByYear']);
    Route::post('DrugAgeByMonth', [BannerController::class, 'DrugAgeByMonth']);
    Route::post('DrugAgeByYear', [BannerController::class, 'DrugAgeByYear']);
    Route::post('DrugAbuseTypeByMonth', [BannerController::class, 'DrugAbuseTypeByMonth']);
    Route::post('DrugAbuseTypeByYear', [BannerController::class, 'DrugAbuseTypeByYear']);
    Route::post('DrugAgeTypeByMonth', [BannerController::class, 'DrugAgeTypeByMonth']);
    Route::post('DrugAgeTypeByYear', [BannerController::class, 'DrugAgeTypeByYear']);
    Route::post('DrugAreaByMonth', [BannerController::class, 'DrugAreaByMonth']);
    Route::post('DrugAreabyYear', [BannerController::class, 'DrugAreabyYear']);
    Route::post('DrugCorrectionAreaByMonth', [BannerController::class, 'DrugCorrectionAreaByMonth']);
    Route::post('DrugCorrectionAreaByYear', [BannerController::class, 'DrugCorrectionAreaByYear']);
    Route::post('DrugCoachByMonth', [BannerController::class, 'DrugCoachByMonth']);
    Route::post('DrugCoachByYear', [BannerController::class, 'DrugCoachByYear']);
    Route::post('DrugServiceByMonth', [BannerController::class, 'DrugServiceByMonth']);
    Route::post('DrugServiceByYear', [BannerController::class, 'DrugServiceByYear']);
    Route::post('DrugCouplingByMonth', [BannerController::class, 'DrugCouplingByMonth']);
    Route::post('DrugCouplingByYear', [BannerController::class, 'DrugCouplingByYear']);
    Route::post('DrugMedicalByMonth', [BannerController::class, 'DrugMedicalByMonth']);
    Route::post('DrugMedicalByYear', [BannerController::class, 'DrugMedicalByYear']);
    Route::post('Drug24HoursByMonth', [BannerController::class, 'Drug24HoursByMonth']);
    Route::post('Drug24HoursByYear', [BannerController::class, 'Drug24HoursByYear']);
    /*end */

    /* ---------------這是第六類---------------*/
    Route::post('TeenagerDrugGenderByMonth', [BannerController::class, 'TeenagerDrugGenderByMonth']);
    Route::post('TeenagerDrugGenderByYear', [BannerController::class, 'TeenagerDrugGenderByYear']);
    Route::post('TeenagerDrugAgeByMonth', [BannerController::class, 'TeenagerDrugAgeByMonth']);
    Route::post('TeenagerDrugAgeByYear', [BannerController::class, 'TeenagerDrugAgeByYear']);
    Route::post('TeenagerDrugAbuseTypeByMonth', [BannerController::class, 'TeenagerDrugAbuseTypeByMonth']);
    Route::post('TeenagerDrugAbuseTypeByYear', [BannerController::class, 'TeenagerDrugAbuseTypeByYear']);
    Route::post('TeenagerDrugAgeTypeByMonth', [BannerController::class, 'TeenagerDrugAgeTypeByMonth']);
    Route::post('TeenagerDrugAgeTypeByYear', [BannerController::class, 'TeenagerDrugAgeTypeByYear']);
    Route::post('TeenagerDrugAreaByMonth', [BannerController::class, 'TeenagerDrugAreaByMonth']);
    Route::post('TeenagerDrugAreabyYear', [BannerController::class, 'TeenagerDrugAreabyYear']);
    Route::post('TeenagerDrugCorrectionAreaByMonth', [BannerController::class, 'TeenagerDrugCorrectionAreaByMonth']);
    Route::post('TeenagerDrugCorrectionAreaByYear', [BannerController::class, 'TeenagerDrugCorrectionAreaByYear']);
    /*end */
    /* ---------------這是第七類---------------*/
    Route::post('TeenagerChunhuiDrugGenderByMonth', [BannerController::class, 'TeenagerChunhuiDrugGenderByMonth']);
    Route::post('TeenagerChunhuiDrugGenderByYear', [BannerController::class, 'TeenagerChunhuiDrugGenderByYear']);
    Route::post('TeenagerChunhuiDrugAgeByMonth', [BannerController::class, 'TeenagerChunhuiDrugAgeByMonth']);
    Route::post('TeenagerChunhuiDrugAgeByYear', [BannerController::class, 'TeenagerChunhuiDrugAgeByYear']);
    Route::post('TeenagerChunhuiDrugAbuseTypeByMonth', [BannerController::class, 'TeenagerChunhuiDrugAbuseTypeByMonth']);
    Route::post('TeenagerChunhuiDrugAbuseTypeByYear', [BannerController::class, 'TeenagerChunhuiDrugAbuseTypeByYear']);
    Route::post('TeenagerChunhuiDrugAgeTypeByMonth', [BannerController::class, 'TeenagerChunhuiDrugAgeTypeByMonth']);
    Route::post('TeenagerChunhuiDrugAgeTypeByYear', [BannerController::class, 'TeenagerChunhuiDrugAgeTypeByYear']);
    Route::post('TeenagerChunhuiDrugAreaByMonth', [BannerController::class, 'TeenagerChunhuiDrugAreaByMonth']);
    Route::post('TeenagerChunhuiDrugAreaByYear', [BannerController::class, 'TeenagerChunhuiDrugAreaByYear']);
    /*end */
    /* ---------------這是第八類---------------*/
    Route::post('TeenagerInterruptDrugGenderByMonth', [BannerController::class, 'TeenagerInterruptDrugGenderByMonth']);
    Route::post('TeenagerInterruptDrugGenderByYear', [BannerController::class, 'TeenagerInterruptDrugGenderByYear']);
    Route::post('TeenagerInterruptDrugAgeByMonth', [BannerController::class, 'TeenagerInterruptDrugAgeByMonth']);
    Route::post('TeenagerInterruptDrugAgeByYear', [BannerController::class, 'TeenagerInterruptDrugAgeByYear']);
    Route::post('TeenagerInterruptDrugAbuseTypeByMonth', [BannerController::class, 'TeenagerInterruptDrugAbuseTypeByMonth']);
    Route::post('TeenagerInterruptDrugAbuseTypeByYear', [BannerController::class, 'TeenagerInterruptDrugAbuseTypeByYear']);
    Route::post('TeenagerInterruptDrugAgeTypeByMonth', [BannerController::class, 'TeenagerInterruptDrugAgeTypeByMonth']);
    Route::post('TeenagerInterruptDrugAgeTypeByYear', [BannerController::class, 'TeenagerInterruptDrugAgeTypeByYear']);
    Route::post('TeenagerInterruptDrugAreaByMonth', [BannerController::class, 'TeenagerInterruptDrugAreaByMonth']);
    Route::post('TeenagerInterruptDrugAreaByYear', [BannerController::class, 'TeenagerInterruptDrugAreaByYear']);
    /*end */
     /* ---------------這是第九類---------------*/
     Route::post('DrugCourtPersonByMonth', [BannerController::class, 'DrugCourtPersonByMonth']);
     Route::post('DrugCourtPersonByYear', [BannerController::class, 'DrugCourtPersonByYear']);
     Route::post('DrugCourtCaseByMonth', [BannerController::class, 'DrugCourtCaseByMonth']);
     Route::post('DrugCourtCaseByYear', [BannerController::class, 'DrugCourtCaseByYear']);
     /*end */

      /* ---------------這是第十類---------------*/
      Route::post('SpecificAreaBusinessIndustryByMonth', [BannerController::class, 'SpecificAreaBusinessIndustryByMonth']);
      Route::post('SpecificAreaBusinessIndustryCommentTextByMonth', [BannerController::class, 'SpecificAreaBusinessIndustryCommentTextByMonth']);
      Route::post('SpecificAreaBusinessIndustryByYear', [BannerController::class, 'SpecificAreaBusinessIndustryByYear']);
      Route::post('SpecificAreaAdministrativeByMonth', [BannerController::class, 'SpecificAreaAdministrativeByMonth']);
      Route::post('SpecificAreaAdministrativeByYear', [BannerController::class, 'SpecificAreaAdministrativeByYear']);
      Route::post('SpecificIndustryByMonth', [BannerController::class, 'SpecificIndustryByMonth']);
      Route::post('SpecificIndustryByYear', [BannerController::class, 'SpecificIndustryByYear']);
      Route::post('SpecificAreaBusinessByMonth', [BannerController::class, 'SpecificAreaBusinessByMonth']);
      Route::post('SpecificAreaBusinessByYear', [BannerController::class, 'SpecificAreaBusinessByYear']);
      Route::post('SixBusinessByMonth', [BannerController::class, 'SixBusinessByMonth']);
      Route::post('SixBusinessByYear', [BannerController::class, 'SixBusinessByYear']);
      /*end */

    // BannerPages
    Route::get('/KaohsiungPoisonAnalysis', function(){
        return View::make('KaohsiungPoisonAnalysis');
    });
    Route::get('KaohsiungAndTaiwanDrugRecidivismRateAnalysis', function(){
        return View::make('KaohsiungAndTaiwanDrugRecidivismRateAnalysis');
    });
    Route::get('TaiwanDrugAnalysis', function(){
        return View::make('TaiwanDrugAnalysis');
    });
    Route::get('DrugTypeAnalysis', function(){
        return View::make('DrugTypeAnalysis');
    });
    Route::get('DrugCase', function(){
        return View::make('DrugCase');
    });
    Route::get('TeenagerDrugCase', function(){
        return View::make('TeenagerDrugCase');
    });
    Route::get('TeenagerChunhuiDrugCase', function(){
        return View::make('TeenagerChunhuiDrugCase');
    });
    Route::get('TeenagerInterruptDrugCase', function(){
        return View::make('TeenagerInterruptDrugCase');
    });
    Route::get('JuvenileCourtDrugCase', function(){
        return View::make('JuvenileCourtDrugCase');
    });
    Route::get('SpecificArea', function(){
        return View::make('SpecificArea');
    });

    // UploadController
    Route::get('Upload_getUploadPage','UploadController@getUploadPage')->name('Upload.getUploadPage');
    Route::get('Upload_getUploadPageUploadFile','UploadController@getUploadPageUploadFile')->name('Upload.getUploadPageUploadFile');
    Route::get('Upload_FileAnalyze','UploadController@getFileAnalyze')->name('Upload.FileAnalyze');
    Route::get('Upload_FileAnalyzeUpload','UploadController@getFileAnalyzeUpload')->name('Upload.FileAnalyzeUpload');
    Route::post('Upload_uploadLogAnalyzeDate','UploadController@uploadLogAnalyzeDate')->name('Upload.uploadLogAnalyzeDate');
    Route::post('Upload_uploadLogDelAnalyzeDate','UploadController@uploadLogDelAnalyzeDate')->name('Upload.uploadLogDelAnalyzeDate');
    Route::post('Upload_uploadLogDelDate','UploadController@uploadLogDelDate')->name('Upload.uploadLogDelDate');
    Route::post('Upload_uploadLogDelVRDate','UploadController@uploadLogDelVRDate')->name('Upload.uploadLogDelVRDate');
    Route::post('/Upload_HealthOther','UploadController@HealthOther')->name('Upload.HealthOther');
    Route::post('/Upload_AllPeopleFiles','UploadController@importAllPeopleFilesExecl')->name('Upload.AllPeopleFiles');
    Route::post('/Upload_DrugcaseFiles','UploadController@importDrugcaseFilesExecl')->name('Upload.DrugcaseFiles');
    Route::post('/Upload_uploadFiles','UploadController@importExecl')->name('Upload.uploadFiles');
    Route::post('/Upload_CatchListFiles','UploadController@Upload_CatchListFiles')->name('Upload.Upload_CatchListFiles');
    Route::post('/Upload_ServiceListFiles','UploadController@Upload_ServiceListFiles')->name('Upload.Upload_ServiceListFiles');
    Route::post('/Upload_24HoursFiles','UploadController@Upload_24HoursFiles')->name('Upload.Upload_24HoursFiles');
    Route::post('/Upload_DrugFiles','UploadController@Upload_DrugFiles')->name('Upload.Upload_DrugFiles');
    Route::post('/Upload_PopulationFiles','UploadController@Upload_PopulationFiles')->name('Upload.Upload_PopulationFiles');
    Route::post('/Upload_MDTreatmentFiles','UploadController@Upload_MDTreatmentFiles')->name('Upload.Upload_MDTreatmentFiles');
    Route::post('/Upload_Juvenile_CourtFiles','UploadController@Upload_Juvenile_CourtFiles')->name('Upload.Upload_Juvenile_CourtFiles');
    Route::post('/Upload_Coupling_Counseling','UploadController@Upload_Coupling_Counseling')->name('Upload.Upload_Coupling_Counseling');
    Route::post('/Upload_DrugList','UploadController@Upload_DrugList')->name('Upload.Upload_DrugList');
    Route::post('/Upload_DrugListTeenager','UploadController@Upload_DrugListTeenager')->name('Upload.Upload_DrugListTeenager');
    Route::post('/Upload_3_4_Lecture','UploadController@Upload_3_4_Lecture')->name('Upload.Upload_3_4_Lecture');
    Route::post('/Upload_DRUGS_LIST_TEENAGER_Interrupt','UploadController@Upload_DRUGS_LIST_TEENAGER_Interrupt')->name('Upload.Upload_DRUGS_LIST_TEENAGER_Interrupt');
    Route::post('/Upload_Specific_Business','UploadController@Upload_Specific_Business')->name('Upload.Upload_Specific_Business');
    Route::post('/Upload_View_Record','UploadController@Upload_View_Record')->name('Upload.Upload_View_Record');

    // FileDownloadController
    Route::post('/File_Download/checkData', 'FileDownloadController@checkData')->name('fileDownload.checkData');
    Route::post('/File_Download', 'FileDownloadController@fileDownload')->name('fileDownload');

    // AnalyzeController
    Route::get('Analyze','AnalyzeController@index')->name('Analyze.index');
    Route::get('Analyze/{type}','AnalyzeController@page')->name('Analyze.page');
    Route::post('Analyze/{type}','AnalyzeController@page');

    Route::prefix('/system')->name('system.')->group(function () {
        Route::get('/', [SystemController::class,'index'])->name('index');
        Route::get('account', [SystemController::class,'account'])->name('account');
        Route::get('account/add', [SystemController::class,'account_add'])->name('account_add');
        Route::get('account/edit/{user}', [SystemController::class,'account_edit'])->name('account_edit');
        Route::post('account/save/{user?}', [SystemController::class,'account_save'])->name('account_save');
        Route::post('account/del/{user}', [SystemController::class,'account_del'])->name('account_del');

        Route::get('log', [SystemController::class,'log'])->name('log');
    });

    Route::prefix('/member')->name('member.')->group(function () {
        Route::get('change_password', [MemberController::class,'change_password'])->name('change_password');
        Route::post('change_password', [MemberController::class,'change_password_save']);
    });
});

require __DIR__.'/auth.php';
