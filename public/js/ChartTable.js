
//  polyfill
if (!Object.create) {
    Object.create = function (o) {
        if (arguments.length > 1) {
            throw new Error('Object.create implementation only accepts the first parameter');
        }

        function F() {};
        F.prototype = o;
        return new F();
    };
}

var basicCSS = {
    "border-top": "1px solid #000",
    "border-right": "1px solid #000",
    "border-left": "1px solid #000",
    "border-sizing": "border-box",
    "background-color": "#FFF"
};

var setCss = function (element) {
    var tdWidth = (element.bodyWidth) / (element.columnSource.length - 1);
    var scale = 1,
        fontSize = (Math.min(element.headerWidth, tdWidth) / element.maxLength) * 0.75;
    if (fontSize < 8)
        scale = fontSize / 8;
    else if (fontSize > 12)
        fontSize = 12;
    element.fontSize=fontSize;
    var fontCSS = {
        "font-size": fontSize + 'px',
        "transform-origin": fontSize*1.5+'px',
        "transform": "scale(" + scale + ")",
        "white-space": "nowrap",
        "font-weight": "bold",
        "display":scale==1?"table-cell":"table-cell"
    };
    var iCss = {
        "font-size": fontSize + 'px',
        "transform": "scale(" + scale*0.7 + ")",
        "display":scale==1?"table-cell":"table-cell"
    };
    $("#" + element.container + " .chart-table").css({
        "width": (element.headerWidth + element.bodyWidth) + 'px',
        'table-layout': 'fixed'
    });
    // $('#' + element.container + ' .chart-table tbody tr td').css("line-height", (fontSize*0.7).toString() + "px")
    $('#' + element.container + ' .chart-table tbody tr:first td:first').css("width", element.headerWidth + 'px');
    $('#' + element.container + ' .chart-table tbody tr:first td:not(:first)').css(basicCSS).css("width", tdWidth + 'px');
    $('#' + element.container + ' .chart-table tbody tr:first td:not(:first) p').css(fontCSS);
    $('#' + element.container + ' .chart-table tbody tr:not(:first) td').css(basicCSS).css("border-bottom", "1px solid #000");
    $('#' + element.container + ' .chart-table tbody tr:not(:first) td p').css(fontCSS);
    $('#' + element.container + ' .chart-table tbody tr td i').css(iCss);
    $('#' + element.container + ' .chart-table tbody tr:not(:first) td:first').css("width", element.headerWidth + 'px');

    element.Height = $('#'+element.container).height();
}

var verticalCss = function(element){
    var tdWidth = (element.bodyWidth) / (element.columnSource.length - 1);
    var scaleH = 1,
        fontSizeH = (Math.min(element.headerWidth, tdWidth) / 1)*0.6;
    if (fontSizeH < 8)
        scaleH = fontSizeH / 8;
    else if (fontSizeH > 12)
        fontSizeH = 12;

    var scale = 1,
        fontSize = (Math.min(element.headerWidth, tdWidth) / element.maxLength) - 0.4;
    if (fontSize < 8)
        scale = fontSize / 8;
    else if (fontSize > 12)
        fontSize = 14;
    element.fontSize=fontSize;
    var fontCSS = {
        "font-size": fontSize + 'px',
        "transform-origin": "0",
        "transform": "scale(" + scale + ")",
        "white-space": "nowrap",
        "font-weight": "bold",
        "display":scale==1?"table-cell":"table-cell"
    };
    var fontCSSH = {
        "font-size": fontSizeH + 'px',
        "transform-origin": 0,
        "transform": "scale(" + scaleH + ")",
        "white-space": "nowrap",
        "font-weight": "bold",
        "display":scaleH==1?"table-cell":"table-cell"
    };
    var iCss = {
        "font-size": fontSize + 'px',
        "transform": "scale(" + scale*0.7 + ")",
        "display":scale==1?"table-cell":"table-cell",
        "line-height": (fontSize).toString() + "px !important"
    };
    $("#" + element.container + " .chart-table").css({
        "width": (element.headerWidth + element.bodyWidth) + 'px',
        'table-layout': 'fixed'
    });
    $('#' + element.container + ' .chart-table tbody tr:first td:first').css("width", element.headerWidth + 'px');
    $('#' + element.container + ' .chart-table tbody tr:first td:not(:first)').css(basicCSS).css("line-height", (fontSize*2).toString() + "px").css("width", tdWidth + 'px');
    $('#' + element.container + ' .chart-table tbody tr:first td:not(:first) p').css(fontCSSH);
    $('#' + element.container + ' .chart-table tbody tr:not(:first) td').css(basicCSS).css("line-height", (fontSize*2).toString() + "px").css("border-bottom", "1px solid #000");
    $('#' + element.container + ' .chart-table tbody tr:not(:first) td i').css(basicCSS).css("line-height", (fontSize).toString() + "px").css("border","0")
    $('#' + element.container + ' .chart-table tbody tr:not(:first) td p').css(fontCSS);
    $('#' + element.container + ' .chart-table tbody tr td i').css(iCss);
    $('#' + element.container + ' .chart-table tbody tr:not(:first) td:first').css("width", element.headerWidth + 'px')

    element.Height = $('#'+element.container).height();
}

var setLargeCss = function (element) {
    var tdWidth = (element.bodyWidth) / (element.columnSource.length - 1);
    var scale = 1,
        fontSize = (Math.min(element.headerWidth, tdWidth) / element.maxLength) - 0.4;
    if (fontSize < 8)
        scale = fontSize / 8;
    else if (fontSize > 12)
        fontSize = 14;
    element.fontSize=fontSize;
    var fontCSS = {
        "font-size": fontSize + 'px',
        "transform-origin": "0",
        "transform": "scale(" + scale + ")",
        "white-space": "nowrap",
        "font-weight": "bold",
        "display":scale==1?"table-cell":"table-cell"
    };
    var iCss = {
        "font-size": fontSize + 'px',
        "transform": "scale(" + scale*0.7 + ")",
        "display":scale==1?"table-cell":"table-cell",
        "line-height": (fontSize).toString() + "px !important"
    };
    $("#" + element.container + " .chart-table").css({
        "width": (element.headerWidth + element.bodyWidth) + 'px',
        'table-layout': 'fixed'
    });
    $('#' + element.container + ' .chart-table tbody tr:first td:first').css("width", element.headerWidth + 'px');
    $('#' + element.container + ' .chart-table tbody tr:first td:not(:first)').css(basicCSS).css("line-height", (fontSize*2).toString() + "px").css("width", tdWidth + 'px');
    $('#' + element.container + ' .chart-table tbody tr:first td:not(:first) p').css(fontCSS);
    $('#' + element.container + ' .chart-table tbody tr:not(:first) td').css(basicCSS).css("line-height", (fontSize*2).toString() + "px").css("border-bottom", "1px solid #000");
    $('#' + element.container + ' .chart-table tbody tr:not(:first) td i').css(basicCSS).css("line-height", (fontSize).toString() + "px").css("border","0")
    $('#' + element.container + ' .chart-table tbody tr:not(:first) td p').css(fontCSS);
    $('#' + element.container + ' .chart-table tbody tr td i').css(iCss);
    $('#' + element.container + ' .chart-table tbody tr:not(:first) td:first').css("width", element.headerWidth + 'px')

    element.Height = $('#'+element.container).height();
}

var setspicial = function (element) {
    var trtdWidth = ((element.bodyWidth) / (element.columnSource.length - 1))/(element.columnSource.length-2);
    var tdWidth = (element.bodyWidth) / (Object.keys(element.dataSource[0]).length - 1);
    var scaleS = 1,
        fontSizeS = ((trtdWidth*3) / 4);
    if (fontSizeS < 8)
        scaleS = fontSizeS / 8;
    else if (fontSizeS > 12)
        fontSizeS = 10;
    var scale = 1,
        fontSize = (Math.min(element.headerWidth, tdWidth) / element.maxLength);
    if (fontSize < 8)
        scale = fontSize / 8;
    else if (fontSize > 12)
        fontSize = 10;
    element.fontSize = fontSize;
    var SfontCSS = {
        "font-size": fontSizeS + 'px',
        "transform-origin": "0",
        "transform": "scale(" + scaleS + ")",
        "white-space": "nowrap",
        "font-weight": "bold",
        "display": "table-cell"
    };
    var fontCSS = {
        "font-size": fontSize + 'px',
        "transform-origin": "0",
        "transform": "scale(" + scale + ")",
        "white-space": "nowrap",
        "font-weight": "bold",
        "display": "table-cell"
    };
    var iCss = {
        "font-size": fontSize + 'px',
        "transform": "scale(" + scale*0.7 + ")",
        "display":scale==1?"table-cell":"table-cell"
    };
    $("#" + element.container + " .chart-table").css({
        "width": (element.headerWidth + element.bodyWidth) + 'px',
        'table-layout': 'fixed'
    });
    $('#' + element.container + ' .chart-table tbody tr:nth-child(-n+1) td:first-child').css("width", element.headerWidth + 'px');
    $('#' + element.container + ' .chart-table tbody tr:nth-child(1) td:not(:first-child)').css("line-height", (fontSize * 2).toString() + "px").css("border-top","");
    $('#' + element.container + ' .chart-table tbody tr:nth-child(2) td:not(:first-child)').css(basicCSS).css("line-height", (fontSizeS * 2).toString() + "px").css("width", trtdWidth + 'px').css("border-top","1px solid #000");

    $('#' + element.container + ' .chart-table tbody tr:nth-child(-n+3) td:not(:first-child) p').css(fontCSS);

    $('#' + element.container + ' .chart-table tbody tr:nth-child(n+3) td').css(basicCSS).css("line-height", (fontSize * 2).toString() + "px").css("border-bottom", "1px solid #000").css("width", tdWidth + 'px');
    $('#' + element.container + ' .chart-table tbody tr:not(:first) td i').css(basicCSS).css("line-height", (fontSize).toString() + "px").css("border","0")
    $('#' + element.container + ' .chart-table tbody tr:nth-child(n+3) td p').css(fontCSS);
    $('#' + element.container + ' .chart-table tbody tr td i').css(iCss);
    $('#' + element.container + ' .chart-table tbody tr:nth-child(n+3) td:first-child').css("width", element.headerWidth + 'px');
    $('#' + element.container + ' .chart-table tbody tr:nth-child(2) td p').css(SfontCSS);

    element.Height = $('#'+element.container).height();
}
var spicial_vertical = function (element) {
    var trtdWidth = ((element.bodyWidth) / (element.columnSource.length - 1))/5;
    var tdWidth = (element.bodyWidth) / (element.columnSource.length - 1);
    var scale = 1,
        fontSize = (Math.min(element.headerWidth, tdWidth) / element.maxLength) - 0.4;
    if (fontSize < 8)
        scale = fontSize / 8;
    else if (fontSize > 12)
        fontSize = 12;
    var fontCSS = {
        "font-size": fontSize + 'px',
        "transform-origin": "0",
        "transform": "scale(" + scale + ")",
        "white-space": "nowrap",
        "font-weight": "bold",
        "display": "table-cell"
    };
    $("#" + element.container + " .chart-table").css({
        "width": (element.headerWidth + element.bodyWidth) + 'px',
        'table-layout': 'fixed'
    });
    $('#' + element.container + ' .chart-table tbody tr:nth-child(-n+1) td:first-child').css("width", element.headerWidth + 'px');
    $('#' + element.container + ' .chart-table tbody tr:nth-child(-n+3) td:not(:first-child)').css(basicCSS).css("line-height", (fontSize).toString() + "px").css("width", trtdWidth + 'px').css("border-top","");
    //$('#' + element.container + ' .chart-table tbody tr:nth-child(2) td:nth-child(1)').css(basicCSS).css("border-left","");
    //$('#' + element.container + ' .chart-table tbody tr:nth-child(2) td').css(basicCSS).css("border-top","");
    $('#' + element.container + ' .chart-table tbody tr:nth-child(-n+3) td:not(:first-child) p').css(fontCSS);
    // $('#' + element.container + ' .chart-table tbody tr:nth-child(-n+2) td:first-child').css("width", element.headerWidth + 'px').css("border-style",'');
    $('#' + element.container + ' .chart-table tbody tr:nth-child(n+3) td').css(basicCSS).css("line-height", (fontSize * 2).toString() + "px").css("border-bottom", "1px solid #000").css("width", tdWidth + 'px');
    $('#' + element.container + ' .chart-table tbody tr:nth-child(n+3) td p').css(fontCSS);
    $('#' + element.container + ' .chart-table tbody tr:nth-child(n+3) td:first-child').css("width", element.headerWidth + 'px');
    element.Height = $('#'+element.container).height();
}

var setNormalCss = function (element) {
    var tdWidth = (element.bodyWidth) / (element.columnSource.length);
    var scale = 1,
        fontSize = (tdWidth / element.maxLength) - 1;
    if (fontSize < 8)
        scale = fontSize / 8;
    else if (fontSize > 12)
        fontSize = 12;

    var fontCSS = {
        "font-size": fontSize + 'px',
        "transform-origin": "0",
        "transform": "scale(" + scale + ")",
        "white-space": "nowrap",
        "font-weight": "bold",
        "display": "table-cell",
    };
    if(element.pie)
        $("#" + element.container + " .chart-table").css({
            "width": element.bodyWidth + 'px',
            "position":"absolute",
            "bottom":0,
            'table-layout': 'fixed'
        });
    else
        $("#" + element.container + " .chart-table").css({
            "width": element.bodyWidth + 'px',
            'table-layout': 'fixed'
        });
    $('#' + element.container + ' .chart-table tbody tr:first td').css(basicCSS).css("line-height", (fontSize * 2).toString() + "px").css("width", tdWidth + 'px');
    $('#' + element.container + ' .chart-table tbody tr:first td p').css(fontCSS);
    $('#' + element.container + ' .chart-table tbody tr:not(:first) td').css(basicCSS).css("line-height", (fontSize * 2).toString() + "px").css("border-bottom", "1px solid #000");
    $('#' + element.container + ' .chart-table tbody tr:not(:first) td p').css(fontCSS);
}

var CTable = (id) => {
    return {
        container: id,
        headerWidth: 0,
        bodyWidth: 0,
        dataSource: [],
        columnSource: [],
        maxLength: 0,
        vertical :false,
        spicial:false,
        spicial_vertical:false,
        fontSize:0,
        Height:0,
        pie:false,
        setWidth: function () {
            if(this.spicial)
                setspicial(this);
            else if(this.spicial_vertical)
                spicial_vertical(this);
            else if(this.bodyWidth>800)
                setLargeCss(this);
            else if (this.headerWidth > 0)
                setCss(this);
            else
                setNormalCss(this);
        },
        bind: function () {
            $("#" + this.container).html("<table class='chart-table'></table");
            var maxLength = 0;
            if(this.vertical)
                var columnElement = this.columnSource.map(function (colum, i) {
                    maxLength = 1;
                        return "<td align='center' align='center'><p style=\"line-height:15px\">" + Array.from(colum).join('<br/>') + "</p></td>";
                }).join('');
            else if(this.spicial_vertical){
                    var columnElement = this.columnSource.map(function (colum, i) {
                        if(colum !=""){
                            maxLength = maxLength < colum.length ? colum.length : maxLength;
                            var inside = colum.category.map(function(each,ieach){
                                return "<td align='center' align='center' style=\"height:10px\"><p style=\"text-align:center;line-height:15px\">" + Array.from(each).join('<br/>') + "</p></td>";
                            }).join('');
                        }
                        return inside;
                    }).join('');
                    var columnElement2 = this.columnSource.map(function(colum,i){
                        if(colum!="")
                            return "<td colspan=5 align='center' align='center'><p>" + colum.title + "</p></td>"
                    }).join();
                    columnElement = "<td align='center' align='center' style=\"height:10px\"><p>" + "" + "</p></td>"+columnElement;
                    columnElement2 = "<tr>"+"<td  align='center' align='center'><p>" + "" + "</p></td>"+columnElement2+"</tr>";
            }
            else if(this.spicial){
                var columnElement = this.columnSource.map(function (colum, i) {
                    if(colum !=""){
                        maxLength = maxLength < colum.title.length ? colum.title.length : maxLength;
                        var inside = colum.category.map(function(each,ieach){
                            if(colum.category.length==1){
                                return "<td align='center' align='center' style=\"height:10px;border-right:1px solid #000;border-left:1px solid #000\"><p>" + each + "</p></td>";
                            }
                            else{
                                if(ieach>0 && ieach<colum.category.length-1)
                                    return "<td align='center' align='center' style=\"height:10px;border-right:0;border-left:0\"><p>" + each + "</p></td>";
                                else if(ieach==0)
                                    return "<td align='center' align='center' style=\"height:10px;border-left:1px solid #000 \"><p>" + each + "</p></td>";
                                else
                                    return "<td align='center' align='center' style=\"height:10px;border-right:1px solid #000 \"><p>" + each + "</p></td>";
                            }

                        }).join('');
                    }
                    return inside;
                }).join('');
                var columnElement2 = this.columnSource.map(function(colum,i){

                    if(colum!=""){
                        return "<td colspan="+colum.category.length+" align='center' align='center'><p>" + colum.title + "</p></td>"
                    }


                }).join();
                columnElement = "<td align='center' align='center' style=\"height:10px\"><p>" + "" + "</p></td>"+columnElement;
                columnElement2 = "<tr>"+"<td  align='center' align='center'><p>" + "" + "</p></td>"+columnElement2+"</tr>";
            }
            else
                var columnElement = this.columnSource.map(function (colum, i) {
                    if(colum.length>10){
                        maxLength = maxLength < colum.split('<br>')[0].length ? colum.split('<br>')[0].length-2 : maxLength;
                    }
                    else
                        maxLength = maxLength < colum.length ? colum.length : maxLength;
                    return "<td align='center' align='center'><p>" + colum + "</p></td>";
                }).join('');
            if(this.spicial){
                var dataElement = this.dataSource.map(function (data) {
                    var td = Object.values(data).map(function(value){
                        if(Array.isArray(value)){
                            var map = value.map(e=>{
                                var test=e.map(i=>{
                                    return "<td align='center' align='center'><p>" + i + "</p></td>";
                                })
                                return test;
                            }).join('');

                            return map;
                        }
                        else{
                            splitValue = value.split(',');
                            var square = splitValue.length==2?'<i class="fa fa-square" style="color:'+splitValue[1]+';background-color:'+splitValue[1]+';"></i>':"";
                            maxLength = maxLength < splitValue[0].length ? splitValue[0].length: maxLength;
                            return "<td align='center' align='center'>"+square+"<p>" + splitValue[0] + "</p></td>";
                            // return "<td align='center' align='center'><p>" +each + "</p></td>";
                        }});
                    return "<tr>" + td + "</tr>";
                }).join('');
            }
            else{
                var dataElement = this.dataSource.map(function (data) {
                    var td = Object.values(data).map(function (value, iValue) {
                        splitValue = value.split(',');
                        var square = splitValue.length==2?'<i class="fa fa-square" style="color:'+splitValue[1]+';background-color:'+splitValue[1]+';"></i>':"";
                        maxLength = maxLength < splitValue[0].length ? splitValue[0].length : maxLength;
                        return "<td align='center' align='center'>"+square+"<p>" + splitValue[0] + "</p></td>";
                    }).join('');
                    return "<tr>" + td + "</tr>";
                }).join('');
            }
            this.maxLength = maxLength;
            //add column
            $("#" + this.container + " .chart-table").append("<tbody><tr>" + columnElement + "</tr>" +columnElement2+ dataElement + "</tdbody>");

            if(this.spicial)
                setspicial(this);
            else if(this.vertical)
                verticalCss(this);
            else if(this.spicial_vertical)
                spicial_vertical(this);
            else if(this.bodyWidth>800)
                setLargeCss(this);
            else if (this.headerWidth > 0)
                setCss(this);
            else
                setNormalCss(this);
        }
    }
}

var tableList = [];
var getChartTable = function (id) {
    return tableList.filter(function (f) {
        return f.container == id;
    })[0];
}
var ChartTable = function (id) {
    // CTable.container = id;
    const table = Object.create(CTable(id));
    tableList.push(table);
    return table;
}
