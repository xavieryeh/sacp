(function () {
    $.extend({
        //一、查獲施用毒品(刑罰)初犯人數同期比較
        getCrimialMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectCrimialMonth", {
                startDate: StartDate,
                endDate: EndDate
            },function(data){callback(data)}, null, true);
        },
        getCrimialYear: function (StartDate, EndDate, callback) {
            // fetch('SelectCrimialYear', {
            //     method: "POST",
            //     body: JSON.stringify({
            //         startDate: StartDate,
            //         endDate: EndDate
            //     }),

            //     headers: {
            //         "Content-Type": "application/json",
            //         "Accept": "application/json", "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            //     }
            // }).then(data => data.json()).then(result => callback(result));
            PostAJAX("SelectCrimialYear", {
                startDate: StartDate,
                endDate: EndDate
            },function(data){callback(data)}, null, true);
        },
        //二、查獲施用毒品(行政罰)初犯人數同期比較
        getAdministrativeMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectAdministrativeMonth", {
                startDate: StartDate,
                endDate: EndDate
            },function(data){callback(data)}, null, true);
        },
        getAdministrativeYear: function (StartDate, EndDate, callback) {
            PostAJAX("SelectAdministrativeYear", {
                startDate: StartDate,
                endDate: EndDate
            },function(data){callback(data)}, null, true);
        },
        //三、查獲毒品上游(製造、運輸、販賣)人數
        getPersonMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectPersonMonth", {
                startDate: StartDate,
                endDate: EndDate
            },function(data){callback(data)}, null, true);
        },
        getPersonYear: function (StartDate, EndDate, callback) {
            PostAJAX("SelectPersonYear", {
                startDate: StartDate,
                endDate: EndDate
            },function(data){callback(data)}, null, true);
        },
        //四、各級毒品查獲重量
        getWeightMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectWeightMonth", {
                startDate: StartDate,
                endDate: EndDate
            },function(data){callback(data)}, null, true);
        },
        //五、各級毒品查獲人數
        getNumMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectNumMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        //六、各級毒品查獲件數
        getCaseMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectCaseMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        //七、各級毒品查獲重量同期比較
        getWeightContrastMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectWeightContrastMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getWeightContrastYear: function (StartDate, EndDate, callback) {
            PostAJAX("SelectWeightContrastYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        //八、各級毒品查獲人數同期比較
        getPeosonContrastMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectPeosonContrastMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getPeosonContrastYear: function (StartDate, EndDate, callback) {
            PostAJAX("SelectPeosonContrastYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        //九、各級毒品查獲件數同期比較
        getCaseContrastMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectCaseContrastMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getCaseContrastYear: function (StartDate, EndDate, callback) {
            PostAJAX("SelectCaseContrastYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        //一、列管期間內施用毒品再犯率
        getRecidivesmRateMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectRecidivesmRateMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getRecidivesmRateYear: function (StartDate, EndDate, callback) {
            PostAJAX("SelectRecidivesmRateYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        //二、列管期滿結案後半年內施用毒品再犯率
        getRecidivesmRateHalfYearMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectRecidivesmRateHalfYearMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getRecidivesmRateHalfYearbyYear: function (StartDate, EndDate, callback) {
            PostAJAX("SelectRecidivesmRateHalfYearbyYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        //三、列管期滿結案後一年內施用毒品再犯率
        getRecidivesmRateOneYearMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectRecidivesmRateOneYearMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getRecidivesmRateOneYearbyYear: function (StartDate, EndDate, callback) {
            PostAJAX("SelectRecidivesmRateOneYearbyYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        //四、列管期滿結案後二年內施用毒品再犯率
        getRecidivesmRateTwoYearMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectRecidivesmRateTwoYearMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getRecidivesmRateTwoYearbyYear: function (StartDate, EndDate, callback) {
            PostAJAX("SelectRecidivesmRateTwoYearbyYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        //一、全國藥癮個案統計(未校正數)
        getLocationeTubeMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectLocationeTubeMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getLocationeTubebyYear: function (StartDate, EndDate, callback) {
            PostAJAX("SelectLocationeTubebyYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        //二、全國藥癮個案統計(校正數)
        getCorrectionLocationeTubeMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectCorrectionLocationeTubeMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getCorrectionLocationeTubebyYear: function (StartDate, EndDate, callback) {
            PostAJAX("SelectCorrectionLocationeTubebyYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugTypebyMonth: function (StartDate, EndDate, callback) {
            PostAJAX("DrugTypebyMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugTypebyYear: function (StartDate, EndDate, callback) {
            PostAJAX("DrugTypebyYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugGenderByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("DrugGenderByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugGenderByYear: function (StartDate, EndDate, callback) {
            PostAJAX("DrugGenderByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugAgeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("DrugAgeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugAgeByYear: function (StartDate, EndDate, callback) {
            PostAJAX("DrugAgeByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugAbuseTypeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("DrugAbuseTypeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugAbuseTypeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("DrugAbuseTypeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugAbuseTypeByYear: function (StartDate, EndDate, callback) {
            PostAJAX("DrugAbuseTypeByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugAgeTypeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("DrugAgeTypeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugAgeTypeByYear: function (StartDate, EndDate, callback) {
            PostAJAX("DrugAgeTypeByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugAreaByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("DrugAreaByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugAreabyYear: function (StartDate, EndDate, callback) {
            PostAJAX("DrugAreabyYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugCorrectionAreaByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("DrugCorrectionAreaByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugCorrectionAreaByYear: function (StartDate, EndDate, callback) {
            PostAJAX("DrugCorrectionAreaByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugCoachByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("DrugCoachByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugCoachByYear: function (StartDate, EndDate, callback) {
            PostAJAX("DrugCoachByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugServiceByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("DrugServiceByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugServiceByYear: function (StartDate, EndDate, callback) {
            PostAJAX("DrugServiceByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugCouplingByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("DrugCouplingByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugCouplingByYear: function (StartDate, EndDate, callback) {
            PostAJAX("DrugCouplingByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugMedicalByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("DrugMedicalByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugMedicalByYear: function (StartDate, EndDate, callback) {
            PostAJAX("DrugMedicalByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrug24HoursByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("Drug24HoursByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrug24HoursByYear: function (StartDate, EndDate, callback) {
            PostAJAX("Drug24HoursByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerDrugGenderByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerDrugGenderByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerDrugGenderByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerDrugGenderByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerDrugAgeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerDrugAgeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerDrugAgeByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerDrugAgeByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerDrugAbuseTypeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerDrugAbuseTypeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerDrugAbuseTypeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerDrugAbuseTypeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerDrugAbuseTypeByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerDrugAbuseTypeByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerDrugAgeTypeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerDrugAgeTypeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerDrugAgeTypeByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerDrugAgeTypeByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerDrugAreaByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerDrugAreaByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerDrugAreabyYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerDrugAreabyYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerDrugCorrectionAreaByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerDrugCorrectionAreaByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerDrugCorrectionAreaByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerDrugCorrectionAreaByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerChunhuiDrugGenderByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerChunhuiDrugGenderByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerChunhuiDrugGenderByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerChunhuiDrugGenderByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerChunhuiDrugAgeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerChunhuiDrugAgeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerChunhuiDrugAgeByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerChunhuiDrugAgeByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerChunhuiDrugAbuseTypeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerChunhuiDrugAbuseTypeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerChunhuiDrugAbuseTypeByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerChunhuiDrugAbuseTypeByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerChunhuiDrugAgeTypeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerChunhuiDrugAgeTypeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerChunhuiDrugAgeTypeByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerChunhuiDrugAgeTypeByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerChunhuiDrugAreaByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerChunhuiDrugAreaByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerChunhuiDrugAreaByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerChunhuiDrugAreaByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerInterruptDrugGenderByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerInterruptDrugGenderByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerInterruptDrugGenderByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerInterruptDrugGenderByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerInterruptDrugAgeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerInterruptDrugAgeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerInterruptDrugAgeByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerInterruptDrugAgeByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerInterruptDrugAbuseTypeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerInterruptDrugAbuseTypeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerInterruptDrugAbuseTypeByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerInterruptDrugAbuseTypeByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerInterruptDrugAgeTypeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerInterruptDrugAgeTypeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerInterruptDrugAgeTypeByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerInterruptDrugAgeTypeByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerInterruptDrugAreaByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerInterruptDrugAreaByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getTeenagerInterruptDrugAreaByYear: function (StartDate, EndDate, callback) {
            PostAJAX("TeenagerInterruptDrugAreaByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugCourtPersonByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("DrugCourtPersonByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugCourtPersonByYear: function (StartDate, EndDate, callback) {
            PostAJAX("DrugCourtPersonByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugCourtCaseByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("DrugCourtCaseByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getDrugCourtCaseByYear: function (StartDate, EndDate, callback) {
            PostAJAX("DrugCourtCaseByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getSpecificAreaBusinessIndustryByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SpecificAreaBusinessIndustryByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getSpecificAreaBusinessIndustryCommentTextByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SpecificAreaBusinessIndustryCommentTextByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getSpecificAreaBusinessIndustryByYear: function (StartDate, EndDate, callback) {
            PostAJAX("SpecificAreaBusinessIndustryByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getSpecificAreaAdministrativeByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SpecificAreaAdministrativeByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getSpecificAreaAdministrativeByYear: function (StartDate, EndDate, callback) {
            PostAJAX("SpecificAreaAdministrativeByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getSpecificIndustryByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SpecificIndustryByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getSpecificIndustryByYear: function (StartDate, EndDate, callback) {
            PostAJAX("SpecificIndustryByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getSpecificAreaBusinessByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SpecificAreaBusinessByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getSpecificAreaBusinessByYear: function (StartDate, EndDate, callback) {
            PostAJAX("SpecificAreaBusinessByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getSixBusinessByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SixBusinessByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getSixBusinessByYear: function (StartDate, EndDate, callback) {
            PostAJAX("SixBusinessByYear", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
        getSelectCommentTextByMonth: function (StartDate, EndDate, callback) {
            PostAJAX("SelectCommentTextByMonth", {
                startDate: StartDate,
                endDate: EndDate
            }, function (data) { callback(data) }, null, true);
        },
    });
})();
function ChartTableData(response, firstName, category, lstColor) {
    const lstFirstCol = [...new Set(response.map(each => {
        return each[firstName];
    }))];
    const lstCategory = [...new Set(response.map(each => {
        return each[category];
    }))];
    var Params = {};
    Params.lstCategory = lstCategory;
    Params.TableBodyRow = [];
    Params.lstData = lstCategory.map(function (each, iItem) {
        var newData = {};
        newData.category = each;
        lstFirstCol.forEach((year, iYear) => {
            newData[year] = response.filter(data => {
                return data[firstName] == year && data[category] == each
            })[0];
            if (newData[year] == undefined)
                newData[year] = 0;
            else
                newData[year] = parseInt(newData[year].count);
        });
        newData.color = lstColor;
        return newData;
    });

    lstFirstCol.forEach((year, iYear) => {
        Params.TableBodyRow.push([year].concat(Params.lstData.map(each => {
            return each[year]
        })));
    });
    return Params;
}

function StackedChartTableData(response, firstName, category, stacked, lstColor) {
    const lstFirstCol = [...new Set(response.map(each => {
        return each[firstName];
    }))];
    const lstCategory = [...new Set(response.map(each => {
        return each[category];
    }))];
    const lstStacked = [...new Set(response.map(each => {
        return each[stacked];
    }))];
    lstStacked.sort(function (a, b) {
        return a.localeCompare(b, "zh-Hant");});
    var sumCategory =[];
    lstFirstCol.forEach(level1=>{
        lstCategory.forEach(ageclass=>{
            sumCategory.push(ageclass+'_'+level1);
        })
    })
    sumCategory.sort();
    //var sumCategory = lstFirstCol.map(each => { return lstCategory.map(eachCategory => { return each + '_' + eachCategory }) })[0];
    var Params = {};
    Params.lstCategory = sumCategory;
    Params.TableBodyRow = [];
    var lstData = [];
    Params.lstData = sumCategory.map(eachcategory=>{
        var newData={};
        newData.category=eachcategory;
        lstStacked.forEach(eachType=>{
            newData[eachType] =response.filter(each=>{return  each[firstName]==eachcategory.split('_')[1]&&each[category]==eachcategory.split('_')[0]&&each[stacked]==eachType})[0]
                if(newData[eachType]==undefined)
                    newData[eachType]=0;
                else
                    newData[eachType]=parseInt(newData[eachType].count);
        })
        newData.color=lstColor;
        return newData;
    })
    Params.lstData.sort(function (a, b) {
        // names must be equal
        return a.category.localeCompare(b.category, "zh-Hant");
        // return a.group - b.group && a.category.split('_')[1]-b.category.split('_')[1];
      });

    lstFirstCol.forEach((year, iYear) => {
        Params.TableBodyRow.push([year].concat(Params.lstData.map(each => {
            return each[year]
        })));
    });
    return Params;
}

function StackedNoSortChartTableData(response, firstName, category, stacked, lstColor) {
    const lstFirstCol = [...new Set(response.map(each => {
        return each[firstName];
    }))];
    const lstCategory = [...new Set(response.map(each => {
        return each[category];
    }))];
    const lstStacked = [...new Set(response.map(each => {
        return each[stacked];
    }))];
    var sumCategory =[];
    lstFirstCol.forEach(level1=>{
        lstCategory.forEach(ageclass=>{
            sumCategory.push(ageclass+'_'+level1);
        })
    })
    //sumCategory.sort();
    // sumCategory.sort(function (a, b) {
    //     return a.localeCompare(b, "zh-Hant");});
    //var sumCategory = lstFirstCol.map(each => { return lstCategory.map(eachCategory => { return each + '_' + eachCategory }) })[0];
    var Params = {};
    Params.lstCategory = sumCategory;
    Params.TableBodyRow = [];
    var lstData = [];
    Params.lstData = sumCategory.map(eachcategory=>{
        var newData={};
        newData.category=eachcategory;
        lstStacked.forEach(eachType=>{
            newData[eachType] =response.filter(each=>{return  each[firstName]==eachcategory.split('_')[1]&&each[category]==eachcategory.split('_')[0]&&each[stacked]==eachType})[0]
                if(newData[eachType]==undefined)
                    newData[eachType]=0;
                else
                    newData[eachType]=parseInt(newData[eachType].count);
        })
        newData.color=lstColor;
        return newData;
    })
    // Params.lstData.sort(function (a, b) {
    //     // names must be equal
    //     return a.category.localeCompare(b.category, "zh-Hant");
    //     // return a.group - b.group && a.category.split('_')[1]-b.category.split('_')[1];
    //   });

    lstFirstCol.forEach((year, iYear) => {
        Params.TableBodyRow.push([year].concat(Params.lstData.map(each => {
            return each[year]
        })));
    });
    return Params;
}
