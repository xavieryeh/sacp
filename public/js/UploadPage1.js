function UploadPage(self){
    self.init = function(){
        $.Log("檔案管理","檔案上傳-"+self.Title+"頁面載入");
        if(self.Type=="LNI"){
            $("#selData").html('<option value="" selected>請選擇資料來源</option>\
            <option value="警察局">警察局</option>\
            <option value="少年及家事法院">少年及家事法院</option>\
            <option value="衛生局">衛生局</option>\
            <option value="民政局">民政局</option>');
        }
        else if(self.Type=="CCMS"){
            $("#selData").html('<option value="" selected>請選擇資料來源</option>\
            <option value="衛福部-其他">衛福部-其他</option>\
            <option value="衛福部-全國藥癮">衛福部-全國藥癮</option>\
            <option value="戶政司">戶政司</option>');
        }
        searchData();
    }
    //各項刪除事件
    self.onDelete = function(system,source,date,fileDate){
        $("#delKeyValue").val(system+"_"+source+"_"+date+"_"+fileDate);
        $("#delKey").html("<br/>來源:"+source+"<br/>上傳時間:"+date);
        $('#delModal').modal('show');
    }  
    //各項刪除確認事件
    $("#btnDelConfirm").on('click', function () {
        $.Log("檔案管理","檔案上傳-"+$("#delKeyValue").val()+"的資料刪除");
        $.PostAJAX("Upload_uploadLogDelDate", {type:self.Type,keyValue:$("#delKeyValue").val()}, response => {
            self.Datas = response;
            searchData();
            $('#delModal').modal('hide');
        });
    });
    // 查詢按鈕事件
    $("#searchData").on('click', function () {
        searchData();
    });    
    //查詢Log
    function searchData() {
        const selSource = $("#selData option:selected")[0].value;
        const keyWord = $("#iKeyWord").val().toLowerCase().trim();
        $.Log("檔案管理","檔案上傳紀錄查詢-條件:"+selSource+"|"+keyWord);
        const filterData = self.Datas.filter(f=>{return (f.File_Source==selSource||selSource=="")
        &&(f.File_Source.toLowerCase().indexOf(keyWord)>-1||f.File_type.toLowerCase().indexOf(keyWord)>-1||f.File_Date.toLowerCase().indexOf(keyWord)>-1
        ||f.UpLoad_Date.toLowerCase().indexOf(keyWord)>-1||f.UserName.toLowerCase().indexOf(keyWord)>-1)})
        var rowData = [];
        filterData.forEach(eachData => {
            rowData.push('\
            <tr class="">\
                <td date-title="檔案來源">'+eachData.File_Source+'</td>\
                <td date-title="檔案種類">'+eachData.File_type+'</td>\
                <td date-title="檔案時間">'+eachData.File_Date+'</td>\
                <td date-title="上傳日期">'+eachData.UpLoad_Date+'</td>\
                <td date-title="上傳人員">'+eachData.UserName+'</td>\
                <td date-title="功能">\
                    <div class="btnBox">\
                        <button type="button" onclick="UploadPage.onDelete(\''+eachData.File_System+'\',\''+eachData.File_Source+'\',\''+eachData.UpLoad_Date+'\',\''+eachData.File_Date+'\')" class="btn btn-danger btn-small">\
                            刪除\
                        </button>\
                    </div>\
                </td>\
            </tr>');
        });
        $("#tBody").html(rowData.join(""));
    }
    return self;
}