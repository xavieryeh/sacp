function getTaiwanDrugAnalysischart(id, div) {
    const chartID = id + (div.indexOf('popup') > -1 ? "_popup" : "");
    var startdate = '2020/01';
    enddate = '2021/05';
    var breadcrumbtitle = document.getElementById("breadcrumbtitle");
    var title = document.getElementById("title");
    var bannerhref = document.getElementById("bannerhref");
    var source = document.getElementById("source");
    var selectYear = document.getElementById("selectYear");
    var selectMonth = document.getElementById("selectMonth");
    var selectStartYear = document.getElementById("selectStartYear");
    var selectEndYear = document.getElementById("selectEndYear");
    var sYear = moment().format('YYYY') -2  +"/"+ moment().format('MM');
    var eYear = moment().format('YYYY/MM');
    var sDate = moment().format('YYYY') + "/01";
    var eDate = moment().format('YYYY/MM');
    $("#SelectCorrectionLocationeTubebyYearTable").css("margin-top", -45 + "px");
    $("#SelectLocationeTubebyYearTable").css("margin-top", -45 + "px");
    $("#SelectCorrectionLocationeTubebyYear_popupTable").css("margin-top", -45 + "px");
    $("#SelectLocationeTubebyYear_popupTable").css("margin-top", -45 + "px");
    var reg = /^\d+$/;
    if (selectYear && selectMonth) {
        if (selectYear.value.match(reg) && selectMonth.value.match(reg)) {
            sDate = (parseInt(selectYear.value) + 1911).toString() + "/01";
            eDate = (parseInt(selectYear.value) + 1911).toString() + "/" + selectMonth.value;
        }
    };
    if (selectStartYear && selectEndYear) {
        if (selectStartYear.value.match(reg) && selectEndYear.value.match(reg)) {
            sYear = (parseInt(selectStartYear.value) + 1911).toString() +'/'+ moment().format('MM')
            eYear = (parseInt(selectEndYear.value) + 1911).toString() +'/'+ moment().format('MM')
        }
    };

    function getTextCss(font_size) {
        var scale = 1;
        if (font_size < 8)
            scale = font_size / 8;
        else if(font_size>12)
            font_size = 12;
        return {
            "font-size": font_size + 'px',
            "transform-origin": "0",
            "white-space": "nowrap",
            "transform": "scale(" + scale + ")"
        };
    };
    var chartParentName = ".card-item";
    if (title && breadcrumbtitle && source && bannerhref)
        chartParentName = ".largechart";
    else if (document.getElementsByClassName("content-index").length > 0)
        chartParentName = ".main-card"
    switch (id) {
        //一、全國藥癮個案統計(未校正數)
        case "SelectLocationeTubeMonth":
            $.getLocationeTubeMonth(sDate, eDate, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {
                    //am4core.addLicense("{{config('custom.ChartsPN')}}");
                    var Params = {};
                    var lstItem = [];
                    Params.Data = [];
                    Params.Color = "#7030A0";
                    Params.ColumnsWidth=30;
                    var tableBodyRow = {
                        title: ' ,' + Params.Color
                    };
                    Params.Title = "一、全國藥癮個案統計(未校正數)";
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.href = "TaiwanDrugAnalysis";
                        source.innerHTML = "來源：衛福部毒癮單窗服務系統";
                        bannerhref.innerHTML = "全國藥癮個案統計分析";
                    }
                    if (data.length > 0) {
                        data.forEach((element, index) => {
                            Params.Data.push({
                                category: element["Location"],
                                count: element["Num"]
                            })
                            lstItem.push(element["Location"]);
                            tableBodyRow["Location" + (index)] = element["Num"];
                        });
                        var rank = data.filter((each) => {
                            return each.Location == "高雄市"
                        })[0];
                        if (rank == undefined)
                            rank = {
                                Rank: "無",
                                Rank_6: "無"
                            };
                        document.getElementById(chartID + "Text").innerHTML = "註：高雄市全國排名第" + rank.Rank + "，六都排名第" + rank.Rank_6;
                        var newChart = amchartfunc(chartID, Params, "Column");
                        setTimeout(() => {
                            var aaTable = new ChartTable(chartID + "Table");
                            aaTable.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                            aaTable.bodyWidth = newChart.plotContainer.contentWidth + 1;
                            aaTable.dataSource = [tableBodyRow];
                            aaTable.columnSource = [""].concat(lstItem);
                            aaTable.vertical = true;
                            aaTable.bind();
                            $("#" + chartID).height(divHeight - aaTable.Height - 40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                        }, 100);
                    }
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });
                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
                }, 100);
            });
            break;
        case "SelectLocationeTubebyYear":
            $.getLocationeTubebyYear(sYear, eYear, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {
                    //am4core.addLicense("{{config('custom.ChartsPN')}}");
                    Params = {};
                    Params.ChartType = "GroupColumnBar";
                    Params.Title = "一、全國藥癮個案統計(未校正數)";
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.href = "TaiwanDrugAnalysis";
                        source.innerHTML = "來源：衛福部毒癮單窗服務系統";
                        bannerhref.innerHTML = "全國藥癮個案統計分析";
                    }
                    Params.TitleColor = "#00B48B";
                    Params.Color = ["#0031CC", "#ff0000", "#0dd3c4"];
                    Params.FontSize = 10;
                    Params.categoryAxisVisible = false;
                    Params.Location = [];
                    Params.Year = [];
                    var lstItem = [];
                    var tableBodyRow = [];
                    data.forEach(element => {
                        Params.Year.push(element["Year"]);
                        Params.Location.push(element["Location"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.Location = [...new Set(Params.Location)];
                    Params.Year.sort();
                    lstItem = Params.Location;
                    Params.Data = Params.Location.map(function (location, iItem) {
                        var newData = {};
                        newData.category = location;
                        data.filter((each) => {
                            return each.Location == location
                        }).forEach(function (each, ieach) {
                            newData[each.Year] = each.Num;
                        });
                        newData.color = Params.Color;
                        return newData;
                    });
                    tableBodyRow = Params.Year.map(function (element, index) {
                        var newData = {};
                        newData.title = element + "年" + ',' + Params.Color[index];
                        data.filter((each) => {
                            return each.Year == element
                        }).forEach(function (each, ieach) {
                            newData["Location" + (ieach)] = each.Num;
                        });
                        return newData;
                    });
                    var rank = data.filter((each) => {
                        return each.Location == "高雄市"
                    })[0];
                    if (rank == undefined)
                        rank = {
                            Rank: "無",
                            Rank_6: "無"
                        };
                    document.getElementById(chartID+"Text").innerHTML = "註：高雄市全國排名第" + rank.Rank + "，六都排名第" + rank.Rank_6;
                    var newChart = amchartfunc(chartID, Params, "GroupColumnBar");
                    setTimeout(() => {
                        var aaTable = new ChartTable(chartID+"Table");
                        aaTable.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                        aaTable.bodyWidth = newChart.plotContainer.contentWidth + 1;
                        aaTable.dataSource = tableBodyRow;
                        aaTable.columnSource = [""].concat(lstItem);
                        aaTable.vertical = true;
                        aaTable.bind();
                        $("#" + chartID).height(divHeight - aaTable.Height-40);
                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                    }, 100);
                })
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });
                setTimeout(() => {
                    var aaTable = getChartTable(chartID+"Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
                }, 100);
            });
            break;
            //二、全國藥癮個案統計(校正數)
        case "SelectCorrectionLocationeTubeMonth":
            $.getCorrectionLocationeTubeMonth(sDate, eDate, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('large') > -1 ? 30 : 0)- (div.indexOf('popup') > -1 ? 80 : 0);
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }

                am4core.ready(function () {
                    //am4core.addLicense("{{config('custom.ChartsPN')}}");
                    var Params = {};
                    var lstItem = [];
                    Params.Data = [];
                    Params.ColumnsWidth=30;
                    Params.Color = "#7030A0";
                    var tableBodyRow = {
                        title: "校正數" + ',' + Params.Color
                    };
                    Params.Title = "二、全國藥癮個案統計(校正數)";
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.href = "TaiwanDrugAnalysis";
                        source.innerHTML = "來源：衛福部毒癮單窗服務系統";
                        bannerhref.innerHTML = "全國藥癮個案統計分析";
                    }
                    if (data.length > 0) {
                        data.forEach((element, index) => {
                            Params.Data.push({
                                category: element["Location"],
                                count: Math.round(element["Num"])
                            })
                            lstItem.push(element["Location"]);
                            tableBodyRow["Location" + (index)] = Math.round(element["Num"]).toString();
                        });
                        var rank = data.filter((each) => {
                            return each.Location == "高雄市"
                        })[0];
                        if (rank == undefined)
                            rank = {
                                Rank: "無",
                                Rank_6: "無"
                            };
                        document.getElementById(chartID + "Text").innerHTML = "註：1. 高雄市全國排名第" + rank.Rank + "，六都排名第" + rank.Rank_6 + "<br>2. 【校正數為(該區列管數/該區人口數)×100,000】";

                        var newChart = amchartfunc(chartID, Params, "Column");
                        setTimeout(() => {
                            var aaTable = new ChartTable(chartID + "Table");
                            aaTable.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                            aaTable.bodyWidth = newChart.plotContainer.contentWidth + 1;
                            aaTable.dataSource = [tableBodyRow];
                            aaTable.columnSource = [""].concat(lstItem);
                            aaTable.vertical = true;
                            aaTable.bind();
                            $("#" + chartID).height(divHeight - aaTable.Height - 40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                        }, 100);
                    }
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
                }, 100);
            });
            break;
        case "SelectCorrectionLocationeTubebyYear":
            $.getCorrectionLocationeTubebyYear(sYear, eYear, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height() -(div.indexOf('large') > -1 ? 30 : 0)- (div.indexOf('popup') > -1 ? 80 : 0);
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {
                    //am4core.addLicense("{{config('custom.ChartsPN')}}");
                    Params = {};
                    Params.ChartType = "GroupColumnBar";
                    Params.Title = "二、全國藥癮個案統計(校正數)";
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.href = "TaiwanDrugAnalysis";
                        source.innerHTML = "來源：衛福部毒癮單窗服務系統";
                        bannerhref.innerHTML = "全國藥癮個案統計分析";
                    }
                    Params.TitleColor = "#00B48B";
                    Params.Color = ["#0031CC", "#ff0000", "#0dd3c4"];
                    Params.FontSize = 10;
                    Params.categoryAxisVisible = false;
                    Params.Location = [];
                    Params.Year = [];
                    var lstItem = [];
                    var tableBodyRow = [];
                    data.forEach(element => {
                        Params.Year.push(element["Year"]);
                        Params.Location.push(element["Location"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.Location = [...new Set(Params.Location)];
                    lstItem = Params.Location;
                    Params.Year.sort();
                    Params.Data = Params.Location.map(function (location, iItem) {
                        var newData = {};
                        newData.category = location;
                        data.filter((each) => {
                            return each.Location == location
                        }).forEach(function (year, iyear) {
                            newData[year.Year] = year.Num;
                        });
                        newData.color = Params.Color;
                        return newData;
                    });
                    tableBodyRow = Params.Year.map(function (element, index) {
                        var newData = {};
                        newData.title = element + "年" + ',' + Params.Color[index];
                        data.filter((each) => {
                            return each.Year == element
                        }).forEach(function (each, ieach) {
                            newData["Location" + (ieach)] = Math.round(each.Num).toString();
                        });
                        return newData;
                    });
                    var rank = data.filter((each) => {
                        return each.Location == "高雄市"
                    })[0];
                    if (rank == undefined)
                        rank = {
                            Rank: "無",
                            Rank_6: "無"
                        };
                    document.getElementById(chartID + "Text").innerHTML = "註：1. 高雄市全國排名第" + rank.Rank + "，六都排名第" + rank.Rank_6 + "<br>2. 【校正數為(該區列管數/該區人口數)×100,000】";
                    var newChart = amchartfunc(chartID, Params, Params.ChartType);
                    setTimeout(() => {
                        var aaTable = new ChartTable(chartID + "Table");
                        aaTable.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                        aaTable.bodyWidth = newChart.plotContainer.contentWidth + 1;
                        aaTable.dataSource = tableBodyRow;
                        aaTable.columnSource = [""].concat(lstItem);
                        aaTable.vertical = true;
                        aaTable.bind();
                        $("#" + chartID).height(divHeight - aaTable.Height - 40);
                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                    }, 100);
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });
                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
                }, 100);
            });
            break;
    }
};
