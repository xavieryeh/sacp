function UploadPage(self){
    var selectPage=1;
    var TotalCount;

    self.init = function(){
        TotalCount = self.Datas.length;
        setPageList(1);
        searchData();
    }
    //各項刪除事件
    self.onDelete = function(Case_ID){
        $("#delKeyValue").val(Case_ID);
        $("#delKey").html("<br/>個案:"+Case_ID);
        $('#delModal').modal('show');
    }
    //各項刪除確認事件
    $("#btnDelConfirm").on('click', function () {
        $.PostAJAX("Upload_uploadLogDelVRDate", {type:self.Type,keyValue:$("#delKeyValue").val()}, response => {
            self.Datas = response;
            searchData();
            $('#delModal').modal('hide');
        });
    });
    // 查詢按鈕事件
    $("#searchData").on('click', function () {
        searchData();
        setPageList(1);
    });

    //選取頁數事件
    self.changePage = function(page) {
        if(page=="pre"){
            if(selectPage==1)
                return;
            else{
                setPageList(parseInt(selectPage)-1);
            }
        }
        else if(page=="next"){
            if(selectPage==Math.ceil(TotalCount/parseInt($("#selShowCount option:selected")[0].value)))
                return;
            else{
                setPageList(parseInt(selectPage)+1);
            }
        }
        else{
            setPageList(parseInt(page));
        }
        searchData();
    }

    //取得頁數並產生頁數
    function setPageList(currentPage){
        selectPage=currentPage;
        const totalPage = Math.ceil(TotalCount / parseInt($("#selShowCount option:selected")[0].value));
        const startPage = (parseInt((currentPage-1)/10)*10)+1;
        const endPage = (startPage+9)>=totalPage?totalPage:(startPage+9);
        var PageList = ['<li class="page-item"><a id="btnPrePage" class="page-link" onclick="UploadPage.changePage(\'pre\')">上一項</a></li>'];
        for (var i = startPage; i < endPage+1; ++i) {
            PageList.push('<li class="page-item'+(i==currentPage?' active':'')+'" aria-current="page"><a class="page-link" onclick="UploadPage.changePage('+i.toString()+')">'+i.toString()+'</a></li>');
        }
        PageList.push('<li class="page-item"><a id="btnNextPage" class="page-link" onclick="UploadPage.changePage(\'next\')">下一項</a></li>');
        $('#ulPageList').html(PageList.join(''));
    }

    $("#selShowCount").on('change', function() {
        setPageList(1);
        searchData();
    });

    //個案管理資料查詢
    function searchData(){
        // iAgeStart iAgeEnd iLevel iKeyWord $("#selShowCount option:selected")[0].value selectPage
        const ageStart = parseInt($("#iAgeStart").val().trim()==""?"0":$("#iAgeStart").val().trim());
        const ageEnd = parseInt($("#iAgeEnd").val().trim()==""?"200":$("#iAgeEnd").val().trim());
        const level = $("#iLevel option:selected")[0].value;
        const keyWord = $("#iKeyWord").val().toLowerCase().trim();
        const showCount = parseInt($("#selShowCount option:selected")[0].value);
        const filterData = self.Datas.filter(f=>{return f.File_Age>=ageStart&&f.File_Age<=ageEnd&& (f.Level==level||level=='-1')
        &&(f.Case_ID.toLowerCase().indexOf(keyWord)>-1||f.SourceNo.toLowerCase().indexOf(keyWord)>-1||f.Gender.toLowerCase().indexOf(keyWord)>-1
        ||f.File_Age.toLowerCase().indexOf(keyWord)>-1||f.Level.toLowerCase().indexOf(keyWord)>-1||f.States.toLowerCase().indexOf(keyWord)>-1
        ||f.CaseType.toLowerCase().indexOf(keyWord)>-1||f.Manage_Type.toLowerCase().indexOf(keyWord)>-1)});
        const filterDataByPage = filterData.filter((f,i)=>{return (i+1)>=((selectPage-1)*showCount+1)&&(i+1)<=(selectPage*showCount)});

        var rowData = [];
        filterDataByPage.forEach(eachData=>{
            rowData.push('\
            <tr class="js-KSarea_page_ownTeacher">\
                <td date-title="個案(去識別化)">'+eachData.Case_ID+'</td>\
                <td date-title="來源別">'+eachData.SourceNo+'</td>\
                <td date-title="性別">'+eachData.Gender+'</td>\
                <td date-title="年齡">'+eachData.File_Age+'</td>\
                <td date-title="毒品級數">'+eachData.Level+'</td>\
                <td date-title="列管(Y/N)">'+eachData.States+'</td>\
                <td date-title="初案/再案(1/2)">'+eachData.CaseType+'</td>\
                <td date-title="類型">'+eachData.Manage_Type+'</td>\
                <td date-title="功能">\
                    <div class="btnBox">\
                        <button type="button" onclick="UploadPage.onDelete(\''+eachData.Case_ID+'\')" class="btn btn-danger btn-small">\
                            刪除\
                        </button>\
                    </div>\
                </td>\
            </tr>');
        });
        $("#tBody").html(rowData.join(""));
        TotalCount = filterData.length;
        $("#sTotalCount").text(TotalCount.toString());
    }
    return self;
}
