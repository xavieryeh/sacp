function amchartfunc(id, Params, type,charSeries) {
    var valueAxisFontSize="10px";
    switch (type) {
        case "Group":
            // am4core.ready(function () {
                // Themes begin
                am4core.options.autoDispose = true;
                // Themes end
                // Create chart instance
                var chart = am4core.create(id, am4charts.XYChart);
                chart.legend = new am4charts.Legend();
                chart.legend.useDefaultMarker = true;
                chart.legend.disabled = true;
                chart.maskBullets = false;
                chart.data = Params.Data;
                chart.plotContainer.background.strokeWidth = 1;
                chart.plotContainer.background.strokeOpacity = 1;
                chart.plotContainer.background.stroke = am4core.color("#000");
                //Create title 2
                if (Params.Title2 !== undefined) {
                    var topContainer = chart.chartContainer.createChild(am4core.Container);
                    topContainer.layout = "absolute";
                    topContainer.toBack();
                    // topContainer.paddingBottom = 15;
                    topContainer.width = am4core.percent(100);
                    var dateTitle = topContainer.createChild(am4core.Label);
                    dateTitle.text = Params.Title2.title;
                    dateTitle.fontSize = Params.Title2.fonSize;
                    dateTitle.fill = am4core.color(Params.Title2.color);
                    dateTitle.align = Params.Title2.align;
                    dateTitle.dy = Params.Title2.dy;
                    dateTitle.fontWeight = 400;
                }
                // Create axes
                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                //categoryAxis.renderer.minGridDistance = 150;
                categoryAxis.dataFields.category = "category";
                categoryAxis.renderer.grid.template.disabled = true;
                categoryAxis.cursorTooltipEnabled = false;
                categoryAxis.renderer.labels.template.disabled = true;
                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.cursorTooltipEnabled = false;
                valueAxis.renderer.labels.template.fontSize=valueAxisFontSize;
                if(Params.ForSixB==true)
                    valueAxis.numberFormatter.numberFormat = "#'%'";
                var iKey = 0;
                var keyFilter = Object.keys(Params.Data[0]).filter(function (key) { return key != "category" && key.indexOf("color") == -1 && key.indexOf("Rate") == -1 });
                keyFilter.forEach(function (key) {
                // if(key!="category" && key.indexOf("color")==-1){
                var series = chart.series.push(new am4charts[charSeries]());
                series.dataFields.valueY = key;
                series.dataFields.categoryX = "category";
                series.dataFields.percent = key+"_Rate";
                series.name = key;
                // series.Stacked =
                // series.tooltipText = "{name}: [bold]{valueY}[/]";
                series.stroke = series.fill =am4core.color(Params.Data[0]["color"][iKey]); // fill;
                if(charSeries == "LineSeries"){
                    var bullet = series.bullets.push(new am4charts.CircleBullet());
                    series.strokeWidth=4;
                    bullet.circle.strokeWidth  =1;
                    if(Params.Max !==undefined)
                        valueAxis.max  = Params.Max;
                }
                else{
                    if(Params.ColumnsWidth!==undefined){
                        series.columns.template.width=am4core.percent(Params.ColumnsWidth);
                    }
                    if(Params.ShowPercent!==undefined){
                        let labelBullet = series.bullets.push(new am4charts.LabelBullet());
                        labelBullet.label.text = "{percent}%";
                        labelBullet.label.align="middle";
                        labelBullet.label.fontSize=12;
                        labelBullet.label.dy=-15;
                        labelBullet.label.truncate=false;
                        labelBullet.label.wrap =false;
                    }
                    if(Params.ForSixB==true){
                        let labelBullet = series.bullets.push(new am4charts.LabelBullet());
                        labelBullet.label.text = "{valueY}";
                        labelBullet.label.align="middle";
                        labelBullet.label.fontSize=12;
                        labelBullet.label.dy=-15;
                        labelBullet.label.truncate=false;
                        labelBullet.label.wrap =false;
                    }
                }
                ++iKey;
                });


                // Add cursor
                chart.cursor = new am4charts.XYCursor();
                // Make chart not mask the bullets

                return chart;
            // }); // end am4core.ready()
            break;
        case "LineGroup":
                // Themes begin

                am4core.options.autoDispose = true;
                // Themes end
                // Create chart instance
                var chart = am4core.create(id, am4charts.XYChart);
                chart.legend = new am4charts.Legend();
                chart.legend.useDefaultMarker = true;
                chart.legend.disabled = true;
                chart.plotContainer.background.strokeWidth = 1;
                chart.plotContainer.background.strokeOpacity = 1;
                chart.plotContainer.background.stroke = am4core.color("#000");
                //chart.colors.list  =Params.ColorList;
                chart.data = Params.Data;
                chart.maskBullets = false;
                //Create title 2
                if (Params.Title2 !== undefined) {
                    var topContainer = chart.chartContainer.createChild(am4core.Container);
                    topContainer.layout = "absolute";
                    topContainer.toBack();
                    // topContainer.paddingBottom = 15;
                    topContainer.width = am4core.percent(100);
                    var dateTitle = topContainer.createChild(am4core.Label);
                    dateTitle.text = Params.Title2.title;
                    dateTitle.fontSize = Params.Title2.fonSize;
                    dateTitle.fill = am4core.color(Params.Title2.color);
                    dateTitle.align = Params.Title2.align;
                    dateTitle.dy = Params.Title2.dy;
                    dateTitle.fontWeight = 400;
                }
                // Create axes
                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "category";
                categoryAxis.renderer.grid.template.disabled = true;
                categoryAxis.renderer.labels.template.disabled = true;
                categoryAxis.cursorTooltipEnabled = false;
                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.renderer.labels.template.fontSize=valueAxisFontSize;
                valueAxis.cursorTooltipEnabled = false;
                var iKey = 0;
                var keyFilter = Object.keys(Params.Data[0]).filter(function (key) { return key != "category" && key.indexOf("color") == -1 && key !="總計"});
                keyFilter.forEach(function (key) {
                var series = chart.series.push(new am4charts[charSeries]());
                series.dataFields.valueY = key;
                series.dataFields.categoryX = "category";
                series.name = key;
                series.strokeWidth=4;
                series.tooltip.getFillFromObject = false;
                // series.tooltipText = "{name}: [bold]{valueY}[/]";
                if(charSeries == "LineSeries"){
                    var bullet = series.bullets.push(new am4charts.CircleBullet());
                    bullet.circle.strokeWidth  =1;
                    bullet.fill =series.tooltip.background.fill=series.stroke = am4core.color(Params.Data[0]["color"][iKey]); // fill
                }else{
                    series.columns.template.fill = series.tooltip.background.fill=series.stroke =am4core.color(Params.Data[0]["color"][iKey]); // fill
                }
                ++iKey;
                });
                //Create Yaxes
                var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis2.renderer.opposite = true;
                valueAxis2.tooltip.disabled = true;
                valueAxis2.cursorTooltipEnabled = false;
                valueAxis2.renderer.labels.template.fontSize=valueAxisFontSize;
                // Create total Series
                var totalseries = chart.series.push(new am4charts.LineSeries());
                totalseries.dataFields.valueY = "總計";
                totalseries.dataFields.categoryX = "category";
                totalseries.name = "總計";
                totalseries.yAxis = valueAxis2;
                totalseries.strokeWidth=4;
                totalseries.stroke = totalseries.fill=am4core.color(Params.Data[0]["color"][5]);
                // totalseries.tooltipText = "{name}: [bold]{valueY}[/]";
                var bullet = totalseries.bullets.push(new am4charts.CircleBullet());
                // Add cursor
                valueAxis2.syncWithAxis = valueAxis; //同步圖表中橫向的線
                chart.cursor = new am4charts.XYCursor();
                chart.cursor.xAxis = categoryAxis;
                return chart;
            // }); // end am4core.ready()
            break;
        case "LineGroupNoCircle":
            // am4core.ready(function () {
                // Themes begin

                am4core.options.autoDispose = true;
                // Themes end
                // Create chart instance
                var chart = am4core.create(id, am4charts.XYChart);
                chart.legend = new am4charts.Legend();
                chart.legend.useDefaultMarker = true;
                chart.legend.disabled = true;
                chart.data = Params.Data;
                chart.plotContainer.background.strokeWidth = 1;
                chart.plotContainer.background.strokeOpacity = 1;
                chart.plotContainer.background.stroke = am4core.color("#000");
                //Create title 2
                if (Params.Title2 !== undefined) {
                    var topContainer = chart.chartContainer.createChild(am4core.Container);
                    topContainer.layout = "absolute";
                    topContainer.toBack();
                    // topContainer.paddingBottom = 15;
                    topContainer.width = am4core.percent(100);
                    var dateTitle = topContainer.createChild(am4core.Label);
                    dateTitle.text = Params.Title2.title;
                    dateTitle.fontSize = Params.Title2.fonSize;
                    dateTitle.fill = am4core.color(Params.Title2.color);
                    dateTitle.align = Params.Title2.align;
                    dateTitle.dy = Params.Title2.dy;
                    dateTitle.fontWeight = 400;
                }
                // Create axes
                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "category";
                categoryAxis.renderer.grid.template.disabled = true;
                categoryAxis.renderer.labels.template.disabled = true;
                categoryAxis.cursorTooltipEnabled = false;
                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.renderer.labels.template.fontSize=valueAxisFontSize;
                valueAxis.cursorTooltipEnabled = false;
                var iKey = 0;
                var keyFilter = Object.keys(Params.Data[0]).filter(function (key) { return key != "category" && key.indexOf("color") == -1 && key !="總計"});
                keyFilter.forEach(function (key) {
                // if(key!="category" && key.indexOf("color")==-1){
                var series = chart.series.push(new am4charts[charSeries]());
                series.dataFields.valueY = key;
                series.dataFields.categoryX = "category";
                series.name = key;
                // series.tooltipText = "{name}: [bold]{valueY}[/]";
                ++iKey;
                });
                //Create Yaxes
                var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis2.renderer.opposite = true;
                valueAxis2.tooltip.disabled = true;
                valueAxis2.cursorTooltipEnabled = false;
                valueAxis2.renderer.labels.template.fontSize=valueAxisFontSize;
                // Create total Series
                var totalseries = chart.series.push(new am4charts.LineSeries());
                totalseries.dataFields.valueY = "總計";
                totalseries.dataFields.categoryX = "category";
                totalseries.name = "總計";
                totalseries.yAxis = valueAxis2;
                // totalseries.tooltipText = "{name}: [bold]{valueY}[/]";
                var bullet = totalseries.bullets.push(new am4charts.CircleBullet());
                // Add cursor
                chart.cursor = new am4charts.XYCursor();
                chart.cursor.xAxis = categoryAxis;
                return chart;
            // }); // end am4core.ready()
        break;
        case "Line":

                am4core.options.autoDispose = true;
                // Themes end
                // Create chart instance
                var chart = am4core.create(id, am4charts.XYChart);
                chart.legend = new am4charts.Legend();
                chart.legend.useDefaultMarker = true;
                chart.legend.disabled = true;
                chart.data = Params.Data;
                chart.maskBullets = false;
                chart.plotContainer.background.strokeWidth = 1;
                chart.plotContainer.background.strokeOpacity = 1;
                chart.plotContainer.background.stroke = am4core.color("#000");
                //Create title 2
                if (Params.Title2 !== undefined) {
                    var topContainer = chart.chartContainer.createChild(am4core.Container);
                    topContainer.layout = "absolute";
                    topContainer.toBack();
                    // topContainer.paddingBottom = 15;
                    topContainer.width = am4core.percent(100);
                    var dateTitle = topContainer.createChild(am4core.Label);
                    dateTitle.text = Params.Title2.title;
                    dateTitle.fontSize = Params.Title2.fonSize;
                    dateTitle.fill = am4core.color(Params.Title2.color);
                    dateTitle.align = Params.Title2.align;
                    dateTitle.dy = Params.Title2.dy;
                    dateTitle.fontWeight = 400;
                }
                // Create axes
                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "category";
                categoryAxis.renderer.grid.template.disabled = true;
                categoryAxis.renderer.labels.template.disabled = true;
                categoryAxis.cursorTooltipEnabled = false;
                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.renderer.labels.template.fontSize=valueAxisFontSize;
                valueAxis.cursorTooltipEnabled = false;
                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "count";
                series.dataFields.categoryX = "category";
                series.strokeWidth = 4;
                series.minBulletDistance = 10;
                series.stroke = series.fill = am4core.color(Params.Color);
                // series.tooltipText ="[bold]{categoryX}:[/] {valueY}\n";
                if(Params.ShowPercent!==undefined){
                    let labelBullet = series.bullets.push(new am4charts.LabelBullet());
                    labelBullet.label.text = "{valueY}%";
                    labelBullet.label.align="middle";
                    labelBullet.label.fontSize=12;
                    labelBullet.label.dy=-15;
                    labelBullet.label.truncate=false;
                    labelBullet.label.wrap =false;
                    labelBullet.label.fill=am4core.color(Params.Color);
                    // series.tooltipText ="[bold]{categoryX}:[/] {valueY}%\n";
                }

                series.tooltip.pointerOrientation = "vertical";
                if(charSeries !="Nocircle"){
                    var bullet = series.bullets.push(new am4charts.CircleBullet());
                }
                // Add cursor
                chart.cursor = new am4charts.XYCursor();
                chart.cursor.xAxis = categoryAxis;
            return chart;
        break;
        case "Column":

            am4core.options.autoDispose = true;
            // Themes end
            // Create chart instance
            var chart = am4core.create(id, am4charts.XYChart);
            chart.legend = new am4charts.Legend();
            chart.legend.useDefaultMarker = true;
            chart.legend.disabled = true;
            chart.data = Params.Data;
            chart.plotContainer.background.strokeWidth = 1;
            chart.plotContainer.background.strokeOpacity = 1;
            chart.plotContainer.background.stroke = am4core.color("#000");
            //Create title 2
            if (Params.Title2 !== undefined) {
                var topContainer = chart.chartContainer.createChild(am4core.Container);
                topContainer.layout = "absolute";
                topContainer.toBack();
                // topContainer.paddingBottom = 15;
                topContainer.width = am4core.percent(100);
                var dateTitle = topContainer.createChild(am4core.Label);
                dateTitle.text = Params.Title2.title;
                dateTitle.fontSize = Params.Title2.fonSize;
                dateTitle.fill = am4core.color(Params.Title2.color);
                dateTitle.align = Params.Title2.align;
                dateTitle.dy = Params.Title2.dy;
                dateTitle.fontWeight = 400;
            }
            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            //dateAxis.renderer.minGridDistance = 50;
            categoryAxis.dataFields.category = "category";
            categoryAxis.renderer.grid.template.disabled = true;
            categoryAxis.renderer.labels.template.disabled = true;
            categoryAxis.cursorTooltipEnabled = false;
            // if(Params.ColumnsWidth!==undefined){
            //     categoryAxis.renderer.cellStartLocation = 1-Params.ColumnsWidth
            //     categoryAxis.renderer.cellEndLocation = Params.ColumnsWidth
            // }
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.labels.template.fontSize=valueAxisFontSize;
            valueAxis.cursorTooltipEnabled = false;
            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "count";
            series.dataFields.categoryX = "category";
            series.strokeWidth = 4;
            series.minBulletDistance = 50;
            series.stroke = series.fill = am4core.color(Params.Color);
            // series.tooltipText = "[bold]{categoryX}:[/] {valueY}\n";
            // series.tooltip.pointerOrientation = "vertical";
            if(Params.ColumnsWidth!==undefined){
                series.columns.template.width=am4core.percent(Params.ColumnsWidth);
            }
            // Add cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.xAxis = categoryAxis;
            return chart;
        case "GroupColumnBar":
                // Create chart instance
                var chart = am4core.create(id, am4charts.XYChart);
                am4core.options.autoDispose = true;
                chart.plotContainer.background.strokeWidth = 1;
                chart.plotContainer.background.strokeOpacity = 1;
                chart.plotContainer.background.stroke = am4core.color("#000");
                // Add data
                chart.data = Params.Data;
                //Create title 2
                if (Params.Title2 !== undefined) {
                    var topContainer = chart.chartContainer.createChild(am4core.Container);
                    topContainer.layout = "absolute";
                    topContainer.toBack();
                    // topContainer.paddingBottom = 15;
                    topContainer.width = am4core.percent(100);
                    var dateTitle = topContainer.createChild(am4core.Label);
                    dateTitle.text = Params.Title2.title;
                    dateTitle.fontSize = Params.Title2.fonSize;
                    dateTitle.fill = am4core.color(Params.Title2.color);
                    dateTitle.align = Params.Title2.align;
                    dateTitle.dy = Params.Title2.dy;
                    dateTitle.fontWeight = 400;
                }
                // Create title

                // Create axes
                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "category";
                categoryAxis.renderer.grid.template.disabled = true;
                categoryAxis.cursorTooltipEnabled = false;
                // categoryAxis.title.text = "Local country offices";
                categoryAxis.renderer.grid.template.location = 0;
                // categoryAxis.renderer.minGridDistance = 20;
                // categoryAxis.renderer.labels.template.maxWidth = 50;
                // categoryAxis.renderer.labels.template.dy = -10;
                // categoryAxis.renderer.labels.template.fontSize = Params.FontSize - 2;
                // categoryAxis.renderer.cellStartLocation = Params.cellStartLocation||0.2
                // categoryAxis.renderer.cellEndLocation = (1-Params.cellStartLocation)||0.8
                categoryAxis.renderer.labels.template.disabled = true;
                // categoryAxis.renderer.grid.template.disabled = !Params.categoryAxisVisible;i

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                // valueAxis.renderer.labels.template.dx = -15;
                valueAxis.cursorTooltipEnabled = false;
                valueAxis.renderer.labels.template.fontSize = Params.FontSize;
                // valueAxis.title.text = "Expenditure (M)";
                var iKey = 0;
                var keyFilter = Object.keys(Params.Data[0]).filter(function (key) { return key != "category" && key.indexOf("color") == -1 });

                keyFilter.forEach(function (key) {

                    // if(key!="category" && key.indexOf("color")==-1){
                    var series = chart.series.push(new am4charts.ColumnSeries());
                    series.dataFields.valueY = key;
                    series.dataFields.categoryX = "category";
                    series.name = key;
                    // series.tooltipText = "{name}: [bold]{valueY}[/]";
                    series.columns.template.strokeWidth = 0;
                    series.columns.template.fill = am4core.color(Params.Data[0]["color"][iKey]); // fill
                    if(Params.ColumnsWidth!==undefined){
                        series.columns.template.width=am4core.percent(Params.ColumnsWidth);
                    }
                    ++iKey;
                    // }
                });
                // Do not try to stack on top of previous series
                // series2.stacked = true;

                // Add cursor
                chart.cursor = new am4charts.XYCursor();
                // chart.leftAxesContainer.width = 100;
                // Add legend
                // chart.legend = new am4charts.Legend();
                return chart;
        break;
        case "GroupStackedColumnBar":
                // Create chart instance
                var chart = am4core.create(id, am4charts.XYChart);
                am4core.options.autoDispose = true;
                chart.plotContainer.background.strokeWidth = 1;
                chart.plotContainer.background.strokeOpacity = 1;
                chart.plotContainer.background.stroke = am4core.color("#000");
                // Add data
                chart.data = Params.Data;
                //Create title 2
                if (Params.Title2 !== undefined) {
                    var topContainer = chart.chartContainer.createChild(am4core.Container);
                    topContainer.layout = "absolute";
                    topContainer.toBack();
                    // topContainer.paddingBottom = 15;
                    topContainer.width = am4core.percent(100);
                    var dateTitle = topContainer.createChild(am4core.Label);
                    dateTitle.text = Params.Title2.title;
                    dateTitle.fontSize = Params.Title2.fonSize;
                    dateTitle.fill = am4core.color(Params.Title2.color);
                    dateTitle.align = Params.Title2.align;
                    dateTitle.dy = Params.Title2.dy;
                    dateTitle.fontWeight = 400;
                }
                // Create axes
                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "category";
                categoryAxis.cursorTooltipEnabled = false;
                // categoryAxis.title.text = "Local country offices";
                categoryAxis.renderer.grid.template.disabled = true;
                // categoryAxis.renderer.grid.template.location = 0;
                // categoryAxis.renderer.minGridDistance = 20;
                // categoryAxis.renderer.labels.template.maxWidth = 50;
                categoryAxis.renderer.labels.template.disabled = true;
                // categoryAxis.renderer.labels.template.dy = -10;
                // categoryAxis.renderer.labels.template.fontSize = Params.FontSize - 2;
                // categoryAxis.renderer.cellStartLocation = Params.cellStartLocation||0.2
                // categoryAxis.renderer.cellEndLocation = (1-Params.cellStartLocation)||0.8
                // categoryAxis.renderer.labels.template.visible = Params.categoryAxisVisible;
                // categoryAxis.renderer.grid.template.disabled = !Params.categoryAxisVisible;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.renderer.labels.template.fontSize=valueAxisFontSize;
                valueAxis.cursorTooltipEnabled = false;
                // valueAxis.renderer.labels.template.dx = -15;
                // valueAxis.renderer.labels.template.fontSize = Params.FontSize;
                // valueAxis.title.text = "Expenditure (M)";
                var iKey = 0;
                var keyFilter = Object.keys(Params.Data[0]).filter(function (key) { return key != "category" && key.indexOf("color") == -1 && key != "group"});

                keyFilter.forEach(function (key) {
                    // if(key!="category" && key.indexOf("color")==-1){
                    var series = chart.series.push(new am4charts.ColumnSeries());
                    series.dataFields.valueY = key;
                    series.dataFields.categoryX = "category";
                    series.name = key;
                    series.stacked = true;
                    // series.tooltipText = "{name}: [bold]{valueY}[/]";
                    series.columns.template.strokeWidth = 0;
                    if(Params.ColumnsWidth!==undefined){
                        series.columns.template.width=am4core.percent(Params.ColumnsWidth);
                    };
                    series.columns.template.fill = am4core.color(Params.Data[0]["color"][iKey]); // fill
                    series.columns.template.adapter.add("fill", function(fill, target) {
                        if(Params.Data[0]["color"].length >3){
                            if(key=="單一")
                                return Params.Data[0]["color"][target.dataItem.index%5];
                            else
                                return am4core.color(Params.Data[0]["color"][target.dataItem.index%5]).lighten(0.5);
                        }
                        else{
                            if(key=="單一")
                                return Params.Data[0]["color"][target.dataItem.index%3];
                            else
                                return am4core.color(Params.Data[0]["color"][target.dataItem.index%3]).lighten(0.5);
                        }
                      });
                    ++iKey;
                    // }
                });
                // Do not try to stack on top of previous series
                // series2.stacked = true;

                // Add cursor
                chart.cursor = new am4charts.XYCursor();
                // chart.leftAxesContainer.width = 100;
                // Add legend
                // chart.legend = new am4charts.Legend();
                return chart;
        break;
        case "StackedColumn":
            // am4core.ready(function () {
                // Themes begin

                am4core.options.autoDispose = true;
                // Themes end
                // Create chart instance
                var chart = am4core.create(id, am4charts.XYChart);
                chart.legend = new am4charts.Legend();
                chart.legend.useDefaultMarker = true;
                chart.legend.disabled = true;
                chart.plotContainer.background.strokeWidth = 1;
                chart.plotContainer.background.strokeOpacity = 1;
                chart.plotContainer.background.stroke = am4core.color("#000");
                chart.maskBullets = false;
                chart.data = Params.Data;

                //Create title 2
                if (Params.Title2 !== undefined) {
                    var topContainer = chart.chartContainer.createChild(am4core.Container);
                    topContainer.layout = "absolute";
                    topContainer.toBack();
                    // topContainer.paddingBottom = 15;
                    topContainer.width = am4core.percent(100);
                    var dateTitle = topContainer.createChild(am4core.Label);
                    dateTitle.text = Params.Title2.title;
                    dateTitle.fontSize = Params.Title2.fonSize;
                    dateTitle.fill = am4core.color(Params.Title2.color);
                    dateTitle.align = Params.Title2.align;
                    dateTitle.dy = Params.Title2.dy;
                    dateTitle.fontWeight = 400;
                }
                // Create axes
                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                //categoryAxis.renderer.minGridDistance = 150;
                categoryAxis.cursorTooltipEnabled = false;
                categoryAxis.renderer.grid.template.disabled = true;
                categoryAxis.dataFields.category = "category";
                categoryAxis.renderer.labels.template.disabled = true;
                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.renderer.labels.template.fontSize=valueAxisFontSize;
                valueAxis.cursorTooltipEnabled = false;
                var iKey = 0;
                var keyFilter = Object.keys(Params.Data[0]).filter(function (key) { return key != "category" && key.indexOf("color") == -1 });
                keyFilter.sort();
                keyFilter.forEach(function (key) {
                // if(key!="category" && key.indexOf("color")==-1){
                var series = chart.series.push(new am4charts[charSeries]());
                series.dataFields.valueY = key;
                series.dataFields.categoryX = "category";
                series.name = key;
                    // if(key=="單一")
                    //     series.stacked=false;
                    // else
                    //     series.stacked=true;
                series.stacked=true;
                // series.tooltipText = "{name}: [bold]{valueY}[/]";
                series.stroke = series.fill =am4core.color(Params.Data[0]["color"][iKey]); // fill;
                series.strokeWidth=0;
                if(Params.ColumnsWidth!==undefined){
                    series.columns.template.width=am4core.percent(Params.ColumnsWidth);
                };
                series.columns.template.adapter.add("fill", function(fill, target) {
                    if(key=="單一")
                        return Params.Data[0]["color"][target.dataItem.index];
                    else
                    {
                        return am4core.color(Params.Data[0]["color"][target.dataItem.index]).lighten(0.5);
                    }
                  });
                if(charSeries == "LineSeries"){
                    var bullet = series.bullets.push(new am4charts.CircleBullet());
                    series.strokeWidth=4;
                    bullet.circle.strokeWidth  =1;
                }
                ++iKey;
                });


                // Add cursor
                chart.cursor = new am4charts.XYCursor();
                chart.cursor.xAxis = categoryAxis;
                // Make chart not mask the bullets

            return chart;
            // }); // end am4core.ready()
        break;
        case "Pie2":
                var chart = am4core.create(id, am4charts.PieChart);
                am4core.options.autoDispose = true;
                // Add data
                chart.data = Params.Data;
                chart.radius = am4core.percent(90);
                chart.dy=-30;
                //Create title 2
                if (Params.Title !== undefined) {
                    var title = chart.titles.create();
                    title.text = Params.Title;
                    title.fontSize = Params.FontSize + 5;
                    title.marginBottom = 5;
                    title.fontWeight = "900";
                    title.fill = am4core.color(Params.TitleColor);
                }
                // Add and configure Series
                var pieSeries = chart.series.push(new am4charts.PieSeries());
                pieSeries.dataFields.value = "count";
                pieSeries.dataFields.category = "category";
                pieSeries.slices.template.propertyFields.fill = "color";
                pieSeries.slices.template.strokeWidth=2
                pieSeries.slices.template.stroke = am4core.color("#fff");
                if(Params.ShowDecimal==true){
                    pieSeries.labels.template.text  = Params.SeriesText || "{category}; {value}; {value.percent.formatNumber('#.00')}%";
                }
                else{
                    pieSeries.labels.template.text =  Params.SeriesText || "{category};{value};{percent.formatNumber('#')}%";
                }
                pieSeries.tooltip.disabled = true;
                pieSeries.labels.template.fontSize = Params.FontSize;
                pieSeries.ticks.template.disabled = true;
                pieSeries.fill ="color";
                pieSeries.alignLabels = false;
                pieSeries.labels.template.radius = am4core.percent(-40);
                pieSeries.labels.template.fill = am4core.color("#fff");
                pieSeries.labels.template.fontSize = 10;
                // pieSeries.labels.template.relativeRotation = 90;
                pieSeries.labels.template.adapter.add("radius", function (radius, target) {
                    if (target.dataItem && (target.dataItem.values.value.percent < 10)) {
                        return am4core.percent(0);
                    }
                    return radius;
                });
                pieSeries.ticks.template.adapter.add("disabled", function (disabled, target) {
                    if (target.dataItem && (target.dataItem.values.value.percent < 10)) {
                        return false;
                    }
                    return disabled;
                });
                pieSeries.labels.template.adapter.add("fill", function (color, target) {
                    if (target.dataItem && (target.dataItem.values.value.percent < 10)) {
                        return am4core.color("#000");
                    }
                    return color;
                });
            return chart;
        break;
        case "LabelBar":
            var chart = am4core.create(id, am4charts.XYChart);
            // Add data
            chart.hiddenState.properties.opacity = 0;
            chart.data = Params.Data;
            chart.zoomOutButton.disabled = true;
            chart.plotContainer.background.strokeWidth = 1;
            chart.plotContainer.background.strokeOpacity = 1;
            chart.plotContainer.background.stroke = am4core.color("#000");
            am4core.options.autoDispose = true;
            chart.paddingLeft = 0;
            // chart.logo.height = -15;
            // Create axes
            if (Params.Title !== undefined) {
                var title = chart.titles.create();
                title.text = Params.Title;
                title.fontSize = Params.FontSize + 5;
                title.marginBottom = 5;
                title.fontWeight = "900";
                title.fill = am4core.color(Params.TitleColor);
            }

            var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "category";
            categoryAxis.cursorTooltipEnabled = false;
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 10;
            categoryAxis.renderer.fontSize = 10;
            categoryAxis.renderer.grid.template.disabled = true;
            categoryAxis.renderer.labels.template.maxWidth = 20;
            // categoryAxis.renderer.labels.template.dx = -23;
            categoryAxis.renderer.labels.template.fontSize = Params.FontSize;
            categoryAxis.renderer.cellStartLocation = 0.2;
            categoryAxis.renderer.cellEndLocation = 0.8;
            // categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
            //     if (target.dataItem && target.dataItem.index & 2 == 2) {
            //         return dy + 25;
            //     }
            //     return dy;
            // });

            var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
            valueAxis.rangeChangeDuration = 500;
            valueAxis.renderer.grid.template.disabled = true;
            valueAxis.renderer.labels.template.dy = -10;
            valueAxis.cursorTooltipEnabled = false;
            valueAxis.renderer.grid.template.disabled = true;
            valueAxis.renderer.labels.template.fontSize = Params.FontSize;;
            // valueAxis.extraMax = 0.1;
            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueX = "count";
            series.dataFields.categoryY = "category";
            series.name = "Count";
            series.stroke = Params.Stroke;
            var columnTemplate = series.columns.template;
            // columnTemplate.width = am4core.percent(30);
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;
            // columnTemplate.tooltipText = "{categoryY}: [bold]{valueX}[/]";
            columnTemplate.fillOpacity = .8;
            columnTemplate.fill = Params.Color;
            series.hiddenState.transitionDuration = 5000;
            series.hiddenState.transitionEasing = am4core.ease.elasticInOut;
            series.calculatePercent = true;
            let labelBullet = series.bullets.push(new am4charts.LabelBullet());
            if(Params.ShowPercent==true)
                labelBullet.label.text = "{valueX}[#ff0000];[/]{valueX.percent.formatNumber('#.00')}%";
            else
                labelBullet.label.text = "{valueX}";
            labelBullet.label.align="right";
            labelBullet.label.fontSize=10;
            labelBullet.label.truncate=false;
            labelBullet.label.wrap =false;
            labelBullet.label.dx=35;
            // var bullet = series.bullets.push(new am4charts.LabelBullet);
            // bullet.label.text = "{valueX}";
            // // bullet.label.rotation = -90;
            // // bullet.locationY = 0.01;
            // bullet.label.dy = -5;
            // bullet.label.fontSize = Params.FontSize - 3;
            // bullet.label.truncate = false;
            // bullet.label.hideOversized = false;

            // chart.leftAxesContainer.width = 125;
        return chart;
        break;
        case "LabelGroupBar":
            var chart = am4core.create(id, am4charts.XYChart);
            // Add data
            chart.hiddenState.properties.opacity = 0;
            chart.data = Params.Data;
            chart.paddingLeft = 0;
            // chart.zoomOutButton.disabled = true;
            chart.legend = new am4charts.Legend()
            chart.legend.position = "absolute";
            chart.legend.x=50;

            chart.legend.y=Params.y;
            var markerTemplate = chart.legend.markers.template;
            markerTemplate.width = 10;
            markerTemplate.height = 10;
            // chart.legend.dy= -100;
            chart.legend.fontSize =  Params.fonSize;
            // chart.legend.labels.template.maxWidth = 95
            chart.paddingTop = am4core.percent(5);
            chart.plotContainer.background.strokeWidth = 1;
            chart.plotContainer.background.strokeOpacity = 1;
            chart.plotContainer.background.stroke = am4core.color("#000");
            // chart.logo.height = -15;
            // Create axes
            if (Params.Title !== undefined) {
                var title = chart.titles.create();
                title.text = Params.Title;
                title.fontSize = Params.FontSize + 5;
                title.marginBottom = 5;
                title.fontWeight = "900";
                title.fill = am4core.color(Params.TitleColor);
            }

            var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "category";
            categoryAxis.cursorTooltipEnabled = false;
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 10;
            categoryAxis.renderer.grid.template.disabled = true;
            categoryAxis.renderer.fontSize = 10;
            categoryAxis.renderer.labels.template.maxWidth = 50;
            // categoryAxis.renderer.labels.template.dx = -23;
            categoryAxis.renderer.labels.template.fontSize = Params.FontSize;
            categoryAxis.renderer.cellStartLocation = 0.2;
            categoryAxis.renderer.cellEndLocation = 0.8;
            // categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
            //     if (target.dataItem && target.dataItem.index & 2 == 2) {
            //         return dy + 25;
            //     }
            //     return dy;
            // });

            var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
            valueAxis.rangeChangeDuration = 500;
            valueAxis.cursorTooltipEnabled = false;
            valueAxis.renderer.grid.template.disabled = true;
            valueAxis.renderer.labels.template.dy = -10;
            valueAxis.renderer.labels.template.fontSize = Params.FontSize;;
            // valueAxis.extraMax = 0.1;
            // Create series
            var iKey=0;
            var keyFilter = Object.keys(Params.Data[0]).filter(function (key) { return key != "category" && key.indexOf("color") == -1 });
            keyFilter.forEach(function (key) {
                // if(key!="category" && key.indexOf("color")==-1){
                var series = chart.series.push(new am4charts.ColumnSeries());
                series.dataFields.valueX = key;
                series.dataFields.categoryY = "category";
                series.name = key;
                // series.tooltipText = "{name}: [bold]{valueX}[/]";
                series.columns.template.strokeWidth = 0;
                series.columns.template.fill = am4core.color(Params.Data[0]["color"][iKey]); // fill
                series.columns.template.width = am4core.percent(60);
                let labelBullet = series.bullets.push(new am4charts.LabelBullet());
                labelBullet.label.text = "{valueX}";
                labelBullet.label.align="right";
                labelBullet.label.fontSize=10;
                labelBullet.label.dx=30;
                labelBullet.label.truncate=false;
                labelBullet.label.wrap =false;
                ++iKey;
                // }
            });

        return chart;
        break;
        case "LineColumn":
                //
                am4core.options.autoDispose = true;
                // Themes end
                // Create chart instance
                var chart = am4core.create(id, am4charts.XYChart);
                chart.legend = new am4charts.Legend();
                chart.legend.useDefaultMarker = true;
                chart.legend.disabled = true;

                chart.data = Params.Data;
                chart.maskBullets = false;
                chart.plotContainer.background.strokeWidth = 1;
                chart.plotContainer.background.strokeOpacity = 1;
                chart.plotContainer.background.stroke = am4core.color("#000");
                //Create title 2
                if (Params.Title2 !== undefined) {
                    var topContainer = chart.chartContainer.createChild(am4core.Container);
                    topContainer.layout = "absolute";
                    topContainer.toBack();
                    // topContainer.paddingBottom = 15;
                    topContainer.width = am4core.percent(100);
                    var dateTitle = topContainer.createChild(am4core.Label);
                    dateTitle.text = Params.Title2.title;
                    dateTitle.fontSize = Params.Title2.fonSize;
                    dateTitle.fill = am4core.color(Params.Title2.color);
                    dateTitle.align = Params.Title2.align;
                    dateTitle.dy = Params.Title2.dy;
                    dateTitle.fontWeight = 400;
                }
                // Create axes
                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "category";
                categoryAxis.renderer.grid.template.disabled = true;
                categoryAxis.cursorTooltipEnabled = false;
                categoryAxis.renderer.labels.template.disabled = true;


                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.cursorTooltipEnabled = false;

                iKey = 0;
                keyFilter = Object.keys(Params.Data[0]).filter(function (key) { return key != "category" && key.indexOf("color") == -1})[0];
                key = keyFilter;
                var series = chart.series.push(new am4charts.ColumnSeries());
                series.dataFields.valueY = key;
                series.dataFields.categoryX = "category";
                series.name = key;
                series.yAxis = valueAxis;
                series.strokeWidth=4;
                series.columns.template.width=am4core.percent(50);
                series.tooltip.getFillFromObject = false;
                // series.tooltipText = "{name}: [bold]{valueY}[/]";
                series.stroke = series.fill=series.columns.template.fill = series.tooltip.background.fill = am4core.color(Params.Data[0]["color"][0])

                //Create Yaxes2
                var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis2.renderer.opposite = true;
                valueAxis2.cursorTooltipEnabled = false;
                valueAxis2.tooltip.disabled = true;

                var iKey = 0;
                var keyFilter = Object.keys(Params.Data[0]).filter(function (key) { return key != "category" && key.indexOf("color") == -1})[1];
                var key = keyFilter;
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = key;
                series.dataFields.categoryX = "category";
                series.name = key;
                series.yAxis = valueAxis2;
                series.strokeWidth=4;
                series.tooltip.getFillFromObject = false;
                series.stroke = am4core.color(Params.Data[0]["color"][1]); // fill
                // series.tooltipText = "{name}: [bold]{valueY}[/]";
                if(Params.LineNoCircle==undefined){
                    var bullet = series.bullets.push(new am4charts.CircleBullet());
                    bullet.circle.strokeWidth  =1;
                    bullet.fill =series.tooltip.background.fill=series.stroke = am4core.color(Params.Data[0]["color"][1]); // fill
                }


                valueAxis2.syncWithAxis = valueAxis;//同步圖表中橫向的線
                chart.cursor = new am4charts.XYCursor();
                chart.cursor.xAxis = categoryAxis;
            return chart;
        break;
        case "LineGroupForTen":
                // Themes begin

                am4core.options.autoDispose = true;
                // Themes end
                // Create chart instance
                var chart = am4core.create(id, am4charts.XYChart);
                chart.legend = new am4charts.Legend();
                chart.legend.useDefaultMarker = true;
                chart.legend.disabled = true;
                //chart.colors.list  =Params.ColorList;
                chart.data = Params.Data;
                chart.maskBullets = false;
                chart.plotContainer.background.strokeWidth = 1;
                chart.plotContainer.background.strokeOpacity = 1;
                chart.plotContainer.background.stroke = am4core.color("#000");
                //Create title 2
                if (Params.Title2 !== undefined) {
                    var topContainer = chart.chartContainer.createChild(am4core.Container);
                    topContainer.layout = "absolute";
                    topContainer.toBack();
                    // topContainer.paddingBottom = 15;
                    topContainer.width = am4core.percent(100);
                    var dateTitle = topContainer.createChild(am4core.Label);
                    dateTitle.text = Params.Title2.title;
                    dateTitle.fontSize = Params.Title2.fonSize;
                    dateTitle.fill = am4core.color(Params.Title2.color);
                    dateTitle.align = Params.Title2.align;
                    dateTitle.dy = Params.Title2.dy;
                    dateTitle.fontWeight = 400;
                }
                // Create axes
                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "category";
                categoryAxis.cursorTooltipEnabled = false;
                categoryAxis.renderer.grid.template.disabled = true;
                categoryAxis.renderer.labels.template.disabled = true;
                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.renderer.labels.template.fontSize=valueAxisFontSize;
                valueAxis.cursorTooltipEnabled = false;
                var iKey = 0;
                var keyFilter = Object.keys(Params.Data[0]).filter(function (key) { return key != "category" && key.indexOf("color") == -1 && key !="列管率"});
                keyFilter.forEach(function (key) {
                var series = chart.series.push(new am4charts[charSeries]());
                series.dataFields.valueY = key;
                series.dataFields.categoryX = "category";
                series.name = key;
                series.strokeWidth=4;
                series.tooltip.getFillFromObject = false;
                // series.tooltipText = "{name}: [bold]{valueY}[/]";
                if(charSeries == "LineSeries"){
                    var bullet = series.bullets.push(new am4charts.CircleBullet());
                    bullet.circle.strokeWidth  =1;
                    bullet.fill =series.tooltip.background.fill=series.stroke = am4core.color(Params.Data[0]["color"][iKey]); // fill
                }else{
                    if(Params.ColumnsWidth!==undefined)
                        series.columns.template.width=am4core.percent(Params.ColumnsWidth);
                    series.columns.template.fill = series.tooltip.background.fill=series.stroke =am4core.color(Params.Data[0]["color"][iKey]); // fill
                }
                ++iKey;
                });
                //Create Yaxes
                var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis2.renderer.opposite = true;
                valueAxis2.cursorTooltipEnabled = false;
                valueAxis2.tooltip.disabled = true;
                valueAxis2.numberFormatter.numberFormat = "#'%'";
                valueAxis2.renderer.labels.template.fontSize=valueAxisFontSize;
                // Create total Series
                var totalseries = chart.series.push(new am4charts.LineSeries());
                totalseries.dataFields.valueY = "列管率";
                totalseries.dataFields.categoryX = "category";
                totalseries.name = "列管率";
                totalseries.yAxis = valueAxis2;
                totalseries.strokeWidth=4;
                totalseries.stroke = totalseries.fill=Params.Data[0]["color"][Params.Data[0]["color"].length-1];
                // totalseries.tooltipText = "{name}: [bold]{valueY}%[/]";
                if(Params.Label!==undefined){
                    let labelBullet = totalseries.bullets.push(new am4charts.LabelBullet());
                    labelBullet.label.text = "[bold]{valueY}";
                    labelBullet.label.verticalCenter ="middle";
                    labelBullet.label.fontSize=12;
                    labelBullet.label.dy=-15;
                    labelBullet.label.truncate=false;
                    labelBullet.label.wrap =false;
                    labelBullet.label.fill=am4core.color("#FF0000");
                }

                var bullet = totalseries.bullets.push(new am4charts.CircleBullet());
                // Add cursor
                valueAxis2.syncWithAxis = valueAxis; //同步圖表中橫向的線
                chart.cursor = new am4charts.XYCursor();
                chart.cursor.xAxis = categoryAxis;
                return chart;
            // }); // end am4core.ready()
            break;
    }
}

