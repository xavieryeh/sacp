function FileDownload() {

        var fileName = "";
        var lecture_type = $('#lecture_type').val();
        var selectYear = $('#selectYear').val();
        var selectMonth = $('#selectMonth').val();
        var reportType = false;
        switch (lecture_type) {
            case 'year':
                fileName = "三四級講習年報"+selectYear+"年";
                reportType = '年報';
                break;
            case 'month':
                fileName = "三四級講習月報"+selectYear+"年"+selectMonth+"月";
                reportType = '月報';
                break;
            default:
                return;
                break;
        }
        var chartInfo = [];
        $("#chartGroup").html("");

        // 撈取月報或年報之資料
    console.log('撈取類型',$('#lecture_type').find('option:selected').html());
        self.ReportCharts.filter(f=>{return f.Type==$('#lecture_type').find('option:selected').html()}).forEach(chart=>{
            const chartDiv=
                '<div class="largechart" style="width:550px;height:170px;position:fixed;">\
                    <div id="download'+chart.Function_ID+'" class="card-item-pic" style="width: 500px;height:250px;margin:0 auto;padding-right:40px;padding-left:20px;" >\
                    <div id="largeChart'+chart.Chart_ID+'"></div>\
                </div>\
                <div style="margin-top:15px;">\
                    <p class="CommentText" id="'+chart.Function_ID+'Text"></p>\
                </div>\
            </div>';
            $("#chartGroup").append(chartDiv);
            // eval(chart.Function_Name + "('" + chart.Function_ID + "','largeChart"+chart.Chart_ID+"')");
            window[chart.Function_Name](chart.Function_ID ,'largeChart'+chart.Chart_ID);
            chartInfo.push({banner:chart.Banner_Name,title:chart.Chart_Name,source:chart.Source_Name,downID:'download'+chart.Function_ID,textID:chart.Function_ID+'Text',type:chart.Type});
        });

        // 撈取月報或年報之圖表
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var formData = new FormData();
        var chartFinish = [];
        var nowExcute = false;
        $("#btnExport").prop('disabled', true);
        var checkTimer = setInterval(() => {
            chartInfo.forEach((chart,i)=>{
                // console.log('檢查點',50);
                // 尋找是否已產生chart
                var amChart = am4core.registry.baseSprites.find(function (chartObj) {
                    console.log(53,chartObj.htmlContainer.id);
                    return chartObj.htmlContainer.id === chart.downID.replace("download","");
                });
                if(amChart===undefined||amChart===null){
                    console.log('檢查點1',chart.downID);
                    return;
                }

                // 判斷是否已處理
                if(chartFinish.find(f=>f==chart.downID) !== undefined){
                    console.log('檢查點2',chart.downID);
                    return;
                }

                // 判斷是否有資料處理中
                if(nowExcute){
                    console.log('檢查點3',chart.downID);
                    return;
                }
                nowExcute = true;
                $("#exportProcess").text("正在處理..."+chart.title);

                //延遲200ms確保產生完整圖表
                setTimeout(() => {
                    domtoimage.toBlob(document.getElementById(chart.downID),{bgcolor:"#fff",height:$("#"+chart.downID).height()})
                        .then(function (blob) {
                            formData.append(chart.downID, blob, chart.downID +".jpg");
                            chartFinish.push(chart.downID);
                            nowExcute = false;
                            // 全數處理完，準備匯出報表
                            if(chartInfo.length == chartFinish.length){
                                $("#exportProcess").text("正在產生報表...");
                                clearInterval(checkTimer);
                                chartInfo.forEach(chart=>{chart.subText= $('#'+chart.textID).text();});
                                formData.append('chartInfo', JSON.stringify(chartInfo));
                                axios({
                                    url: 'Report_exportWord',
                                    method: 'post',
                                    headers: {
                                        "X-CSRF-TOKEN": CSRF_TOKEN
                                    },
                                    data: formData,
                                    responseType: 'blob'
                                })
                                    .then((response) => {
                                        $("#exportProcess").text("完成");
                                        $("#btnExport").prop('disabled', false);
                                        const url = window.URL.createObjectURL(new Blob([response.data]));
                                        const link = document.createElement('a');
                                        link.href = url;
                                        link.setAttribute('download', fileName+'.doc');
                                        document.body.appendChild(link);
                                        link.click();
                                        document.body.removeChild(link);
                                    });
                            }
                        });
                    $('#btnExport').attr('disabled',false);
                }, 200);
            });
        }, 500);
}
