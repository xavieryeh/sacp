function PostAJAX(url,data,callback,error,async){
    $.ajax({
        async:async,
        method: "POST",
        url: url,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
        },
        data: data,
        success: function(data) {
            callback(data);
        },
        error: function(data) {
            console.log(data);
        }
    })
}
