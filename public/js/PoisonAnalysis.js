function getPoisonAnalysischart(id, div) {
    const chartID = id + (div.indexOf('popup') > -1 ? "_popup" : "");
    var startdate = '2020/01';
    var enddate = '2021/05';
    var breadcrumbtitle = document.getElementById("breadcrumbtitle");
    var title = document.getElementById("title");
    var bannerhref = document.getElementById("bannerhref");
    var source = document.getElementById("source");
    var selectYear = document.getElementById("selectYear");
    var selectMonth = document.getElementById("selectMonth");
    var selectStartYear = document.getElementById("selectStartYear");
    var selectEndYear = document.getElementById("selectEndYear");
    var sYear = moment().format('YYYY') - 2;
    var eYear = moment().format('YYYY');
    var sDate = moment().format('YYYY') + "/01";
    var eDate = moment().format('YYYY/MM');
    var reg = /^\d+$/;
    if (selectYear && selectMonth) {
        if (selectYear.value.match(reg) && selectMonth.value.match(reg)) {
            sDate = (parseInt(selectYear.value) + 1911).toString() + "/01";
            eDate = (parseInt(selectYear.value) + 1911).toString() + "/" + selectMonth.value;
        }
    };
    if (selectStartYear && selectEndYear) {
        if (selectStartYear.value.match(reg) && selectEndYear.value.match(reg)) {
            sYear = (parseInt(selectStartYear.value) + 1911).toString()
            eYear = (parseInt(selectEndYear.value) + 1911).toString()
        }
    };

    $()
    function CommentText(Params, unit) {
        return "註：" + Params.Year[0] + "年" + Params.month[0] + "~" + Params.month.slice(-1)[0] + "月總計" + Params.total[0].Value + unit + "，" +
            Params.Year[1] + "年" + Params.month[0] + "~" + Params.month.slice(-1)[0] + "月總計" + Params.total[1].Value + unit;
    };

    function getTextCss(font_size) {
        var scale = 1;
        if (font_size < 8)
            scale = font_size / 8;
        else if(font_size>12)
            font_size = 12;
        return {
            "font-size": font_size + 'px',
            "transform-origin": "0",
            "white-space": "nowrap",
            "transform": "scale(" + scale + ")"
        };
    };
    var chartParentName = ".card-item";
    if (title && breadcrumbtitle && source && bannerhref)
        chartParentName = ".largechart";
    else if (document.getElementsByClassName("content-index").length > 0)
        chartParentName = ".main-card"
    switch (id) {
        //一、查獲施用毒品(刑罰)初犯人數同期比較
        case "SelectCrimialMonth":
            //const chartID = "SelectCrimialMonth" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getCrimialMonth(sDate, eDate, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                    <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {
                    var Params = {};
                    var lstItem = [];
                    var tableBodyRow = [];
                    Params.fonSize = 12;
                    Params.Color = [ "#996633","#0dd3c4"];
                    Params.Title = "一、查獲施用毒品(刑罰)初犯人數同期比較";
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                    };
                    Params.month = [];
                    Params.Year = [];
                    if (data.length > 0) {
                        data.forEach(element => {
                            Params.month.push(element["MONTH"]);
                            Params.Year.push(element["YEAR"]);
                        });

                        Params.month = [...new Set(Params.month)];
                        Params.Year = [...new Set(Params.Year)];
                        Params.month.pop();
                        Params.ChartType = "LineSeries";
                        if (Params.month[Params.month.length - 1] < 7)
                            Params.ChartType = "ColumnSeries";
                        Params.total = data.filter(data => {
                            return data.MONTH == "合計"
                        });
                        document.getElementById(chartID + "Text").innerHTML = CommentText(Params, "人");

                        Params.Data = Params.month.map((element, index) => {
                            var newData = {};
                            lstItem.push(element + "月");
                            newData.category = element;
                            data.filter((each) => {
                                return each.MONTH == element
                            }).forEach(function (each, ieach) {
                                newData[each.YEAR + "年"] = each.Value + "人";
                            });
                            newData.color = Params.Color;
                            return newData;
                        });
                        tableBodyRow = Params.Year.map(function (element, index) {
                            var newData = {};
                            newData.title = element + "年" + ',' + Params.Color[index];
                            data.filter((each) => {
                                return each.YEAR == element && each.MONTH != "合計"
                            }).forEach(function (each, ieach) {
                                newData["Month" + ieach] = each.Value;
                            });
                            return newData;
                        });
                        var newChart = amchartfunc(chartID, Params, "Group", Params.ChartType);
                        setTimeout(() => {
                            var aaTable = new ChartTable(chartID + "Table");
                            aaTable.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                            aaTable.bodyWidth = newChart.plotContainer.contentWidth + 1;
                            aaTable.dataSource = tableBodyRow;
                            aaTable.columnSource = [""].concat(lstItem);
                            aaTable.bind();
                            $("#" + chartID).height(divHeight - aaTable.Height - 40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 37));
                        }, 300);
                    }
                }); // end am4core.ready()
            });
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 37));
                }, 300);
            });
            break;
        case "SelectCrimialYear":
            //const chartID = "SelectCrimialYear" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getCrimialYear(sYear, eYear, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                    <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {

                    var Params = {};
                    Params.Title = "一、查獲施用毒品(刑罰)初犯人數同期比較";
                    Params.fonSize = 12;
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                    };
                    Params.Color = "#7030A0";
                    Params.Data = [];
                    var lstItem = [];
                    var tableBodyRow = {
                        title: "刑罰" + ',' + Params.Color
                    };
                    data.forEach((element, index) => {
                        Params.Data.push({
                            category: element["YEAR"],
                            count: element["Value"]
                        })
                        tableBodyRow["col" + index] = element["Value"];
                        lstItem.push(element["YEAR"] + "年");
                    });
                    Params.Title = "毒品初犯人數年報";
                    var newChart = amchartfunc(chartID, Params, "Line");
                    setTimeout(() => {
                        var aaTable = new ChartTable(chartID + "Table");
                        aaTable.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                        aaTable.bodyWidth = newChart.plotContainer.contentWidth + 1;
                        aaTable.dataSource = [tableBodyRow];
                        aaTable.columnSource = [""].concat(lstItem);
                        aaTable.bind();
                        $("#" + chartID).height(divHeight - aaTable.Height - 40);
                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 37));
                    }, 300);
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 37));
                }, 300);
            });
            break;
            //二、查獲施用毒品(行政罰)初犯人數同期比較
        case "SelectAdministrativeMonth":
            //const chartID = "SelectAdministrativeMonth" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getAdministrativeMonth(sDate, eDate, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                    <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {

                    var Params = {};
                    var lstItem = [];
                    var tableBodyRow = [];
                    Params.fonSize = 12;
                    Params.Color = [ "#996633","#0dd3c4"];
                    Params.Title = "二、查獲施用毒品(行政罰)初犯人數同期比較";
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                    };
                    Params.month = [];
                    Params.Year = [];
                    if (data.length > 0) {
                        data.forEach(element => {
                            Params.month.push(element["MONTH"]);
                            Params.Year.push(element["YEAR"]);
                        });
                        Params.month = [...new Set(Params.month)];
                        Params.Year = [...new Set(Params.Year)];
                        Params.month.pop();
                        Params.ChartType = "LineSeries";
                        if (Params.month[Params.month.length - 1] < 7)
                            Params.ChartType = "ColumnSeries";
                        Params.total = data.filter(data => {
                            return data.MONTH == "合計"
                        });
                        document.getElementById(chartID + "Text").innerHTML = CommentText(Params, "人");

                        Params.Data = Params.month.map((element, index) => {
                            var newData = {};
                            lstItem.push(element + "月");
                            newData.category = element;
                            data.filter((each) => {
                                return each.MONTH == element
                            }).forEach(function (year, iyear) {
                                newData[year.YEAR] = year.Value;
                            });
                            newData.color =  Params.Color;
                            return newData;
                        });
                        tableBodyRow = Params.Year.map(function (element, index) {
                            var newData = {};
                            newData.title = element + "年" + ',' + Params.Color[index];
                            data.filter((each) => {
                                return each.YEAR == element && each.MONTH != "合計"
                            }).forEach(function (year, iyear) {
                                newData["Month" + iyear] = year.Value;
                            });
                            return newData;
                        })
                        Params.Title = "毒品行政法月報";
                        var newChart = amchartfunc(chartID, Params, "Group", Params.ChartType);
                        setTimeout(() => {
                            var aaTable = new ChartTable(chartID + "Table");
                            aaTable.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                            aaTable.bodyWidth = newChart.plotContainer.contentWidth + 1;
                            aaTable.dataSource = tableBodyRow;
                            aaTable.columnSource = [""].concat(lstItem);
                            aaTable.bind();
                            $("#" + chartID).height(divHeight - aaTable.Height - 40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 37));
                        }, 300);
                    }
                }); // end am4core.ready()
            });
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 37));
                }, 300);
            });
            break;
        case "SelectAdministrativeYear":
            //const chartID = "SelectAdministrativeYear" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getAdministrativeYear(sYear, eYear, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                    <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {
                    var Params = {};
                    Params.Title = "二、查獲施用毒品(行政罰)初犯人數同期比較";
                    Params.fonSize = 12;
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                    };
                    Params.Color = "#7030A0";
                    Params.Data = [];
                    var lstItem = [];
                    var tableBodyRow = {
                        title: "行政罰" + ',' + Params.Color
                    };
                    data.forEach((element, index) => {
                        Params.Data.push({
                            category: element["YEAR"],
                            count: element["Value"]
                        })
                        tableBodyRow["col" + index.toString()] = element["Value"];
                        lstItem.push(element["YEAR"] + "年");
                    });
                    var newChart = amchartfunc(chartID, Params, "Line");
                    setTimeout(() => {
                        var aaTable = new ChartTable(chartID + "Table");
                        aaTable.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                        aaTable.bodyWidth = newChart.plotContainer.contentWidth + 1;
                        aaTable.dataSource = [tableBodyRow];
                        aaTable.columnSource = [""].concat(lstItem);
                        aaTable.bind();
                        $("#" + chartID).height(divHeight - aaTable.Height - 40);
                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 37));
                    }, 300);
                }); // end am4core.ready()
            });
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 37));
                }, 300);
            });
            break;
            //三、查獲毒品上游(製造、運輸、販賣)人數
        case "SelectPersonMonth":
            //const chartID = "SelectPersonMonth" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getPersonMonth(sDate, eDate, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                    <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {

                    var Params = {};
                    var lstItem = [];
                    var tableBodyRow = [];
                    Params.Title = "三、查獲毒品上游(製造、運輸、販賣)人數";
                    Params.fonSize = 12;
                    Params.Color =  [ "#996633","#0dd3c4"];
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                    };
                    Params.Data = [];
                    Params.month = [];
                    Params.Year = [];
                    if (data.length > 0) {
                        data.forEach(element => {
                            Params.month.push(element["MONTH"]);
                            Params.Year.push(element["YEAR"]);
                        });
                        Params.month = [...new Set(Params.month)];
                        Params.Year = [...new Set(Params.Year)];
                        Params.month.pop();
                        Params.ChartType = "LineSeries";
                        if (Params.month[Params.month.length - 1] < 7)
                            Params.ChartType = "ColumnSeries";
                        Params.total = data.filter(data => {
                            return data.MONTH == "合計"
                        });
                        document.getElementById(chartID + "Text").innerHTML = CommentText(Params, "人");

                        Params.Data = Params.month.map((element, index) => {
                            var newData = {};
                            lstItem.push(element + "月");
                            newData.category = element;
                            data.filter((each) => {
                                return each.MONTH == element
                            }).forEach(function (year, iyear) {
                                newData[year.YEAR] = year.Value;
                            });
                            newData.color = Params.Color;
                            return newData;
                        });
                        tableBodyRow = Params.Year.map(function (element, index) {
                            var newData = {};
                            newData.title = element + "年" + ',' + Params.Color[index];
                            data.filter((each) => {
                                return each.YEAR == element && each.MONTH != "合計"
                            }).forEach(function (year, iyear) {
                                newData["Month" + iyear] = year.Value;
                            });
                            return newData;
                        })
                        var newChart = amchartfunc(chartID, Params, "Group", Params.ChartType);
                        setTimeout(() => {
                            var aaTable = new ChartTable(chartID + "Table");
                            aaTable.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                            aaTable.bodyWidth = newChart.plotContainer.contentWidth + 1;
                            aaTable.dataSource = tableBodyRow;
                            aaTable.columnSource = [""].concat(lstItem);
                            aaTable.bind();
                            $("#" + chartID).height(divHeight - aaTable.Height - 40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 37));
                        }, 300);
                    }
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 37));
                }, 300);
            });
            break;
        case "SelectPersonYear":
            //const chartID = "SelectPersonYear" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getPersonYear(sYear, eYear, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                    <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {
                    var Params = {};
                    Params.Title = "三、查獲毒品上游(製造、運輸、販賣)人數";
                    Params.fonSize = 12;
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                    };
                    Params.Color = "#7030A0";
                    Params.Data = [];
                    var lstItem = [];
                    var tableBodyRow = {
                        title: "毒品上游" + ',' + Params.Color
                    };
                    data.forEach((element, index) => {
                        Params.Data.push({
                            category: element["YEAR"],
                            count: element["Value"]
                        })
                        tableBodyRow["col" + index.toString()] = element["Value"];
                        lstItem.push(element["YEAR"] + "年");
                    });
                    var newChart = amchartfunc(chartID, Params, "Line");
                    setTimeout(() => {
                        var aaTable = new ChartTable(chartID + "Table");
                        aaTable.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                        aaTable.bodyWidth = newChart.plotContainer.contentWidth + 1;
                        aaTable.dataSource = [tableBodyRow];
                        aaTable.columnSource = [""].concat(lstItem);
                        aaTable.bind();
                        $("#" + chartID).height(divHeight - aaTable.Height - 40);
                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 37));
                    }, 300);
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 37));
                }, 300);
            });
            break;
            //四、各級毒品查獲重量
        case "SelectWeightMonth":
            //const chartID = "SelectWeightMonth" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getWeightMonth(sDate, eDate, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                    <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {

                    var Params = {};
                    Params.Title = "四、各級毒品查獲重量";
                    Params.fonSize = 12;
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：公克",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: -5
                    };
                    var lstItem = [];
                    var tableBodyRow = [];
                    Params.Month = [];
                    Params.Color = ["#0031CC", "#FF3300", "#0dd3c4", "#996633", "#7030a0", "#c00000"];
                    Params.Level = [];
                    if (data.length > 0) {
                        data.forEach(element => {
                            Params.Month.push(element["MONTH"]);
                            Params.Level.push(element["LEVEL"]);
                        });
                        Params.Month = [...new Set(Params.Month)];
                        Params.Level = [...new Set(Params.Level)];
                        Params.ChartType = "LineSeries";
                        if (Params.Month[Params.Month.length - 1] < 7)
                            Params.ChartType = "ColumnSeries";
                        Params.Data = Params.Month.map((element, index) => {
                            var newData = {};
                            lstItem.push(element + "月");
                            newData.category = element;
                            data.filter((each) => {
                                return each.MONTH == element
                            }).forEach(function (year, iyear) {
                                newData[Params.Level[iyear]] = year.Gram;
                            });
                            newData.color = Params.Color;
                            return newData;
                        });
                        tableBodyRow = Params.Level.map(function (each, ieach) {
                            newData = {}
                            newData.title = each + ',' + Params.Color[ieach];
                            data.filter(eachdata => {
                                return eachdata.LEVEL == each
                            }).forEach((element, index) => {
                                newData["data" + index] = element.Gram;
                            })
                            return newData;
                        });
                        var newChart = amchartfunc(chartID, Params, "LineGroup", Params.ChartType);
                        setTimeout(() => {
                            var aaTable = new ChartTable(chartID + "Table");
                            const firstLength = tableBodyRow[0].title.split(',')[0].length + 2;
                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow, firstLength);
                            $("#" + chartID).height(divHeight - aaTable.Height - 40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 37));
                        }, 100);
                    }
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 37));
                }, 300);
            });
            break;
            //五、各級毒品查獲人數
        case "SelectNumMonth":
            //const chartID = "SelectNumMonth" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getNumMonth(sDate, eDate, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                    <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {

                    var Params = {};
                    Params.Title = "五、各級毒品查獲人數";
                    Params.fonSize = 12;
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: -5,
                    };
                    var lstItem = [];
                    var tableBodyRow = [];
                    Params.Month = [];
                    Params.Level = [];
                    Params.Color = ["#0031CC", "#FF3300", "#0dd3c4", "#996633", "#7030a0", "#c00000"];
                    if (data.length > 0) {
                        data.forEach(element => {
                            Params.Month.push(element["MONTH"]);
                            Params.Level.push(element["LEVEL"]);
                        });
                        Params.Month = [...new Set(Params.Month)];
                        Params.Level = [...new Set(Params.Level)];
                        Params.ChartType = "LineSeries";
                        if (Params.Month[Params.Month.length - 1] < 7)
                            Params.ChartType = "ColumnSeries";
                        Params.Data = Params.Month.map((element, index) => {
                            var newData = {};
                            lstItem.push(element + "月");
                            newData.category = element;
                            data.filter((each) => {
                                return each.MONTH == element
                            }).forEach(function (year, iyear) {
                                newData[Params.Level[iyear]] = year.Num;
                            });
                            newData.color = Params.Color;
                            return newData;
                        });
                        tableBodyRow = Params.Level.map(function (each, ieach) {
                            newData = {}
                            newData.title = each + ',' + Params.Color[ieach];
                            data.filter(eachdata => {
                                return eachdata.LEVEL == each
                            }).forEach((element, index) => {
                                newData["data" + index] = element.Num;
                            })
                            return newData;
                        });
                        var newChart = amchartfunc(chartID, Params, "LineGroup", Params.ChartType);
                        setTimeout(() => {
                            var aaTable = new ChartTable(chartID + "Table");
                            const firstLength = tableBodyRow[0].title.split(',')[0].length + 2;
                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow, firstLength);
                            $("#" + chartID).height(divHeight - aaTable.Height - 40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 37));
                        }, 300);
                    }
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });
                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 37));
                }, 300);
            });
            break;
            //六、各級毒品查獲件數
        case "SelectCaseMonth":
            //const chartID = "SelectCaseMonth" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getCaseMonth(sDate, eDate, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                    <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {

                    var Params = {};
                    Params.Title = "六、各級毒品查獲件數";
                    Params.fonSize = 12;
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：件數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: -5
                    };
                    var lstItem = [];
                    var tableBodyRow = [];
                    Params.month = [];
                    Params.Color = ["#0031CC", "#FF3300", "#0dd3c4", "#996633", "#7030a0", "#c00000"];
                    Params.Level = [];
                    Params.ChartType = "LineSeries";
                    if (data.length > 0) {
                        data.forEach(element => {
                            Params.month.push(element["MONTH"]);
                            Params.Level.push(element["LEVEL"]);
                        });
                        Params.month = [...new Set(Params.month)];
                        Params.Level = [...new Set(Params.Level)];
                        if (Params.month[Params.month.length - 1] < 7)
                            Params.ChartType = "ColumnSeries";
                        Params.Data = Params.month.map((element, index) => {
                            var newData = {};
                            lstItem.push(element + "月");
                            newData.category = element;
                            data.filter((each) => {
                                return each.MONTH == element
                            }).forEach(function (year, iyear) {
                                newData[Params.Level[iyear]] = year.CASE;
                            });
                            newData.color = Params.Color;
                            return newData;
                        });
                        tableBodyRow = Params.Level.map(function (each, ieach) {
                            newData = {}
                            newData.title = each + ',' + Params.Color[ieach];

                            data.filter(eachdata => {
                                return eachdata.LEVEL == each
                            }).forEach((element, index) => {
                                newData["data" + index] = element.CASE;
                            })
                            return newData;
                        });
                        var newChart = amchartfunc(chartID, Params, "LineGroup", Params.ChartType);
                        setTimeout(() => {
                            var aaTable = new ChartTable(chartID + "Table");
                            const firstLength = tableBodyRow[0].title.split(',')[0].length + 2;
                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow, firstLength);
                            $("#" + chartID).height(divHeight - aaTable.Height - 40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 37));
                        }, 300);
                    }
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 37));
                }, 300);
            });
            break;
            //七、各級毒品查獲重量同期比較
        case "SelectWeightContrastMonth":
            //const chartID = "SelectWeightContrastMonth" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getWeightContrastMonth(sDate, eDate, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {

                    var Params = {};
                    Params.Title = "七、各級毒品查獲重量同期比較";
                    Params.fonSize = 12;
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：公克",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                    };
                    //var lstItem = [];
                    var tableBodyRow = [];
                    Params.data = [];
                    Params.Year = [];
                    Params.Level = [];
                    Params.Color = [ "#996633","#0dd3c4"];
                    Params.month = [sDate.split('/')[1].replace('0', ''), eDate.split('/')[1].replace('0', ''), "總計"];
                    if (data.length > 0) {
                        data.forEach(element => {
                            Params.Year.push(element["Year"]);
                            Params.Level.push(element["LEVEL"]);
                        })
                        Params.Year = [...new Set(Params.Year)];
                        Params.Level = [...new Set(Params.Level)];
                        lstItem = Params.Level;
                        Params.month.pop();
                        Params.Level.pop();
                        Params.total = data.filter(data => {
                            return data.LEVEL == "總計"
                        });
                        document.getElementById(chartID + "Text").innerHTML = CommentText(Params, "公克");
                        tableBodyRow = Params.Year.map(function (element, index) {
                            var newData = {};
                            newData.title = element + "年" + Params.month[0] + "-" + Params.month[1] + "月" + ',' + Params.Color[index];
                            data.map(function (each, ieach) {
                                if (each["Year"] == element && each.LEVEL != "總計")
                                    newData["Month" + ieach] = each["Value"];
                                return newData;
                            })
                            return newData;
                        })
                        Params.Data = Params.Level.map((element, index) => {
                            var newData = {};
                            newData.category = element;
                            data.filter((each) => {
                                return each.LEVEL == element && each.LEVEL != "總計"
                            }).forEach(function (year, iyear) {
                                newData[year.Year] = year.Value;
                            });
                            newData.color = Params.Color;
                            return newData;
                        });
                        var newChart = amchartfunc(chartID, Params, "Group", "ColumnSeries");
                        setTimeout(() => {
                            var aaTable = new ChartTable(chartID + "Table");
                            const firstLength = tableBodyRow[0].title.split(',')[0].length + 2;
                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow, firstLength);
                            $("#" + chartID).height(divHeight - aaTable.Height - 40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 37));
                        }, 300);
                    }
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 37));
                }, 300);
            });
            break;
        case "SelectWeightContrastYear":
            //const chartID = "SelectWeightContrastYear" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getWeightContrastYear(sYear, eYear, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                    <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {

                    var Params = {};
                    Params.Title = "七、各級毒品查獲重量同期比較";
                    Params.fonSize = 12;
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：公克",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                    };
                    Params.Color = ["#0031CC", "#ff0000", "#0dd3c4", "#c89058", "#7030a0"];
                    Params.data = [];
                    Params.Year = [];
                    var lstItem = [];
                    var tableBodyRow = [];
                    Params.Level = [];
                    data.forEach(function (each) {
                        Params.Level.push(each["Level"]);
                        Params.Year.push(each["Year"]);
                    })
                    Params.Level = [...new Set(Params.Level)];
                    Params.Year = [...new Set(Params.Year)];
                    tableBodyRow = Params.Level.map(function (element, index) {
                        newData = {}
                        newData.title = element + ',' + Params.Color[index];
                        data.filter((eachdata) => {
                            return eachdata.Level == element
                        }).forEach(function (each, ieach) {
                            newData["data" + ieach] = each.Gram;
                        });
                        return newData;
                    });

                    Params.Data = Params.Year.map((element, index) => {
                        var newData = {};
                        lstItem.push(element + "年");
                        newData.category = element;
                        data.filter((each) => {
                            return each.Year == element
                        }).forEach(function (year, iyear) {
                            newData[Params.Level[iyear]] = year.Gram;
                        });
                        newData.color = Params.Color;
                        return newData;
                    });
                    var newChart = amchartfunc(chartID, Params, "LineGroup", "LineSeries");
                    setTimeout(() => {
                        var aaTable = new ChartTable(chartID + "Table");
                        aaTable.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                        aaTable.bodyWidth = newChart.plotContainer.contentWidth + 1;
                        aaTable.dataSource = tableBodyRow;
                        aaTable.columnSource = [""].concat(lstItem);
                        aaTable.bind();
                        $("#" + chartID).height(divHeight - aaTable.Height - 40);
                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 37));
                    }, 300);
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 37));
                }, 300);
            });
            break;
            //八、各級毒品查獲人數同期比較
        case "SelectPeosonContrastMonth":
            //const chartID = "SelectPeosonContrastMonth" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getPeosonContrastMonth(sDate, eDate, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                    <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {

                    var Params = {};
                    Params.Title = "八、各級毒品查獲人數同期比較";
                    Params.fonSize = 12;
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                    };
                    var lstItem = [];
                    var tableBodyRow = [];
                    Params.Data = [];
                    Params.Year = [];
                    Params.Level = [];
                    Params.Color = [ "#996633","#0dd3c4"];
                    Params.month = [sDate.split('/')[1].replace('0', ''), eDate.split('/')[1].replace('0', ''), "總計"];
                    if (data.length > 0) {
                        data.forEach(element => {
                            Params.Year.push(element["Year"]);
                            Params.Level.push(element["Level"]);
                        })
                        Params.Year = [...new Set(Params.Year)];
                        Params.Level = [...new Set(Params.Level)];
                        Params.Level.pop();
                        lstItem = Params.Level;
                        Params.month.pop();
                        Params.total = data.filter(data => {
                            return data.Level == "總計"
                        });
                        document.getElementById(chartID + "Text").innerHTML = CommentText(Params, "人");
                        tableBodyRow = Params.Year.map(function (element, index) {
                            var newData = {};
                            newData.title = element + "年" + Params.month[0] + "-" + Params.month[1] + "月" + ',' + Params.Color[index];
                            data.map(function (each, ieach) {
                                if (each["Year"] == element && each.Level != "總計")
                                    newData["Month" + ieach] = each["Value"];
                                return newData;
                            })
                            return newData;
                        })
                        Params.Data = Params.Level.map((element, index) => {
                            var newData = {};
                            newData.category = element;
                            data.filter((each) => {
                                return each.Level == element && each.Level != "總計"
                            }).forEach(function (year, iyear) {
                                newData[year.Year] = year.Value;
                            });
                            newData.color = Params.Color;
                            return newData;
                        });
                        var newChart = amchartfunc(chartID, Params, "Group", "ColumnSeries");
                        setTimeout(() => {
                            var aaTable = new ChartTable(chartID + "Table");
                            const firstLength = tableBodyRow[0].title.split(',')[0].length + 2;
                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow, firstLength);
                            $("#" + chartID).height(divHeight - aaTable.Height - 40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 37));
                        }, 300);
                    }
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 37));
                }, 300);
            });
            break;
        case "SelectPeosonContrastYear":
            //const chartID = "SelectPeosonContrastYear" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getPeosonContrastYear(sYear, eYear, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                    <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {

                    var Params = {};
                    Params.Title = "八、各級毒品查獲人數同期比較";
                    Params.fonSize = 12;
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                    };
                    Params.Color = ["#0031CC", "#ff0000", "#0dd3c4", "#c89058", "#7030a0"];
                    Params.Level = [];
                    Params.Year = [];
                    var lstItem = [];
                    var tableBodyRow = [];
                    if (data.length > 0) {
                        data.forEach(function (each) {
                            Params.Level.push(each["Level"]);
                            Params.Year.push(each["Year"]);
                        })
                        Params.Level = [...new Set(Params.Level)];
                        Params.Year = [...new Set(Params.Year)];

                    }
                    tableBodyRow = Params.Level.map(function (element, index) {
                        newData = {}
                        newData.title = element + ',' + Params.Color[index];
                        data.filter((eachdata) => {
                            return eachdata.Level == element
                        }).forEach(function (each, ieach) {
                            newData["data" + ieach] = each.NUM;
                        });
                        return newData;
                    });
                    Params.Data = Params.Year.map((element, index) => {
                        var newData = {};
                        lstItem.push(element + "年");
                        newData.category = element;
                        data.filter((each) => {
                            return each.Year == element
                        }).forEach(function (year, iyear) {
                            newData[Params.Level[iyear]] = year.NUM;
                        });
                        newData.color = Params.Color;
                        return newData;
                    });
                    var newChart = amchartfunc(chartID, Params, "LineGroup", "LineSeries");
                    setTimeout(() => {
                        var aaTable = new ChartTable(chartID + "Table");
                        aaTable.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                        aaTable.bodyWidth = newChart.plotContainer.contentWidth + 1;
                        aaTable.dataSource = tableBodyRow;
                        aaTable.columnSource = [""].concat(lstItem);
                        aaTable.bind();
                        $("#" + chartID).height(divHeight - aaTable.Height - 40);
                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 37));
                    }, 300);
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 37));
                }, 300);
            });
            break;
            //九、各級毒品查獲件數同期比較
        case "SelectCaseContrastMonth":
            //const chartID = "SelectCaseContrastMonth" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getCaseContrastMonth(sDate, eDate, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                    <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {

                    var Params = {};
                    Params.Title = "九、各級毒品查獲件數同期比較";
                    Params.fonSize = 12;
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：件數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                    };
                    var lstItem = [];
                    var tableBodyRow = [];
                    Params.Year = [];
                    Params.Level = [];
                    Params.Color = [ "#996633","#0dd3c4"];
                    Params.month = [sDate.split('/')[1].replace('0', ''), eDate.split('/')[1].replace('0', ''), "總計"];
                    if (data.length > 0) {
                        data.forEach(element => {
                            Params.Year.push(element["Year"]);
                            Params.Level.push(element["Level"]);
                        })
                        Params.Year = [...new Set(Params.Year)];
                        Params.Level = [...new Set(Params.Level)];
                        Params.Level.pop();
                        lstItem = Params.Level;
                        Params.month.pop();
                        Params.total = data.filter(data => {
                            return data.Level == "總計"
                        });
                        document.getElementById(chartID + "Text").innerHTML = CommentText(Params, "件");

                        tableBodyRow = Params.Year.map(function (element, index) {
                            var newData = {};
                            newData.title = element + "年" + Params.month[0] + "-" + Params.month[1] + "月" + ',' + Params.Color[index];
                            data.map(function (each, ieach) {
                                if (each["Year"] == element && each.Level != "合計" && each.Level != "總計")
                                    newData["Month" + ieach] = each["Value"];
                                return newData;
                            })
                            return newData;
                        })
                        Params.Data = Params.Level.map((element) => {
                            var newData = {};
                            newData.category = element;
                            data.filter((each) => {
                                return each.Level == element && each.Level != "總計"
                            }).forEach(function (year, iyear) {
                                newData[year.Year] = year.Value;
                            });
                            newData.color = Params.Color;
                            return newData;
                        });

                        var newChart = amchartfunc(chartID, Params, "Group", "ColumnSeries");
                        setTimeout(() => {
                            var aaTable = new ChartTable(chartID + "Table");
                            const firstLength = tableBodyRow[0].title.split(',')[0].length + 2;
                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow, firstLength);
                            $("#" + chartID).height(divHeight - aaTable.Height - 40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 37));
                        }, 300);
                    }
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                    $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 37));
                }, 300);
            });
            break;
        case "SelectCaseContrastYear":
            //const chartID = "SelectCaseContrastYear" + (div.indexOf('popup') > -1 ? "_popup" : "");
            $.getCaseContrastYear(sYear, eYear, function (data) {
                const divHeight = $("#" + div).parents(chartParentName).height() < 150 ? 150 : $("#" + div).parents(chartParentName).height()  - (div.indexOf('popup') > -1 ? 80 : 0);;
                if ($("#" + div).html() == "") {
                    $("#" + div).html('<div name="mChart" id="' + chartID + '" style="height:' + (divHeight - 100) + 'px;width:100%;"></div>\
                    <div name="mTable" id="' + chartID + 'Table" style="margin-top:-16px"></div>');
                }
                am4core.ready(function () {

                    var Params = {};
                    Params.Title = "九、各級毒品查獲件數同期比較";
                    Params.fonSize = 12;
                    if (title && breadcrumbtitle && source && bannerhref) {
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市查獲毒品概況分析";
                        bannerhref.href = "KaohsiungPoisonAnalysis";
                        source.innerHTML = "來源 : 高雄市政府警察局";
                        Params.fonSize = 18;
                    }
                    Params.Title2 = {
                        title: "單位：件數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                    };
                    Params.Color = ["#0031CC", "#ff0000", "#0dd3c4", "#c89058", "#7030a0"];
                    Params.Level = [];
                    Params.Year = [];
                    var lstItem = [];
                    var tableBodyRow = [];
                    if (data.length > 0) {
                        data.forEach(function (each) {
                            Params.Level.push(each["Level"]);
                            Params.Year.push(each["Year"]);
                        })
                        Params.Level = [...new Set(Params.Level)];
                        Params.Year = [...new Set(Params.Year)];
                    }
                    tableBodyRow = Params.Level.map(function (element, index) {
                        newData = {}
                        newData.title = element + ',' + Params.Color[index];
                        data.filter((eachdata) => {
                            return eachdata.Level == element
                        }).forEach(function (each, ieach) {
                            newData["data" + ieach] = each.CASE;
                        });
                        return newData;
                    });
                    Params.Data = Params.Year.map((element, index) => {
                        var newData = {};
                        newData.category = element;
                        lstItem.push(element + "年");
                        data.filter((each) => {
                            return each.Year == element
                        }).forEach(function (year, iyear) {
                            newData[Params.Level[iyear]] = year.CASE;
                        });
                        newData.color = Params.Color;
                        return newData;
                    });
                    var newChart = amchartfunc(chartID, Params, "LineGroup", "LineSeries");
                    setTimeout(() => {
                        var aaTable = new ChartTable(chartID + "Table");
                        aaTable.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                        aaTable.bodyWidth = newChart.plotContainer.contentWidth + 1;
                        aaTable.dataSource = tableBodyRow;
                        aaTable.columnSource = [""].concat(lstItem);
                        aaTable.bind();
                        $("#" + id).height(divHeight - aaTable.Height - 40);
                    }, 300);
                }); // end am4core.ready()
            })
            $(window).on('resize', function () {
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });

                setTimeout(() => {
                    var aaTable = getChartTable(chartID + "Table");
                    aaTable.headerWidth = chart.leftAxesContainer.contentWidth + 14;
                    aaTable.bodyWidth = chart.plotContainer.contentWidth + 1;
                    aaTable.setWidth();
                }, 300);
            });
            break;
    }
}
