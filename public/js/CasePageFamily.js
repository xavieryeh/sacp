function CasePageFamily(self) {
    // 親等選項連動稱謂選項
    $("#selRelative").on('change', function () {
        $.PostAJAX("Case_casePageFamilyGetTitle", {
            relative: $("#selRelative option:selected")[0].value
        }, response => {
            $("#selTitle").html('<option value="" selected>請選擇稱謂</option>' + response.map(res => {
                return '<option value="' + res.Title + '">' + res.Title + '</option>'
            }).join(''));
        });
    });
    // 日期防呆
    // $("#iBirth").on('change', function () {
    //     if(moment().isBefore($("#iBirth").val()))
    //         $("#iBirth").val(moment().format('YYYY-MM-DD'));
    // });
    // 輸入格式驗證成功事件
    $('#addForm').parsley().on('form:success', function (formInstance) {
        $('.bs-callout-info').toggleClass('hidden', false);
        $('.bs-callout-warning').toggleClass('hidden', true);

        $('#addModal').modal('show');
    });
    // 輸入格式驗證失敗事件
    $('#addForm').parsley().on('form:error', function (formInstance) {
        $('.parsley-required').addClass('hidden');
        $('.parsley-length').addClass('hidden');
        $('.parsley-custom-error-message').css({"font-size":"10px","color":"#FF2D2D","margin-top":"0","margin-bottom":"0"});
    });
    // $('#addForm').parsley().on('field:validated', function () {
    //     var ok = $('.parsley-error').length === 0 ;
    //     $('.bs-callout-info').toggleClass('hidden', !ok);
    //     $('.bs-callout-warning').toggleClass('hidden', ok);
    //     $('.parsley-required').addClass('hidden');
    //     $('.parsley-length').addClass('hidden');
    //     $('.parsley-custom-error-message').css({"font-size":"10px","color":"#FF2D2D","margin-top":"0","margin-bottom":"0"});
    //     console.log($('.parsley-error').length+','+$('.parsley-custom-error-message').length);
    //     if(ok)
    //         $('#addModal').modal('show');
    // });
    //新增按鈕事件
    $("#btnAdd").on('click', function () {
        $('#addForm').parsley().validate();
    });      
    //取消按鈕事件
    $("#btnCancel").on('click', function () {
        window.location = self.reLoadUrl;
    });    
    
    //新增按鈕確認事件
    $("#btnAddConfirm").on('click', function (e) {
        e.preventDefault();
        var form = $('#addForm')[0];
        var formData = new FormData(form);
        $.PostFormAJAX(self.addUrl, formData,response=>{window.location = self.reLoadUrl;});
    });
    //各項編輯事件
    self.onEdit = function(id){
        // const fam = self.Familys.filter(f=>{return f.Case==self.caseNo&&f.ID==id})[0];
        const fam = self.Familys.find(f=>f.ID==id);
        // console.log('fam', fam);
        // console.log('fam.Problem', fam.Problem, self.ProblemList);
        // var problems = fam.Problem.split(',').map(m=>self.ProblemList.find(f=>f.Message==m)).map(m=>m.Color);
        // var structures = fam.Structure.split(',').map(m=>self.StructureList.find(f=>f.Message==m)).map(m=>m.Color);
        // console.log('problems', problems);
        var problems = fam.家庭問題 ? fam.家庭問題.split(','): null;
        var structures = fam.家庭結構 ? fam.家庭結構.split(','): null;
        $("#selRelative").val(fam.親等);
        $("#selTitle").val(fam.稱謂);
        $("#iName").val(fam.姓名);
        $("#iGender").val(fam.性別);
        $("#iBirth").val(fam.出生年月日);
        $("#iID").val(fam.身份證字號);
        $("#iProfession").val(fam.職業);
        $("#iDrugRecoder").val(fam.涉毒紀錄);
        $("#iCohabit").val(fam.是否同住);
        // $("#iProblem").val(self.ProblemList.find(f=>f.Message==fam.Problem).Color);
        $("#iProblem").val(problems);
        // $("#iStructure").val(self.StructureList.find(f=>f.Message==fam.Structure).Color);
        $("#iStructure").val(structures);
        $("#iRelatives").val(fam.是否親屬);
        $("#iAlive").val(fam.是否在世);
        $("#iRemark").val(fam.備註);
        $("#isFirst").val(fam.初犯再犯);
        $('#db_id').val(fam.ID);
        $('html, body').animate({ scrollTop: 0 }, 200);
    }
    //各項刪除事件
    self.db_id = 0;
    self.onDelete = function(title,id){
        $("#delKey").text(title);
        self.db_id = id;
        $('#delModal').modal('show');
    }
    //各項刪除確認事件
    $("#btnDelConfirm").on('click', function () {
        // $.PostAJAX("Case_casePageFamilyDelDate", {caseNo:self.caseNo,title:$("#delKey").text()}, response => {window.location = self.reLoadUrl;});
        $.PostAJAX("Case_casePageFamilyDelDate", {id:self.db_id}, response => {window.location = self.reLoadUrl;});
    });
    return self;
}
