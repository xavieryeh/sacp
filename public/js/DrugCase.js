function getDrugCase(id,div){
    const chartID = id + (div.indexOf('popup') > -1 ? "_popup" : "");
    var breadcrumbtitle=document.getElementById("breadcrumbtitle");
    var title=document.getElementById("title");
    var bannerhref = document.getElementById("bannerhref");
    var source  = document.getElementById("source");
    var selectYear = document.getElementById("selectYear");
    var selectMonth = document.getElementById("selectMonth");
    var selectStartYear = document.getElementById("selectStartYear");
    var selectEndYear = document.getElementById("selectEndYear");
    var sYear = moment().format('YYYY') -2  +"/"+ moment().format('MM');
    var eYear = moment().format('YYYY/MM');
    var sDate = moment().format('YYYY')+"/01";
    var eDate = moment().format('YYYY/MM');
    var reg = /^\d+$/;
    if(selectYear && selectMonth){
        if(selectYear.value.match(reg) && selectMonth.value.match(reg)){
            sDate =(parseInt(selectYear.value)+1911).toString()+"/01";
            eDate =(parseInt(selectYear.value)+1911).toString() +"/"+ selectMonth.value;
        }
    };
    if(selectStartYear && selectEndYear){
        if(selectStartYear.value.match(reg) && selectEndYear.value.match(reg)){
            sYear = (parseInt(selectStartYear.value) + 1911).toString() +'/'+ moment().format('MM')
            eYear = (parseInt(selectEndYear.value) + 1911).toString() +'/'+ moment().format('MM')
        }
    };
    function CommentText (Params,unit){
        return  "註：" + Params.Year + "年" + Params.month + "月在案人數總計" + Params.Total + unit;
    };
    function getTextCss(font_size) {
        var scale = 1;
        if (font_size < 8)
            scale = font_size / 8;
            else if(font_size>12)
            font_size = 12;
        return {
            "font-size": font_size + 'px',
            "transform-origin": "0",
            "white-space": "nowrap",
            "transform": "scale(" + scale + ")"
        };
    };
    function ChartTable_FitSize(div,offset) {
        const id = $("#"+div+ " div[name='mChart']").attr("id");
        const divHeight = $("#"+div).parents(".main-card").height();
        var Chart = am4core.registry.baseSprites.find(function (chartObj) {
            return chartObj.htmlContainer.id === id;
        });
        var Table = getChartTable(id+"Table");
        if(Table!==undefined){
            const firstLength=$("#"+chartID+"Table").find("tbody").find("tr:nth-child(2)").find("td:first").text()+2;
            $.FitTableOnResize(Table, Chart,firstLength);
            $("#"+id).css("height",(divHeight-Table.Height-offset)+'px');
        }
        else
            $("#"+id).css("height",(divHeight-offset)+'px');
    }
    var chartParentName = ".card-item";
    if(title && breadcrumbtitle && source && bannerhref)
        chartParentName = ".largechart";
    else if(document.getElementsByClassName("content-index").length>0)
        chartParentName = ".main-card"
    switch(id){
//一、性別統計
    case"DrugGenderByMonth":
        $.getDrugGenderByMonth(sDate,eDate,function(data){
            $.getSelectCommentTextByMonth(sDate,eDate,function(text){
                am4core.ready(function () {
                    const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);
                    if($("#"+div).html()==""){
                        $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                        <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                    }
                    $("#"+chartID).height(divHeight-40);
                    var Params = {};
                    var lstItem = ["人數","百分比"];
                    Params.ChartType = "Pie2";
                    var tableBodyRow = [];
                    Params.Data = [];
                    Params.fill=["#0033CC","#FF0000"];
                    Params.Name = eDate;
                    Params.Title = "一、性別統計";
                    Params.fonSize=12;
                    Params.Total = 0;
                    Params.ShowDecimal=true;
                    Params.Year = sDate.substring(0,4)-1911;
                    Params.Month = eDate.split('/')[1].replace('0','');
                    if(title && breadcrumbtitle && source && bannerhref){
                            title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                            bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                            bannerhref.href="DrugCase";
                            source.innerHTML="來源：高雄市政府毒品防制局";
                            Params.fonSize=18;

                        }
                    if(data.length>0){
                        data.forEach((element,index)=>{
                            Params.Data.push({category:element["Gender"]+"性",count:element["Person"],color:Params.fill[index]})
                            Params.Total+=parseInt( element["Person"]);
                        });

                        document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月三四級講習"+text.find(each=>{return each.type=="三四級講習"}).Person+"人、案管系統"+
                        text.find(each=>{return each.type=="案管系統"}).Person+"人、在案人數總計"+text.find(each=>{return each.type=="在案人數"}).Person+"人";
                        $("#"+chartID+"Text").css("fontSize",Params.fonSize+"px");
                    var newChart = amchartfunc(chartID,Params,Params.ChartType);
                                    setTimeout(() => {
                                                $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                            }, 100);
                                }
                }); // end am4core.ready()
            })
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"DrugGenderByYear":
        $.getDrugGenderByYear(sYear,eYear,function(data){
                const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                    if($("#"+div).html()==""){
                        $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                        <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                    }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.ChartType = "LineGroup";
                var tableBodyRow = [];
                Params.Data = [];
                Params.Color=["#0033CC","#FF0000"];
                Params.Year  = [];
                Params.Gender=[];
                Params.Title = "一、性別統計";
                Params.fonSize=12;
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;

                    }
                Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach(element => {
                        Params.Year.push(element["YEAR"]);
                        Params.Gender.push(element["Gender"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.Gender = [...new Set(Params.Gender)];
                    Params.Year.sort();
                    Params.Data =  Params.Year.map(function(item,iItem){
                        var newData={};
                        newData.category=item;
                        Params.Gender.forEach(gender=>{
                            var findPerson = data.find((each)=>{return each.YEAR==item && each.Gender==gender});
                            if(findPerson!==undefined)
                                newData[gender] = parseInt(findPerson.Person);
                            else
                                newData[gender] = 0;
                        })
                        newData.color=Params.Color;
                        lstItem.push(item+"年");
                        return newData;
                    });
                    tableBodyRow = Params.Gender.map(function(element,index){
                        var newData = {};
                        newData.title =element+"性"+','+Params.Color[index];
                        Params.Year.forEach(year=>{
                            var findPerson = data.find((each)=>{return each.YEAR==year && each.Gender==element});
                            if(findPerson!==undefined)
                                newData["Year"+year] = findPerson.Person;
                            else
                                newData["Year"+year] = '0';
                        })
                        return newData;
                    });
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"LineSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=tableBodyRow;
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                            }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
//二、年齡層統計
    case"DrugAgeByMonth":
        $.getDrugAgeByMonth(sDate,eDate,function(data){
            $.getSelectCommentTextByMonth(sDate,eDate,function(text){
                const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                    if($("#"+div).html()==""){
                        $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                        <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                    }
                am4core.ready(function () {
                    var Params = {};
                    var lstItem = [];
                    Params.ChartType = "Column";
                    Params.Data = [];
                    Params.Title = "二、年齡層統計";
                    Params.fonSize=12;
                    Params.Color="#7030A0";
                    Params.Total = 0;
                    Params.Year = sDate.substring(0,4)-1911;
                    Params.Month = eDate.split('/')[1].replace('0','');
                    Params.ColumnsWidth=50;
                    var tableBodyRow = {title:Params.Year+"年"+Params.Month+"月"+','+Params.Color};
                    if(title && breadcrumbtitle && source && bannerhref){
                            title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                            bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                            bannerhref.href="DrugCase";
                            source.innerHTML="來源：高雄市政府毒品防制局";
                            Params.fonSize=18;

                        }
                    Params.Title2 = {
                            title: "單位：人數",
                            fonSize: Params.fonSize,
                            color: "#000000",
                            align: "right",
                            valign: "top",
                            dy: 0
                    };
                    if(data.length>0){
                        data.forEach((element,index)=>{
                        Params.Data.push({category:element["Age_Class"],count:element["Person"]})
                        lstItem.push(element["Age_Class"]);
                        Params.Total += parseInt(element["Person"]);
                        tableBodyRow["Age_Class"+(index)] = element["Person"];
                        });
                        document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月三四級講習"+text.find(each=>{return each.type=="三四級講習"}).Person+"人、案管系統"+
                        text.find(each=>{return each.type=="案管系統"}).Person+"人、在案人數總計"+text.find(each=>{return each.type=="在案人數"}).Person+"人";
                    ChartTable_FitSize("chart2",0);
                    var newChart = amchartfunc(chartID,Params,Params.ChartType);
                                    setTimeout(() => {
                                                var aaTable = new ChartTable(chartID+"Table");
                                                aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                                aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                                aaTable.dataSource=[tableBodyRow];
                                                aaTable.columnSource=[""].concat(lstItem);
                                                aaTable.bind();
                                                const firstLength=[tableBodyRow][0].title.split(',')[0].length+2;
                                                $.FitTable(aaTable, newChart, [""].concat(lstItem), [tableBodyRow],firstLength);
                                                $("#"+chartID).height(divHeight-aaTable.Height-40);
                                                $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                            }, 100);
                                }
                }); // end am4core.ready()
            })
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"DrugAgeByYear":
        $.getDrugAgeByYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                var lstItem = [];
                Params.ChartType = "Group";
                var tableBodyRow = [];
                Params.Data = [];
                Params.Color=["#0031CC", "#ff0000", "#0dd3c4"];
                Params.Year  = [];
                Params.Age_Class=[];
                //Params.Name = moment(sDate).format("YYYY/MM");
                Params.Title = "二、年齡層統計";
                Params.fonSize=12;
                Params.ColumnsWidth=50;
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;

                    }
                Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach(element => {
                        Params.Year.push(element["YEAR"]);
                        Params.Age_Class.push(element["Age_Class"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.Age_Class = [...new Set(Params.Age_Class)];
                    Params.Year.sort();
                    Params.Age_Class.sort(function(a,b) { return a.replace('未','0').localeCompare(b.replace('未','0'), "zh-Hant"); });
                    //Params.Gender.reverse();
                    lstItem = Params.Age_Class;
                    Params.Data =  Params.Age_Class.map(function(item,iItem){
                        var newData={};
                        newData.category=item;
                        Params.Year.forEach(year=>{
                            var findPerson = data.find((each)=>{return each.YEAR==year && each.Age_Class==item});
                            if(findPerson!==undefined)
                                newData[year] = parseInt(findPerson.Person);
                            else
                                newData[year] = 0;
                        })
                        newData.color=  Params.Color;
                        return newData;
                    });
                    tableBodyRow = Params.Year.map(function(element,index){
                        var newData = {};
                        newData.title =element+"年"+','+Params.Color[index];
                        Params.Age_Class.forEach(age=>{
                            var findPerson = data.find((each)=>{return each.YEAR==element && each.Age_Class==age});
                            if(findPerson!==undefined)
                                newData["col"+age] = findPerson.Person;
                            else
                                newData["col"+age] = '0';
                        })
                        return newData;
                    });
                    document.getElementById(chartID+"Text").innerHTML = "註：含案管系統及三四級講習";
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"ColumnSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=tableBodyRow;
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                            }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
//三、藥物濫用類型統計
    case"DrugAbuseTypeByMonth":
        $.getDrugAbuseTypeByMonth(sDate,eDate,function(data){
            $.getSelectCommentTextByMonth(sDate,eDate,function(text){
                const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                    if($("#"+div).html()==""){
                        $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                        <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                    }
                am4core.ready(function () {
                    //am4core.addLicense("{{config('custom.ChartsPN')}}");
                    var Params = {};
                    var lstItem = [];
                    Params.ChartType = "StackedColumn";
                    Params.Type=[];
                    Params.Level=[];
                    Params.Data = [];
                    Params.Color = ["#0031CC", "#ff0000", "#0dd3c4", "#c89058", "#7030a0"];
                    Params.Name = eDate;
                    Params.Title = "三、藥物濫用類型統計";
                    Params.fonSize=12;
                    Params.Total = 0;
                    Params.Stacked = true;
                    Params.ColumnsWidth=50;
                    Params.Year = sDate.substring(0,4)-1911;
                    Params.Month = eDate.split('/')[1].replace('0','');
                    var tableBodyRow = [];
                    if(title && breadcrumbtitle && source && bannerhref){
                            title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                            bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                            bannerhref.href="DrugCase";
                            source.innerHTML="來源：高雄市政府毒品防制局";
                            Params.fonSize=18;

                        }
                    Params.Title2 = {
                            title: "單位：人數",
                            fonSize: Params.fonSize,
                            color: "#000000",
                            align: "right",
                            valign: "top",
                            dy: 0
                    };
                    if(data.length>0){
                        data.forEach((element,index)=>{
                        Params.Level.push(element["LEVEL"]);
                        Params.Type.push(element["Type"]);
                        });

                        Params.Level = [...new Set(Params.Level)];
                        Params.Type = [...new Set(Params.Type)];
                        Params.Data = Params.Level.map((element,index)=>{
                            var newData={};
                            // lstItem.push(Params.Year+"年"+Params.Month+"月" + "<br>" + element);
                            lstItem.push( element);
                            newData.category = element;
                            Params.Type.forEach(type=>{
                                var findCount = data.find(each=>{return each.Type==type && each.LEVEL == element});
                                if(findCount!==undefined)
                                    newData[type] = findCount.Person;
                                else
                                    newData[type] = 0;
                            })
                            newData.color=Params.Color;
                            return newData;
                        })
                        tableBodyRow = Params.Type.map(function(type,index){
                            var newData = {};
                            newData.title =type;
                            Params.Level.forEach((level,index)=>{
                                var findCount = data.find(each=>{return each.Type==type && each.LEVEL == level});
                                    if(findCount==undefined)
                                        if(level=="其他"&&type=="混用")
                                            newData[level]='';
                                        else
                                            newData[level]='0';
                                    else
                                        newData[level]=findCount.Person;
                            })
                            return newData;
                        })
                        document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月三四級講習"+text.find(each=>{return each.type=="三四級講習"}).Person+"人、案管系統"+
                        text.find(each=>{return each.type=="案管系統"}).Person+"人、在案人數總計"+text.find(each=>{return each.type=="在案人數"}).Person+"人";
                    var newChart = amchartfunc(chartID,Params,Params.ChartType,"ColumnSeries");
                                    setTimeout(() => {
                                                var aaTable = new ChartTable(chartID+"Table");
                                                aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                                aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                                aaTable.dataSource=tableBodyRow;
                                                aaTable.columnSource=[""].concat(lstItem);
                                                aaTable.bind();
                                                $("#"+chartID).height(divHeight-aaTable.Height-40);
                                                $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                            }, 100);
                                }
                }); // end am4core.ready()
            })
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"DrugAbuseTypeByYear":
        $.getDrugAbuseTypeByYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                var lstItem = [];
                Params.ChartType = "GroupStackedColumnBar";
                Params.Type=[];
                Params.Level=[];
                Params.Data = [];
                Params.fill=["#FFC000","#A5A5A5"];
                Params.Name = eDate;
                Params.Title = "三、藥物濫用類型統計";
                Params.fonSize=12;
                Params.Color=["#0031CC","#ff0000","#0dd3c4"];
                Params.Total = 0;
                Params.Stacked = true;
                Params.Year =[];
                Params.ColumnsWidth=50;
                Params.month = sDate.split('/')[1].replace('0','');
                var tableBodyRow = [];
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;

                    }
                Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach((element,index)=>{
                    Params.Level.push(element["LEVEL"]);
                    Params.Year.push(element["YEAR"]);
                    Params.Type.push(element["Type"]);
                    });
                    Params.Level = [...new Set(Params.Level)];
                    Params.Type = [...new Set(Params.Type)];
                    Params.Year = [...new Set(Params.Year)];

                    const ChartTableData = StackedNoSortChartTableData(data,"LEVEL","YEAR","Type",  Params.Color);
                    Params.Data = ChartTableData.lstData;
                    Params.Level.sort(
                        function(a,b){return a.localeCompare(b,"zh-Hant");}
                    );
                    Params.Year.sort();
                    lstItem=Params.Level.map(each=>{
                        var newData={};
                        newData.title=each
                        newData["category"]=[]
                        Params.Year.map((eachyear,index)=>{
                            newData["category"][index]=eachyear;
                            return newData;
                        })
                        return newData;
                    })
                    Params.Type.sort(function (a, b) {
                        return a.localeCompare(b, "zh-Hant");});
                    tableBodyRow = Params.Type.map(function(element,lelement){
                        var newData = {};
                        newData.title =element;
                        Params.Level.forEach((eachLevel,index)=>{
                            Params.Year.forEach((eachYear,ieach)=>{
                                newData["value"+lelement+index+ieach] = data.filter(each=>{return each.Type==element && each.LEVEL == eachLevel &&each.YEAR==eachYear})[0]
                                if(eachLevel=="其他"&&element=="混用")
                                    newData["value"+lelement+index+ieach] ='0';
                                else if(newData["value"+lelement+index+ieach] ==undefined)
                                    newData["value"+lelement+index+ieach] ='0';
                                else
                                    newData["value"+lelement+index+ieach] = newData["value"+lelement+index+ieach].count;
                            })
                            return newData;
                        })
                        return newData;
                    })
                    document.getElementById(chartID+"Text").innerHTML = "註：含案管系統及三四級講習";
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"ColumnSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=tableBodyRow;
                                            aaTable.spicial=true;
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
//四、年齡層與類型交叉分析
    case"DrugAgeTypeByMonth":
        $.getDrugAgeTypeByMonth(sDate,eDate,function(data){
            $.getSelectCommentTextByMonth(sDate,eDate,function(text){
                const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                    if($("#"+div).html()==""){
                        $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                        <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                    }
                am4core.ready(function () {
                    //am4core.addLicense("{{config('custom.ChartsPN')}}");
                    var Params = {};
                    var lstItem = [];
                    Params.ChartType = "GroupStackedColumnBar";
                    Params.Level=[];
                    Params.Age_Class=[];
                    Params.Type=[];
                    Params.Data = [];
                    Params.Datasourece=[];
                    Params.Name = eDate;
                    Params.Title = "四、年齡層與類型交叉分析";
                    Params.fonSize=12;
                    // Params.Color=["#0031CC","#FF0000","#008000","#833C0B","#FFC000"];
                    Params.Color = ["#0031CC", "#FF0000", "#0dd3c4", "#996633", "#7030a0"];
                    Params.TitleColor = ["#b4c7e7","#0031CC","#ffb19f", "#FF0000","#00ffcc", "#0dd3c4","#cc9900", "#996633", "#7030a0"];
                    Params.Total = 0;
                    Params.Year = sDate.substring(0,4)-1911;
                    Params.Month = eDate.split('/')[1].replace('0','');
                    Params.xAxis = ["一級毒品","二級毒品","三級毒品","四級毒品","其他"];
                    var tableBodyRow =[];
                    if(title && breadcrumbtitle && source && bannerhref){
                            title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                            bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                            bannerhref.href="DrugCase";
                            source.innerHTML="來源：高雄市政府毒品防制局";
                            Params.fonSize=18;

                        }
                    Params.Title2 = {
                            title: "單位：人數",
                            fonSize: Params.fonSize,
                            color: "#000000",
                            align: "right",
                            valign: "top",
                            dy: 0
                    };
                    if(data.length>0){
                        data.forEach((element,index)=>{
                        Params.Level.push(element["LEVEL"]);
                        Params.Age_Class.push(element["Age_Class"]);
                        Params.Type.push(element["Type"]);
                        Params.Total+=parseInt(element["count"]);
                        });
                        Params.Level = [...new Set(Params.Level)];
                        Params.Age_Class = [...new Set(Params.Age_Class)];
                        Params.Type = [...new Set(Params.Type)];
                        Params.Age_Class.sort(function(a,b) { return a.replace('未','0').localeCompare(b.replace('未','0'), "zh-Hant"); });
                        const ChartTableData = StackedNoSortChartTableData(data,"LEVEL","Age_Class","Type", Params.Color);
                        Params.Data = ChartTableData.lstData.sort(function(a,b) { return a.category.replace('未','0').localeCompare(b.category.replace('未','0'), "zh-Hant"); });
                        lstItem = Params.Age_Class;
                        Params.Type.sort(function (a, b) {
                            return a.localeCompare(b, "zh-Hant");});
                        var sumTitle = [];
                        Params.Level.forEach(each=>{
                            Params.Type.forEach(type=>{
                                if(each=="其他" && type=="單一")
                                    sumTitle.push(each);
                                else if(each!="其他")
                                    sumTitle.push(each+"-"+type);
                            })
                        })
                        tableBodyRow = sumTitle.map((title,index)=>{
                            var newData={};
                            newData.title  = title+','+Params.TitleColor[index];
                            Params.Age_Class.forEach(age=>{
                                var findCount = data.find(each=>{return each.Type == title.split("-")[1] && each.LEVEL == title.split("-")[0] && each.Age_Class==age});
                                if(findCount!==undefined)
                                    newData[age] = findCount.count;
                                else
                                    newData[age] = '0'
                            })
                            return newData;
                        })
                        document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月三四級講習"+text.find(each=>{return each.type=="三四級講習"}).Person+"人、案管系統"+
                        text.find(each=>{return each.type=="案管系統"}).Person+"人、在案人數總計"+text.find(each=>{return each.type=="在案人數"}).Person+"人";
                    var newChart = amchartfunc(chartID,Params,Params.ChartType,"ColumnSeries");
                                    setTimeout(() => {
                                                var aaTable = new ChartTable(chartID+"Table");
                                                const firstLength=tableBodyRow[0].title.split(',')[0].length+8;
                                                $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow,firstLength);
                                                $("#"+chartID).height(divHeight-aaTable.Height-40);
                                                $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                            }, 100);
                                }
                }); // end am4core.ready()
            })
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"DrugAgeTypeByYear":
        $.getDrugAgeTypeByYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                var lstItem = [];
                Params.ChartType = "Group";
                Params.Level=[];
                Params.Age_Class=[];
                Params.Type=[];
                Params.Data = [];
                Params.fill=["#FFC000","#A5A5A5"];
                Params.Name = eDate;
                Params.Title = "四、年齡層與類型交叉分析";
                Params.fonSize=12;
                Params.Color = ["#0031CC", "#FF0000", "#0dd3c4", "#996633", "#7030a0"];
                Params.Total = 0;
                Params.Year =[];
                Params.month = sDate.split('/')[1].replace('0','');
                Params.xAxis = ["一級毒品","二級毒品","三級毒品","四級毒品","其他"];
                var tableBodyRow = [];
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;

                    }
                Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach((element,index)=>{
                    Params.Level.push(element["LEVEL"]);
                    Params.Age_Class.push(element["Age_Class"]);
                    Params.Year.push(element["Year"]);
                    });
                    Params.Level = [...new Set(Params.Level)];
                    Params.Age_Class = [...new Set(Params.Age_Class)];
                    Params.Year = [...new Set(Params.Year)];
                    Params.Year.sort();
                    Params.Level.sort(function(a,b){return a.localeCompare(b,"zh-Hant")});
                    const ChartTableData = StackedNoSortChartTableData(data,"Year","Age_Class","LEVEL", Params.Color);
                    Params.Data = ChartTableData.lstData;
                    Params.Data.sort(function(a,b){return a.category.replace("未","0").localeCompare(b.category.replace("未","0"),"zh-Hant")})

                    lstItem =  Params.Age_Class.map((each,index)=>{
                        var newData={};
                        newData.title = each;
                        newData.category = [];
                        Params.Year.map(each=>{
                            newData.category.push(each);
                        })
                        return newData;
                    })
                    tableBodyRow = Params.Level.map(function(element,lelement){
                        var newData = {};
                        newData.title =element+','+Params.Color[lelement];
                        Params.Age_Class.forEach((eachage,ieach)=>{
                            Params.Year.forEach((eachYear,index)=>{
                                newData["value"+lelement+index+ieach] = data.filter(each=>{return each.LEVEL==element && each.Year == eachYear && each.Age_Class==eachage})[0]
                                if(newData["value"+lelement+index+ieach] ==undefined)
                                    if(element == "混用" && eachLevel == "其他")
                                        newData["value"+lelement+index+ieach] =' ';
                                    else
                                        newData["value"+lelement+index+ieach] ='0';
                                else
                                    newData["value"+lelement+index+ieach] = newData["value"+lelement+index+ieach] .count;
                            })
                            return newData;
                        })
                        return newData;
                    })
                    document.getElementById(chartID+"Text").innerHTML = "註：含案管系統及三四級講習";
                    ChartTable_FitSize("chartY4",0);
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"ColumnSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=tableBodyRow;
                                            aaTable.spicial=true;
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            const firstLength=tableBodyRow[0].title.split(',')[0].length+4;
                                            if(aaTable.fontSize>0){
                                                const firstColumnWidth = aaTable.fontSize*(firstLength);
                                                const chartWidth = newChart.contentWidth-newChart.rightAxesContainer.contentWidth;
                                                if(firstColumnWidth>newChart.leftAxesContainer.contentWidth){
                                                    newChart.leftAxesContainer.width = firstColumnWidth;
                                                    aaTable.headerWidth = firstColumnWidth+14;
                                                    aaTable.bodyWidth = chartWidth-firstColumnWidth+1;
                                                    aaTable.setWidth();
                                                }
                                            }
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
//五、區域(戶籍地)未校正數
    case"DrugAreaByMonth":
    $.getDrugAreaByMonth(sDate,eDate,function(data){
        const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
        am4core.ready(function () {
            //am4core.addLicense("{{config('custom.ChartsPN')}}");
            var Params = {};
            var lstItem = [];
            Params.Data = [];
            Params.Color="#7030A0";
            var tableBodyRow = {title:"列管人數"+','+Params.Color};
            Params.Title = "五、區域(戶籍地)未校正數";
            Params.Year = sDate.substring(0,4)-1911;
            Params.month = sDate.split('/')[1].replace('0','');
            Params.Total=0;
            Params.FontSize=12;
            Params.ColumnsWidth=50;
            if(title && breadcrumbtitle && source && bannerhref){
                    title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                    bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                    bannerhref.href="DrugCase";
                    source.innerHTML="來源：衛福部毒癮單窗服務系統";
                    Params.FontSize=15+"px";

                }
            if(data.length>0){
                data.forEach((element,index)=>{
                Params.Data.push({category:element["Area_Name"],count:element["Person"]})
                lstItem.push(element["Area_Name"]);
                tableBodyRow["Area_Name"+(index)] = element["Person"];
                Params.Total+=parseInt(element["Person"]);
                });

                var TopThree= data.filter((each,index)=>{return index<3}).map(each=>{return each.Area_Name.substring(0,2)}).join("、");
                document.getElementById(chartID+"Text").innerHTML ="註："+ Params.month + "月列管總計" + Params.Total + "人，前三熱區：" + TopThree;

                var newChart = amchartfunc(chartID,Params,"Column");

                setTimeout(() => {
                            var aaTable = new ChartTable(chartID+"Table");
                            const firstLength=[tableBodyRow][0].title.split(',')[0].length+2;
                            $.FitTable(aaTable, newChart, [""].concat(lstItem), [tableBodyRow],firstLength,true);
                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                        }, 100);
            };


        }); // end am4core.ready()
    })
    $(window).on('resize',function(){
        var chart = am4core.registry.baseSprites.find(function(chartObj) {
            return chartObj.htmlContainer.id === chartID;
        });
        setTimeout(() => {
            var aaTable = getChartTable(chartID+"Table");
            aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
            aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
            aaTable.setWidth();
            $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
        }, 100);
    });
    break;
    case"DrugAreabyYear":
        $.getDrugAreabyYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('large')>-1?80:0)-(div.indexOf('popup')>-1?80:0);
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                Params = {};
                Params.ChartType = "GroupColumnBar";
                Params.Title = "五、區域(戶籍地)未校正數";
                Params.fontSize = 10;
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：衛福部毒癮單窗服務系統";
                        Params.fontSize = 15;
                        $("#"+chartID+"Table").css("margin-top","-16px");

                }
                Params.TitleColor = "#00B48B";
                Params.Color=["#0031CC", "#FF0000", "#0dd3c4"];
                Params.FontSize = 10;
                Params.categoryAxisVisible = false;
                Params.Location = [];
                Params.Year=[];
                var lstItem = [];
                var tableBodyRow = [];
                data.forEach(element => {
                    Params.Year.push(element["YEAR"]);
                    Params.Location.push(element["Area_Name"]);
                });
                Params.Year = [...new Set(Params.Year)];
                Params.Location = [...new Set(Params.Location)];
                Params.Year.sort();
                lstItem=Params.Location;
                Params.Data =  Params.Location.map(function(location,iItem){
                    var newData={};
                    newData.category=location;
                    data.filter((each)=>{return each.Area_Name==location}).forEach(function(each,ieach){
                        newData[each.YEAR]=each.Person;
                    });
                    newData.color=Params.Color;
                    return newData;
                });
                tableBodyRow =  Params.Year.map(function(year,iItem){
                    var newData={};
                    newData.title = year+"年"+','+Params.Color[iItem];
                    Params.Location.forEach((eachL,ieachL)=>{
                        newData["value"+(ieachL)] = data.filter((each)=>{return each.YEAR==year && each.Area_Name==eachL})[0];
                        if(newData["value"+(ieachL)]==undefined)
                            newData["value"+(ieachL)]='0';
                        else
                            newData["value"+(ieachL)]= newData["value"+(ieachL)].Person;
                    })
                    return newData;
                });
                Params.lastYear = Params.Year.pop();
                document.getElementById(chartID+"Text").innerHTML ="註：前三熱區："+Params.Year.map((eachYear,index)=>{
                    var TopThree=data.filter(each=>{
                        return each.YEAR==eachYear&&each.NUM<4
                    }).map(eachLocation=>{return eachLocation.Area_Name.substring(0,2)}).join("、");
                    return eachYear+"年："+TopThree;
                }).join("、") + "<br>" + Params.lastYear+"年："+data.filter(each=>{return each.YEAR==Params.lastYear&&each.NUM<4}).map(eachLocation=>{return eachLocation.Area_Name.substring(0,2)}).join("、");
                var newChart = amchartfunc(chartID,Params, Params.ChartType);
                setTimeout(() => {
                            var aaTable = new ChartTable(chartID+"Table");
                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                            aaTable.dataSource=tableBodyRow;
                            aaTable.columnSource=[""].concat(lstItem);
                            aaTable.vertical = true;
                            aaTable.bind();
                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                        }, 100);
            })
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
//六、區域(戶籍地)校正數
    case"DrugCorrectionAreaByMonth":
        $.getDrugCorrectionAreaByMonth('2021/12',eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('large')>-1?80:0)-(div.indexOf('popup')>-1?80:0);;;

                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                var lstItem = [];
                Params.Data = [];
                Params.Color="#7030A0";
                var tableBodyRow = {title:"校正數"+','+Params.Color};
                Params.Title = "六、區域(戶籍地)校正數";
                Params.Year = sDate.substring(0,4)-1911;
                Params.Month = eDate.split('/')[1].replace('0','');
                Params.Total=0;
                Params.ColumnsWidth=50;
                Params.fontSzie = 10;
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：衛福部毒癮單窗服務系統";
                        Params.fontSzie=15;

                    }
                if(data.length>0){
                    data.forEach((element,index)=>{
                    Params.Data.push({category:element["Location"],count:element["Person"]})
                    lstItem.push(element["Location"]);
                    Params.Total += parseInt(element["Person"])
                    tableBodyRow["Location"+(index)] = element["Person"];
                    });
                    var TopThree= data.filter((each,index)=>{return index<3}).map(each=>{return each.Location.substring(0,2)}).join("、");
                    document.getElementById(chartID+"Text").innerHTML ="註：<br>1. "+Params.Month+"月列管總計"+Params.Total+"人，前三熱區："+TopThree
                    +"<br>2.【校正數為(該區列管數/該區人口數)×100,000】";
                    }

                    var newChart = amchartfunc(chartID,Params,"Column");
                        setTimeout(() => {
                                    var aaTable = new ChartTable(chartID+"Table");
                                    aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                    aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                    aaTable.dataSource=[tableBodyRow];
                                    aaTable.columnSource=[""].concat(lstItem);
                                    aaTable.vertical = true;
                                    aaTable.bind();
                                    $("#"+chartID).height(divHeight-aaTable.Height-60);
                                    $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                }, 100);
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"DrugCorrectionAreaByYear":
        $.getDrugCorrectionAreaByYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('large')>-1?80:0)-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                Params = {};
                Params.ChartType = "GroupColumnBar";
                Params.Title = "六、區域(戶籍地)校正數";
                Params.fontSize=10;
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="TaiwanDrugAnalysis";
                        source.innerHTML="來源：衛福部毒癮單窗服務系統";
                        Params.fontSize=15;

                }
                Params.TitleColor = "#00B48B";
                Params.Color=["#0031CC", "#FF0000", "#0dd3c4"];
                Params.FontSize = 10;
                Params.categoryAxisVisible = false;
                Params.Location = [];
                Params.Year=[];
                var lstItem = [];
                var tableBodyRow = [];
                data.forEach(element => {
                    Params.Year.push(element["CHINA_Year"]);
                    Params.Location.push(element["Location"]);
                });
                Params.Year = [...new Set(Params.Year)];
                Params.Location = [...new Set(Params.Location)];
                Params.Year.sort();
                lstItem=Params.Location;
                Params.Data =  Params.Location.map(function(location,iItem){
                    var newData={};
                    newData.category=location;
                    data.filter((each)=>{return each.Location==location}).forEach(function(each,ieach){
                        newData[each.CHINA_Year]=each.Person;
                    });
                    newData.color=Params.Color;
                    return newData;
                });
                tableBodyRow =  Params.Year.map(function(year,iItem){
                    var newData={};
                    newData.title = year+"年"+','+Params.Color[iItem];
                    data.filter((each)=>{return each.CHINA_Year==year}).forEach(function(each,ieach){
                        newData["Location"+(ieach)]=each.Person;
                    });
                    return newData;
                });
                    document.getElementById(chartID+"Text").innerHTML ="註：<br>1. 前三熱區："+Params.Year.map((eachYear,index)=>{
                        var TopThree=data.filter(each=>{
                            return each.CHINA_Year==eachYear&&each.NUM<4
                        }).map(eachLocation=>{return eachLocation.Location.substring(0,2)}).join("、");
                        return eachYear+"年："+TopThree;
                    }).join("、")+"<br>2. 【校正數為(該區列管數/該區人口數)×100,000】";
                var newChart = amchartfunc(chartID,Params,  Params.ChartType);
                setTimeout(() => {
                            var aaTable = new ChartTable(chartID+"Table");
                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                            aaTable.dataSource=tableBodyRow;
                            aaTable.columnSource=[""].concat(lstItem);
                            aaTable.vertical = true;
                            aaTable.bind();
                            $("#"+chartID).height(divHeight-aaTable.Height-70);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                        }, 100);
            })
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id ===chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.vertical = true;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
//七、本市藥癮個案列管追蹤輔導數
    case"DrugCoachByMonth":
        $.getDrugCoachByMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;

                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.Data = [];
                Params.Color="#7030A0";
                Params.Total=0;
                Params.fonSize=12;
                Params.ColumnsWidth=50;
                Params.Title = "七、本市藥癮個案列管追蹤輔導數";
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：衛福部毒癮單窗服務系統";
                        Params.fonSize=18;

                    }
                Params.Title2 = {
                        title: "單位：人次",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    var tableBodyRow = {title:data[0]["Date_Merk"]+','+Params.Color};
                    data.forEach((element,index)=>{
                    if(element["Item"]!=="全部"){
                        Params.Data.push({category:element["Item"],count:element["Count"]});
                        lstItem.push(element["Item"]);
                        tableBodyRow["Item"+(index)] = element["Count"];
                    }
                    else
                        Params.Total += parseInt(element["Count"]);
                    });
                    document.getElementById(chartID+"Text").innerHTML="註：總計"+Params.Total+"人";

                var newChart = amchartfunc(chartID,Params,"Column");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=[tableBodyRow];
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                            }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"DrugCoachByYear":
    $.getDrugCoachByYear(sYear,eYear,function(data){
        const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
            }
        am4core.ready(function () {
            var Params = {};
            var lstItem = [];
            var tableBodyRow=[];
            Params.Data = [];
            Params.Color=["#0031CC", "#FF0000", "#0dd3c4"];
            Params.Total=0;
            Params.fonSize=12;
            Params.ColumnsWidth=30;
            Params.Year=[];
            Params.Item = [];
            Params.ChartType="GroupColumnBar";
            Params.Title = "七、本市藥癮個案列管追蹤輔導數";
            Params.Title2 = {
                title: "單位：人次",
                fonSize: Params.fonSize,
                color: "#000000",
                align: "right",
                valign: "top",
                dy: 0
            };
            if(title && breadcrumbtitle && source && bannerhref){
                    title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                    bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                    bannerhref.href="DrugCase";
                    source.innerHTML="來源：衛福部毒癮單窗服務系統";
                    Params.fonSize=18;

                }
            if(data.length>0){
                data.forEach(element=>{
                    Params.Year.push(element["Year"]);
                    Params.Item.push(element["Item"]);
                })
                Params.Year = [...new Set(Params.Year)];
                Params.Item = [...new Set(Params.Item)];
                Params.Year.sort();
                lstItem = Params.Item;
                Params.Data = Params.Item.map(function (Item, iItem) {
                    var newData = {};
                    newData.category = Item;
                    data.filter((each) => {
                        return each.Item == Item
                    }).forEach(function (each, ieach) {
                        newData[each.Year+"年"] = each.Count;
                    });
                    newData.color = Params.Color;
                    return newData;
                });

                tableBodyRow = Params.Year.map(function (element, index) {
                    var newData = {};
                    newData.title = element + "年" + ',' + Params.Color[index];
                    data.filter((each) => {
                        return each.Year == element
                    }).forEach(function (each, ieach) {
                        newData["Location" + (ieach)] = each.Count;
                    });
                    return newData;
                });
            var newChart = amchartfunc(chartID,Params,Params.ChartType);
                            setTimeout(() => {
                                        var aaTable = new ChartTable(chartID+"Table");
                                        aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                        aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                        aaTable.dataSource=tableBodyRow;
                                        aaTable.columnSource=[""].concat(lstItem);
                                        aaTable.bind();
                                        $("#"+chartID).height(divHeight-aaTable.Height-40);
                                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                    }, 100);
                        }
        }); // end am4core.ready()
    })
    $(window).on('resize',function(){
        var chart = am4core.registry.baseSprites.find(function(chartObj) {
            return chartObj.htmlContainer.id === chartID;
        });
        setTimeout(() => {
            var aaTable = getChartTable(chartID+"Table");
            aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
            aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
            aaTable.setWidth();
            $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
        }, 100);
    });
    break;
//八、本市藥癮個案轉介服務數
    case "DrugServiceByMonth":
        $.getDrugServiceByMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            $("#"+chartID).height(divHeight-40);
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                var tableBodyRow = [];
                Params.Data = [];
                Params.Color=["#FF0000","#0031CC","#008000","#833C0B","#0dd3c4","#7030a0"];
                Params.fonSize=12;
                Params.ChartType="Pie2";
                Params.Title = "八、本市藥癮個案轉介服務數";
                Params.Total=0;
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：衛福部毒癮單窗服務系統";
                        Params.fonSize=18;

                    }
                Params.Title2 = {
                        title: "單位：人次",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };

                if(data.length>0){
                    var total = [];
                    if(data[0]["Item"]=="全部")
                        total = data.shift();
                    data.forEach((element,index)=>{
                        if(element["Count"]!=0)
                            Params.Data.push({category:element["Item"],count:element["Count"],percent:Math.round(element["Rate"]*100),color:Params.Color[index]})


                    });
                    document.getElementById(chartID+"Text").innerHTML="註："+ total["Date_Merk"].split("年")[1] +"總計"+total["Count"]+"人";
                var newChart = amchartfunc(chartID,Params,Params.ChartType);
                                setTimeout(() => {
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                            }
            }); // end am4core.ready()
    })
    $(window).on('resize',function(){
        var chart = am4core.registry.baseSprites.find(function(chartObj) {
            return chartObj.htmlContainer.id === chartID;
        });
        setTimeout(() => {
            $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
        }, 100);
    });
    break;
    case "DrugServiceByYear":
        $.getDrugServiceByYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            $("#"+chartID).height(divHeight-40);
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                var tableBodyRow = [];
                Params.Data = [];
                Params.Color=["#0031CC", "#FF0000", "#0dd3c4"];
                Params.fonSize=12;
                Params.ChartType="GroupColumnBar";
                Params.Title = "八、本市藥癮個案轉介服務數";
                Params.Total=0;
                Params.ColumnsWidth=30;
                Params.Year=[];Params.Item=[];
                Params.Title2 = {
                    title: "單位：人數",
                    fonSize: Params.fonSize,
                    color: "#000000",
                    align: "right",
                    valign: "top",
                    dy: 0
                };
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：衛福部毒癮單窗服務系統";
                        Params.fonSize=18;

                    }
                Params.Title2 = {
                        title: "單位：人次",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };

                if(data.length>0){
                    data.forEach(element=>{
                        Params.Year.push(element["Year"]);
                        Params.Item.push(element["Item"]);
                    })
                    Params.Year = [...new Set(Params.Year)];
                    Params.Item = [...new Set(Params.Item)];
                    Params.Year.sort();
                    lstItem = Params.Item;
                    Params.Data = Params.Item.map(function (Item, iItem) {
                        var newData = {};
                        newData.category = Item;
                        data.filter((each) => {
                            return each.Item == Item
                        }).forEach(function (each, ieach) {
                            newData[each.Year+"年"] = each.Count;
                        });
                        newData.color = Params.Color;
                        return newData;
                    });

                    tableBodyRow = Params.Year.map(function (element, index) {
                        var newData = {};
                        newData.title = element + "年" + ',' + Params.Color[index];
                        data.filter((each) => {
                            return each.Year == element
                        }).forEach(function (each, ieach) {
                            newData["Location" + (ieach)] = each.Count;
                        });
                        return newData;
                    });

                var newChart = amchartfunc(chartID,Params,Params.ChartType);
                                setTimeout(() => {
                                        var aaTable = new ChartTable(chartID+"Table");
                                        aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                        aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                        aaTable.dataSource=tableBodyRow;
                                        aaTable.columnSource=[""].concat(lstItem);
                                        aaTable.bind();
                                        $("#"+chartID).height(divHeight-aaTable.Height-40);
                                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                            }
            }); // end am4core.ready()
    })
    $(window).on('resize',function(){
        var chart = am4core.registry.baseSprites.find(function(chartObj) {
            return chartObj.htmlContainer.id === chartID;
        });
        setTimeout(() => {
            var aaTable = getChartTable(chartID+"Table");
            aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
            aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
            aaTable.setWidth();
            $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
        }, 100);
    });
    break;
//九、監所銜接輔導數
    case "DrugCouplingByMonth":
        $.getDrugCouplingByMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                var tableBodyRow = [];
                Params.Data = [];
                Params.fonSize=12;
                Params.Session=[];Params.Person_Times=[];
                Params.ChartType="LineColumn";
                Params.Title = "九、監所銜接輔導數";
                Params.Total=0;
                Params.TableTitle = ["場次","人次"];

                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;

                    }
                Params.Color=["#0dd3c4","#7030A0"];
                if(data.length>0){
                    data.forEach((element)=>{
                        Params.Session.push(element["Session"]);
                        Params.Person_Times.push(element["Person_Times"]);
                    })
                    Params.Data =  data.map((element,index)=>{
                        var newData={};
                        newData.category = element["MONTH"]+"月";
                        newData["場次"] = element["Session"];
                        newData["人次"] = element["Person_Times"];
                        newData.color = Params.Color;
                        lstItem.push(sDate.substring(0,4)-1911+"年"+element["MONTH"]+"月");
                        return newData;
                    });
                    tableBodyRow = Params.TableTitle.map((element,index)=>{
                        var newData = {};
                        newData.title = element+','+Params.Color[index];
                        if(element=="場次"){
                            Params.Session.map((each,i) => {
                                newData["col"+i] = each;
                            });
                            return newData;
                        }
                        else{
                            Params.Person_Times.map((each,i) => {
                                newData["col"+i] = each;
                            });
                            return newData;
                        }
                    })

                var newChart = amchartfunc(chartID,Params,Params.ChartType);
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=tableBodyRow;
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                            }
            }); // end am4core.ready()
    })
    $(window).on('resize',function(){
        var chart = am4core.registry.baseSprites.find(function(chartObj) {
            return chartObj.htmlContainer.id === chartID;
        });
        setTimeout(() => {
            var aaTable = getChartTable(chartID+"Table");
            aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
            aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
            aaTable.setWidth();
            $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
        }, 100);
    });
    break;
    case "DrugCouplingByYear":
        $.getDrugCouplingByYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                var tableBodyRow = [];
                Params.Data = [];
                Params.fonSize=12;
                Params.Session=[];Params.Person_Times=[];
                Params.ChartType="LineColumn";
                Params.Title = "九、監所銜接輔導數";
                Params.Total=0;
                Params.Year=[];
                Params.LineNoCircle=true;
                Params.TableTitle = ["場次","人次"];
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;

                    }
                Params.Color=["#0dd3c4","#7030A0"];
                if(data.length>0){
                    data.forEach((element)=>{
                        Params.Year.push(element["YEAR"]);
                        Params.Session.push(element["Session"]);
                        Params.Person_Times.push(element["Person_Times"]);
                    })
                    Params.Year.sort();
                    Params.Data = Params.Year.map(function (y, i) {
                        var newData = {};
                        newData.category = y;
                        lstItem.push(y+"年");
                        data.filter((each) => {
                            return each.YEAR == y
                        }).forEach(function (each, ieach) {
                            newData["場次"] = each["Session"];
                            newData["人次"] = each["Person_Times"];
                        });
                        newData.color = Params.Color;
                        return newData;
                    });
                    tableBodyRow = Params.TableTitle.map(function (element, index) {
                        var newData = {};
                        newData.title = element +',' + Params.Color[index];
                        Params.Year.map((eachY)=>{
                            data.filter((each) => {
                                return each.YEAR == eachY
                            }).forEach(function (each, ieach) {
                                if(element=="場次"){
                                        newData["Year"+eachY] = each.Session;
                                    return newData;
                                }
                                else{
                                        newData["Year"+eachY] = each.Person_Times;
                                    return newData;
                                }
                            });
                            return newData;
                        })
                        return newData;
                    });
                var newChart = amchartfunc(chartID,Params,Params.ChartType);
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=tableBodyRow;
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                            }
            }); // end am4core.ready()
    })
    $(window).on('resize',function(){
        var chart = am4core.registry.baseSprites.find(function(chartObj) {
            return chartObj.htmlContainer.id === chartID;
        });
        setTimeout(() => {
            var aaTable = getChartTable(chartID+"Table");
            aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
            aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
            aaTable.setWidth();
            $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
        }, 100);
    });
    break;
//十、醫療戒治累計數
    case "DrugMedicalByMonth":
        $.getDrugMedicalByMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.Color="#7030A0";
                var tableBodyRow = {title:"累計人數"+','+Params.Color};
                Params.Data = [];
                Params.fonSize=12;
                Params.ChartType="Column";
                Params.Title = "十、醫療戒治累計數";
                Params.Total=0;
                Params.Title2 = {
                    title: "單位：人數",
                    fonSize: Params.fonSize,
                    color: "#000000",
                    align: "right",
                    valign: "top",
                    dy: 0
                };
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：高雄市政府衛生局";
                        Params.fonSize=18;

                    }
                // Params.Title2 = {
                //         title: "單位：人次",
                //         fonSize: Params.fonSize,
                //         color: "#000000",
                //         align: "right",
                //         valign: "top",
                //         dy: -5
                // };

                if(data.length>0){
                    data.forEach((element)=>{
                        Params.Data.push({category:element["Month"]+"月",count:element["Num"]});
                        tableBodyRow["col"+element["Month"]] = element["Num"];
                        lstItem.push(element["Month"]+"月");
                    })
                var newChart = amchartfunc(chartID,Params,Params.ChartType);
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=[tableBodyRow];
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                            }
            }); // end am4core.ready()
    })
    $(window).on('resize',function(){
        var chart = am4core.registry.baseSprites.find(function(chartObj) {
            return chartObj.htmlContainer.id === chartID;
        });
        setTimeout(() => {
            var aaTable = getChartTable(chartID+"Table");
            aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
            aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
            aaTable.setWidth();
            $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
        }, 100);
    });
    break;
    case "DrugMedicalByYear":
        $.getDrugMedicalByYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.Color="#7030A0";
                var tableBodyRow = {title:"醫療戒治"+','+Params.Color};
                Params.Data = [];
                Params.fonSize=12;
                Params.ChartType="Column";
                Params.Title = "十、醫療戒治累計數";
                Params.Total=0;
                Params.ColumnsWidth=30;
                Params.Year=[];
                Params.Title2 = {
                    title: "單位：人數",
                    fonSize: Params.fonSize,
                    color: "#000000",
                    align: "right",
                    valign: "top",
                    dy: 0
                };
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：高雄市政府衛生局";
                        Params.fonSize=18;

                    }
                if(data.length>0){
                    data.forEach((element)=>{
                        Params.Year.push(element["YEAR"]);
                    })
                    Params.Year.sort();
                    Params.Data = Params.Year.map(function (y, i) {
                        var newData = {};
                        newData.category = y;
                        lstItem.push(y+"年");
                        data.filter((each) => {
                            return each.YEAR == y
                        }).forEach(function (each, ieach) {
                            newData["count"] = each["Num"];
                            tableBodyRow["Y"+each.YEAR] = each["Num"];
                        });
                        newData.color = Params.Color;

                        return newData;
                    });
                var newChart = amchartfunc(chartID,Params,Params.ChartType);
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=[tableBodyRow];
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                            }
            }); // end am4core.ready()
    })
    $(window).on('resize',function(){
        var chart = am4core.registry.baseSprites.find(function(chartObj) {
            return chartObj.htmlContainer.id === chartID;
        });
        setTimeout(() => {
            var aaTable = getChartTable(chartID+"Table");
            aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
            aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
            aaTable.setWidth();
            $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
        }, 100);
    });
    break;
//十一、24小時專線服務類型累計數
    case"Drug24HoursByMonth":
        $.getDrug24HoursByMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('large')>-1?50:0)-(div.indexOf('popup')>-1?80:0);;;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                ');
            }
            $("#"+chartID).height(divHeight-55);
            am4core.ready(function () {
                var Params = {};
                Params.ChartType = "LabelBar";
                Params.Data = [];
                Params.Color="#7030A0";
                Params.Name = eDate;
                Params.Title = "十一、24小時專線服務類型累計數";
                Params.fonSize=12;

                Params.Title2 = {
                    title: "單位：次數",
                    fonSize: Params.fonSize,
                    color: "#000000",
                    align: "right",
                    valign: "top",
                    dy: 0
                };
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：衛福部毒癮單窗服務系統";
                        Params.fonSize=18;

                    }
                if(data.length>0){
                    data.forEach((element,index)=>{
                    Params.Data.push({category:element["Item"],count:parseInt(element["Count"])})
                    });
                var newChart = amchartfunc(chartID,Params, Params.ChartType);
                                setTimeout(() => {
                                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                    }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"Drug24HoursByYear":
        $.getDrug24HoursByYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('large')>-1?50:0)-(div.indexOf('popup')>-1?80:0);;;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                ');
            }
            $("#"+chartID).height(divHeight-55);
            am4core.ready(function () {
                var Params = {};
                Params.ChartType = "LabelGroupBar";
                Params.Data = [];
                Params.Fill="#7030A0";
                Params.Year=[];
                Params.Item=[];
                Params.Color=["#0031CC","#FF0000","#0dd3c4"];
                Params.Title = "十一、24小時專線服務類型累計數";
                Params.fonSize=10;
                Params.y=120;
                Params.Title2 = {
                    title: "單位：次數",
                    fonSize: Params.fonSize,
                    color: "#000000",
                    align: "right",
                    valign: "top",
                    dy: 0
                };
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市18歲(含)以上藥癮個案概況分析";
                        bannerhref.href="DrugCase";
                        source.innerHTML="來源：衛福部毒癮單窗服務系統";
                        Params.fonSize=12;
                        Params.y=390;

                    }
                if(data.length>0){
                    data.forEach((element)=>{
                        Params.Year.push(element["YEAR"]);
                        Params.Item.push(element["Item"]);
                    })
                    Params.Item = [...new Set(Params.Item)];
                    Params.Year = [...new Set(Params.Year)];
                    Params.Year.sort();
                    Params.Data = Params.Item.map(function (y, i) {
                        var newData = {};
                        newData.category = y;
                        data.filter((each) => {
                            return each.Item == y
                        }).forEach(function (each, ieach) {
                            newData[each.YEAR+"年"] = each["Count"];
                        });
                        newData.color = Params.Color;
                        return newData;
                    })
                var newChart = amchartfunc(chartID,Params, Params.ChartType);
                                setTimeout(() => {
                                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                    }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
}};
