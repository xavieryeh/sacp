function UploadPageUploadFile(self) {
    self.init = function(){
        selDataBind();
    }

    $("#selFileType").on('change', function () {
        selDataBind();
    });

    function selDataBind(){
        if($("#selFileType option:selected")[0].value=="LNI"){
            $("#dataGroup").show();
            $("#selData").html('<option value="">請選擇資料類別</option>\
            <option value="data1" selected>警察局</option>\
            <option value="data2">少年及家事法院</option>\
            <option value="data3">衛生局</option>\
            <option value="data4">民政局</option>');
        }
        else if($("#selFileType option:selected")[0].value=="CCMS"){
            $("#dataGroup").show();
            $("#selData").html('<option value="">請選擇資料類別</option>\
            <option value="data1" selected>衛福部-其他</option>\
            <option value="data2">衛福部-全國藥癮</option>\
            <option value="data3">戶政司</option>');
        }
        else if($("#selFileType option:selected")[0].value=="CCMS1"){
            $("#selData").html('');
            $("#dataGroup").hide();
        }
        else if($("#selFileType option:selected")[0].value=="CPCMS"){
            $("#selData").html('');
            $("#dataGroup").hide();
        }
        else if($("#selFileType option:selected")[0].value=="LTAFL"){
            $("#selData").html('');
            $("#dataGroup").hide();
        }
        else if($("#selFileType option:selected")[0].value=="CPCMS2"){
            $("#selData").html('');
            $("#dataGroup").hide();
        }
        else if($("#selFileType option:selected")[0].value=="CPCMS3"){
            $("#selData").html('');
            $("#dataGroup").hide();
        }
        else if($("#selFileType option:selected")[0].value=="CPCMS4"){
            $("#selData").html('');
            $("#dataGroup").hide();
        }
        else{
            $("#selData").html('');
            $("#dataGroup").hide();
        }
    }

    $("#btnFileUpload").on('click', function () {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var formData = new FormData();
        formData.append('year', $("#selYear option:selected")[0].value);
        formData.append('month', $("#selMonth option:selected")[0].value);
        var input = document.getElementById('customFile');
        if (input.files){
            var fileList = input.files;
            fileList.forEach(function (file) {
                formData.append('Files', file, file.name);
            });
        }

        var url = "";
        if($("#selFileType option:selected")[0].value=="LNI"){
            var dataType = $("#selData option:selected")[0].value;
            switch (dataType) {
                case 'data1':
                    url='Upload_DrugFiles';
                    break;
                case 'data2':
                    url='Upload_Juvenile_CourtFiles';
                    break;
                case 'data3':
                    url='Upload_MDTreatmentFiles';
                    break;
                case 'data4':
                    url='Upload_PopulationFiles';
                    break;
            }
        }
        else if($("#selFileType option:selected")[0].value=="CCMS"){
            var dataType = $("#selData option:selected")[0].value;
            console.log(dataType);
            switch (dataType) {
                
                case 'data1':
                    url='Upload_HealthOther';
                    break;
                case 'data2':
                    url='Upload_DrugcaseFiles';
                    break;
                case 'data3':
                    url='Upload_AllPeopleFiles';
                    break;
            }
        }
        else if($("#selFileType option:selected")[0].value=="CCMS1"){
            url='Upload_DrugList'; 
        }
        else if($("#selFileType option:selected")[0].value=="CPCMS"){
            url='Upload_DrugListTeenager';
        }
        else if($("#selFileType option:selected")[0].value=="LTAFL"){
            url='Upload_3_4_Lecture';
        }
        else if($("#selFileType option:selected")[0].value=="VR"){
            url='Upload_View_Record';
        }
        else if($("#selFileType option:selected")[0].value=="CPCMS2"){
            url='Upload_DRUGS_LIST_TEENAGER_Interrupt';
        }
        else if($("#selFileType option:selected")[0].value=="CPCMS3"){
            url='Upload_Coupling_Counseling';
        }
        else if($("#selFileType option:selected")[0].value=="CPCMS4"){
            url='Upload_Specific_Business';
        }

        axios({
            url: url,
            method: 'post',
            headers: {
                "X-CSRF-TOKEN": CSRF_TOKEN
            },
            data: formData,
            responseType: 'text'
        })
        .then((response) => {
            alert(response.data);
        });
    });

    return self;
}
