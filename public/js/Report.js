function Report(self) {
    self.init = function(){
        $("#monthDiv").show();
        $("#yearDiv").hide();
    }   

    // 年月報連動選項
    $("#reportType").on('change', function () {
        if($("#reportType option:selected")[0].value=="月報"){
            $("#monthDiv").show();
            $("#yearDiv").hide();
            $("#selectStartYear").val('');
            $("#selectEndYear").val('');
        }
        else if($("#reportType option:selected")[0].value=="年報"){
            $("#monthDiv").hide();
            $("#yearDiv").show();
            $("#selectYear").val('');
            $("#selectMonth").val('');
        }
    });
    //產出報表按鈕事件
    $("#btnExport").on('click', function () {
        var fileName = "";
        if($("#reportType option:selected")[0].value=="月報"){
            if($("#selectYear option:selected")[0].value==""||$("#selectMonth option:selected")[0].value==""){
                alert("請選年度及月份");
                return;
            }
            fileName = "高雄市政府毒品防制局毒品防制監測月報"+$("#selectYear option:selected")[0].value+"年"+$("#selectMonth option:selected")[0].value+"月";
        }
        else if($("#reportType option:selected")[0].value=="年報"){
            if($("#selectStartYear option:selected")[0].value==""||$("#selectEndYear option:selected")[0].value==""){
                alert("請選年度");
                return;
            }
            fileName = "高雄市政府毒品防制局毒品防制監測年報"+$("#selectStartYear option:selected")[0].value+"年~"+$("#selectEndYear option:selected")[0].value+"年";
        }
        var chartInfo = [];
        $("#chartGroup").html("");

        // 撈取月報或年報之資料
        self.ReportCharts.filter(f=>{return f.Type==$("#reportType option:selected")[0].value}).forEach(chart=>{ 
            const chartDiv=
            '<div id="download'+chart.Function_ID+'" class="largechart" style="width:700px;height:400px;">\
                <div class="card-item" style="width: 700px;height:390px;margin:0 auto;" >\
                    <div id="largeChart'+chart.Chart_ID+'" style="margin-right:40px;margin-left:20px;"></div>\
                    <div style="margin-top:7px;">\
                        <p class="CommentText" id="'+chart.Function_ID+'Text"></p>\
                        <p class="main-card-info">'+chart.Source_Name+'</p>\
                    </div>\
                </div>\
            </div>';
            $("#chartGroup").append(chartDiv);
            try{
                eval(chart.Function_Name + "('" + chart.Function_ID + "','largeChart"+chart.Chart_ID+"')");
            }
            catch{

            }            
            chartInfo.push({banner:chart.Banner_Name,title:chart.Chart_Name,source:chart.Source_Name,downID:'download'+chart.Function_ID,textID:chart.Function_ID+'Text',type:chart.Type});
        });

        // 撈取月報或年報之圖表
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var formData = new FormData();
        var chartFinish = [];
        var nowExcute = false;
        var errorChart = [];
        $("#btnExport").prop('disabled', true);
        var checkTimer = setInterval(() => {
            chartInfo.forEach((chart,i)=>{
                // 尋找是否已產生chart
                var chartID = chart.downID.replace("download","");
                var amChart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === chartID;
                });
                if(amChart===undefined||amChart===null){
                    if(errorChart.filter(f=>{return f==chartID}).length<3){
                        errorChart.push(chartID)
                        return;
                    }                    
                }
                   
                // 判斷是否已處理
                if(chartFinish.find(f=>f==chart.downID) !== undefined)
                    return;

                // 判斷是否有資料處理中
                if(nowExcute)
                    return;
                nowExcute = true;
                $("#exportProcess").text("正在處理..."+chart.title);

                //延遲300ms確保產生完整圖表
                setTimeout(() => {                                          
                    domtoimage.toBlob(document.getElementById(chart.downID),{bgcolor:"#fff",height:$("#"+chart.downID).height()})
                    .then(function (blob) {
                        formData.append(chart.downID, blob, chart.downID +".jpg");
                        chartFinish.push(chart.downID);
                        nowExcute = false;
                        // 全數處理完，準備匯出報表
                        if(chartInfo.length == chartFinish.length){
                            $("#exportProcess").text("正在產生報表...");
                            clearInterval(checkTimer);
                            chartInfo.forEach(chart=>{chart.subText= $('#'+chart.textID).text();});
                            formData.append('chartInfo', JSON.stringify(chartInfo));
                            axios({
                                url: 'Report_exportWord',
                                method: 'post',
                                headers: {
                                    "X-CSRF-TOKEN": CSRF_TOKEN
                                },
                                data: formData,
                                responseType: 'blob'
                            })
                            .then((response) => {
                                $("#exportProcess").text("完成");
                                $("#btnExport").prop('disabled', false);
                                const url = window.URL.createObjectURL(new Blob([response.data]));
                                const link = document.createElement('a');
                                link.href = url;
                                link.setAttribute('download', fileName+'.doc');
                                document.body.appendChild(link);
                                link.click();
                                document.body.removeChild(link);
                            });
                        }
                    });
                }, 300);
            });
        }, 500);
    });  

    return self;
}