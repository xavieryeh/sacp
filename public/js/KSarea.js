function KSarea(self){
    $.Log("主畫面","行政區"+self.areaName + "地圖點擊");
// $(function () {
    makeWCChart();
    function makeWCChart() {
        am4core.ready(function () {
            // Themes begin
            // am4core.unuseAllThemes();
            // am4core.useTheme(am4themes_material);
            // am4core.unuseTheme(am4themes_animated);
            am4core.addLicense(self.ChartsPN);
            // Themes end
            const id = "chart3";
            // Create chart instance
            var chart = am4core.registry.baseSprites.find(function (chartObj) {
                return chartObj.htmlContainer.id === id;
            });

            // $file = file('database/wordcloud.txt');
            $.PostAJAX("KSarea_areaProtectionFactor", {areaName:self.areaName}, response => {
                if (chart === undefined) {
                    var Params = {};
                    Params.ChartType = "WordCloud";
                    Params.Data = response.chart3.map(function (each) {
                        return {
                            category: each.category,
                            count: each.count
                        };
                    });
                    MakeAMChart(id, Params);
                } else {
                    var NewChartData = response.chart3.map(function (each) {
                        return {
                            category: each.category,
                            count: each.count,
                        };
                    });

                    chart.data = NewChartData;
                    chart.invalidateRawData();
                }

                var chart3 = [{
                    id: "chart1",
                    title: "風險因子",
                    color: "#E98440"
                }, {
                    id: "chart2",
                    title: "保護因子",
                    color: "#3FBEEC"
                }];
                chart3.forEach((subChart, iSub) => {
                    var sub_chart = am4core.registry.baseSprites.find(function (
                        chartObj) {
                        return chartObj.htmlContainer.id === subChart.id;
                    });
                    if (sub_chart === undefined) {
                        var Params = {};
                        Params.ChartType = "Radar";
                        Params.Title = {
                            title: subChart.title,
                            fonSize: 15,
                            color: subChart.color,
                            align: "center",
                            valign: "top",
                            dy: -10
                        };
                        Params.FontSize = 10;
                        Params.name = subChart.title;
                        Params.color = subChart.color;
                        Params.Data = response[subChart.id].map(function (each, i) {
                            return {
                                category: each.category,
                                count: each.count
                            };
                        });
                        MakeAMChart(subChart.id, Params);
                    } else {
                        var NewChartData = response[subChart.id].map(function (each, i) {
                            return {
                                category: each.category,
                                count: each.count
                            };
                        });

                        sub_chart.data = NewChartData;
                        sub_chart.invalidateRawData();
                    }
                });
            });
        }); // end am4core.ready()
    }
// });
}