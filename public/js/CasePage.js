function CasePage(self){
    getGenogram2();
    makeWCChart();
    //取得家系圖
    function getGenogram() {
        $.PostAJAX("Case_getGenogram", {caseNo: self.caseNo}, response => {

            var Params = {};
            Params.bindingData = response.bindingData;
            Params.focus = response.Data.find(f=>f.Title=='個案').Key;
            Params.Data = [];            
            response.Data.forEach(element => {
                var vRelation = response.Relation.filter(f => {
                    return f.Title == element.Title
                })[0];
                var PersonData = {};
                PersonData["key"] = parseInt(element.Key);
                PersonData["n"] = element.Name;
                PersonData["s"] = element.Gender;
                if (vRelation !== undefined) {
                    PersonData["m"] = vRelation.m === undefined ? undefined : (response.Data
                        .filter(f => {
                            return f.Title == vRelation.m
                        }).length == 0 ? undefined : parseInt(response.Data.filter(
                            f => {
                                return f.Title == vRelation.m
                            })[0].Key));
                    PersonData["f"] = vRelation.f === undefined ? undefined : (response.Data
                        .filter(f => {
                            return f.Title == vRelation.f
                        }).length == 0 ? undefined : parseInt(response.Data.filter(
                            f => {
                                return f.Title == vRelation.f
                            })[0].Key));
                    PersonData["ux"] = vRelation.ux === undefined ? undefined : (response
                        .Data.filter(f => {
                            return f.Title == vRelation.ux
                        }).length == 0 ? undefined : parseInt(response.Data.filter(
                            f => {
                                return f.Title == vRelation.ux
                            })[0].Key));
                    PersonData["vir"] = vRelation.vir === undefined ? undefined : (response
                        .Data.filter(f => {
                            return f.Title == vRelation.vir
                        }).length == 0 ? undefined : parseInt(response.Data.filter(
                            f => {
                                return f.Title == vRelation.vir
                            })[0].Key));
                };
                // PersonData["coh"] = element.Relatives;
                PersonData["coh"] = element.Cohabit;
                PersonData["lv"] = parseInt(vRelation.Level);
                // PersonData["a"] = element.DrugRecoder.split(',');
                PersonData["a"] = Array.from(element.Drug??'');
                Params.Data.push(PersonData);
            });
            // console.log('家系圖', Params);
            makeGenogram("myDiagramDiv", Params);
        });
    }
    function getGenogram2() {
        $.PostAJAX("Case_getGenogram", {caseNo: self.caseNo}, response => {
            // console.log('家系圖', Params);
            makeGenogram2("myDiagramDiv", response);
        });
    }

    // console.log('CasePage');
    function makeWCChart() {
        am4core.ready(function () {
            // Themes begin
            // am4core.unuseAllThemes();
            // am4core.useTheme(am4themes_material);
            // am4core.unuseTheme(am4themes_animated);
            am4core.addLicense(self.ChartsPN);
            // Themes end
            const id = "chart3";
            // Create chart instance
            var chart = am4core.registry.baseSprites.find(function (chartObj) {
                return chartObj.htmlContainer.id === id;
            });

            // $file = file('database/wordcloud.txt');
            $.PostAJAX("Case_casePageOwnTeacherProtectionFactor", {
                caseNo: self.caseNo
            }, response => {
                if (chart === undefined) {
                    var Params = {};
                    Params.ChartType = "WordCloud";
                    Params.Data = response.chart3.map(function (each) {
                        return {
                            category: each.category,
                            count: each.count
                        };
                    });
                    MakeAMChart(id, Params);
                } else {
                    var NewChartData = response.chart3.map(function (each) {
                        return {
                            category: each.category,
                            count: each.count,
                        };
                    });

                    chart.data = NewChartData;
                    chart.invalidateRawData();
                }

                var chart3 = [{
                    id: "chart1",
                    title: "風險因子",
                    color: "#E98440"
                }, {
                    id: "chart2",
                    title: "保護因子",
                    color: "#3FBEEC"
                }];
                chart3.forEach((subChart, iSub) => {
                    var sub_chart = am4core.registry.baseSprites.find(function (
                        chartObj) {
                        return chartObj.htmlContainer.id === subChart.id;
                    });
                    if (sub_chart === undefined) {
                        var Params = {};
                        Params.ChartType = "Radar";
                        Params.Title = {
                            title: subChart.title,
                            fonSize: 18,
                            color: subChart.color,
                            align: "center",
                            valign: "top",
                            dy: -15
                        };
                        Params.FontSize = 14;
                        Params.name = subChart.title;
                        Params.color = subChart.color;
                        Params.max_count = response.radar_max; // 固定兩組圖最大值
                        Params.Data = response[subChart.id].map(function (each, i) {
                            return {
                                category: each.category,
                                count: each.count
                            };
                        });
                        MakeAMChart(subChart.id, Params);
                    } else {
                        var NewChartData = response[subChart.id].map(function (each, i) {
                            return {
                                category: each.category,
                                count: each.count
                            };
                        });

                        sub_chart.data = NewChartData;
                        sub_chart.invalidateRawData();
                    }
                });
            });
        }); // end am4core.ready()
    }
}
