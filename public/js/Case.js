function Case(self) {
    makeWCChart();

    // 個案管理文字雲、雷達圖
    function makeWCChart() {
        am4core.ready(function () {
            // Themes begin
            am4core.addLicense(self.ChartsPN);
            // Themes end
            const id = "chart3";
            // Create chart instance
            var chart = am4core.registry.baseSprites.find(function (chartObj) {
                return chartObj.htmlContainer.id === id;
            });

            // $file = file('database/wordcloud.txt');
            $.PostAJAX("Case_caseProtectionFactor", {
                Manager: self.Manager
            }, response => {
                if (chart === undefined) {
                    var Params = {};
                    Params.ChartType = "WordCloud";
                    Params.Data = response.chart3.map(function (each) {
                        return {
                            category: each.category,
                            count: each.count
                        };
                    });
                    MakeAMChart(id, Params);
                } else {
                    var NewChartData = response.chart3.map(function (each) {
                        return {
                            category: each.category,
                            count: each.count,
                        };
                    });

                    chart.data = NewChartData;
                    chart.invalidateRawData();
                }

                var chart3 = [{
                    id: "chart1",
                    title: "風險因子",
                    color: "#E98440"
                }, {
                    id: "chart2",
                    title: "保護因子",
                    color: "#3FBEEC"
                }];
                chart3.forEach((subChart, iSub) => {
                    var sub_chart = am4core.registry.baseSprites.find(function (
                        chartObj) {
                        return chartObj.htmlContainer.id === subChart.id;
                    });
                    if (sub_chart === undefined) {
                        var Params = {};
                        Params.ChartType = "Radar";
                        Params.Title = {
                            title: subChart.title,
                            fonSize: 18,
                            color: subChart.color,
                            align: "center",
                            valign: "top",
                            dy: -15
                        };
                        Params.FontSize = 14;
                        Params.name = subChart.title;
                        Params.color = subChart.color;
                        Params.max_count = response.radar_max; // 固定兩組圖最大值
                        // console.log('max_count', response.radar_max);
                        Params.Data = response[subChart.id].map(function (each, i) {
                            return {
                                category: each.category,
                                count: each.count
                            };
                        });
                        MakeAMChart(subChart.id, Params);
                    } else {
                        var NewChartData = response[subChart.id].map(function (each, i) {
                            return {
                                category: each.category,
                                count: each.count
                            };
                        });

                        sub_chart.data = NewChartData;
                        sub_chart.invalidateRawData();
                    }
                });
            });
        }); // end am4core.ready()
    }

    init();

    var Cases;
    var selectPage = 1;
    var TotalCount;

    function init() {
        Cases = self.Cases;
        TotalCount = Cases.length;
        setPageList(1);
        searchData();
    }
    //選取頁數事件
    self.changePage = function (page) {
        if (page == "pre") {
            if (selectPage == 1)
                return;
            else {
                setPageList(parseInt(selectPage) - 1);
            }
        } else if (page == "next") {
            if (selectPage == Math.ceil(TotalCount / parseInt($("#selShowCount option:selected")[0].value)))
                return;
            else {
                setPageList(parseInt(selectPage) + 1);
            }
        } else {
            setPageList(parseInt(page));
        }
        searchData();
    }

    //取得頁數並產生頁數
    function setPageList(currentPage) {
        selectPage = currentPage;
        const totalPage = Math.ceil(TotalCount / parseInt($("#selShowCount option:selected")[0].value));
        const startPage = (parseInt((currentPage-1)/10)*10)+1;
        const endPage = (startPage+9)>=totalPage?totalPage:(startPage+9);
        var PageList = ['<li class="page-item"><a id="btnPrePage" class="page-link" onclick="Case.changePage(\'pre\')">上一項</a></li>'];
        for (var i = startPage; i < endPage+1; ++i) {
            PageList.push('<li class="page-item' + (i == currentPage ? ' active' : '') + '" aria-current="page"><a class="page-link" onclick="Case.changePage(' + i.toString() + ')">' + i.toString() + '</a></li>');
        }
        PageList.push('<li class="page-item"><a id="btnNextPage" class="page-link" onclick="Case.changePage(\'next\')">下一項</a></li>');
        $('#ulPageList').html(PageList.join(''));
    }

    $("#selShowCount").on('change', function () {
        setPageList(1);
        searchData();
    });

    $("#btnSearch").on('click', function () {
        searchData();
        setPageList(1);
    });

    //個案管理資料查詢
    function searchData() {
        const showCount = parseInt($("#selShowCount option:selected")[0].value);
        const filterDataByPage = Cases.filter((f, i) => {
            return (i + 1) >= ((selectPage - 1) * showCount + 1) && (i + 1) <= (selectPage * showCount)
        });

        const route = self.route;
        var rowData = [];
        filterDataByPage.forEach(eachData => {
            rowData.push('\
                <tr class="js-KSarea_page_ownTeacher">\
                    <td date-title="個案(去識別化)" scope="row"><a\
                            style="text-decoration:underline;color:#0080FF"\
                            href="' + route + '?caseNo='+ eachData.Case_ID + '">' + eachData.Case_ID + '</a>\
                    </td>\
                    <td date-title="來源別"><a href="#" style="text-decoration:underline;color:#0080FF">' + eachData.SourceNo + '</a></td>\
                    <td date-title="性別">' + eachData.Gender + '</td>\
                    <td date-title="年齡">' + eachData.File_Age + '</td>\
                    <td date-title="毒品級數">' + eachData.Level + '</td>\
                    <td date-title="列管(Y/N)">' + eachData.States + '</td>\
                    <td date-title="初案/再案(1/2)">' + eachData.CaseType + '</td>\
                    <td date-title="類型">' + eachData.Manage_Type + '</td>\
                </tr>');
        });
        $("#tBody").html(rowData.join(""));
        TotalCount = Cases.length;
        $("#sTotalCount").text(TotalCount.toString());
    }

    return self;
}
