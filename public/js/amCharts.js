function MakeAMChart(container, Params, callback) {
    switch (Params.ChartType) {
        case "ColumnBar":
            var chart = am4core.create(container, am4charts.XYChart);
            // Add data
            chart.hiddenState.properties.opacity = 0;
            chart.data = Params.Data;
            chart.zoomOutButton.disabled = true;
            // chart.paddingTop = am4core.percent(5);
            // chart.logo.height = -15;

            // Create title
            if (Params.Title !== undefined) {
                var title = chart.titles.create();
                title.text = Params.Title;
                title.fontSize = Params.FontSize + 5;
                title.marginBottom = -10;
                title.fontWeight = "900";
                title.fill = am4core.color(Params.TitleColor);
            }

            //Create title 2
            if (Params.Title2 !== undefined) {
                var topContainer = chart.chartContainer.createChild(am4core.Container);
                topContainer.layout = "absolute";
                topContainer.toBack();
                // topContainer.paddingBottom = 15;
                topContainer.width = am4core.percent(100);
                var dateTitle = topContainer.createChild(am4core.Label);
                dateTitle.text = Params.Title2.title;
                dateTitle.fontSize = Params.Title2.fonSize;
                dateTitle.fill = am4core.color(Params.Title2.color);
                dateTitle.align = Params.Title2.align;
                dateTitle.dy = Params.Title2.dy;
                dateTitle.fontWeight = 400;
            }

            //Create title 3
            if (Params.Title3 !== undefined) {
                var label = chart.createChild(am4core.Label);
                //label.isMeasured = false; //uncomment to make the label not adjust the rest of the chart elements to accommodate its placement
                label.fontSize = Params.Title3.fonSize;
                label.dy = Params.Title3.dy;
                label.align = Params.Title3.align;
                label.valign = Params.Title3.valign;
                label.fill = am4core.color(Params.Title3.color);
                label.text = Params.Title3.title;
            }

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "category";
            categoryAxis.renderer.labels.template.disabled = true;
            categoryAxis.renderer.grid.template.disabled = true;
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 10;
            categoryAxis.renderer.labels.template.maxWidth = 50;
            categoryAxis.renderer.labels.template.fontSize = Params.FontSize;
            // categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
            //     if (target.dataItem && target.dataItem.index & 2 == 2) {
            //         return dy + 25;
            //     }
            //     return dy;
            // });
            categoryAxis.events.on("sizechanged", function (ev) {
                var axis = ev.target;
                var cellWidth = axis.pixelWidth / (axis.endIndex - axis.startIndex);
                if (cellWidth < axis.renderer.labels.template.maxWidth) {
                    axis.renderer.labels.template.dy = -15;
                    axis.renderer.labels.template.rotation = -60;
                    axis.renderer.labels.template.horizontalCenter = "right";
                    axis.renderer.labels.template.verticalCenter = "middle";
                } else {
                    axis.renderer.labels.template.rotation = 0;
                    axis.renderer.labels.template.horizontalCenter = "middle";
                    axis.renderer.labels.template.verticalCenter = "top";
                }
            });

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.rangeChangeDuration = 500;
            // valueAxis.renderer.labels.template.dx = -15;
            valueAxis.renderer.labels.template.fontSize = Params.FontSize;;
            // valueAxis.extraMax = 0.05;
            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "count";
            series.dataFields.categoryX = "category";
            series.name = "Count";
            series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;
            series.columns.template.fill = Params.Fill;
            series.hiddenState.transitionDuration = 5000;
            series.hiddenState.transitionEasing = am4core.ease.elasticInOut;

            var bullet = series.bullets.push(new am4charts.LabelBullet);
            // bullet.label.text = "{valueY}";
            // bullet.label.rotation = -90;
            // bullet.locationY = 0.01;
            bullet.label.dy = -5;
            bullet.label.fontSize = Params.FontSize - 3;
            bullet.label.truncate = false;
            bullet.label.hideOversized = false;

            var columnTemplate = series.columns.template;
            series.stroke = Params.Stroke;
            columnTemplate.width = am4core.percent(30);
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;
            // chart.leftAxesContainer.width = 23;
        return chart;
            break;
        case "GroupColumnBar":
            // Create chart instance
            var chart = am4core.create(container, am4charts.XYChart);
            // Add data
            chart.data = Params.Data;

            // Create title
            if (Params.Title !== undefined) {
                var title = chart.titles.create();
                title.text = Params.Title;
                title.fontSize = Params.FontSize + 5;
                title.marginBottom = 5;
                title.fontWeight = "900";
                title.fill = am4core.color(Params.TitleColor);
            }

            //Create title 2
            if (Params.Title2 !== undefined) {
                let topContainer = chart.chartContainer.createChild(am4core.Container);
                topContainer.layout = "absolute";
                topContainer.toBack();
                // topContainer.toFront();
                // topContainer.paddingBottom = 15;
                topContainer.width = am4core.percent(100);
                var dateTitle = topContainer.createChild(am4core.Label);
                dateTitle.text = Params.Title2.title;
                dateTitle.fontSize = Params.Title2.fonSize;
                dateTitle.fill = am4core.color(Params.Title2.color);
                dateTitle.align = Params.Title2.align;
                dateTitle.dy = Params.Title2.dy;
                dateTitle.fontWeight = 400;
            }

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "category";
            // categoryAxis.title.text = "Local country offices";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 20;
            categoryAxis.renderer.labels.template.maxWidth = 50;
            categoryAxis.renderer.labels.template.dy = -10;
            categoryAxis.renderer.labels.template.fontSize = Params.FontSize - 2;
            categoryAxis.renderer.cellStartLocation = Params.cellStartLocation || 0.2
            categoryAxis.renderer.cellEndLocation = (1 - Params.cellStartLocation) || 0.8
            categoryAxis.renderer.labels.template.visible = Params.categoryAxisVisible;
            categoryAxis.renderer.grid.template.disabled = !Params.categoryAxisVisible;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            // valueAxis.renderer.labels.template.dx = -15;
            valueAxis.renderer.labels.template.fontSize = Params.FontSize;
            // valueAxis.strictMinMax = true;
            // valueAxis.title.text = "Expenditure (M)";
            var iKey = 0;
            var keyFilter = Object.keys(Params.Data[0]).filter(function (key) {
                return key != "category" && key.indexOf("color") == -1 && key != "合計"
            });

            keyFilter.forEach(function (key) {
                // if(key!="category" && key.indexOf("color")==-1){
                var series = chart.series.push(new am4charts.ColumnSeries());
                series.dataFields.valueY = key;
                series.dataFields.categoryX = "category";
                series.name = key;
                series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
                series.stroke=am4core.color(Params.Data[0]["color"][iKey]); // fill
                series.adapter.add("tooltipText", function (ev) {
                    var text = "[bold]{categoryX}[/]\n"
                    chart.series.each(function (item) {
                        if (!item.isHidden) {
                            text += "[" + item.stroke.hex + "]■[/] " + item.name + ": {" + item.dataFields.valueY + "}\n";
                        }
                    });
                    return text;
                });

                series.tooltip.getFillFromObject = false;
                series.tooltip.background.fill = am4core.color("#fff");
                series.tooltip.label.fill = am4core.color("#000");

                // Prevent cross-fading of tooltips
                series.tooltip.defaultState.transitionDuration = 0;
                series.tooltip.hiddenState.transitionDuration = 0;

                // series.tooltipText = "{name}: [bold]{valueY}[/]";
                // series.tooltip.pointerOrientation = "vertical";
                series.columns.template.strokeWidth = 0;
                series.columns.template.fill = am4core.color(Params.Data[0]["color"][iKey]); // fill
                series.columns.template.width = am4core.percent(80);
                ++iKey;
                // }
            });
            // Do not try to stack on top of previous series
            // series2.stacked = true;
            // Add cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.maxTooltipDistance = 0;

            if(Params.leftAxesWidth!==undefined)
                chart.leftAxesContainer.width = Params.leftAxesWidth;
            // Add legend
            // chart.legend = new am4charts.Legend();
            return chart;
            break;
        case "Bar":
            var chart = am4core.create(container, am4charts.XYChart);
            // Add data
            chart.hiddenState.properties.opacity = 0;
            chart.data = Params.Data;
            chart.zoomOutButton.disabled = true;
            chart.paddingTop = am4core.percent(5);
            // chart.logo.height = -15;
            // Create axes
            if (Params.Title !== undefined) {
                var title = chart.titles.create();
                title.text = Params.Title;
                title.fontSize = Params.FontSize + 5;
                title.marginBottom = 5;
                title.fontWeight = "900";
                title.fill = am4core.color(Params.TitleColor);
            }

            var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "category";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 10;
            categoryAxis.renderer.labels.template.maxWidth = 50;
            // categoryAxis.renderer.labels.template.dx = -23;
            categoryAxis.renderer.labels.template.fontSize = Params.FontSize;
            categoryAxis.renderer.cellStartLocation = 0.2;
            categoryAxis.renderer.cellEndLocation = 0.8;
            // categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
            //     if (target.dataItem && target.dataItem.index & 2 == 2) {
            //         return dy + 25;
            //     }
            //     return dy;
            // });

            var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
            valueAxis.rangeChangeDuration = 500;
            valueAxis.renderer.labels.template.dy = -10;
            valueAxis.renderer.labels.template.fontSize = Params.FontSize;;
            // valueAxis.extraMax = 0.1;
            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueX = "count";
            series.dataFields.categoryY = "category";
            series.name = "Count";
            series.stroke = Params.Stroke;
            var columnTemplate = series.columns.template;
            // columnTemplate.width = am4core.percent(30);
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;
            columnTemplate.tooltipText = "{categoryY}: [bold]{valueX}[/]";
            columnTemplate.fillOpacity = .8;
            columnTemplate.fill = Params.Fill;
            series.hiddenState.transitionDuration = 5000;
            series.hiddenState.transitionEasing = am4core.ease.elasticInOut;

            // var bullet = series.bullets.push(new am4charts.LabelBullet);
            // bullet.label.text = "{valueX}";
            // // bullet.label.rotation = -90;
            // // bullet.locationY = 0.01;
            // bullet.label.dy = -5;
            // bullet.label.fontSize = Params.FontSize - 3;
            // bullet.label.truncate = false;
            // bullet.label.hideOversized = false;

            // chart.leftAxesContainer.width = 125;
            break;
        case "GroupBar":
            var chart = am4core.create(container, am4charts.XYChart);
            // Add data
            chart.hiddenState.properties.opacity = 0;
            chart.data = Params.Data;
            chart.zoomOutButton.disabled = true;
            chart.paddingTop = am4core.percent(5);

            chart.legend = new am4charts.Legend();
            chart.legend.useDefaultMarker = true;
            var markerTemplate = chart.legend.markers.template;
            markerTemplate.width = Params.FontSize;
            markerTemplate.height = Params.FontSize;
            chart.legend.labels.template.fontSize=Params.FontSize;
            markerTemplate.dy = -20;
            chart.legend.labels.template.dy = -20;
            // chart.logo.height = -15;
            // Create axes
            if (Params.Title !== undefined) {
                var title = chart.titles.create();
                title.text = Params.Title;
                title.fontSize = Params.FontSize + 5;
                title.marginBottom = 5;
                title.fontWeight = "900";
                title.fill = am4core.color(Params.TitleColor);
            }

            var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "category";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 10;
            categoryAxis.renderer.labels.template.maxWidth = 50;
            // categoryAxis.renderer.labels.template.dx = -23;
            categoryAxis.renderer.labels.template.fontSize = Params.FontSize;
            categoryAxis.renderer.cellStartLocation = 0.2;
            categoryAxis.renderer.cellEndLocation = 0.8;
            // categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
            //     if (target.dataItem && target.dataItem.index & 2 == 2) {
            //         return dy + 25;
            //     }
            //     return dy;
            // });

            var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
            valueAxis.rangeChangeDuration = 500;
            valueAxis.renderer.labels.template.dy = -10;
            valueAxis.renderer.labels.template.fontSize = Params.FontSize;;
            // valueAxis.extraMax = 0.1;
            // Create series
            // var series = chart.series.push(new am4charts.ColumnSeries());
            // series.dataFields.valueX = "count";
            // series.dataFields.categoryY = "category";
            // series.name = "Count";
            // series.stroke = Params.Stroke;
            // var columnTemplate = series.columns.template;
            // // columnTemplate.width = am4core.percent(30);
            // columnTemplate.strokeWidth = 2;
            // columnTemplate.strokeOpacity = 1;
            // columnTemplate.tooltipText = "{categoryY}: [bold]{valueX}[/]";
            // columnTemplate.fillOpacity = .8;
            // columnTemplate.fill = Params.Fill;
            // series.hiddenState.transitionDuration = 5000;
            // series.hiddenState.transitionEasing = am4core.ease.elasticInOut;
            var iKey = 0;
            var keyFilter = Object.keys(Params.Data[0]).filter(function (key) {
                return key != "category" && key.indexOf("color") == -1 && key != "合計"
            });

            keyFilter.forEach(function (key) {
                // if(key!="category" && key.indexOf("color")==-1){
                var series = chart.series.push(new am4charts.ColumnSeries());
                series.dataFields.valueX = key;
                series.dataFields.categoryY = "category";
                series.name = key;
                // series.tooltipText = "{categoryY}: [bold]{valueX}[/]";
                // series.adapter.add("tooltipText", function (ev) {
                //     var text = "[bold]{categoryY}[/]\n"
                //     chart.series.each(function (item) {
                //         if (!item.isHidden) {
                //             text += "[" + item.stroke.hex + "]■[/] " + item.name + ": {" + item.dataFields.valueX + "}\n";
                //         }
                //     });
                //     return text;
                // });

                series.tooltip.getFillFromObject = false;
                series.tooltip.background.fill = am4core.color("#fff");
                series.tooltip.label.fill = am4core.color("#000");

                // Prevent cross-fading of tooltips
                series.tooltip.defaultState.transitionDuration = 0;
                series.tooltip.hiddenState.transitionDuration = 0;

                // series.tooltipText = "{name}: [bold]{valueY}[/]";
                // series.tooltip.pointerOrientation = "vertical";
                var columnTemplate = series.columns.template;
                columnTemplate.tooltipText = "{categoryY}: [bold]{valueX}[/]";
                columnTemplate.adapter.add("tooltipText", function (ev) {
                    var text = "[bold]{categoryY}[/]\n"
                    chart.series.each(function (item) {
                        if (!item.isHidden) {
                            text += "[" + item.stroke.hex + "]■[/] " + item.name + ": {" + item.dataFields.valueX + "}\n";
                        }
                    });
                    return text;
                });
                columnTemplate.strokeWidth = 0;
                columnTemplate.fill = am4core.color(Params.Data[0]["color"][iKey]); // fill
                columnTemplate.width = am4core.percent(80);
                ++iKey;
                // }
            });
            // var bullet = series.bullets.push(new am4charts.LabelBullet);
            // bullet.label.text = "{valueX}";
            // // bullet.label.rotation = -90;
            // // bullet.locationY = 0.01;
            // bullet.label.dy = -5;
            // bullet.label.fontSize = Params.FontSize - 3;
            // bullet.label.truncate = false;
            // bullet.label.hideOversized = false;

            // chart.leftAxesContainer.width = 125;
            break;
        case "WordCloud":
            var chart = am4core.create(container, am4plugins_wordCloud.WordCloud);
            var series = chart.series.push(new am4plugins_wordCloud.WordCloudSeries());
            series.showOnInit = false;
            series.accuracy = 4;
            series.step = 15;
            // series.randomness = 0.5
            series.rotationThreshold = 0.7;
            series.maxCount = 200;
            series.minWordLength = 2;
            // series.labels.template.margin(4, 4, 4, 4);
            series.maxFontSize = am4core.percent(40);
            // series.defaultState.transitionDuration = 3000;
            series.hiddenState.transitionDuration = 3000;

            series.data = Params.Data;
            series.dataFields.word = "category";
            series.dataFields.value = "count";

            series.colors = new am4core.ColorSet();
            series.colors.passOptions = {}; // makes it loop

            //series.labelsContainer.rotation = 45;
            // series.angles = [0, -90];
            series.fontWeight = "700";

            // series.heatRules.push({
            //     "target": series.labels.template,
            //     "property": "fill",
            //     "min": am4core.color("#863DD0"),
            //     "max": am4core.color("#d84fda"),
            //     "dataField": "value"
            // });
            // series.labels.template.tooltipText = "{word}:\n[bold]{value}[/]";

            series.events.on("arrangestarted", function (ev) {
                ev.target.baseSprite.preloader.show(0);
            });

            series.events.on("arrangeprogress", function (ev) {
                ev.target.baseSprite.preloader.progress = ev.progress;
            });
            // setInterval(function () {
            //     series.dataItems.getIndex(Math.round(Math.random() * (series.dataItems.length - 1))).setValue("value", Math.round(Math.random() * 10));
            // }, 10000)

            break;
        case "Radar":
            var chart = am4core.create(container, am4charts.RadarChart);

            /* Add data */
            chart.data = Params.Data;

            // Title
            if (Params.Title !== undefined) {
                var title = chart.titles.create();
                title.text = Params.Title.title;
                title.fontSize = Params.Title.fonSize;
                title.marginBottom = -10;
                title.fontWeight = "900";
                title.fill = am4core.color(Params.Title.color);
                title.dy = Params.Title.dy;
            }
            //Create title 3
            // if (Params.Title !== undefined) {
            //     var label = chart.createChild(am4core.Label);
            //     //label.isMeasured = false; //uncomment to make the label not adjust the rest of the chart elements to accommodate its placement
            //     label.fontSize = Params.Title.fonSize;
            //     label.dy = Params.Title.dy;
            //     label.align = Params.Title.align;
            //     label.valign = Params.Title.valign;
            //     label.fill = am4core.color(Params.Title.color);
            //     label.text = Params.Title.title;
            // }
            /* Create axes */
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "category";
            categoryAxis.renderer.labels.template.fontSize = Params.FontSize;
            categoryAxis.renderer.labels.template.wrap = true;
            categoryAxis.renderer.labels.template.maxWidth = 80;
            categoryAxis.renderer.labels.template.textAlign = 'middle'; // 文字置中
            var axisTooltip = categoryAxis.tooltip;
            // axisTooltip.background.fill = am4core.color("#07BEB8");
            axisTooltip.background.strokeWidth = 0;
            axisTooltip.background.cornerRadius = 3;
            axisTooltip.background.pointerLength = 0;
            // axisTooltip.dy = 5;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
            valueAxis.renderer.axisFills.template.fillOpacity = 0.05;
            valueAxis.renderer.labels.template.fontSize = Params.FontSize;
            valueAxis.renderer.gridType = "polygons";
            valueAxis.min = 0;
            valueAxis.max = Params.max_count || 10; // 固定兩組圖最大值

            /* Create and configure series */
            var series = chart.series.push(new am4charts.RadarSeries());
            series.dataFields.valueY = "count";
            series.dataFields.categoryX = "category";
            series.name = Params.name;
            series.tooltipText = "Series: {name}\nCategory: {categoryX}\nValue: {valueY}";
            series.strokeWidth = 3;
            series.stroke = am4core.color(Params.color);

            break;
        case "Pie":
            var chart = am4core.create(container, am4charts.PieChart);
            // Add data
            chart.data = Params.Data;
            chart.radius = am4core.percent(70);
            //Create title 2
            if (Params.Title !== undefined) {
                var title = chart.titles.create();
                title.text = Params.Title;
                title.fontSize = Params.FontSize + 5;
                title.marginBottom = 5;
                title.fontWeight = "900";
                title.fill = am4core.color(Params.TitleColor);
            }

            // Add and configure Series
            var pieSeries = chart.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = "count";
            pieSeries.dataFields.category = "category";
            pieSeries.slices.template.propertyFields.fill = "color";
            pieSeries.slices.template.strokeWidth=2
            pieSeries.slices.template.stroke = am4core.color("#fff");
            pieSeries.labels.template.text = Params.SeriesText || "{category}: {value.percent.formatNumber('#.0')}%";
            pieSeries.labels.template.fontSize = Params.FontSize;
            pieSeries.ticks.template.disabled = true;
            pieSeries.alignLabels = false;
            pieSeries.labels.template.radius = am4core.percent(-40);
            pieSeries.labels.template.fill = am4core.color("white");
            // pieSeries.labels.template.relativeRotation = 90;
            pieSeries.labels.template.adapter.add("radius", function (radius, target) {
                if (target.dataItem && (target.dataItem.values.value.percent < 10)) {
                    return 0;
                }
                return radius;
            });

            pieSeries.labels.template.adapter.add("fill", function (color, target) {
                if (target.dataItem && (target.dataItem.values.value.percent < 10)) {
                    return am4core.color("#000");
                }
                return color;
            });
            return chart;
            break;
        case "GroupBar":
            // Create chart instance
            var chart = am4core.create(container, am4charts.XYChart);
            // Add data
            chart.data = Params.Data;
            chart.legend = new am4charts.Legend();
            // Create title
            if (Params.Title !== undefined) {
                var title = chart.titles.create();
                title.text = Params.Title;
                title.fontSize = Params.FontSize + 5;
                title.marginBottom = 5;
                title.fontWeight = "900";
                title.fill = am4core.color(Params.TitleColor);
            }

            //Create title 2
            if (Params.Title2 !== undefined) {
                let topContainer = chart.chartContainer.createChild(am4core.Container);
                topContainer.layout = "absolute";
                topContainer.toBack();
                // topContainer.toFront();
                // topContainer.paddingBottom = 15;
                topContainer.width = am4core.percent(100);
                var dateTitle = topContainer.createChild(am4core.Label);
                dateTitle.text = Params.Title2.title;
                dateTitle.fontSize = Params.Title2.fonSize;
                dateTitle.fill = am4core.color(Params.Title2.color);
                dateTitle.align = Params.Title2.align;
                dateTitle.dy = Params.Title2.dy;
                dateTitle.fontWeight = 400;
            }

            // Create axes
            var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "category";
            // categoryAxis.title.text = "Local country offices";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 20;
            //categoryAxis.renderer.labels.template.maxWidth = 50;
            //categoryAxis.renderer.labels.template.dy = -10;
            categoryAxis.renderer.labels.template.marginLeft = 0
            categoryAxis.renderer.labels.template.fontSize = 12;
            categoryAxis.renderer.cellStartLocation = Params.cellStartLocation || 0.2
            categoryAxis.renderer.cellEndLocation = (1 - Params.cellStartLocation) || 0.8
            // categoryAxis.renderer.labels.template.visible = Params.categoryAxisVisible;
            // categoryAxis.renderer.grid.template.disabled = !Params.categoryAxisVisible;

            var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
            // valueAxis.renderer.labels.template.dx = -15;
            //valueAxis.renderer.labels.template.fontSize = Params.FontSize;
            // valueAxis.strictMinMax = true;
            // valueAxis.title.text = "Expenditure (M)";
            var iKey = 0;
            var keyFilter = Object.keys(Params.Data[0]).filter(function (key) {
                return key != "category" && key.indexOf("color") == -1 && key !="合計"
            });
            Params.Year.forEach(function (key) {
                // if(key!="category" && key.indexOf("color")==-1){
                var series = chart.series.push(new am4charts.ColumnSeries());
                series.dataFields.valueX = key;
                series.dataFields.categoryY = "category";
                series.name = key;
                series.tooltipText = "{categoryY}: [bold]{valueX}[/]";

                series.adapter.add("tooltipText", function (ev) {
                    var text = "[bold]{categoryY}[/]\n"
                    chart.series.each(function (item) {
                        if (!item.isHidden) {
                            text += "[" + item.stroke.hex + "]●[/] " + item.name + ": {" + item.dataFields.valueX + "}\n";
                        }
                    });
                    return text;
                });
                let labelBullet = series.bullets.push(new am4charts.LabelBullet());
                labelBullet.label.text = "{valueX}";
                labelBullet.label.align="right";
                labelBullet.label.fontSize=10;
                labelBullet.label.dx=20;
                series.tooltip.getFillFromObject = false;
                series.tooltip.background.fill = am4core.color("#fff");
                series.tooltip.label.fill = am4core.color("#000");

                // Prevent cross-fading of tooltips
                series.tooltip.defaultState.transitionDuration = 0;
                series.tooltip.hiddenState.transitionDuration = 0;

                // series.tooltipText = "{name}: [bold]{valueY}[/]";
                // series.tooltip.pointerOrientation = "vertical";
                series.columns.template.strokeWidth = 0;
                series.columns.template.fill = am4core.color(Params.Data[0]["color"][iKey]); // fill
                series.columns.template.width = am4core.percent(80);
                ++iKey;
                // }
            });
            // Do not try to stack on top of previous series
            // series2.stacked = true;
            // Add cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.maxTooltipDistance = 0;

            // chart.leftAxesContainer.width = 100;
            // Add legend
            // chart.legend = new am4charts.Legend();
            return chart;
        break;
    }
}
