function FileAnalyze(self){
    self.init = function(){
        searchData();
    }
    //各項刪除事件
    self.onDelete = function(date){
        $("#delKeyValue").val(date);
        $("#delKey").html("<br/>上傳時間:"+date);
        $('#delModal').modal('show');
    }  
    //各項刪除確認事件
    $("#btnDelConfirm").on('click', function () {
        $.PostAJAX("Upload_uploadLogDelAnalyzeDate", {keyValue:$("#delKeyValue").val()}, response => {
            self.Datas = response;
            searchData();
            $('#delModal').modal('hide');
        });
    });
    // 查詢按鈕事件
    $("#searchData").on('click', function () {
        searchData();
    });    
    //查詢Log
    function searchData() {
        const keyWord = $("#iKeyWord").val().toLowerCase().trim();
        const filterData = self.Datas.filter(f=>{return f.File_type.toLowerCase().indexOf(keyWord)>-1||f.File_Date.toLowerCase().indexOf(keyWord)>-1
        ||f.UpLoad_Date.toLowerCase().indexOf(keyWord)>-1||f.UserName.toLowerCase().indexOf(keyWord)>-1});
        var rowData = [];
        filterData.forEach(eachData => {
            rowData.push('\
            <tr class="">\
                <td date-title="檔案種類">'+eachData.File_type+'</td>\
                <td date-title="檔案時間">'+eachData.File_Date+'</td>\
                <td date-title="上傳日期">'+eachData.UpLoad_Date+'</td>\
                <td date-title="上傳人員">'+eachData.UserName+'</td>\
                <td date-title="功能">\
                    <div class="btnBox">\
                        <button type="button" onclick="FileAnalyze.onDelete(\''+eachData.UpLoad_Date+'\')" class="btn btn-danger btn-small">\
                            刪除\
                        </button>\
                    </div>\
                </td>\
            </tr>');
        });
        $("#tBody").html(rowData.join(""));
    }
    return self;
}