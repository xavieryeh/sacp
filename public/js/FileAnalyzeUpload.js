function FileAnalyzeUpload(self) {
    self.init = function(){
        $("#monthDiv").show();
        $("#yearDiv").hide();
    }

    // 年月報連動選項
    $("#selType").on('change', function () {
        if($("#selType option:selected")[0].value=="月報"){
            $("#monthDiv").show();
            $("#yearDiv").hide();
            $("#selectStartYear").val('');
            $("#selectEndYear").val('');
        }
        else if($("#selType option:selected")[0].value=="年報"){
            $("#monthDiv").hide();
            $("#yearDiv").show();
            $("#selectYear").val('');
            $("#selectMonth").val('');
        }
    });

    $("#btnFileUpload").on('click', function () {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var formData = new FormData();
        if($("#selType option:selected")[0].value=="月報"){
            if($("#selectYear option:selected")[0].value==""||$("#selectMonth option:selected")[0].value==""){
                alert("請選年度及月份");
                return;
            }
            formData.append('type', '月度資料');
            formData.append('Date1', $("#selectYear option:selected")[0].value);
            formData.append('Date2', $("#selectMonth option:selected")[0].value);
        }
        else if($("#selType option:selected")[0].value=="年報"){
            if($("#selectStartYear option:selected")[0].value==""||$("#selectEndYear option:selected")[0].value==""){
                alert("請選年度");
                return;
            }
            formData.append('type', '年度資料');
            formData.append('Date1', $("#selectStartYear option:selected")[0].value);
            formData.append('Date2', $("#selectEndYear option:selected")[0].value);
        }
        formData.append('message', $("#txtMessage").val());
        
        axios({
            url: 'Upload_uploadLogAnalyzeDate',
            method: 'post',
            headers: {
                "X-CSRF-TOKEN": CSRF_TOKEN
            },
            data: formData,
            responseType: 'text'
        })
        .then((response) => {
            alert(response.data);
            $("#txtMessage").val("");
        });
    });

    return self;
}
