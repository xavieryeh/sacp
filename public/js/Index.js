function IndexPage(self) {
    var eventText = "";
        self.init = function(){
            $.Log("主畫面","主畫面載入");
            self.getMap();
            // IndexPage.makeDAChart("chart1");
            // IndexPage.makeAAChart("chart2");
            // IndexPage.makeCTPRChart("chart3");
            // IndexPage.makeESAChart("chart5");
            // IndexPage.makeLPSPChart("chart6");

            // 文字雲、雷達圖
            self.makeWCChart("chart4");
            $(".js-popupBtn-open-4").click(function () {
                $.Log("主畫面","右上小圖放大");
                eventText = "右上小圖";
                $(".js-popupBox-4").attr({
                    style: " display: flex;"
                });
                setTimeout(() => {
                    self.makeWCChart("chart4_popup");
                }, 100);
                return false;
            });
            var originalHeight= ($(window).height()-200)/3;
            var rate = 0.25;
            if($(window).width()<900){
                rate = 1;
                originalHeight=400;
            }                                
            else if($(window).width()<1024)
                rate = 0.11;
            else if($(window).width()<1500)
                rate = 0.16;
            var originalWidth = ($(window).width()-(rate==1?50:250))*rate;                 
            $(".main-cardBoxItem .main-card").css({"opacity":"0","width":"700px","height":"300px"});
            $.PostAJAX("Index_mainChartsMaker", {}, response => {
                response.forEach(res=>{
                    // 主畫面chart建置
                    $("#chartTitle"+res.Num).text(res.Title);
                    $("#chartSource"+res.Num).text(res.Source_Name);
                    $("#chartText"+res.Num).html('<p class="CommentText" id="'+res.Function_ID+'Text"></p>');
                    eval(res.Function_Name + "('" + res.Function_ID + "','"+res.ChartName+"')");

                    // 主畫面彈出放大chart建置
                    $(".js-popupBtn-open-"+res.Num).click(function () {
                        $.Log("主畫面",res.Main + "小圖放大");
                        eventText = res.Main + "小圖";
                        $(".js-popupBox-"+res.Num).attr({
                            style: " display: flex;"
                        });
                        setTimeout(() => {
                            $("#chartTitle"+res.Num+"_popup").text($("#chartTitle"+res.Num).text());
                            $("#chartSource"+res.Num+"_popup").text($("#chartSource"+res.Num).text());
                            $("#chartText"+res.Num+"_popup").html('<p class="CommentText" id="'+res.Function_ID+'_popupText"></p>');
                            eval(res.Function_Name + "('" + res.Function_ID + "','"+res.ChartName+"_popup')");
                        }, 100);
                        return false;
                    });

                    //畫面變更事件建置
                    $(window).on('resize', function () {
                        setTimeout(() => {
                            var originalHeight= ($(window).height()-200)/3;
                            var rate = 0.25;
                            if($(window).width()<900){
                                rate = 1;
                                originalHeight=400;
                            }                                
                            else if($(window).width()<1024)
                                rate = 0.11;
                            else if($(window).width()<1500)
                                rate = 0.16;
                            var originalWidth = ($(window).width()-(rate==1?50:250))*rate;                            
                            $(".main-cardBoxItem .main-card").css({'transform-origin':'0;','transform':'scale('+originalWidth/700+','+(originalHeight)/300+')'});
                            // ChartTable_FitSize(res.ChartName,60);
                        }, 200);
                    });
                });

                $(".main-cardBoxItem .main-card").css({'transform-origin':'0','transform':'scale('+originalWidth/700+','+(originalHeight)/300+')'});
                $(".main-cardBoxItem .main-card").animate({"opacity":"1"});
                // $(".main-card").css({"opacity":"0","width":originalWidth+"px","height":originalHeight+"px"});
            });

            setInterval(() => {
                self.updateMap();
            }, 30000);
        
            $(".js-popupBtn-close").click(function () {
                $.Log("主畫面",eventText+"放大視窗關閉");
                $(".popupBox").hide();
                return false;
            });
        }
        // $(window).on('resize', function () {
        //     setTimeout(() => {
        //         ChartTable_FitSize("chart1",30);
        //         ChartTable_FitSize("chart2",30);
        //         ChartTable_FitSize("chart3",0);
        //         ChartTable_FitSize("chart5",10);
        //         ChartTable_FitSize("chart6",10);
        //     }, 200);
        // });
        

        function ChartTable_FitSize(div,offset) {
            const id = $("#"+div+ " div[name='mChart']").attr("id");
            const divHeight = $("#"+div).parents(".main-card").height();
            var Chart = am4core.registry.baseSprites.find(function (chartObj) {
                return chartObj.htmlContainer.id === id;
            });
            var Table = getChartTable(id+"Table");
            if(Table!==undefined){
                const firstLength=$("#"+id+"Table").find("tbody").find("tr:nth-child(2)").find("td:first").text()+2;                            
                $.FitTableOnResize(Table, Chart,firstLength);
                $("#"+id).css("height",(divHeight-Table.Height-offset)+'px');
            }
            else
                $("#"+id).css("height",(divHeight-offset)+'px');
        }

        // function ExcuteFN(name, id) {
        //     eval(name + "('" + id + "')");
        // }

        function sortByDesc(a, b) {
            return b.count - a.count;
        };

        function sort(a, b) {
            return a.count - b.count;
        };

        self.getMap = function() {
            xhr = new XMLHttpRequest();
            xhr.open("GET", "asset/images/map_UI.svg", true);
            // Following line is just to be on the safe side;
            // not needed if your server delivers SVG with correct MIME type
            xhr.overrideMimeType("image/svg+xml");
            xhr.onload = function (e) {
                // You might also want to check for xhr.readyState/xhr.status here
                $(".map-area").html(xhr.responseXML.documentElement);
                $(".map-area").append('<div class="todayMarkBox"></div>');
                self.updateMap();
                var KSarea = $("#KSareaAll > g");
                for (let i = 1; i < KSarea.length + 1; i++) {
                    $("#KSarea_" + i).on("click", function () {
                        // $.Log("主畫面",res.Main + "小圖放大");
                        location.href = self.KSareaHref+"?areaNo=" + i.toString();
                    });
                    // window.location.href = "KSarea?areaNo="+i.toString();
                }
                var KStext = $("#KStextAll g");
                for (let i = 1; i < KStext.length + 1; i++) {
                    $("#KSareaText_" + i).on("click", function () {
                        // $.Log("主畫面",res.Main + "小圖放大");
                        location.href = self.KSareaHref+"?areaNo=" + i.toString();
                    });
                };
            }
            xhr.send("");
        };

        self.updateMap = function() {
            $.PostAJAX("Index_mapDistribution", {}, response => {
                var lstNew = [];
                var sumCount = 0;
                var newCount = 0;
                var maxCount = 0;
                var blinkCount = 1;
                $("#mapList").html(response.sort((a,b)=>{return a.ID - b.ID;}).map(res=>{return '<li><a class="'+res.Class.replace('KSareaQuantity','quantityMark')+'" id="mapListItem'+res.ID+'" href="'+self.KSareaHref+'?areaNo='+res.ID+'">'+res.Location+'</a></li>'}).join(""));
                response.sort((a,b)=>{return b.count - a.count;}).forEach(data => {
                    if(blinkCount<=3) {
                        // 只閃前3高項目, 後端API須已經高到低排序
                        $("#KSarea_" + data.ID).attr("class",data.Class + ' blink'); // 增加警示閃動
                        blinkCount++;
                    } else {
                        $("#KSarea_" + data.ID).attr("class",data.Class);
                    }
                    // if (data.new == 'V') {
                    //     lstNew.push(
                    //         '<div class="todayMarkBox"><div class="todayMark MAPtodayMark MAPtodayMark-' +
                    //         data.ID + '"></div>');
                    //     $("#mapListItem"+ data.ID).addClass("todayMarkListitem");
                    // }
                    sumCount += parseInt(data.count);
                    newCount += parseInt(data.new_Count);
                    maxCount = Math.max(maxCount,data.count);
                });
                $("#caseCount").text(sumCount);
                $("#newCount").text(newCount);
                $(".todayMarkBox").html(lstNew.join(''));

                var listQuantity = [0, 0.2, 0.4, 0.8, 1];
                $("#noticeQuantity").html(listQuantity.map(m=>{return '<li>'+(m*maxCount).toFixed(0)+'</li>'}).join(''));
            });
        }

        am4core.useTheme(am4themes_animated);
        am4core.addLicense(self.ChartsPN);
        const chartParentName = ".main-card";
        // 1.全國藥癮個案統計分析
        self.makeDAChart = function(div){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);
            const id = div+"_DA";
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+id+'" style="height:'+divHeight+'px;width:100%;"></div>\
                <div name="mTable" id="'+id+'Table" style="margin-top:-16px"></div>');
            }        
            am4core.ready(function () {
                // Themes begin
                am4core.useTheme(am4themes_material);
                // Themes end

                // Create chart instance
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === id;
                });

                $.PostAJAX("Index_locationPopulation", {
                    startDate: moment().format('YYYY')
                }, response => {
                    if (chart === undefined) {
                        // $("#"+id+"_popup").height($($("#"+id+"_popup").parent().height-100);
                        Params = {};
                        TableBodyRow = {
                            title: "校正數" + ',' + "#7030A0"
                        };
                        var lstData = [];
                        Params.ChartType = "ColumnBar";
                        // Params.Title = "全國藥癮個案統計分析";
                        // Params.TitleColor = "#00B48B";
                        const Kaohsiung = response.find(f => f.category == '高雄市');
                        Params.Title2 = {
                            title: "110年11月高雄市排名全國第" + Kaohsiung.All + "名,六都第" + Kaohsiung.Municipality + "名",
                            fonSize: ($("#" + id).width() / 25) == 0 ? 25 : ($("#" + id).width() / 25),
                            color: "#FF201E",
                            align: "right",
                            valign: "top",
                            dy: -5
                        };
                        // Params.Title3 = {
                        //     title: "備註 :【校正數為(該縣市列管數/該縣市人口數)*100,000】\n來源 : 衛生福利部毒品危害防制中心案件管理系統",
                        //     fonSize: ($("#"+id).width()/35)==0?25:($("#"+id).width()/35),
                        //     color: "#000000",
                        //     align: "left",
                        //     valign: "bottom",
                        //     dy: -20
                        // };
                        Params.FontSize = 10;
                        Params.Fill = am4core.color("#7030A0");
                        Params.Stroke = am4core.color("#7030A0");
                        Params.Data = response.map(function (each) {
                            return {
                                category: each.category,
                                count: each.count,
                            };
                        }).sort(sortByDesc);
                        response.forEach((each, index) => {
                            lstData.push(each.category);
                            TableBodyRow["col" + index] = (Math.round(each.count, 0)).toString();
                        })
                        // MakeAMChart(id, Params);
                        // MakeAMChart(id + "_popup", Params);
                        var newChart = MakeAMChart(id, Params);
                        setTimeout(() => {
                            var Table = new ChartTable(id + "Table");
                            Table.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                            Table.bodyWidth = newChart.plotContainer.contentWidth + 1;
                            Table.dataSource = [TableBodyRow];
                            Table.vertical = true;
                            Table.columnSource = [""].concat(lstData);
                            Table.bind();
                            $("#"+id).height(divHeight-Table.Height-30);
                        }, 100);
                    } else {
                        const NewChartData = response.map(function (each) {
                            return {
                                category: each.category,
                                count: each.count,
                            };
                        }).sort(sortByDesc);
                        chart.data = NewChartData;
                        chart.invalidateRawData();
                        setTimeout(() => {
                            var aaTable = getChartTable(id + "Table");
                            aaTable.dataSource = [TableBodyRow];
                            aaTable.columnSource = [""].concat(lstData);
                            aaTable.bind();
                        }, 100);
                        // var chart_popup = am4core.registry.baseSprites.find(function (
                        //     chartObj) {
                        //     return chartObj.htmlContainer.id === id + "_popup";
                        // });
                        // if (chart_popup !== undefined) {
                        //     chart_popup.data = NewChartData;
                        //     chart_popup.invalidateRawData();
                        // }
                    }
                });
            }); // end am4core.ready()
        }

        // 2.年齡層統計分析圖
        self.makeAAChart = function(div) {
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);
            const id = div+"_AA";
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+id+'" style="height:'+divHeight+'px;width:100%;"></div>\
                <div name="mTable" id="'+id+'Table" style="margin-top:-15px"></div>');
            }   
            am4core.ready(function () {
                am4core.useTheme(am4themes_kelly);

                // Create chart instance
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === id;
                });
                var sDate = moment().format('YYYY') + "/01";
                $.PostAJAX("DrugAgeByMonth", {
                    startDate: sDate,
                    endDate: '2021/01'
                }, response => {
                    const ChartTableSource = $.ChartTableData(response, "Year", "category", [
                        "#7030A0", "#02FF02"
                    ]);
                    if (chart === undefined) {
                        var lstItem = [];
                        Params = {};
                        Params.Data = [];
                        Params.Color = "#7030A0";
                        Params.ChartType = "Column";
                        Params.Year = sDate.substring(0, 4) - 1911;
                        Params.Month = sDate.split('/')[1].replace('0', '');
                        var tableBodyRow = {
                            title: Params.Year + "年" + Params.Month + "月" + ',' + Params.Color
                        };
                        // Params.Title = "年齡層統計分析圖";
                        Params.Title2 = {
                            title: "單位：人數",
                            fonSize: 12,
                            color: "#000000",
                            align: "right",
                            valign: "top",
                            dy: 20
                        };
                        Params.ColumnsWidth = 30;
                        response.forEach((element, index) => {
                            Params.Data.push({
                                category: element["Age_Class"],
                                count: element["Person"]
                            })
                            lstItem.push(element["Age_Class"]);
                            Params.Total += parseInt(element["Person"]);
                            tableBodyRow["Age_Class" + (index)] = element["Person"];
                        });
                        // Params.Data = ChartTableSource.lstData;
                        // Params.TitleColor = "#00B48B";
                        Params.FontSize = 10;
                        Params.categoryAxisVisible = false;
                        var newChart = amchartfunc(id, Params, Params.ChartType);
                        // var newChart = MakeAMChart(id, Params);
                        setTimeout(() => {                            
                            var Table = new ChartTable(id+"Table");
                            const firstLength=[tableBodyRow][0].title.split(',')[0].length+2;                            
                            $.FitTable(Table, newChart, [""].concat(lstItem), [tableBodyRow],firstLength);
                            $("#"+id).height(divHeight-Table.Height-30);
                        }, 100);
                    } else {
                        setTimeout(() => {
                            var aaTable = getChartTable(id + "Table");
                            aaTable.dataSource = [tableBodyRow];
                            aaTable.columnSource = [""].concat(lstItem);
                            aaTable.bind();
                        }, 100);

                        chart.data = ChartTableSource.lstData;
                        chart.invalidateRawData();
                    }
                });
            }); // end am4core.ready()
        }

        // 3.列管特定營業場所別比例分析圖
        self.makeCTPRChart = function(div) {
            am4core.ready(function () {
                const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?0:0);
                const id = div+"_CTPR";
                // $("#"+div).attr("class","PieTable"+(div.indexOf('popup')>-1?'_popup':'')).css("height",(divHeight-80)+"px");
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+id+'" style="margin-left:10%;height:'+(divHeight-20)+'px;width:80%;"></div>');
                }    
                // Create chart instance
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === id;
                });
                const color = ["#FF0A00", "#0F02FF", "#008000"];

                $.PostAJAX("Index_tubeSpecificAnalyze", {}, response => {
                    const lstData = response.map((data, i) => {
                        return {
                            category: data.category,
                            count: data.count,
                            color: color[i]
                        }
                    }).sort(sort);

                    var TableBodyRow = [];
                    response.forEach((data, i) => {
                        TableBodyRow.push(Object.values(data));
                    });

                    if (chart === undefined) {
                        Params = {};
                        Params.ChartType = "Pie";
                        // Params.Title = "列管特定營業場所別比例分析圖";
                        // Params.TitleColor = "#00B48B";
                        Params.FontSize = 10;
                        Params.SeriesText =
                            "{category}: {value.value}家, {value.percent.formatNumber('#.0')}%";
                        Params.Data = lstData;
                        var newChart = MakeAMChart(id, Params);

                        // setTimeout(() => {
                        //     var CTPRTable = new ChartTable(id+"Table");
                        //     CTPRTable.headerWidth = 0;
                        //     CTPRTable.bodyWidth = newChart.contentWidth * 0.7;
                        //     CTPRTable.dataSource = TableBodyRow;
                        //     CTPRTable.columnSource = ["營業場所", "數量", "比例"];
                        //     CTPRTable.bind();
                        // }, 100);
                    } else {
                        // var CTPRTable = getChartTable(id+"Table");
                        // CTPRTable.dataSource = TableBodyRow;
                        // CTPRTable.columnSource = ["營業場所", "數量", "比例"];
                        // CTPRTable.bind();
                        //Setting the new data to the graph
                        // NewChartData.forEach(function(each,i){
                        //     chart.data[i]["country"] = each.country;
                        //     chart.data[i]["count"] = each.count;
                        // });

                        chart.data = lstData;
                        chart.invalidateRawData();
                    }
                });
            }); // end am4core.ready()
        }

        // 4.文字雲&雷達圖
        self.makeWCChart = function(div) {
            am4core.ready(function () {
                // <div class="chart" id="WCChart" style="height: 50%"></div>
                $("#"+div).css('height','100%');
                // const divHeight = $("#"+div).parent().height()<300?300:$("#"+div).parent().height()-(div.indexOf('popup')>-1?80:0);
                const id = div+"_WC";
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+id+'" style="height:50%;width:100%;"></div>\
                    <div class="'+(div.indexOf('popup')>-1?'chart3-sub-block_popup':'chart3-sub-block')+'">\
                        <div name="mChart" id="'+id+'_sub1" style="height:100%;width:100%"></div>\
                        <div name="mChart" id="'+id+'_sub2" style="height:100%;width:100%"></div>\
                    </div>');
                }   
                // Create chart instance
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === id;
                });

                // $file = file('database/wordcloud.txt');
                $.PostAJAX("Index_wordCloud", {}, response => {
                    if (chart === undefined) {
                        var Params = {};
                        Params.ChartType = "WordCloud";
                        Params.Data = response.wordCloud.map(function (each) {
                            return {
                                category: each.category,
                                count: each.count
                            };
                        });
                        MakeAMChart(id, Params);
                    } else {
                        // var NewChartData = response.wordCloud.map(function (each) {
                        //     return {
                        //         category: each.category,
                        //         count: each.count,
                        //     };
                        // });

                        // chart.data = NewChartData;
                        // chart.invalidateRawData();
                    }

                    var chart3 = [{
                        id: id+'_sub1',
                        title: "風險因子",
                        color: "#E98440"
                    }, {
                        id: id+'_sub2',
                        title: "保護因子",
                        color: "#3FBEEC"
                    }]
                    chart3.forEach((subChart, iSub) => {
                        var sub_chart = am4core.registry.baseSprites.find(function (
                            chartObj) {
                            return chartObj.htmlContainer.id === subChart.id;
                        });
                        if (sub_chart === undefined) {
                            var Params = {};
                            Params.ChartType = "Radar";
                            // Params.Title = {
                            //     title: subChart.title,
                            //     fonSize: 10,
                            //     color: subChart.color,
                            //     align: "center",
                            //     valign: "top",
                            //     dy: -20
                            // };
                            Params.FontSize = 8;
                            Params.name = subChart.title;
                            Params.color = subChart.color;
                            Params.Data = response["wordCloudSub" + (iSub + 1)
                                .toString()].map(function (each, i) {
                                return {
                                    category: each.category,
                                    count: each.count
                                };
                            });
                            MakeAMChart(subChart.id, Params);
                        } else {
                            var NewChartData = response["wordCloudSub" + (iSub + 1)
                                .toString()].map(function (each, i) {
                                return {
                                    category: each.category,
                                    count: each.count
                                };
                            });

                            sub_chart.data = NewChartData;
                            sub_chart.invalidateRawData();
                        }
                    });
                });
            }); // end am4core.ready()
        }

        // 5.收案來源分析
        self.makeESAChart = function(div) {
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);
            const id = div+"_ESA";
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+id+'" style="height:'+(divHeight-20)+'px;width:100%;"></div>');
            }     
            am4core.ready(function () {
                // Themes begin
                am4core.useTheme(am4themes_material);
                // Themes end

                // Create chart instance
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === id;
                });

                $.PostAJAX("Index_caseSourceAnalyze", {
                    startDate: "2021/08"
                    // startDate: moment().format('YYYY')
                }, response => {
                    if (chart === undefined) {
                        Params = {};
                        Params.ChartType = "LabelBar";
                        // Params.Title = "收案來源分析";
                        // Params.TitleColor = "#00B48B";
                        Params.FontSize = 10;
                        Params.Fill = am4core.color("#7030A0");
                        Params.Stroke = am4core.color("#7030A0");
                        Params.Data = response.sort(sort);
                        var newChart = amchartfunc(id, Params, Params.ChartType);
                    } else {
                        const NewChartData = response.sort(sort);
                        //Setting the new data to the graph
                        // NewChartData.forEach(function(each,i){
                        //     chart.data[i]["country"] = each.country;
                        //     chart.data[i]["count"] = each.count;
                        // });

                        chart.data = NewChartData;
                        chart.invalidateRawData();
                    }
                });
            }); // end am4core.ready()
        }

        // 各級毒品查獲人數分析
        self.makeLPSPChart = function(div) {
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);
            const id = div+"_LPSP";
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+id+'" style="height:'+divHeight+'px;width:100%;"></div>\
                <div name="mTable" id="'+id+'Table" style="margin-top:-46px"></div>');
            }   
            am4core.ready(function () {
                am4core.useTheme(am4themes_kelly);

                // Create chart instance
                var chart = am4core.registry.baseSprites.find(function (chartObj) {
                    return chartObj.htmlContainer.id === id;
                });

                $.PostAJAX("Index_levelSeizedPeopleAnalyze", {
                    startDate: moment().format('YYYY') + '/01',
                    endDate: moment().format('YYYY/MM')
                }, response => {
                    const ChartTableSource = $.ChartTableData(response, "Level", "category", ["#0031CC", "#FF3300", "#008000", "#833C0B", "#FFC000", "#FF0000"]);
                    if (chart === undefined) {
                        Params = {};
                        Params.ChartType = "GroupColumnBar";
                        // Params.Title = "各級毒品查獲人數分析";
                        Params.Title2 = {
                            title: "單位：人數",
                            fonSize: 12,
                            color: "#000000",
                            align: "right",
                            valign: "top",
                            dy: 20
                        };
                        // Params.TitleColor = "#00B48B";
                        Params.FontSize = 10;
                        Params.cellStartLocation = 0.1;
                        Params.categoryAxisVisible = false;
                        Params.Data = ChartTableSource.lstData;
                        var newChart = MakeAMChart(id, Params);

                        setTimeout(() => {
                            var Table = new ChartTable(id+"Table");
                            const firstLength=ChartTableSource.TableBodyRow[0][0].split(',')[0].length+2;                            
                            $.FitTable(Table, newChart, [""].concat(ChartTableSource.lstCategory), ChartTableSource.TableBodyRow,firstLength);
                            $("#"+id).height(divHeight-Table.Height-10);
                        }, 100);
                    } else {
                        setTimeout(() => {
                            var aaTable = getChartTable(id + "Table");
                            aaTable.dataSource = ChartTableSource.TableBodyRow;
                            aaTable.columnSource = [""].concat(ChartTableSource
                                .lstCategory);
                            aaTable.bind();
                        }, 100);
                        //Setting the new data to the graph
                        // NewChartData.forEach(function(each,i){
                        //     chart.data[i]["country"] = each.country;
                        //     chart.data[i]["count"] = each.count;
                        // });

                        chart.data = ChartTableSource.lstData;
                        chart.invalidateRawData();
                    }
                });
            }); // end am4core.ready()
        }
        // setTimeout(() => {
        //     AutoFitHeight();
        // }, 200);
        // });
    // });
    return self;
}
