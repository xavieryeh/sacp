(function () {
    // AJAX
    $.extend({
        PostFormAJAX: function (url, data, callback, async, errorBack, completeBack) {
            $.ajax({
                type: "POST",
                async: true,
                url: url,
                data: data,
                processData: false,
                contentType : false,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                //beforeSend: function () {
                //    $.popup('dialog-box-alert3', 'dialog-overlay-alert3', '60', '60');
                //},
                //complete: function () {
                //    $.PopupClose('dialog-box-alert3', 'dialog-overlay-alert3');
                //},
                error: function (xhr, textStatus) {
                    if (errorBack === null || errorBack === undefined) {
                        console.log(xhr.responseText);
                    } else {
                        errorBack(xhr, textStatus);
                    }
                },
                success: function (response) {
                    if (callback !== null)
                        callback(response);
                },
                complete: function (XMLHttpRequest, textStatus) {
                    if (completeBack === null || completeBack === undefined) {

                    } else {
                        completeBack(XMLHttpRequest, textStatus);
                    }
                }
            });
        },
        PostAJAX: function (url, data, callback, async, errorBack, completeBack) {
            $.ajax({
                type: "POST",
                async: true,
                url: url,
                data: data,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                //beforeSend: function () {
                //    $.popup('dialog-box-alert3', 'dialog-overlay-alert3', '60', '60');
                //},
                //complete: function () {
                //    $.PopupClose('dialog-box-alert3', 'dialog-overlay-alert3');
                //},
                error: function (xhr, textStatus) {
                    if (errorBack === null || errorBack === undefined) {
                        console.log(xhr.responseText);
                    } else {
                        errorBack(xhr, textStatus);
                    }
                },
                success: function (response) {
                    if (callback !== null)
                        callback(response);
                },
                complete: function (XMLHttpRequest, textStatus) {
                    if (completeBack === null || completeBack === undefined) {

                    } else {
                        completeBack(XMLHttpRequest, textStatus);
                    }
                }
            });
        },
        PostAxios: function (url, data, callback, async) {
            axios({
                    method: "POST",
                    async: true,
                    url: url,
                    data: data,
                    responseType: 'json',
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    }
                }).then((response) => {
                        if (callback !== null)
                            callback(response.data);
                });
                //beforeSend: function () {
                //    $.popup('dialog-box-alert3', 'dialog-overlay-alert3', '60', '60');
                //},
                //complete: function () {
                //    $.PopupClose('dialog-box-alert3', 'dialog-overlay-alert3');
                //},
        },
        PostFileAJAX: function (url, data, file, callback, async) {
            $.ajax(url, {
                type: "POST",
                //async: false,
                files: file,
                data: data,
                iframe: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                error: function (xhr, textStatus) {
                    console.log(xhr.responseText);
                },
                //beforeSend: function () {
                //    $.popup('dialog-box-alert3', 'dialog-overlay-alert3', '60', '60');
                //},
                //complete: function () {
                //    $.PopupClose('dialog-box-alert3', 'dialog-overlay-alert3');
                //},
                success: function (response) {
                    if (callback !== null)
                        callback(response.replace("<head></head><body>", "").replace("</body>", ""));
                }
            });
        },
        Log: function (func,action) {
            $.PostAJAX("Log", {func:func,action:action},null);
        },
        FitTableOnResize:function(Table,newChart,firstLength){
            if(Table.fontSize>0){
                const firstColumnWidth = Table.fontSize*firstLength;
                const chartWidth = newChart.contentWidth;
                console.log(firstColumnWidth);
                if(firstColumnWidth>newChart.leftAxesContainer.contentWidth){

                    newChart.leftAxesContainer.contentWidth = firstColumnWidth;
                    Table.headerWidth = firstColumnWidth+14;
                    Table.bodyWidth = chartWidth-firstColumnWidth+1;
                    // console.log(firstColumnWidth+','+chartWidth-firstColumnWidth);
                    Table.setWidth();
                }
                else{
                    Table.headerWidth = newChart.leftAxesContainer.contentWidth+14;
                    Table.bodyWidth = newChart.plotContainer.contentWidth+1;
                    // console.log(newChart.leftAxesContainer.contentWidth+','+newChart.plotContainer.contentWidth+1);
                    Table.setWidth();
                }
            }
        },
        FitTable:function(Table,newChart,columnSource,dataSource,firstLength,vertical,spicial,fixed){
            Table.headerWidth = newChart.leftAxesContainer.contentWidth+14;
            Table.bodyWidth = newChart.plotContainer.contentWidth+1;
            Table.dataSource = dataSource;
            Table.columnSource = columnSource;
            if(vertical!==undefined)
                Table.vertical = vertical;
            if(spicial!==undefined)
                Table.spicial = spicial;
            if(fixed!==undefined)
                Table.fixed = true;

            Table.bind();
            if(Table.fontSize>0){
                const firstColumnWidth = Table.fontSize*(firstLength);
                const chartWidth = newChart.contentWidth-newChart.rightAxesContainer.contentWidth;
                if(firstColumnWidth>newChart.leftAxesContainer.contentWidth){
                    newChart.leftAxesContainer.width = firstColumnWidth;
                    Table.headerWidth = firstColumnWidth+14;
                    Table.bodyWidth = chartWidth-firstColumnWidth+1;
                    Table.setWidth();
                }
            }
        },
        ChartTableData:function (response, firstName, category, lstColor) {
            const lstFirstCol = [...new Set(response.map(each => {
                return each[firstName];
            }))];
            const lstCategory = [...new Set(response.map(each => {
                return each[category];
            }))];
            var Params = {};
            Params.lstCategory = lstCategory;
            Params.TableBodyRow = [];
            Params.lstData = lstCategory.map(function (each, iItem) {
                var newData = {};
                newData.category = each;
                lstFirstCol.forEach((year, iYear) => {
                    if(each=="合計"){
                        newData[year]=null;
                        return;
                    }
                    newData[year] = response.filter(data => {
                        return data[firstName] == year && data[category] == each
                    })[0];
                    if(newData[year]==undefined)
                        newData[year]=0;
                    else
                        newData[year]=parseInt(newData[year].count);
                });
                newData.color = lstColor;
                return newData;
            });
            var TableData = lstCategory.map(function (each, iItem) {
                var newData = {};
                newData.category = each;
                lstFirstCol.forEach((year, iYear) => {
                    newData[year] = response.filter(data => {
                        return data[firstName] == year && data[category] == each
                    })[0];
                    if(newData[year]==undefined)
                        newData[year]='0';
                    else
                        newData[year]=newData[year].count;
                });
                newData.color = lstColor;
                return newData;
            });
            lstFirstCol.forEach((year, iYear) => {
                var year_block = (year!=="合計"&&lstColor[iYear]!==undefined?(year+','+lstColor[iYear]):year+',transparent');
                Params.TableBodyRow.push([year_block].concat(TableData.map(each => {
                    return each[year];
                })));
            });
            return Params;
        }
    });

})();
