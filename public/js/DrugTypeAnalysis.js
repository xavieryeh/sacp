function getDrugTypeAnalysischart(id,div){
    const chartID = id + (div.indexOf('popup') > -1 ? "_popup" : "");
    var startdate = '2020/01';enddate = '2021/05';
    var breadcrumbtitle=document.getElementById("breadcrumbtitle");
    var title=document.getElementById("title");
    var bannerhref = document.getElementById("bannerhref");
    var source  = document.getElementById("source");
    var selectYear = document.getElementById("selectYear");
    var selectMonth = document.getElementById("selectMonth");
    var selectStartYear = document.getElementById("selectStartYear");
    var selectEndYear = document.getElementById("selectEndYear");
    var sYear = moment().format('YYYY') -2  +"/"+ moment().format('MM');
    var eYear = moment().format('YYYY/MM');
    var sDate = moment().format('YYYY') + "/01";
    var eDate = moment().format('YYYY/MM');
    var reg = /^\d+$/;
    if (selectYear && selectMonth) {
        if (selectYear.value.match(reg) && selectMonth.value.match(reg)) {
            sDate = (parseInt(selectYear.value) + 1911).toString() + "/01";
            eDate = (parseInt(selectYear.value) + 1911).toString() + "/" + selectMonth.value;
        }
    };
    if (selectStartYear && selectEndYear) {
        if (selectStartYear.value.match(reg) && selectEndYear.value.match(reg)) {
            sYear = (parseInt(selectStartYear.value) + 1911).toString() +'/'+ moment().format('MM')
            eYear = (parseInt(selectEndYear.value) + 1911).toString() +'/'+ moment().format('MM')
        }
    };
    function getTextCss(font_size) {
        var scale = 1;
        if (font_size < 8)
            scale = font_size / 8;
        else if(font_size>12)
            font_size = 12;
        return {
            "font-size": font_size + 'px',
            "transform-origin": "0",
            "white-space": "nowrap",
            "transform": "scale(" + scale + ")"
        };
    };
    var chartParentName = ".card-item";
    if(title && breadcrumbtitle && source && bannerhref)
        chartParentName = ".largechart";
    else if(document.getElementsByClassName("content-index").length>0)
        chartParentName = ".main-card"
    switch(id){
//一、???
    case"DrugTypebyMonth":
        $.getDrugTypebyMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                ');
                // <div name="mTable" id="'+id+'Table" style="margin-top:-16px"></div>
            }
            $("#"+chartID).height(divHeight-55);
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                var lstItem = [];
                Params.ChartType = "LabelBar";
                var tableBodyRow = {title:"未校正"};
                Params.Data = [];
                Params.month = eDate.split("/")[1];

                Params.Color = "#7030a0";
                Params.Name = eDate;
                Params.Title = "本市藥癮個案收案類型分析";
                Params.fonSize=12;
                Params.ShowPercent = true;
                Params.Total=0;
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市藥癮個案收案類型分析";
                        bannerhref.href="DrugTypeAnalysis";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;
                    }
                Params.Title2 = {
                        title: "單位：個案數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach((element,index)=>{
                    Params.Total +=parseInt(element["Person"]);
                    lstItem.push(element["Manage_Type"]);
                    //tableBodyRow["Manage_Type"+(index)] = element["Person"];
                    });
                    data.forEach(element=>{
                        Params.Data.push({category:element["Manage_Type"],count:parseInt(element["Person"]),Percent:Math.round((parseInt(element["Person"])/Params.Total),2)*100/100})
                    })

                    document.getElementById(chartID + "Text").innerHTML ="註："+ Params.month+"月列管在案人數總計"+Params.Total+"人";
                    var newChart = amchartfunc(chartID,Params, Params.ChartType);
                            setTimeout(() => {
                            //                 var aaTable = new ChartTable("DrugTypebyMonthTable");
                            //                 aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                            //                 aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                            //                 aaTable.dataSource=[tableBodyRow];
                            //                 aaTable.columnSource=[""].concat(lstItem);
                            //                 aaTable.bind();
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                            }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === "SelectLocationeTubeMonth";
            });
            setTimeout(() => {
        //         var aaTable = getChartTable("SelectLocationeTubeMonthTable");
        //         aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
        //         aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
        //         aaTable.setWidth();
        $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"DrugTypebyYear":
        $.getDrugTypebyYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
            }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.ChartType = "Group";
                var tableBodyRow = [];
                Params.Data = [];
                Params.Color = ["#0031CC", "#ff0000", "#0dd3c4"];
                Params.Year  = [];
                Params.Manage_Type=[];
                Params.Title = "本市藥癮個案收案類型分析";
                Params.fonSize=12;
                Params.Total={};
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市藥癮個案收案類型分析";
                        bannerhref.href="DrugTypeAnalysis";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;
                    }
                Params.Title2 = {
                        title: "單位：個案數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach(element => {
                        Params.Year.push(element["YEAR"]);
                        Params.Manage_Type.push(element["Manage_Type"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.Manage_Type = [...new Set(Params.Manage_Type)];
                    Params.Manage_Type.forEach(each=>{
                        if(each.length>4)
                        {
                            if(each.length>12)
                                lstItem.push(each.slice(0,4)+"<br>"+each.slice(4,8)+"<br>"+each.slice(8,each.length));
                            else
                                lstItem.push(each.slice(0,4)+"<br>"+each.slice(4,each.length));
                        }
                        else
                            lstItem.push(each);
                    })
                    Params.Year.forEach((year,index)=>{
                        Params.Total[year]=0;
                        data.filter(each=>{return each.YEAR==year}).forEach(each=>{
                            Params.Total[year]+=parseInt(each.Person);
                        })
                    })
                    Params.Data =  Params.Manage_Type.map(function(type,iItem){
                        var newData={};
                        newData.category=type;
                        Params.Year.forEach((eachYear,index)=>{
                            newData[eachYear] = data.filter((each)=>{return each.Manage_Type==type && each.YEAR == eachYear})[0]
                            if(newData[eachYear]==undefined)
                                newData[eachYear]=0;
                            else
                                newData[eachYear]=newData[eachYear].Person;
                        })
                        newData.color=Params.Color;
                        return newData;
                    });
                    Params.Year.sort();
                    tableBodyRow = Params.Year.map((element,index)=>{
                        var newData={};
                        newData.category =element+"年"+','+Params.Color[index] ;
                        Params.Manage_Type.forEach((eachtype,index)=>{
                            newData[eachtype] = data.filter(each=>{return each.YEAR==element && each.Manage_Type == eachtype})[0]
                                if(newData[eachtype]==undefined)
                                    newData[eachtype]='0';
                                else
                                    newData[eachtype]=newData[eachtype].Person;
                        })
                        return newData;
                    })

                    document.getElementById(chartID + "Text").innerHTML = "註："+Params.Year.map(year=>{
                        return year+"年列管在案人數"+Params.Total[year]+"人"
                    }).join("、");
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"ColumnSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=tableBodyRow;
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                            }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
            }, 100);
        });
    break;
}};
