function getDrugRecidivismRateChartid(id,div){
    const chartID = id + (div.indexOf('popup') > -1 ? "_popup" : "");
    var startdate = '2020/01';var enddate = '2021/05';
    var breadcrumbtitle=document.getElementById("breadcrumbtitle");
    var title=document.getElementById("title");
    var bannerhref = document.getElementById("bannerhref");
    var source  = document.getElementById("source");
    var selectYear = document.getElementById("selectYear");
    var selectMonth = document.getElementById("selectMonth");
    var selectStartYear = document.getElementById("selectStartYear");
    var selectEndYear = document.getElementById("selectEndYear");
    var sYear = moment().format('YYYY')-2;
    var eYear = moment().format('YYYY');
    var sDate = moment().format('YYYY')+"/01";
    var eDate = moment().format('YYYY/MM');
    var reg = /^\d+$/;
    if(selectYear && selectMonth){
        if(selectYear.value.match(reg) && selectMonth.value.match(reg)){
            sDate =(parseInt(selectYear.value)+1911).toString()+"/01";
            eDate =(parseInt(selectYear.value)+1911).toString() +"/"+ selectMonth.value;
        }
    };
    if(selectStartYear && selectEndYear){
        if(selectStartYear.value.match(reg) && selectEndYear.value.match(reg)){
            sYear = (parseInt(selectStartYear.value)+1911).toString()
            eYear = (parseInt(selectEndYear.value)+1911).toString()
        }
    };
    function getTextCss(font_size) {
        var scale = 1;
        if (font_size < 8)
            scale = font_size / 8;
        else if(font_size>12)
            font_size = 12;
        return {
            "font-size": font_size + 'px',
            "transform-origin": "0",
            "white-space": "nowrap",
            "transform": "scale(" + scale + ")"
        };
    };
    var chartParentName = ".card-item";
    if(title && breadcrumbtitle && source && bannerhref)
        chartParentName = ".largechart";
    else if(document.getElementsByClassName("content-index").length>0)
        chartParentName = ".main-card"
    switch(id){
//一、列管期間內施用毒品再犯率
    case "SelectRecidivesmRateMonth":
        $.getRecidivesmRateMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
            }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                var tableBodyRow = [];
                Params.Color=["#0dd3c4","#7030A0"];
                Params.Title = "一、列管期間內施用毒品再犯率";
                if(title && breadcrumbtitle && source && bannerhref ){
                    title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                    bannerhref.innerHTML = "本市及全國藥癮個案再犯率比較分析犯率比較分析";
                    source.innerHTML="來源：衛福部毒癮單窗服務系統";
                    bannerhref.href="KaohsiungAndTaiwanDrugRecidivismRateAnalysis";
                }
                Params.Location = [];
                Params.month=[];
                if(data.length>0){
                    data.forEach(element => {
                        Params.month.push(element["MONTH"]);
                        Params.Location.push(element["Location"]);
                    });
                    Params.month = [...new Set(Params.month)];
                    Params.Location = [...new Set(Params.Location)];
                    Params.ChartType="LineSeries";
                    if(Params.month[Params.month.length-1]<7)
                        Params.ChartType="ColumnSeries";

                tableBodyRow = Params.Location.map(function(element,index){
                    var newData={};
                    newData.title = element+','+Params.Color[index];
                        data.filter((each)=>{return each.Location==element}).forEach(function(each,ieach){
                            newData["M"+each.MONTH]=(Math.round(each.Recidivism_Rate*100)/100).toString()+"%";
                        });
                    return newData;
                });
                Params.Data = Params.month.map((element,index)=>{
                    var newData={};
                    lstItem.push(element+"月");
                    newData.category = element;
                    data.filter((each)=>{return each.MONTH==element}).forEach(function(each,ieach){
                        newData[each.Location]=each.Recidivism_Rate;
                    });
                    newData.color=Params.Color;
                    return newData;
                });
                var newChart = amchartfunc(chartID,Params,"Group",Params.ChartType);
                        setTimeout(() => {
                                    var aaTable = new ChartTable(chartID+"Table");
                                    aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                    aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                    aaTable.dataSource=tableBodyRow;
                                    aaTable.columnSource=[""].concat(lstItem);
                                    aaTable.bind();
                                    $("#"+chartID).height(divHeight-aaTable.Height-40);
                                    $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                }, 100);
            }
        }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });

            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
            }, 100);
        });
     break;
    case "SelectRecidivesmRateYear":
        $.getRecidivesmRateYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
            }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                Params.data = [];
                Params.Color=["#0dd3c4","#7030A0"];
                Params.Title = "一、列管期間內施用毒品再犯率";
                if(title && breadcrumbtitle && source && bannerhref ){
                    title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                    bannerhref.innerHTML = "本市及全國藥癮個案再犯率比較分析犯率比較分析";
                    source.innerHTML="來源：衛福部毒癮單窗服務系統";
                    bannerhref.href="KaohsiungAndTaiwanDrugRecidivismRateAnalysis";
                }
                Params.Year =[];
                Params.Location = [];
                var lstItem = [];
                var tableBodyRow = [];
                if(data.length>0){
                    data.forEach(element => {
                        Params.Year.push(element["YEAR"]);
                        Params.Location.push(element["Location"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.Location = [...new Set(Params.Location)];

                    Params.Data = Params.Year.map((element,index)=>{
                        var newData={};
                        lstItem.push(element+"年");
                        newData.category = element;
                            data.filter((each)=>{return each.YEAR==element}).forEach(function(each,ieach){
                                newData[each.Location]=(Math.round(each.Recidivism_Rate*100)/100).toString();
                            });
                            newData.color=Params.Color;
                        return newData;
                    });
                    tableBodyRow = Params.Location.map(function(element,index){
                        var newData={};
                        newData.title = element+','+Params.Color[index];
                            data.filter((each)=>{return each.Location==element}).forEach(function(each,ieach){
                                newData["Y"+each.YEAR]=(Math.round(each.Recidivism_Rate*100)/100).toString();
                            });
                        return newData;
                    });
                    var newChart = amchartfunc(chartID,Params,"LineGroup","LineSeries");
                        setTimeout(() => {
                                    var aaTable = new ChartTable(chartID+"Table");
                                    aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                    aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                    aaTable.dataSource=tableBodyRow;
                                    aaTable.columnSource=[""].concat(lstItem);
                                    aaTable.bind();
                                    $("#"+chartID).height(divHeight-aaTable.Height-40);
                                    $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                }, 100);
                    }
            }); // end am4core.ready()
        });
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });

            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
            }, 100);
        });
    break;
//二、列管期滿結案後半年內施用毒品再犯率
    case "SelectRecidivesmRateHalfYearMonth":
        $.getRecidivesmRateHalfYearMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
            }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                var lstItem = [];
                var tableBodyRow = [];
                Params.data = [];
                Params.Color=["#0dd3c4","#7030A0"];
                Params.Title = "二、列管期滿結案後半年內施用毒品再犯率";
                if(title && breadcrumbtitle && source && bannerhref ){
                    title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                    bannerhref.innerHTML = "本市及全國藥癮個案再犯率比較分析犯率比較分析";
                    source.innerHTML="來源：衛福部毒癮單窗服務系統";
                    bannerhref.href="KaohsiungAndTaiwanDrugRecidivismRateAnalysis";
                }
                Params.Location = [];
                Params.month=[];
                if(data.length>0){
                    data.forEach(element => {
                        Params.month.push(element["MONTH"]);
                        Params.Location.push(element["Location"]);
                    });
                    Params.month = [...new Set(Params.month)];
                    Params.Location = [...new Set(Params.Location)];
                    Params.ChartType="LineSeries";
                    if(Params.month[Params.month.length-1]<7)
                        Params.ChartType="ColumnSeries";

                tableBodyRow = Params.Location.map(function(element,index){
                    var newData={};
                    newData.title = element+','+Params.Color[index];
                        data.filter((each)=>{return each.Location==element}).forEach(function(each,ieach){
                            newData["M"+each.MONTH]=(Math.round(each.Recidivism_Rate*100)/100).toString()+"%";
                        });
                    return newData;
                });
                Params.Data = Params.month.map((element,index)=>{
                    var newData={};
                    lstItem.push(element+"月");
                    newData.category = element;
                    data.filter((each)=>{return each.MONTH==element}).forEach(function(each,ieach){
                        newData[each.Location]=each.Recidivism_Rate;
                    });
                    newData.color=Params.Color;
                    return newData;
                });
                var newChart = amchartfunc(chartID,Params,"LineGroup",Params.ChartType);
                            setTimeout(() => {
                                        var aaTable = new ChartTable(chartID+"Table");
                                        aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                        aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                        aaTable.dataSource=tableBodyRow;
                                        aaTable.columnSource=[""].concat(lstItem);
                                        aaTable.bind();
                                        $("#"+chartID).height(divHeight-aaTable.Height-40);
                                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                    }, 100);
                }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });

            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
            }, 100);
        });
    break;
    case "SelectRecidivesmRateHalfYearbyYear":
        $.getRecidivesmRateHalfYearbyYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
            }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                Params.data = [];
                Params.Title = "二、列管期滿結案後半年內施用毒品再犯率";
                Params.Color=["#0dd3c4","#7030A0"];
                if(title && breadcrumbtitle && source && bannerhref ){
                    title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                    bannerhref.innerHTML = "本市及全國藥癮個案再犯率比較分析犯率比較分析";
                    source.innerHTML="來源：衛福部毒癮單窗服務系統";
                    bannerhref.href="KaohsiungAndTaiwanDrugRecidivismRateAnalysis";
                }
                Params.Year =[];
                Params.Location = [];
                var lstItem = [];
                var tableBodyRow = [];
                if(data.length>0){
                    data.forEach(element => {
                        Params.Year.push(element["YEAR"]);
                        Params.Location.push(element["Location"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.Location = [...new Set(Params.Location)];
                    Params.Data = Params.Year.map((element,index)=>{
                        var newData={};
                        lstItem.push(element+"年");
                        newData.category = element;
                            data.filter((each)=>{return each.YEAR==element}).forEach(function(each,ieach){
                                newData[each.Location]=(Math.round(each.Recidivism_Rate*100)/100).toString();
                            });
                            newData.color=Params.Color;
                        return newData;
                    });
                    tableBodyRow = Params.Location.map(function(element,index){
                        var newData={};
                        newData.title =element+','+Params.Color[index];
                            data.filter((each)=>{return each.Location==element}).forEach(function(each,ieach){
                                newData["col"+each.YEAR]=(Math.round(each.Recidivism_Rate*100)/100).toString();
                            });
                        return newData;
                    });
                    var newChart = amchartfunc(chartID,Params,"LineGroup","LineSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=tableBodyRow;
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });

            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
            }, 100);
        });
    break;
//三、列管期滿結案後一年內施用毒品再犯率
    case "SelectRecidivesmRateOneYearMonth":
        $.getRecidivesmRateOneYearMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
            }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                var lstItem = [];
                var tableBodyRow = [];
                Params.Color=["#0dd3c4","#7030A0"];
                Params.Title = "三、列管期滿結案後一年內施用毒品再犯率";
                if(title && breadcrumbtitle && source && bannerhref ){
                    title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                    bannerhref.innerHTML = "本市及全國藥癮個案再犯率比較分析犯率比較分析";
                    source.innerHTML="來源：衛福部毒癮單窗服務系統";
                    bannerhref.href="KaohsiungAndTaiwanDrugRecidivismRateAnalysis";
                }
                Params.Location = [];
                Params.month=[];
                if(data.length>0){
                    data.forEach(element => {
                            Params.month.push(element["MONTH"]);
                            Params.Location.push(element["Location"]);
                        });
                        Params.month = [...new Set(Params.month)];
                        Params.Location = [...new Set(Params.Location)];
                        Params.ChartType="LineSeries";
                        if(Params.month[Params.month.length-1]<7)
                            Params.ChartType="ColumnSeries";
                    tableBodyRow = Params.Location.map(function(element,index){
                        var newData={};
                        newData.title = element+','+Params.Color[index];
                            data.filter((each)=>{return each.Location==element}).forEach(function(each,ieach){
                                newData["col"+each.MONTH]=(Math.round(each.Recidivism_Rate*100)/100).toString()+"%";
                            });
                        return newData;
                    });
                    Params.Data = Params.month.map((element,index)=>{
                        var newData={};
                        newData.category = element;
                        lstItem.push(element+"月")
                        data.filter((each)=>{return each.MONTH==element}).forEach(function(each,ieach){
                            newData[each.Location]=each.Recidivism_Rate;
                        });
                        newData.color= Params.Color;
                        return newData;
                    });
                var newChart = amchartfunc(chartID,Params,"Group",Params.ChartType);
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=tableBodyRow;
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                    }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });

            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
            }, 100);
        });
    break;
    case "SelectRecidivesmRateOneYearbyYear":
        $.getRecidivesmRateOneYearbyYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
            }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                Params.data = [];
                Params.Color=["#0dd3c4","#7030A0"];
                Params.Title = "三、列管期滿結案後一年內施用毒品再犯率";
                if(title && breadcrumbtitle && source && bannerhref ){
                    title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                    bannerhref.innerHTML = "本市及全國藥癮個案再犯率比較分析犯率比較分析";
                    source.innerHTML="來源：衛福部毒癮單窗服務系統";
                    bannerhref.href="KaohsiungAndTaiwanDrugRecidivismRateAnalysis";
                }
                Params.Year =[];
                Params.Location = [];
                var lstItem = [];
                var tableBodyRow = [];
                if(data.length>0){
                    data.forEach(element => {
                        Params.Year.push(element["YEAR"]);
                        Params.Location.push(element["Location"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.Location = [...new Set(Params.Location)];
                    Params.Data = Params.Year.map((element,index)=>{
                        var newData={};
                        newData.category = element;
                        lstItem.push(element+"年");
                            data.filter((each)=>{return each.YEAR==element}).forEach(function(each,ieach){
                                newData[each.Location]=(Math.round(each.Recidivism_Rate*100)/100).toString();
                            });
                            newData.color=Params.Color;
                        return newData;
                    });
                    tableBodyRow = Params.Location.map(function(element,index){
                        var newData={};
                        newData.title = element+','+Params.Color[index];
                            data.filter((each)=>{return each.Location==element}).forEach(function(each,ieach){
                                newData["col"+each.YEAR]=(Math.round(each.Recidivism_Rate*100)/100).toString();
                            });
                        return newData;
                    });
                var newChart = amchartfunc(chartID,Params,"LineGroup","LineSeries");
                                    setTimeout(() => {
                                                var aaTable = new ChartTable(chartID+"Table");
                                                aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                                aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                                aaTable.dataSource=tableBodyRow;
                                                aaTable.columnSource=[""].concat(lstItem);
                                                aaTable.bind();
                                                $("#"+chartID).height(divHeight-aaTable.Height-40);
                                                $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                            }, 100);
                    }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });

            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
            }, 100);
        });
    break;
//四、列管期滿結案後二年內施用毒品再犯率
    case "SelectRecidivesmRateTwoYearMonth":
        $.getRecidivesmRateTwoYearMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
            }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                var lstItem = [];
                var tableBodyRow = [];
                Params.data = [];
                Params.Color=["#0dd3c4","#7030A0"];
                Params.Title = "四、列管期滿結案後二年內施用毒品再犯率";
                if(title && breadcrumbtitle && source && bannerhref ){
                    title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                    bannerhref.innerHTML = "本市及全國藥癮個案再犯率比較分析犯率比較分析";
                    source.innerHTML="來源：衛福部毒癮單窗服務系統";
                    bannerhref.href="KaohsiungAndTaiwanDrugRecidivismRateAnalysis";
                }
                Params.Location = [];
                Params.month=[];
                if(data.length>0){
                    data.forEach(element => {
                            Params.month.push(element["MONTH"]);
                            Params.Location.push(element["Location"]);
                        });
                        Params.month = [...new Set(Params.month)];
                        Params.Location = [...new Set(Params.Location)];
                        Params.ChartType="LineSeries";
                        if(Params.month[Params.month.length-1]<7)
                            Params.ChartType="ColumnSeries";
                        tableBodyRow = Params.Location.map(function(element,index){
                            var newData={};
                            newData.title = element+','+Params.Color[index];
                                data.filter((each)=>{return each.Location==element}).forEach(function(each,ieach){
                                    newData["col"+each.MONTH]=(Math.round(each.Recidivism_Rate*100)/100).toString()+"%";
                                });
                            return newData;
                        });
                        Params.Data = Params.month.map((element,index)=>{
                            var newData={};
                            newData.category = element;
                            lstItem.push(element+"月")
                            data.filter((each)=>{return each.MONTH==element}).forEach(function(each,ieach){
                                newData[each.Location]=each.Recidivism_Rate;
                            });
                            newData.color=Params.Color;
                            return newData;
                        });
                var newChart = amchartfunc(chartID,Params,"Group",Params.ChartType);
                                    setTimeout(() => {
                                                var aaTable = new ChartTable(chartID+"Table");
                                                aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                                aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                                aaTable.dataSource=tableBodyRow;
                                                aaTable.columnSource=[""].concat(lstItem);
                                                aaTable.bind();
                                                $("#"+chartID).height(divHeight-aaTable.Height-40);
                                                $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                            }, 100);
                        }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });

            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
            }, 100);
        });
    break;
    case "SelectRecidivesmRateTwoYearbyYear":
        $.getRecidivesmRateTwoYearbyYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
            }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                Params.data = [];
                Params.Color=["#0dd3c4","#7030A0"];
                Params.Title = "四、列管期滿結案後二年內施用毒品再犯率";
                if(title && breadcrumbtitle && source && bannerhref ){
                    title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                    bannerhref.innerHTML = "本市及全國藥癮個案再犯率比較分析犯率比較分析";
                    source.innerHTML="來源：衛福部毒癮單窗服務系統";
                    bannerhref.href="KaohsiungAndTaiwanDrugRecidivismRateAnalysis";
                }
                Params.Year =[];
                Params.Location = [];
                var lstItem = [];
                var tableBodyRow = [];
                if(data.length>0){
                    data.forEach(element => {
                        Params.Year.push(element["YEAR"]);
                        Params.Location.push(element["Location"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.Location = [...new Set(Params.Location)];
                    Params.Data = Params.Year.map((element,index)=>{
                        var newData={};
                        newData.category = element;
                        lstItem.push(element+"年");
                            data.filter((each)=>{return each.YEAR==element}).forEach(function(each,ieach){
                                newData[each.Location]=Math.round(each.Recidivism_Rate*100)/100;
                            });
                            newData.color=Params.Color;
                        return newData;
                    });
                    tableBodyRow = Params.Location.map(function(element,index){
                        var newData={};
                        newData.title = element+','+Params.Color[index];
                            data.filter((each)=>{return each.Location==element}).forEach(function(each,ieach){
                                newData["col"+each.YEAR]=(Math.round(each.Recidivism_Rate*100)/100).toString();
                            });
                        return newData;
                    });
                    var newChart = amchartfunc(chartID,Params,"LineGroup","LineSeries");
                                        setTimeout(() => {
                                                    var aaTable = new ChartTable(chartID+"Table");
                                                    aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                                    aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                                    aaTable.dataSource=tableBodyRow;
                                                    aaTable.columnSource=[""].concat(lstItem);
                                                    aaTable.bind();
                                                    $("#"+chartID).height(divHeight-aaTable.Height-40);
                                                    $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                                }, 100);
                }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });

            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
            }, 100);
        });
    break;
    }
}
