function getTeenagerDrugCase(id,div){
    const chartID = id + (div.indexOf('popup') > -1 ? "_popup" : "");
    var breadcrumbtitle=document.getElementById("breadcrumbtitle");
    var title=document.getElementById("title");
    var bannerhref = document.getElementById("bannerhref");
    var source  = document.getElementById("source");
    var selectYear = document.getElementById("selectYear");
    var selectMonth = document.getElementById("selectMonth");
    var selectStartYear = document.getElementById("selectStartYear");
    var selectEndYear = document.getElementById("selectEndYear");
    var sYear = moment().format('YYYY')-2; //2019
    var eYear = moment().format('YYYY'); //2021
    var sDate = moment().format('YYYY')+"/01"; //2021/01
    var eDate = moment().format('YYYY/MM'); //2021/12
    var reg = /^\d+$/;
    if(selectYear && selectMonth){
        if(selectYear.value.match(reg) && selectMonth.value.match(reg)){
            sDate =(parseInt(selectYear.value)+1911).toString()+"/01";
            eDate =(parseInt(selectYear.value)+1911).toString() +"/"+ selectMonth.value;
        }
    };
    if(selectStartYear && selectEndYear){
        if(selectStartYear.value.match(reg) && selectEndYear.value.match(reg)){
            sYear = (parseInt(selectStartYear.value)+1911).toString()
            eYear = (parseInt(selectEndYear.value)+1911).toString()
        }
    };
    function CommentText (Params,unit){
        return  "註：" + Params.Year + "年" + Params.month + "月在案人數總計" + Params.Total + unit;
    };
    function getTextCss(font_size) {
        var scale = 1;
        if (font_size < 8)
            scale = font_size / 8;
        else if(font_size>12)
            font_size = 12;
        return {
            "font-size": font_size + 'px',
            "transform-origin": "0",
            "white-space": "nowrap",
            "transform": "scale(" + scale + ")"
        };
    };
    function ChartTable_FitSize(div,offset) {
        const id = $("#"+div+ " div[name='mChart']").attr("id");
        const divHeight = $("#"+div).parents(".main-card").height();
        var Chart = am4core.registry.baseSprites.find(function (chartObj) {
            return chartObj.htmlContainer.id === id;
        });
        var Table = getChartTable(id+"Table");
        if(Table!==undefined){
            const firstLength=$("#"+chartID+"Table").find("tbody").find("tr:nth-child(2)").find("td:first").text()+2;
            $.FitTableOnResize(Table, Chart,firstLength);
            $("#"+id).css("height",(divHeight-Table.Height-offset)+'px');
        }
        else
            $("#"+id).css("height",(divHeight-offset)+'px');
    }
    var chartParentName = ".card-item";
    if(title && breadcrumbtitle && source && bannerhref)
        chartParentName = ".largechart";
    else if(document.getElementsByClassName("content-index").length>0)
        chartParentName = ".main-card"
    switch(id){
//一、性別同期比較
    case"TeenagerDrugGenderByMonth":
        $.getTeenagerDrugGenderByMonth(sDate,eDate,function(data){
            am4core.ready(function () {
                const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
                $("#"+chartID).height(divHeight-40);
                var Params = {};
                var lstItem = ["人數","百分比"];
                Params.ChartType = "Pie2";
                var tableBodyRow = [];
                Params.Data = [];
                Params.fill=["#FF0000","#0033CC"];
                Params.Name = eDate;
                Params.Title = "一、性別統計";
                Params.fonSize=12;
                Params.Total = 0;
                Params.ShowDecimal=true;
                Params.Year = sDate.substring(0,4)-1911;
                Params.Month = eDate.split('/')[1].replace('0','');
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市17歲(含)以下藥癮個案概況分析";
                        bannerhref.href="TeenagerDrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;
                    }
                if(data.length>0){
                    data.forEach((element,index)=>{
                        Params.Data.push({category:element["Gender"]+"性",count:element["Count"],color:Params.fill[index]})
                        Params.Total+=parseInt( element["Count"]);
                    });
                    // data.forEach((element,index)=>{
                    //     tableBodyRow.push({category:element["Gender"],count:element["Person"],percent:Math.round(((element["Person"]/ Params.Total)*100)*100)/100+"%"})
                    //     });
                    // tableBodyRow.reverse();
                    Params.Data.reverse();
                    // document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月三四級講習0人、案管系統0人、在案人數總計"+Params.Total+"人";
                    document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月在案人數總計"+Params.Total+"人";
                    $("#"+chartID+"Text").css("fontSize",Params.fonSize+"px");
                var newChart = amchartfunc(chartID,Params,Params.ChartType);
                                setTimeout(() => {
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"TeenagerDrugGenderByYear":
        $.getTeenagerDrugGenderByYear(sYear,eYear,function(data){
                const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                    if($("#"+div).html()==""){
                        $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                        <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                    }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.ChartType = "LineGroup";
                var tableBodyRow = [];
                Params.Data = [];
                Params.Color=["#0033CC","#FF0000"];
                Params.Year  = [];
                Params.Gender=[];
                Params.Title = "一、性別同期比較";
                Params.fonSize=12;
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市17歲(含)以下藥癮個案概況分析";
                        bannerhref.href="TeenagerDrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;
                    }
                Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach(element => {
                        Params.Year.push(element["Year"]);
                        Params.Gender.push(element["Gender"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.Gender = [...new Set(Params.Gender)];
                    Params.Year.sort();
                    Params.Data =  Params.Year.map(function(item,iItem){
                        var newData={};
                        newData.category=item;
                        data.filter((each)=>{return each.Year==item}).forEach(function(each,ieach){
                            newData[each.Gender]=each.Count;
                        });
                        newData.color=Params.Color;
                        lstItem.push(item+"年");
                        return newData;
                    });
                    tableBodyRow = Params.Gender.map(function(element,index){
                        var newData = {};
                        newData.title =element+"性"+','+Params.Color[index];
                        data.filter((each)=>{return each.Gender==element}).forEach(function(each,ieach){
                            newData["Year"+ieach]=each.Count;
                        });
                        return newData;
                    });
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"LineSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=tableBodyRow;
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                            }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
//二、年齡統計
    case"TeenagerDrugAgeByMonth":
        $.getTeenagerDrugAgeByMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.ChartType = "Column";
                Params.Data = [];
                Params.Title = "二、年齡統計";
                Params.fonSize=12;
                Params.Color="#7030A0";
                Params.Total = 0;
                Params.Year = eDate.substring(0,4)-1911;
                Params.Month = eDate.split('/')[1].replace('0','');
                Params.ColumnsWidth=50;
                var tableBodyRow = {title:Params.Year+"年"+Params.Month+"月"+','+Params.Color};
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市17歲(含)以下藥癮個案概況分析";
                        bannerhref.href="TeenagerDrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;
                    }
                // Params.Title2 = {
                //         title: "單位：人數",
                //         fonSize: Params.fonSize,
                //         color: "#000000",
                //         align: "right",
                //         valign: "top",
                //         dy: 0
                // };
                if(data.length>0){
                    data.forEach((element,index)=>{
                    Params.Data.push({category:element["Entry_Age"],count:element["Count"]})
                    lstItem.push(element["Entry_Age"]+"歲");
                    Params.Total += parseInt(element["Count"]);
                    tableBodyRow["Entry_Age"+(index)] = element["Count"];
                    });
                // document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月三四級講習0人、案管系統0人、在案人數總計"+Params.Total+"人";
                document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月在案人數總計"+Params.Total+"人";
                ChartTable_FitSize("chart2",0);
                var newChart = amchartfunc(chartID,Params,Params.ChartType);
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            const firstLength=[tableBodyRow][0].title.split(',')[0].length+4;
                                            $.FitTable(aaTable, newChart, [""].concat(lstItem), [tableBodyRow],firstLength);
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"TeenagerDrugAgeByYear":
        $.getTeenagerDrugAgeByYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                var lstItem = [];
                Params.ChartType = "LineGroup";
                var tableBodyRow = [];
                Params.Data = [];
                Params.Color=["#0033CC","#FF0000","#0dd3c4"];
                Params.Year  = [];
                Params.Age_Class=[];
                //Params.Name = moment(sDate).format("YYYY/MM");
                Params.Title = "二、年齡統計";
                Params.fonSize=12;
                Params.ColumnsWidth=50;
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市17歲(含)以下藥癮個案概況分析";
                        bannerhref.href="TeenagerDrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;
                    }
                Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach(element => {
                        Params.Year.push(element["Year"]);
                        Params.Age_Class.push(element["Entry_Age"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.Age_Class = [...new Set(Params.Age_Class)];
                    Params.Year.sort();
                    Params.Age_Class.sort(function(a,b) { return a.replace('未','0').localeCompare(b.replace('未','0'), "zh-Hant"); });
                    Params.Data = Params.Age_Class.map(function(element,index){
                        var newData = {};
                        newData.category = element+"歲";
                        lstItem.push(element+"歲");
                        Params.Year.map(year=>{
                            var findCount=data.find((each)=>{return each.Entry_Age==element && each.Year==year});
                            if(findCount!==undefined)
                                newData[year+"年"]= findCount.Count;
                            else
                                newData[year+"年"]= 0;
                            });
                        newData.color=  Params.Color;
                        return newData;
                    });
                    tableBodyRow =  Params.Year.map(function(element,index){
                        var newData={};
                        newData.title=element+'年,'+Params.Color[index];
                        Params.Age_Class.map(age=>{
                            var findCount=data.find((each)=>{return each.Entry_Age==age && each.Year==element})
                            if(findCount!==undefined)
                                newData["Y"+age]= findCount.Count;
                            else
                                newData["Y"+age]= '0';
                            });
                        return newData;
                    });
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"LineSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            const firstLength=tableBodyRow[0].title.split(',')[0].length+2;
                                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow,firstLength);
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                            }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
//三、藥物濫用類型統計
    case"TeenagerDrugAbuseTypeByMonth":
        $.getTeenagerDrugAbuseTypeByMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.ChartType = "StackedColumn";
                Params.MIX=[];
                Params.Level=[];
                Params.Data = [];
                Params.Name = eDate;
                Params.Title = "三、藥物濫用類型統計";
                Params.fonSize=12;
                Params.Color = ["#0031CC", "#FF0000", "#0dd3c4", "#996633", "#7030a0"];
                Params.Total = 0;
                Params.Stacked = true;
                Params.ColumnsWidth=50;
                Params.Year = sDate.substring(0,4)-1911;
                Params.Month = eDate.split('/')[1].replace('0','');
                var tableBodyRow = [];
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市17歲(含)以下藥癮個案概況分析";
                        bannerhref.href="TeenagerDrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;
                    }
                Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach((element,index)=>{
                    Params.Level.push(element["LEVEL"]);
                    Params.MIX.push(element["MIX"]);
                    Params.Total+=parseInt(element["Count"]);
                    });

                    Params.Level = [...new Set(Params.Level)];
                    Params.MIX = [...new Set(Params.MIX)];
                    Params.Data = Params.Level.map((element,index)=>{
                        var newData={};
                        lstItem.push(element);
                        newData.category = element;
                        Params.MIX.forEach(mix=>{
                            var findCount = data.find(each=>{return each.MIX==mix && each.LEVEL == element});
                            if(findCount!==undefined)
                                newData[mix] = findCount.Count;
                            else
                                newData[mix] = 0;
                        })
                        newData.color= Params.Color;
                        return newData;
                    })
                    tableBodyRow = Params.MIX.map(function(mix,index){
                        var newData = {};
                        newData.title =mix;
                        Params.Level.forEach((level,index)=>{
                            var findCount = data.find(each=>{return each.MIX==mix && each.LEVEL == level});
                                if(findCount==undefined)
                                    if(level=="其他"&&mix=="混用")
                                        newData[level]='';
                                    else
                                        newData[level]='0';
                                else
                                    newData[level]=findCount.Count;
                        })
                        return newData;
                    })
                    document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月在案人數總計"+Params.Total+"人";

                var newChart = amchartfunc(chartID,Params,Params.ChartType,"ColumnSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=tableBodyRow;
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"TeenagerDrugAbuseTypeByYear":
        $.getTeenagerDrugAbuseTypeByYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.ChartType = "LineGroup";
                Params.Type=[];
                Params.Level=[];
                Params.Data = [];
                Params.fill=["#FFC000","#A5A5A5"];
                Params.Name = eDate;
                Params.Title = "三、藥物濫用類型統計";
                Params.fonSize=12;
                Params.Color = ["#0031CC", "#FF0000", "#0dd3c4", "#996633", "#7030a0"];
                Params.Total = 0;
                Params.Stacked = true;
                Params.Year =[];Params.LEVEL=[];
                Params.ColumnsWidth=50;
                Params.month = sDate.split('/')[1].replace('0','');
                var tableBodyRow = [];
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市17歲(含)以下藥癮個案概況分析";
                        bannerhref.href="TeenagerDrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;
                        $("#"+chartID+"Table").css("margin-top","-16px");
                    }
                Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach(element => {
                        Params.Year.push(element["YEAR"]);
                        Params.LEVEL.push(element["LEVEL"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.LEVEL = [...new Set(Params.LEVEL)];
                    Params.Year.sort();
                    Params.Data =  Params.Year.map(function(item,iItem){
                        var newData={};
                        newData.category=item;
                        lstItem.push(item+"年");
                        data.filter((each)=>{return each.YEAR==item}).forEach(function(each,ieach){
                            newData[each.LEVEL]=each.Count;
                        });
                        newData.color=  Params.Color;
                        return newData;
                    });
                    tableBodyRow = Params.LEVEL.map(function(element,index){
                        var newData = {};
                        newData.title =element+','+Params.Color[index];
                        data.filter((each)=>{return each.LEVEL==element}).forEach(function(each,ieach){
                            newData["Year"+ieach]=each.Count;
                        });
                        return newData;
                    });
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"LineSeries");
                                setTimeout(() => {
                                        var aaTable = new ChartTable(chartID+"Table");
                                        const firstLength=tableBodyRow[0].title.split(',')[0].length+2;
                                        $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow,firstLength);
                                        $("#"+chartID).height(divHeight-aaTable.Height-40);
                                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
//四、年齡與類型交叉分析
    case"TeenagerDrugAgeTypeByMonth":
        $.getTeenagerDrugAgeTypeByMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                var lstItem = [];
                Params.ChartType = "GroupStackedColumnBar";
                Params.Level=[];
                Params.Age_Class=[];
                Params.Type=[];
                Params.Entry_Age=[];
                Params.Data = [];
                Params.Name = eDate;
                Params.Title = "四、年齡與類型交叉分析";
                Params.fonSize=12;
                Params.Color = ["#0031CC", "#FF0000", "#0dd3c4", "#996633", "#7030a0"];
                Params.TitleColor = ["#b4c7e7","#0031CC","#ffb19f", "#FF0000","#00ffcc", "#0dd3c4","#cc9900", "#996633", "#7030a0"];
                Params.Total = 0;
                Params.Year = sDate.substring(0,4)-1911;
                Params.Month = eDate.split('/')[1].replace('0','');
                Params.xAxis = ["一級毒品","二級毒品","三級毒品","四級毒品","其他"];
                var tableBodyRow =[];
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市17歲(含)以下藥癮個案概況分析";
                        bannerhref.href="TeenagerDrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;
                        $("#"+chartID+"Table").css("margin-top","-16px");
                    }
                Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach((element,index)=>{
                    Params.Level.push(element["LEVEL"]);
                    Params.Entry_Age.push(element["Entry_Age"]);
                    Params.Type.push(element["MIX"]);
                    Params.Total+=parseInt(element["count"]);
                    });
                    Params.Level = [...new Set(Params.Level)];
                    Params.Entry_Age = [...new Set(Params.Entry_Age)];
                    Params.Type = [...new Set(Params.Type)];
                    Params.Entry_Age.sort(function(a,b) { return a.replace('未','0').localeCompare(b.replace('未','0'), "zh-Hant"); });
                    const ChartTableData = StackedNoSortChartTableData(data,"LEVEL","Entry_Age","MIX",  Params.Color);
                    Params.Data = ChartTableData.lstData.sort(function(a,b) { return a.category.replace('未','0').localeCompare(b.category.replace('未','0'), "zh-Hant"); });
                    Params.Entry_Age.forEach(each=>{
                        lstItem.push(each+"歲");
                    })
                    Params.Type.sort(function (a, b) {
                        return a.localeCompare(b, "zh-Hant");});
                    var sumTitle = [];
                    Params.Level.forEach(each=>{
                        Params.Type.forEach(type=>{
                            if(each=="其他" && type=="單一")
                                sumTitle.push(each);
                            else if(each!="其他")
                                sumTitle.push(each+"-"+type);
                        })
                    })
                    tableBodyRow = sumTitle.map((title,index)=>{
                        var newData={};
                        newData.title  = title+','+Params.TitleColor[index];
                        Params.Entry_Age.forEach(age=>{
                            var findCount = data.find(each=>{return each.MIX == title.split("-")[1] && each.LEVEL == title.split("-")[0] && each.Entry_Age==age});
                            if(findCount!==undefined)
                                newData["o"+age] = findCount.count;
                            else
                                newData["o"+age] = '0'
                        })
                        return newData;
                    })
                // document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月三四級講習0人、案管系統0人、在案人數總計"+Params.Total+"人";
                document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月在案人數總計"+Params.Total+"人";
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"ColumnSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            const firstLength=tableBodyRow[0].title.split(',')[0].length+10;
                                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow,firstLength);
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"TeenagerDrugAgeTypeByYear":
        $.getTeenagerDrugAgeTypeByYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                var lstItem = [];
                Params.ChartType = "Group";
                Params.Level=[];
                Params.Age_Class=[];
                Params.Type=[];
                Params.Data = [];
                Params.fill=["#FFC000","#A5A5A5"];
                Params.Name = eDate;
                Params.Title = "四、年齡層與類型交叉分析";
                Params.fonSize=12;
                Params.Color = ["#0031CC", "#FF0000", "#0dd3c4", "#996633", "#7030a0"];
                Params.Total = 0;
                Params.Year =[];
                Params.month = sDate.split('/')[1].replace('0','');
                Params.xAxis = ["一級毒品","二級毒品","三級毒品","四級毒品","其他"];
                var tableBodyRow = [];
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市17歲(含)以下藥癮個案概況分析";
                        bannerhref.href="TeenagerDrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;
                    }
                Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach((element,index)=>{
                    Params.Level.push(element["LEVEL"]);
                    Params.Age_Class.push(element["Entry_Age"]);
                    Params.Year.push(element["YEAR"]);
                    });
                    Params.Level = [...new Set(Params.Level)];
                    Params.Age_Class = [...new Set(Params.Age_Class)];
                    Params.Year = [...new Set(Params.Year)];
                    Params.Year.sort();
                    Params.Level.sort(function(a,b){return a.localeCompare(b,"zh-Hant")});
                    const ChartTableData = StackedNoSortChartTableData(data,"YEAR","Entry_Age","LEVEL", Params.Color);
                    Params.Data = ChartTableData.lstData;
                    Params.Data.sort(function(a,b){return a.category.replace("未","0").localeCompare(b.category.replace("未","0"),"zh-Hant")})

                    lstItem =  Params.Age_Class.map((each,index)=>{
                        var newData={};
                        newData.title = each+"歲";
                        newData.category = [];
                        Params.Year.map(each=>{
                            newData.category.push(each);
                        })
                        return newData;
                    })
                    tableBodyRow = Params.Level.map(function(element,lelement){
                        var newData = {};
                        newData.title =element+','+Params.Color[lelement];
                        Params.Age_Class.forEach((eachage,ieach)=>{
                            Params.Year.forEach((eachYear,index)=>{
                                newData["value"+lelement+index+ieach] = data.filter(each=>{return each.LEVEL==element && each.YEAR == eachYear && each.Entry_Age==eachage})[0]
                                if(newData["value"+lelement+index+ieach] ==undefined)
                                    if(element == "混用" && eachLevel == "其他")
                                        newData["value"+lelement+index+ieach] =' ';
                                    else
                                        newData["value"+lelement+index+ieach] ='0';
                                else
                                    newData["value"+lelement+index+ieach] = newData["value"+lelement+index+ieach] .count;
                            })
                            return newData;
                        })
                        return newData;
                    })
                    ChartTable_FitSize("chartY4",0);
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"ColumnSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=tableBodyRow;
                                            aaTable.spicial=true;
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            const firstLength=tableBodyRow[0].title.split(',')[0].length+2;
                                            if(aaTable.fontSize>0){
                                                const firstColumnWidth = aaTable.fontSize*(firstLength);
                                                const chartWidth = newChart.contentWidth-newChart.rightAxesContainer.contentWidth;
                                                if(firstColumnWidth>newChart.leftAxesContainer.contentWidth){
                                                    newChart.leftAxesContainer.width = firstColumnWidth;
                                                    aaTable.headerWidth = firstColumnWidth+14;
                                                    aaTable.bodyWidth = chartWidth-firstColumnWidth+1;
                                                    aaTable.setWidth();
                                                }
                                            }
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
//五、個案區域(戶籍地)未校正數
    case"TeenagerDrugAreaByMonth":
    $.getTeenagerDrugAreaByMonth(sDate,eDate,function(data){
        const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
        am4core.ready(function () {
            //am4core.addLicense("{{config('custom.ChartsPN')}}");
            var Params = {};
            var lstItem = [];
            Params.Data = [];
            Params.Color="#7030A0";
            var tableBodyRow = {title:"列管人數"+','+Params.Color};
            Params.Title = "五、個案區域(戶籍地)未校正數";
            Params.Year = sDate.substring(0,4)-1911;
            Params.month = sDate.split('/')[1].replace('0','');
            Params.Total=0;
            Params.FontSize=12;
            Params.ColumnsWidth=50;
            if(title && breadcrumbtitle && source && bannerhref){
                    title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                    bannerhref.innerHTML = "本市17歲(含)以下藥癮個案概況分析";
                    bannerhref.href="TeenagerDrugCase";
                    source.innerHTML="來源：高雄市政府毒品防制局";
                    Params.FontSize=15+"px";
                }
            if(data.length>0){
                data.forEach((element,index)=>{
                Params.Data.push({category:element["Area_Name"],count:element["Count"]})
                lstItem.push(element["Area_Name"]);
                tableBodyRow["Area_Name"+(index)] = element["Count"];
                Params.Total+=parseInt(element["Count"]);
                });
                var TopThree= data.filter((each,index)=>{return index<3}).map(each=>{return each.Area_Name.substring(0,2)}).join("、");
                // var TopThree = data[0].Area_Name.substring(0,2) +"、" + data[1].Area_Name.substring(0,2) +"、" + data[2].Area_Name.substring(0,2);
                document.getElementById(chartID+"Text").innerHTML ="註：" + Params.Year + "年" + Params.month + "月在案人數總計" + Params.Total + "人，前三熱區："+TopThree;

                var newChart = amchartfunc(chartID,Params,"Column");

                setTimeout(() => {
                            var aaTable = new ChartTable(chartID+"Table");
                            const firstLength=[tableBodyRow][0].title.split(',')[0].length+2;
                            $.FitTable(aaTable, newChart, [""].concat(lstItem), [tableBodyRow],firstLength,true);
                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                        }, 100);
            };


        }); // end am4core.ready()
    })
    $(window).on('resize',function(){
        var chart = am4core.registry.baseSprites.find(function(chartObj) {
            return chartObj.htmlContainer.id === chartID;
        });
        setTimeout(() => {
            var aaTable = getChartTable(chartID+"Table");
            aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
            aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
            aaTable.setWidth();
            $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
        }, 100);
    });
    break;
    case"TeenagerDrugAreabyYear":
        $.getTeenagerDrugAreabyYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                Params = {};
                Params.ChartType = "GroupColumnBar";
                Params.Title = "五、個案區域(戶籍地)未校正數";
                Params.fontSize = 10;
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市17歲(含)以下藥癮個案概況分析";
                        bannerhref.href="TeenagerDrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fontSize = 15;
                        $("#"+chartID+"Table").css("margin-top","-16px");
                }
                Params.TitleColor = "#00B48B";
                Params.Color=["#0031CC","#833C0B","#008000"];
                Params.FontSize = 10;
                Params.categoryAxisVisible = false;
                Params.Location = [];
                Params.Year=[];
                var lstItem = [];
                var tableBodyRow = [];
                data.forEach(element => {
                    Params.Year.push(element["YEAR"]);
                    Params.Location.push(element["Area_Name"]);
                });
                Params.Year = [...new Set(Params.Year)];
                Params.Location = [...new Set(Params.Location)];
                Params.Year.sort();
                lstItem=Params.Location;
                Params.Data =  Params.Location.map(function(location,iItem){
                    var newData={};
                    newData.category=location;
                    data.filter((each)=>{return each.Area_Name==location}).forEach(function(each,ieach){
                        newData[each.YEAR]=each.Count;
                    });
                    newData.color=Params.Color;
                    return newData;
                });
                tableBodyRow =  Params.Year.map(function(year,iItem){
                    var newData={};
                    newData.title = year+"年"+','+Params.Color[iItem];
                    Params.Location.forEach((eachL,ieachL)=>{
                        newData["value"+(ieachL)] = data.filter((each)=>{return each.YEAR==year && each.Area_Name==eachL})[0];
                        if(newData["value"+(ieachL)]==undefined)
                            newData["value"+(ieachL)]='0';
                        else
                            newData["value"+(ieachL)]= newData["value"+(ieachL)].Count;
                    })
                    return newData;
                });

                document.getElementById(chartID+"Text").innerHTML ="前三熱區："+Params.Year.map((eachYear,index)=>{
                    var TopThree=data.filter(each=>{
                        return each.YEAR==eachYear&&each.NUM<4
                    }).map(eachLocation=>{return eachLocation.Area_Name.substring(0,2)}).join("、");
                    return eachYear+"年："+TopThree;
                }).join("<br>")
                // tableBodyRow.sort(function(a,b){return a.replace("t","0").localeCompare(b.replace("t","0"))});

                var newChart = amchartfunc(chartID,Params,"GroupColumnBar");
                setTimeout(() => {
                            var aaTable = new ChartTable(chartID+"Table");
                            const firstLength=tableBodyRow[0].title.split(',')[0].length+2;
                                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow,firstLength,true);
                            $("#"+chartID).height(divHeight-aaTable.Height-65);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                        }, 100);
            })
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
//六、個案區域(戶籍地)校正數
    case"TeenagerDrugCorrectionAreaByMonth":
        $.getTeenagerDrugCorrectionAreaByMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.Data = [];
                Params.Color="#7030A0";
                var tableBodyRow = {title:"校正數"+','+Params.Color};
                Params.Title = "六、個案區域(戶籍地)校正數";
                Params.Year = sDate.substring(0,4)-1911;
                Params.Month = sDate.split('/')[1].replace('0','');
                Params.Total=0;
                Params.ColumnsWidth=50;
                Params.fontSzie = 10;
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市17歲(含)以下藥癮個案概況分析";
                        bannerhref.href="TeenagerDrugCase";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fontSzie=15;
                    }
                if(data.length>0){

                    data.forEach((element,index)=>{
                    Params.Data.push({category:element["Location"],count:element["Person"]})
                    lstItem.push(element["Location"]);
                    Params.Total += parseInt(element["Person"])
                    tableBodyRow["Location"+(index)] = element["Person"];
                    });
                    var TopThree = data[0].Location.substring(0,2) +"、" + data[1].Location.substring(0,2) +"、" + data[2].Location.substring(0,2);
                    document.getElementById(chartID+"Text").innerHTML ="註：1."+Params.Year+"年"+Params.Month+"月在案人數總計"+Params.Total+"人，前三熱區："+TopThree+
                    "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.【校正數為(該區列管數/該區人口數)*100,000】";
                    }

                    var newChart = amchartfunc(chartID,Params,"Column");
                        setTimeout(() => {
                                    var aaTable = new ChartTable(chartID+"Table");
                                    aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                    aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                    aaTable.dataSource=[tableBodyRow];
                                    aaTable.columnSource=[""].concat(lstItem);
                                    aaTable.vertical = true;
                                    aaTable.bind();
                                    $("#"+chartID).height(divHeight-aaTable.Height-50);
                                    $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                }, 100);
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"TeenagerDrugCorrectionAreaByYear":
        $.getTeenagerDrugCorrectionAreaByYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                Params = {};
                Params.ChartType = "GroupColumnBar";
                Params.Title = "六、個案區域(戶籍地)校正數";
                Params.fontSize=10;
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市17歲(含)以下藥癮個案概況分析";
                        bannerhref.href="TaiwanDrugAnalysis";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fontSize=15;

                }
                Params.TitleColor = "#00B48B";
                Params.Color=["#0031CC","#833C0B","#008000"];
                Params.FontSize = 10;
                Params.categoryAxisVisible = false;
                Params.Location = [];
                Params.Year=[];
                var lstItem = [];
                var tableBodyRow = [];
                data.forEach(element => {
                    Params.Year.push(element["CHINA_Year"]);
                    Params.Location.push(element["Location"]);
                });
                Params.Year = [...new Set(Params.Year)];
                Params.Location = [...new Set(Params.Location)];
                Params.Year.sort();
                lstItem=Params.Location;
                Params.Data =  Params.Location.map(function(location,iItem){
                    var newData={};
                    newData.category=location;
                    data.filter((each)=>{return each.Location==location}).forEach(function(each,ieach){
                        newData[each.CHINA_Year]=each.Person;
                    });
                    newData.color=Params.Color;
                    return newData;
                });
                tableBodyRow =  Params.Year.map(function(year,iItem){
                    var newData={};
                    newData.title = year+"年"+','+Params.Color[iItem];
                    data.filter((each)=>{return each.CHINA_Year==year}).forEach(function(each,ieach){
                        newData["Location"+(ieach)]=each.Person;
                    });
                    return newData;
                });
                //var TopThree = data[0].Location.substring(0,2) +"、" + data[1].Location.substring(0,2) +"、" + data[2].Location.substring(0,2);
                    document.getElementById(chartID+"Text").innerHTML ="註：1.前三熱區："+Params.Year.map((eachYear,index)=>{
                        var TopThree=data.filter(each=>{
                            return each.CHINA_Year==eachYear&&each.NUM<4
                        }).map(eachLocation=>{return eachLocation.Location.substring(0,2)}).join("、");
                        return eachYear+"年："+TopThree;
                    }).join("、")+"<br>2. 校正數為(該區列管數/該區人口數)*100,000】";

                var newChart = amchartfunc(chartID,Params,"GroupColumnBar");
                setTimeout(() => {
                            var aaTable = new ChartTable(chartID+"Table");
                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                            aaTable.dataSource=tableBodyRow;
                            aaTable.columnSource=[""].concat(lstItem);
                            aaTable.vertical = true;
                            aaTable.bind();
                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                        }, 100);
            })
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id ===chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.vertical = true;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
}};
