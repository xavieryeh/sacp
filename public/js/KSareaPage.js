function KSareaPage(self){
    init();

    var Cases;
    var selectPage=1;
    var TotalCount;

    function init(){
        $.Log("主畫面","行政區"+self.areaName+"-"+self.villageName+"個案管理載入");
        Cases = self.Cases;
        TotalCount = Cases.length;
        setPageList(1);
        searchData();
    }
    //選取頁數事件
    self.changePage = function(page) {
        if(page=="pre"){
            if(selectPage==1)
                return;
            else{
                setPageList(parseInt(selectPage)-1);
            }
        }
        else if(page=="next"){
            if(selectPage==Math.ceil(TotalCount/parseInt($("#selShowCount option:selected")[0].value)))
                return;
            else{
                setPageList(parseInt(selectPage)+1);
            }
        }
        else{
            setPageList(parseInt(page));
        }
        searchData();
    }

    //取得頁數並產生頁數
    function setPageList(currentPage){
        selectPage=currentPage;
        var PageList = ['<li class="page-item"><a id="btnPrePage" class="page-link" onclick="kSareaPage.changePage(\'pre\')">上一項</a></li>'];
        for(var i=1;i<Math.ceil(TotalCount/parseInt($("#selShowCount option:selected")[0].value))+1;++i){
            PageList.push('<li class="page-item'+(i==currentPage?' active':'')+'" aria-current="page"><a class="page-link" onclick="kSareaPage.changePage('+i.toString()+')">'+i.toString()+'</a></li>');
        }
        PageList.push('<li class="page-item"><a id="btnNextPage" class="page-link" onclick="kSareaPage.changePage(\'next\')">下一項</a></li>');
        $('#ulPageList').html(PageList.join(''));
    }

    $("#selShowCount").on('change', function() {
        setPageList(1);
        searchData();
    });

    $("#btnSearch").on('click', function() {
        searchData();
        setPageList(1);
    });

    //個案管理資料查詢
    function searchData(){
        // iAgeStart iAgeEnd iLevel iKeyWord $("#selShowCount option:selected")[0].value selectPage
        const ageStart = parseInt($("#iAgeStart").val().trim()==""?"0":$("#iAgeStart").val().trim());
        const ageEnd = parseInt($("#iAgeEnd").val().trim()==""?"200":$("#iAgeEnd").val().trim());
        const level = $("#iLevel option:selected")[0].value;
        const keyWord = $("#iKeyWord").val().toLowerCase().trim();
        const showCount = parseInt($("#selShowCount option:selected")[0].value);
        $.Log("主畫面","行政區"+self.areaName+"-"+self.villageName.trim()+"個案管理搜尋，條件範圍:"+ageStart+"-"+ageEnd+"|"+$("#iLevel option:selected")[0].text+"|"+keyWord);
        const filterData = Cases.filter(f=>{return f.File_Age>=ageStart&&f.File_Age<=ageEnd&& (f.Level==level||level=='-1')
        &&(f.Case_ID.toLowerCase().indexOf(keyWord)>-1||f.SourceNo.toLowerCase().indexOf(keyWord)>-1||f.Gender.toLowerCase().indexOf(keyWord)>-1
        ||f.File_Age.toLowerCase().indexOf(keyWord)>-1||f.Level.toLowerCase().indexOf(keyWord)>-1||f.States.toLowerCase().indexOf(keyWord)>-1
        ||f.CaseType.toLowerCase().indexOf(keyWord)>-1||f.Manage_Type.toLowerCase().indexOf(keyWord)>-1)});
        const filterDataByPage = filterData.filter((f,i)=>{return (i+1)>=((selectPage-1)*showCount+1)&&(i+1)<=(selectPage*showCount)});

        const areaNo = self.areaNo;
        const route = self.route;
        var rowData = [];
        filterDataByPage.forEach(eachData=>{
            rowData.push('\
            <tr class="js-KSarea_page_ownTeacher">\
                <td date-title="個案(去識別化)" scope="row"><a\
                        style="text-decoration:underline;color:#0080FF"\
                        href="'+route+'?areaNo='+areaNo+'_'+eachData.Case_ID+'">'+eachData.Case_ID+'</a>\
                </td>\
                <td date-title="來源別">'+eachData.SourceNo+'</td>\
                <td date-title="性別">'+eachData.Gender+'</td>\
                <td date-title="年齡">'+eachData.File_Age+'</td>\
                <td date-title="毒品級數">'+eachData.Level+'</td>\
                <td date-title="列管(Y/N)">'+eachData.States+'</td>\
                <td date-title="初案/再案(1/2)">'+eachData.CaseType+'</td>\
                <td date-title="類型">'+eachData.Manage_Type+'</td>\
            </tr>');
        });
        $("#tBody").html(rowData.join(""));
        TotalCount = filterData.length;
        $("#sTotalCount").text(TotalCount.toString());
    }

    $(window).on('resize', function () {
        setTimeout(() => {
            ChartTable_FitSize("chart1",30);
            ChartTable_FitSize("chart2",30);
            ChartTable_FitSize("chart3",0);
            ChartTable_FitSize("chart4",0);
            ChartTable_FitSize("chart5",10);
            ChartTable_FitSize("chart6",10);
        }, 200);
    });


    function ChartTable_FitSize(div,offset) {
        const id = $("#"+div+ " div[name='mChart']").attr("id");
        const divHeight = $("#"+div).parents(".main-card").height();
        var Chart = am4core.registry.baseSprites.find(function (chartObj) {
            return chartObj.htmlContainer.id === id;
        });
        var Table = getChartTable(id+"Table");
        if(Table!==undefined){
            const firstLength=$("#"+id+"Table").find("tbody").find("tr:nth-child(2)").find("td:first").text()+2;
            $.FitTableOnResize(Table, Chart,firstLength);
            $("#"+id).css("height",(divHeight-Table.Height-offset)+'px');
        }
        else
            $("#"+id).css("height",(divHeight-offset)+'px');
    }

    am4core.useTheme(am4themes_animated);
    am4core.useTheme(am4themes_kelly);
    am4core.addLicense(self.ChartsPN);
    const chartParentName = ".main-card";
    // 1.該里個案年齡分佈
    self.makeCADChart = function(div){
        const divHeight = $("#"+div).parents(chartParentName).height()<300?300:$("#"+div).parent().height()-(div.indexOf('popup')>-1?80:0);
        const id = div+"_CAD";
        if($("#"+div).html()==""){
            $("#"+div).html('<div name="mChart" id="'+id+'" style="height:'+divHeight+'px;width:100%;"></div>\
            <div id="'+id+'Table" style="margin-top:-46px"></div>');
        }
        am4core.ready(function () {
            // Create chart instance
            var chart = am4core.registry.baseSprites.find(function (chartObj) {
                return chartObj.htmlContainer.id === id;
            });
            $.PostAJAX("KSarea_caseAgeDistributed", {villageName:self.villageName}, response => {
                const ChartTableSource = $.ChartTableData(response, "Year", "category", ["#23FD35"]);
                if (chart === undefined) {
                    Params = {};
                    Params.ChartType = "GroupColumnBar";
                    Params.Data = ChartTableSource.lstData;
                    Params.FontSize = 10;
                    Params.categoryAxisVisible = false;
                    Params.leftAxesWidth = div.indexOf('popup')>-1?100:undefined;

                    var newChart = MakeAMChart(id, Params);
                    setTimeout(() => {
                        var Table = new ChartTable(id+"Table");
                        const firstLength=ChartTableSource.TableBodyRow[0][0].split(',')[0].length+5;
                        $.FitTable(Table, newChart, [""].concat(ChartTableSource.lstCategory), ChartTableSource.TableBodyRow,firstLength);
                        // $("#"+id).height(divHeight-Table.Height-30);
                    }, 100);
                } else {
                    setTimeout(() => {
                        var Table = getChartTable(id+"Table");
                        Table.dataSource = ChartTableSource.TableBodyRow;
                        Table.columnSource = [""].concat(ChartTableSource.lstCategory);
                        Table.bind();
                    }, 100);
                    chart.data = ChartTableSource.lstData;
                    chart.invalidateRawData();
                }
            });
        }); // end am4core.ready()
    }

    // 2.各級毒品人數統計
    self.makeDSChart = function(div){
        const divHeight = $("#"+div).parents(chartParentName).height()<300?300:$("#"+div).parent().height()-(div.indexOf('popup')>-1?80:0);
        const id = div+"_DS";
        if($("#"+div).html()==""){
            $("#"+div).html('<div name="mChart" id="'+id+'" style="height:'+divHeight+'px;width:100%;"></div>');
        }
        am4core.ready(function () {
            // Create chart instance
            var chart = am4core.registry.baseSprites.find(function (chartObj) {
                return chartObj.htmlContainer.id === id;
            });
            $.PostAJAX("KSarea_drugStatistics", {villageName:self.villageName}, response => {
                const ChartTableSource = $.ChartTableData(response, "Year", "category", ["#54D35F","#1C9B9C","#FFC000","#07478E","#E98440","#C1CEE3","#A3CE62","#90A2D9"]);
                if (chart === undefined) {
                    Params = {};
                    Params.ChartType = "GroupBar";
                    Params.Data = ChartTableSource.lstData;
                    Params.FontSize = 10;
                    Params.categoryAxisVisible = false;
                    var newChart = MakeAMChart(id, Params);
                } else {
                    chart.data = ChartTableSource.lstData;
                    chart.invalidateRawData();
                }
            });
        }); // end am4core.ready()
    }

    // 3.年齡與類型交叉分析
    self.makeATCAChart = function(div){
        const divHeight = $("#"+div).parents(chartParentName).height()<300?250:$("#"+div).parent().height()-(div.indexOf('popup')>-1?80:0);
        const id = div+"_ATCA";
        $("#"+div).css("height",divHeight);
        if($("#"+div).html()==""){
            $("#"+div).html('<div name="mChart" id="'+id+'" style="height:'+(divHeight-50)+'px;width:100%;"></div>\
            <div id="'+id+'Table" style="margin-top:-46px"></div>');
        }
        am4core.ready(function () {
            // Create chart instance
            var chart = am4core.registry.baseSprites.find(function (chartObj) {
                return chartObj.htmlContainer.id === id;
            });
            $.PostAJAX("KSarea_ageTypeCrossAnalysis", {villageName:self.villageName}, response => {
                const ChartTableSource = $.ChartTableData(response, "Year", "category", ["#0848DF","#D4071C","#23FD35","#E98440","#7F55A3"]);
                if (chart === undefined) {
                    Params = {};
                    Params.ChartType = "GroupColumnBar";
                    Params.Data = ChartTableSource.lstData;
                    Params.FontSize = 10;
                    Params.categoryAxisVisible = false;
                    // Params.leftAxesWidth = div.indexOf('popup')>-1?100:80;

                    var newChart = MakeAMChart(id, Params);
                    setTimeout(() => {
                        var Table = new ChartTable(id+"Table");
                        const firstLength=ChartTableSource.TableBodyRow[0][0].split(',')[0].length+2;
                        $.FitTable(Table, newChart, [""].concat(ChartTableSource.lstCategory), ChartTableSource.TableBodyRow,firstLength);
                    }, 100);
                } else {
                    setTimeout(() => {
                        var Table = getChartTable(id+"Table");
                        Table.dataSource = ChartTableSource.TableBodyRow;
                        Table.columnSource = [""].concat(ChartTableSource.lstCategory);
                        Table.bind();
                    }, 100);
                    chart.data = ChartTableSource.lstData;
                    chart.invalidateRawData();
                }
            });
        }); // end am4core.ready()
    }

    // 4.性別統計分析
    self.makeGSAChart = function(div){
        const divHeight = $("#"+div).parents(chartParentName).height()<300?270:$("#"+div).parent().height()-(div.indexOf('popup')>-1?80:0);
        const id = div+"_GSA";
        $("#"+div).css("height",divHeight);
        if($("#"+div).html()==""){
            $("#"+div).html('<div name="mChart" id="'+id+'" style="height:'+(divHeight-40)+'px;width:100%;"></div>\
            <div id="'+id+'Table" style="margin-top:-46px"></div>');
        }
        am4core.ready(function () {
            // Create chart instance
            var chart = am4core.registry.baseSprites.find(function (chartObj) {
                return chartObj.htmlContainer.id === id;
            });
            $.PostAJAX("KSarea_genderStatisticalAnalysis", {villageName:self.villageName,startDate:'2020'}, response => {
                const ChartTableSource = $.ChartTableData(response, "Gender", "category", ["#0848DF","#E98440"]);
                if (chart === undefined) {
                    Params = {};
                    Params.ChartType = "GroupColumnBar";
                    Params.Data = ChartTableSource.lstData;
                    Params.FontSize = 10;
                    Params.categoryAxisVisible = false;
                    Params.leftAxesWidth = div.indexOf('popup')>-1?100:80;
                    Params.Title2 = {
                        title: "單位：人數",
                        fonSize: 12,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                    };
                    var newChart = MakeAMChart(id, Params);
                    setTimeout(() => {
                        var Table = new ChartTable(id+"Table");
                        Table.headerWidth = newChart.leftAxesContainer.contentWidth + 14;
                        Table.bodyWidth = newChart.plotContainer.contentWidth + 1;
                        Table.dataSource = ChartTableSource.TableBodyRow;
                        Table.columnSource = [""].concat(ChartTableSource.lstCategory);
                        Table.bind();
                    }, 100);
                } else {
                    setTimeout(() => {
                        var Table = getChartTable(id+"Table");
                        Table.dataSource = ChartTableSource.TableBodyRow;
                        Table.columnSource = [""].concat(ChartTableSource.lstCategory);
                        Table.bind();
                    }, 100);
                    chart.data = ChartTableSource.lstData;
                    chart.invalidateRawData();
                }
            });
        }); // end am4core.ready()
    }

    // 5.收案來源分析
    self.makeSAAChart = function(div){
        const divHeight = $("#"+div).parents(chartParentName).height()<300?300:$("#"+div).parent().height()-(div.indexOf('popup')>-1?0:0);
        const id = div+"_SAA";
        $("#"+div).css("height",divHeight);
        if($("#"+div).html()==""){
            $("#"+div).html('<div name="mChart" id="'+id+'" style="height:'+(divHeight-20)+'px;width:100%;"></div>');
        }
        am4core.ready(function () {
            // Create chart instance
            var chart = am4core.registry.baseSprites.find(function (chartObj) {
                return chartObj.htmlContainer.id === id;
            });
            $.PostAJAX("KSarea_sourceAcceptanceAnalysis", {villageName:self.villageName}, response => {
                // const ChartTableSource = $.ChartTableData(response, "Gender", "category", ["#0848DF","#E98440"]);
                if (chart === undefined) {
                    Params = {};
                    Params.ChartType = "Bar";
                    Params.FontSize = 10;
                    Params.Fill = am4core.color("#5081BB");
                    Params.Stroke = am4core.color("#5081BB");
                    Params.Data = response.sort(sort);
                    MakeAMChart(id, Params);
                } else {
                    const NewChartData = response.sort(sort);

                    chart.data = NewChartData;
                    chart.invalidateRawData();
                }
            });
        }); // end am4core.ready()
    }

    // 6.初案/再案統計
    self.makeCSChart = function(div){
        const divHeight = $("#"+div).parents(chartParentName).height()<300?300:$("#"+div).parent().height()-(div.indexOf('popup')>-1?0:0);
        const id = div+"_CS";
        $("#"+div).attr("class","PieTable"+(div.indexOf('popup')>-1?'_popup':'')).css("height",(divHeight-80)+"px");
        if($("#"+div).html()==""){
            $("#"+div).html('<div name="mChart" id="'+id+'" style="height:'+divHeight+'px;width:60%;"></div>\
            <div class="cTable" id="'+id+'Table"></div>');
        }
        am4core.ready(function () {
            // Create chart instance
            var chart = am4core.registry.baseSprites.find(function (chartObj) {
                return chartObj.htmlContainer.id === id;
            });
            const color = ["#48ABC8", "#F7983E"];
            $.PostAJAX("KSarea_caseStatistics", {villageName:self.villageName}, response => {
                const lstData = response.map((data, i) => {
                    return {
                        category: data.category,
                        count: data.count,
                        color: color[i]
                    }
                }).sort(sort);
                var TableBodyRow = [];
                response.forEach((data, i) => {
                    TableBodyRow.push(Object.values(data));
                });
                if (chart === undefined) {
                    Params = {};
                    Params.ChartType = "Pie";
                    Params.FontSize = 10;
                    Params.SeriesText =
                        "{category}: {value.percent.formatNumber('#.0')}%";
                    Params.Data = lstData;
                    var newChart = MakeAMChart(id, Params);

                    setTimeout(() => {
                        var Table = new ChartTable(id+"Table");
                        Table.headerWidth = 0;
                        Table.bodyWidth = newChart.contentWidth * 0.8;
                        Table.dataSource = TableBodyRow;
                        Table.columnSource = ["", "人數(人)", "比例"];
                        Table.bind();
                    }, 100);
                } else {
                    var Table = getChartTable(id+"Table");
                    Table.dataSource = TableBodyRow;
                    Table.columnSource = ["", "人數(人)", "比例"];
                    Table.bind();

                    chart.data = lstData;
                    chart.invalidateRawData();
                }
            });
        }); // end am4core.ready()
    }

    function sort(a, b) {
        return a.count - b.count;
    };
    return self;
}
