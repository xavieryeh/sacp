

function getSpecificArea(id,div){
    const chartID = id + (div.indexOf('popup') > -1 ? "_popup" : "");
    var breadcrumbtitle=document.getElementById("breadcrumbtitle");
    var title=document.getElementById("title");
    var bannerhref = document.getElementById("bannerhref");
    var source  = document.getElementById("source");
    var selectYear = document.getElementById("selectYear");
    var selectMonth = document.getElementById("selectMonth");
    var selectStartYear = document.getElementById("selectStartYear");
    var selectEndYear = document.getElementById("selectEndYear");
    var sYear = moment().format('YYYY')-2;
    var eYear = moment().format('YYYY');
    var sDate = moment().format('YYYY')+"/01";
    var eDate = moment().format('YYYY/MM');
    var reg = /^\d+$/;
    if(selectYear && selectMonth){
        if(selectYear.value.match(reg) && selectMonth.value.match(reg)){
            sDate =(parseInt(selectYear.value)+1911).toString()+"/01";
            eDate =(parseInt(selectYear.value)+1911).toString() +"/"+ selectMonth.value;
        }
    };
    if(selectStartYear && selectEndYear){
        if(selectStartYear.value.match(reg) && selectEndYear.value.match(reg)){
            sYear = (parseInt(selectStartYear.value)+1911).toString()
            eYear = (parseInt(selectEndYear.value)+1911).toString()
        }
    };
    function CommentText (Params,unit){
        return  "註：" + Params.Year + "年" + Params.month + "月在案人數總計" + Params.Total + unit;
    };
    function getTextCss(font_size) {
        var scale = 1;
        if (font_size < 8)
            scale = font_size / 8;
        else if(font_size>12)
            font_size = 12;
        return {
            "font-size": font_size + 'px',
            "transform-origin": "0",
            "white-space": "nowrap",
            "transform": "scale(" + scale + ")"
        };
    };
    function ChartTable_FitSize(div,offset) {
        const id = $("#"+div+ " div[name='mChart']").attr("id");
        const divHeight = $("#"+div).parents(".main-card").height();
        var Chart = am4core.registry.baseSprites.find(function (chartObj) {
            return chartObj.htmlContainer.id === id;
        });
        var Table = getChartTable(id+"Table");
        if(Table!==undefined){
            const firstLength=$("#"+chartID+"Table").find("tbody").find("tr:nth-child(2)").find("td:first").text()+2;
            $.FitTableOnResize(Table, Chart,firstLength);
            $("#"+id).css("height",(divHeight-Table.Height-offset)+'px');
        }
        else
            $("#"+id).css("height",(divHeight-offset)+'px');
    }
    var chartParentName = ".card-item";
    if(title && breadcrumbtitle && source && bannerhref)
        chartParentName = ".largechart";
    else if(document.getElementsByClassName("content-index").length>0)
        chartParentName = ".main-card"
    switch(id){
//一、本市列管特定營業場所業別比例分析
    case"SpecificAreaBusinessIndustryByMonth":
        $.getSpecificAreaBusinessIndustryByMonth(sDate,eDate,function(data){
            $.getSpecificAreaBusinessIndustryCommentTextByMonth(sDate,eDate,function(text){
                am4core.ready(function () {
                    const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                    if($("#"+div).html()==""){
                        $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                        <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                    }
                    $("#"+chartID).height(divHeight-40);
                    var Params = {};
                    var lstItem = ["人數","百分比"];
                    Params.ChartType = "Pie2";
                    var tableBodyRow = [];
                    Params.Data = [];
                    Params.Color = ["#FF0000", "#0031CC", "#0dd3c4", "#996633", "#7030a0"];
                    Params.Name = eDate;
                    Params.Title = "一、本市列管特定營業場所業別比例分析";
                    Params.fonSize=12;
                    Params.Total = 0;
                    Params.ShowDecimal=true;
                    Params.Year = sDate.substring(0,4)-1911;
                    Params.Month = eDate.split('/')[1].replace('0','');
                    if(title && breadcrumbtitle && source && bannerhref){
                            title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                            bannerhref.innerHTML = "本市特定營業場所列管概況分析";
                            bannerhref.href="SpecificArea";
                            source.innerHTML="來源：高雄市政府毒品防制局";
                            Params.fonSize=18;
                        }
                    if(data.length>0){
                        data.forEach((element,index)=>{
                            Params.Data.push({category:element["Business"],count:element["Count"],color:Params.Color[index]})
                            Params.Total+=parseInt( element["Count"]);
                        });
                        // Params.Data.reverse();
                        document.getElementById(chartID+"Text").innerHTML = "註：列管場所家數總計"+Params.Total+"家、分布於"+text.length+"個行政區";
                        $("#"+chartID+"Text").css("fontSize",Params.fonSize+"px");
                    var newChart = amchartfunc(chartID,Params,Params.ChartType);
                                    setTimeout(() => {
                                                $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                            }, 100);
                                }
                }); // end am4core.ready()
            });
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"SpecificAreaBusinessIndustryByYear":
        $.getSpecificAreaBusinessIndustryByYear(sYear,eYear,function(data){
                const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                    if($("#"+div).html()==""){
                        $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                        <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                    }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.ChartType = "Group";
                var tableBodyRow = [];
                Params.Data = [];
                Params.Color=["#FF0000","#0033CC","#0dd3c4","#c89058","#ed7d31","#a6a6a6"];
                Params.Year  = [];
                Params.Business=[];
                Params.Title = "一、本市列管特定營業場所業別比例分析";
                Params.fonSize=12;
                Params.ShowPercent=true;
                Params.Total = 0;
                Params.ColumnsWidth=20;
                Params.Percent={};
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市特定營業場所列管概況分析";
                        bannerhref.href="SpecificArea";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;
                    }
                Params.Title2 = {
                        title: "",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                var index=0;
                if(data.length>0){
                    data.forEach(element => {
                        Params.Year.push(element["YEAR"]);
                        Params.Business.push(element["Business"]);
                        Params.Total += parseInt(element["Count"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.Business = [...new Set(Params.Business)];
                    Params.Year.sort();
                    Params.Data =  Params.Year.map(function(item,iItem){
                        var newData={};
                        newData.category=item;
                        Params.Business.forEach((Business)=>{
                            var findCount = data.find((each)=>each.Business==Business&&each.YEAR==item);
                            if(findCount!==undefined){
                                newData[Business] = parseInt(findCount.Count);
                                newData[Business+"_Rate"] = findCount.Rate;
                            }
                            else
                                newData[Business] = 0;
                            index++;
                        });
                        newData.color=Params.Color;
                        lstItem.push(item+"年");
                        return newData;
                    });
                    tableBodyRow = Params.Business.map(function(element,index){
                        var newData = {};
                        newData.title =element+','+Params.Color[index];
                        Params.Year.forEach(year=>{
                            var findCount = data.find((each)=>each.Business==element&&each.YEAR==year);
                            if(findCount!==undefined)
                                newData["Y"+findCount.YEAR] = findCount.Count;
                            else
                                newData["Y"+findCount.YEAR] = '0';
                        })
                        return newData;
                    });
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"ColumnSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            const firstLength=tableBodyRow[1].title.split(',')[0].length+2;
                                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow,firstLength);
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                            }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
//二、本市列管特定營業場所業別與行政區交叉分析
    case"SpecificAreaAdministrativeByMonth":
        $.getSpecificAreaAdministrativeByMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.ChartType = "Column";
                Params.Data = [];
                Params.Title = "二、本市列管特定營業場所業別與行政區交叉分析";
                Params.fonSize=12;
                Params.Color="#0dd3c4";
                Params.Total = 0;
                Params.Year = eDate.substring(0,4)-1911;
                Params.Month = eDate.split('/')[1].replace('0','');
                Params.ColumnsWidth=10;
                Params.China_Date=[];
                Params.Location=[];
                var tableBodyRow = {title:Params.Year+"年"+Params.Month+"月"+','+Params.Color};
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市特定營業場所列管概況分析";
                        bannerhref.href="SpecificArea";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;
                    }
                // Params.Title2 = {
                //         title: "單位：人數",
                //         fonSize: Params.fonSize,
                //         color: "#000000",
                //         align: "right",
                //         valign: "top",
                //         dy: 0
                // };
                if(data.length>0){
                        data.forEach(element => {
                            Params.China_Date.push(element["China_Date"]);
                            Params.Location.push(element["Domicile"]);
                            Params.Data.push({category:element["Domicile"],count:element["Count"]})
                            Params.Total += parseInt(element["Count"]);
                        });
                        Params.China_Date = [...new Set(Params.China_Date)];
                        Params.Location = [...new Set(Params.Location)];
                        // Params.Year.sort();
                        lstItem=Params.Location;
                        tableBodyRow =  Params.China_Date.map(function(date,index){
                            var newData={};
                            newData.title = date+','+Params.Color;
                            Params.Location.forEach((location,ieachL)=>{
                                var findCount = data.find(each=>{return each.China_Date==date && each.Domicile==location});
                                if(findCount!==undefined)
                                    newData[location]=findCount.Count;
                                else
                                    newData[location]='0';
                            })
                            return newData;
                        });
                // document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月三四級講習0人、案管系統0人、在案人數總計"+Params.Total+"人";
                document.getElementById(chartID+"Text").innerHTML = "註：" +Params.China_Date[0] + "總計"+Params.Total+"家數，"+data.length+"個行政區分布數";
                // ChartTable_FitSize("chart2",0);
                var newChart = amchartfunc(chartID,Params,Params.ChartType);
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            // aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            // aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            // aaTable.dataSource=tableBodyRow;
                                            // aaTable.columnSource=[""].concat(lstItem);
                                            // aaTable.bind();
                                            const firstLength=tableBodyRow[0].title.split(',')[0].length+3;
                                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow,firstLength,true);
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"SpecificAreaAdministrativeByYear":
    $.getSpecificAreaAdministrativeByYear(sYear,eYear,function(data){
        const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
            }
        am4core.ready(function () {
            var Params = {};
            var lstItem = [];
            Params.ChartType = "GroupColumnBar";
            var tableBodyRow = [];
            Params.Data = [];
            Params.Color=["#0031CC","#FF0000","#0dd3c4"];
            Params.Year  = [];
            Params.Domicile=[];
            Params.Title = "二、本市列管特定營業場所業別與行政區交叉分析";
            Params.fonSize=12;
            Params.ColumnsWidth=10;
            if(title && breadcrumbtitle && source && bannerhref){
                    title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                    bannerhref.innerHTML = "本市特定營業場所列管概況分析";
                    bannerhref.href="SpecificArea";
                    source.innerHTML="來源：高雄市政府毒品防制局";
                    Params.fonSize=18;
                }
            Params.Title2 = {
                    title: "",
                    fonSize: Params.fonSize,
                    color: "#000000",
                    align: "right",
                    valign: "top",
                    dy: 0
            };
            if(data.length>0){
                data.forEach(element => {
                    Params.Year.push(element["YEAR"]);
                    Params.Domicile.push(element["Domicile"]);
                });
                Params.Year = [...new Set(Params.Year)];
                Params.Domicile = [...new Set(Params.Domicile)];
                Params.Year.sort();
                Params.Data = Params.Domicile.map(function (location, iItem) {
                    var newData = {};
                    lstItem.push(location);
                    newData.category = location;
                    data.filter((each) => {
                        return each.Domicile == location
                    }).forEach(function (year, iyear) {
                        newData[year.YEAR] = year.Count;
                    });
                    newData.color = Params.Color;
                    return newData;
                });
                tableBodyRow = Params.Year.map(function (element, index) {
                    var newData = {};
                    newData.title = element + "年" + ',' + Params.Color[index];
                    data.filter((each) => {
                        return each.YEAR == element
                    }).forEach(function (each, ieach) {
                        newData["Location" + (ieach)] = each.Count;
                    });
                    return newData;
                });
            var newChart = amchartfunc(chartID,Params,Params.ChartType);
                            setTimeout(() => {
                                        var aaTable = new ChartTable(chartID+"Table");
                                        const firstLength=tableBodyRow[0].title.split(',')[0].length+2;
                                        $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow,firstLength,true);
                                        $("#"+chartID).height(divHeight-aaTable.Height-40);
                                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                    }, 100);
                        }
        }); // end am4core.ready()
    })
    $(window).on('resize',function(){
        var chart = am4core.registry.baseSprites.find(function(chartObj) {
            return chartObj.htmlContainer.id === chartID;
        });
        setTimeout(() => {
            var aaTable = getChartTable(chartID+"Table");
            aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
            aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
            aaTable.setWidth();
            $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
        }, 100);
    });
    break;
//三、本市特定營業場所業別列管率
    case"SpecificIndustryByMonth":
        $.getSpecificIndustryByMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.ChartType = "LineGroupForTen";
                Params.MIX=[];
                Params.Level=[];
                Params.Data = [];
                Params.fill=["#FFC000","#A5A5A5"];
                Params.Name = eDate;
                Params.Title = "四、本市特定營業場所業別列管率";
                Params.fonSize=12;
                Params.Color=["#0dd3c4","#0033cc","#c00000"];
                Params.Total = 0;
                Params.Stacked = true;
                Params.ColumnsWidth=50;
                Params.Item=[];
                Params.Label=true;
                Params.Year = sDate.substring(0,4)-1911;
                Params.Month = eDate.split('/')[1].replace('0','');
                var tableBodyRow = [];
                Params.TableName=  ["登記業者","列管家數","列管率"];
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市特定營業場所列管概況分析";
                        bannerhref.href="SpecificArea";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;
                    }
                Params.Title2 = {
                        title: "",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach((element,index)=>{
                        Params.Item.push(element["Item"]);
                        });
                        Params.Item = [...new Set(Params.Item)];
                        Params.Data = Params.Item.map((element,index)=>{
                            var newData={};
                            newData.category = element;
                            lstItem.push(element);

                            data.filter((each)=>{return each.Item==element}).forEach(function(each,ieach){
                                newData[ Params.TableName[0]]=each.登記業者;
                                newData[ Params.TableName[1]]=each.列管家數;
                                newData[ "列管率"]=parseFloat(each.列管率*100);
                            });
                            newData.color=  Params.Color;
                            return newData;
                        })
                    tableBodyRow = Params.TableName.map(function(element,index){
                        var newData = {};
                        newData.title =element+','+Params.Color[index];
                        Params.Item.map((item,index2)=>{
                            var findCount = data.find(each=>{return each.Item==item});
                            if(findCount!==undefined){
                                if(element=="列管率"){
                                    if(findCount.列管率==0)
                                        newData[element+index2] = "0.0%";
                                    else
                                        newData[element+index2] = parseFloat(findCount.列管率*100).toString()+"%";

                                }
                                else
                                    newData[element+index2] = findCount[Params.TableName[index]];
                            }
                            else{
                                if(element=="列管率")
                                    newData[element+index2] = '0.0%';
                                else
                                    newData[element+index2] = '0';
                            }

                        })
                        return newData;
                    })
                    // document.getElementById(chartID+"Text").innerHTML = "列管特定營業場所XX家，分布於XX個行政區";

                var newChart = amchartfunc(chartID,Params,Params.ChartType,"ColumnSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            const firstLength=tableBodyRow[0].title.split(',')[0].length+3;
                                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow,firstLength);
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"SpecificIndustryByYear":
        $.getSpecificIndustryByYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.ChartType = "LineGroup";
                Params.Type=[];
                Params.Level=[];
                Params.Data = [];
                Params.Color=["#0033cc","#FF0000","#0dd3c4"];
                Params.Name = eDate;
                Params.Title = "三、本市特定營業場所業別列管率";
                Params.fonSize=12;
                Params.Total = 0;
                Params.Stacked = true;
                Params.Year =[];Params.Item=[];
                Params.ColumnsWidth=50;
                Params.month = sDate.split('/')[1].replace('0','');
                var tableBodyRow = [];
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市特定營業場所列管概況分析";
                        bannerhref.href="SpecificArea";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;

                    }
                Params.Title2 = {
                        title: "",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach(element => {
                        Params.Year.push(element["YEAR"]);
                        Params.Item.push(element["Item"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.Item = [...new Set(Params.Item)];
                    Params.Year.sort();
                    Params.Data =  Params.Item.map(function(item,iItem){
                        var newData={};
                        newData.category=item;
                        lstItem.push(item);
                        Params.Year.forEach(year=>{
                            var findCount = data.find((each)=>each.YEAR==year&&each.Item==item);
                            if(findCount!==undefined)
                                newData[year] = findCount.Rate;
                            else
                                newData[year] = 0;
                        })
                        newData.color=  Params.Color;
                        return newData;
                    });
                    tableBodyRow = Params.Year.map(function(year,index){
                        var newData = {};
                        newData.title =year+'年,'+Params.Color[index];
                        Params.Item.forEach(item=>{
                            var findCount = data.find((each)=>each.YEAR==year&&each.Item==item);
                            if(findCount!==undefined)
                                newData[item] = Math.round(findCount.Rate).toString()+"%";
                            else
                            newData[item] = '0%';
                        })
                        return newData;
                    });

                var newChart = amchartfunc(chartID,Params,Params.ChartType,"LineSeries");
                                setTimeout(() => {
                                        var aaTable = new ChartTable(chartID+"Table");
                                        const firstLength=tableBodyRow[0].title.split(',')[0].length+2;
                                        $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow,firstLength);
                                        $("#"+chartID).height(divHeight-aaTable.Height-40);
                                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth-2;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+14;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
//四、本市特定營業場所月列管率
    case"SpecificAreaBusinessByMonth":
        $.getSpecificAreaBusinessByMonth(sDate,eDate,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                //am4core.addLicense("{{config('custom.ChartsPN')}}");
                var Params = {};
                var lstItem = [];
                Params.ChartType = "LineGroupForTen";
                Params.Item=[];
                Params.Data = [];
                Params.Name = eDate;
                Params.Title = "四、本市特定營業場所業別列管率";
                Params.fonSize=12;
                Params.Color=["#7030a0","#ff0000"];
                Params.Total = 0;
                Params.Year = sDate.substring(0,4)-1911;
                Params.Month = [];
                Params.Label=true;
                Params.ColumnsWidth=10;
                // Params.Month = eDate.split('/')[1].replace('0','');
                var tableBodyRow =[];
                Params.TableName=  ["列管家數","登記業者"];
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市特定營業場所列管概況分析";
                        bannerhref.href="SpecificArea";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fonSize=18;

                    }
                Params.Title2 = {
                        title: "",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach((element,index)=>{
                    Params.Item.push(element["Item"]);
                    Params.Month.push(element["Month"]);
                    if(element["Item"]!=="列管率")
                        Params.Total+=parseInt(element["Count"]);
                    });
                    Params.Item = [...new Set(Params.Item)];
                    Params.Month = [...new Set(Params.Month)];
                    Params.Item.sort();
                    Params.Data = Params.Month.map((element,index)=>{
                        var newData={};
                        lstItem.push(element+"月");
                        newData.category = element;
                        Params.Item.forEach(item=>{
                            if(item!=="登記業者"){
                                var findCount = data.find((each)=>each.Month==element&&each.Item==item);
                                if(findCount!==undefined)
                                    newData[item]=findCount.Count;
                                else
                                    newData[item]=0;
                            }
                        })
                        newData.color=  Params.Color;
                        return newData;
                    })
                    Params.Item.sort(function(a,b) { return a.replace('列管率','0').localeCompare(b.replace('列管率','0'), "zh-Hant"); });
                    Params.Item.shift();
                    tableBodyRow = Params.Item.map(function(element,lelement){
                        var newData = {};
                        if(element=="登記業者")
                            newData.title =element+',#fff';
                        else
                            newData.title =element+',#7030a0';
                        Params.Month.forEach(em=>{
                            if(element!=="列管率"){
                                var findCount = data.find((each)=>each.Month==em&&each.Item==element);
                                if(findCount!==undefined)
                                    newData["M"+em]=Math.round(findCount.Count).toString();
                                else
                                    newData["M"+em]='0';
                            }
                        })
                        return newData;
                    })
                // document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月三四級講習0人、案管系統0人、在案人數總計"+Params.Total+"人";
                document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月在案人數總計"+Params.Total+"人";
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"ColumnSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            const firstLength=tableBodyRow[0].title.split(',')[0].length+25;
                                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow,firstLength);
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"SpecificAreaBusinessByYear":
    $.getSpecificAreaBusinessByYear(sYear,eYear,function(data){
        const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
            }
        am4core.ready(function () {
            var Params = {};
            var lstItem = [];
            Params.Item=[];
            Params.Data = [];
            Params.Year =[];
            Params.ChartType = "Line";
            Params.Color="#7030a0";
            Params.Title = "四、本市特定營業場所總列管率";
            Params.fonSize=12;
            Params.ShowPercent = true;
            var tableBodyRow = {title:"總列管率,#7030a0"};
            if(title && breadcrumbtitle && source && bannerhref){
                    title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                    bannerhref.innerHTML = "本市特定營業場所列管概況分析";
                    bannerhref.href="SpecificArea";
                    source.innerHTML="來源：高雄市政府毒品防制局";
                    Params.fonSize=18;
                }
            Params.Title2 = {
                    title: "",
                    fonSize: Params.fonSize,
                    color: "#000000",
                    align: "right",
                    valign: "top",
                    dy: 0
            };
            if(data.length>0){
                data.forEach((element,index)=>{
                Params.Item.push(element["Item"]);
                Params.Year.push(element["Year"]);
                Params.Data.push({category:element["Year"]+"年",count:element["Count"]==null?0:element["Count"]})
                });
                Params.Item = [...new Set(Params.Item)];
                Params.Year = [...new Set(Params.Year)];
                Params.Year.sort();
                Params.Year.forEach((element,index)=>{
                    lstItem.push(element+"年");
                    var filterdata = data.filter(each=>{return each.Year == element})[0];
                    tableBodyRow["col"+index] = filterdata.Count==null?'0%':Math.round(filterdata.Count).toString()+"%";
                })
            var newChart = amchartfunc(chartID,Params,Params.ChartType,"LineSeries");
                            setTimeout(() => {
                                        var aaTable = new ChartTable(chartID+"Table");
                                        const firstLength=[tableBodyRow][0].title.split(',')[0].length+2;
                                        $.FitTable(aaTable, newChart, [""].concat(lstItem), [tableBodyRow],firstLength);
                                        $("#"+chartID).height(divHeight-aaTable.Height-40);
                                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                    }, 100);
                         }
        }); // end am4core.ready()
    })
    $(window).on('resize',function(){
        var chart = am4core.registry.baseSprites.find(function(chartObj) {
            return chartObj.htmlContainer.id === chartID;
        });
        setTimeout(() => {
            var aaTable = getChartTable(chartID+"Table");
            aaTable.headerWidth=chart.leftAxesContainer.contentWidth-17;
            aaTable.bodyWidth=chart.plotContainer.contentWidth+30;
            aaTable.setWidth();
            $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
        }, 100);
    });
break;
//五、本市毒危條例規範六大業別場所業者主動通報率
    case"SixBusinessByMonth":
    $.getSixBusinessByMonth(sDate,eDate,function(data){
        const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
        am4core.ready(function () {
            //am4core.addLicense("{{config('custom.ChartsPN')}}");
            var Params = {};

            Params.Data = [];
            Params.Color=["#7030A0","#008000"];
            var tableBodyRow = [];
            Params.Title = "五、本市毒危條例規範六大業別場所業者主動通報率";
            Params.Year = sDate.substring(0,4)-1911;
            Params.month = eDate.split('/')[1].replace('0','');
            var lstItem = ["月份",Params.Year+"年"+'1-'+Params.month+"月","月份",Params.Year+"年"+'1-'+Params.month+"月"];
            Params.Total=0;
            Params.FontSize=12;
            Params.Type=[];
            Params.Year=["110"];
            Params.Rate= ["通報率"];
            Params.Item=[];
            Params.ForSixB=true;
            Params.ColumnsWidth=50;
            if(title && breadcrumbtitle && source && bannerhref){
                    title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                    bannerhref.innerHTML = "本市特定營業場所列管概況分析";
                    bannerhref.href="SpecificArea";
                    source.innerHTML="來源：高雄市政府毒品防制局";
                    Params.FontSize=15+"px";
                }
            if(data.length>0){
                data.forEach((element,index)=>{
                    Params.Type.push(element["Type"]);
                    Params.Item.push(element["Item"]);
                    // Params.Year.push(element["YEAR"]);
                });
                Params.Type = [...new Set(Params.Type)];
                Params.Item = [...new Set(Params.Item)];
                var listNUM = [...new Set(data.map(m=>{return m.NUM}))];

                // Params.Year = [...new Set(Params.Year)];
                Params.Data = Params.Rate.map((element,index) =>{
                    var newData={};
                    newData.category = element;
                    // Params.Type.forEach()
                    data.filter(each=>{return each.Item==element}).forEach(e=>{
                        newData[e.Type] = e.Count+"%";
                    })
                    newData.color=Params.Color;
                    return newData;
                })
                tableBodyRow=[{title:"列管家數(含除管),#fff"},{title:"主動通報家數,#7030A0"},{title:"通報率,#7030A0"}]


                // var TopThree = data[0].Domicile.substring(0,2) +"、" + data[1].Domicile.substring(0,2) +"、" + data[2].Domicile.substring(0,2);
                // document.getElementById(chartID+"Text").innerHTML ="註：" + Params.Year + "年" + Params.month + "月在案人數總計" + Params.Total + "人，前三熱區："+TopThree;

                var newChart = amchartfunc(chartID,Params,"Group","ColumnSeries");

                setTimeout(() => {
                                $("#"+chartID+"Table").html("<table class='chart-table'><tbody><tr><td  align=\"center\"><p>月份</p></td><td  align=\"center\"><p>"+Params.Year+"年"+'1-'+Params.month+"月</p></td><td  align=\"center\"><p>月份</p></td><td  align=\"center\"><p>"+Params.Year+"年"+'1-'+Params.month+"月</p></td></tr>"
                                +listNUM.map((num,index)=>{
                                    var EachRow = data.filter(f=>{return f.NUM==num});
                                    return '<tr><td align="center"><p>'+(EachRow.find(f=>f.Item).Item.indexOf("列管")==-1?('<i class="fa fa-square" style="color:'+Params.Color[0]+';background-color:'+Params.Color[0]+';"></i>'):"")+EachRow.find(f=>f.Type=='列管').Item+'</p></td><td  align="center"><p>'+Math.round(EachRow.find(f=>f.Type=='列管').Count*100)/100+(num=="3"?"%":"")+'</p></td><td  align="center"><p>'+(EachRow.find(f=>f.Item).Item.indexOf("列管")==-1?('<i class="fa fa-square" style="color:'+Params.Color[1]+';background-color:'+Params.Color[1]+';"></i>'):"")+EachRow.find(f=>f.Type=='非列管').Item+'</p></td><td  align="center"><p>'+Math.round(EachRow.find(f=>f.Type=='非列管').Count*100)/100+(num=="3"?"%":"")+'</p></td></tr>'
                                }).join("")+"</tbody></table>");

                                setNormalCss(chartID+"Table",newChart.contentWidth);

                            // $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                        }, 100);
            };


        }); // end am4core.ready()
    })

    var setNormalCss = function (id,width) {
        var tdWidth = width / 4;
        var scale = 1,
            fontSize = (tdWidth /12)-1;
        if (fontSize < 8)
            scale = fontSize / 8;
        else if (fontSize > 12)
            fontSize = 12;

        var fontCSS = {
            "font-size": fontSize + 'px',
            "transform-origin": "0",
            "transform": "scale(" + scale + ")",
            "white-space": "nowrap",
            "display": "table-cell",
        };
        $("#" + id + " .chart-table").css({
            "width": width + 'px',
            'table-layout': 'fixed'
        });

        $('#' + id + ' .chart-table tbody tr:first td').css(basicCSS).css("line-height", (fontSize * 2).toString() + "px").css("width", tdWidth + 'px');
        $('#' + id + ' .chart-table tbody tr:first td p').css(fontCSS);
        $('#' + id + ' .chart-table tbody tr:not(:first) td').css(basicCSS).css("line-height", (fontSize * 2).toString() + "px").css("border-bottom", "1px solid #000").css("text-aligh","center");
        $('#' + id + ' .chart-table tbody tr:not(:first) td p').css(fontCSS);
    }

    $(window).on('resize',function(){
        var chart = am4core.registry.baseSprites.find(function(chartObj) {
            return chartObj.htmlContainer.id === chartID;
        });
        setTimeout(() => {
            // var aaTable = getChartTable(chartID+"Table");
            // aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
            // aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
            // aaTable.setWidth();
            $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
        }, 100);
    });
    break;
    case"SixBusinessByYear":
        $.getSixBusinessByYear(sYear,eYear,function(data){
            const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
            am4core.ready(function () {
                Params = {};
                Params.ChartType = "Group";
                Params.Title = "五、本市毒危條例規範六大業別場所業者主動通報率";
                Params.fontSize = 10;
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市特定營業場所列管概況分析";
                        bannerhref.href="SpecificArea";
                        source.innerHTML="來源：高雄市政府毒品防制局";
                        Params.fontSize = 15;

                }
                Params.TitleColor = "#00B48B";
                Params.Color=["#7030a0","#0dd3c4"];
                Params.FontSize = 10;
                Params.categoryAxisVisible = false;
                Params.Item = [];
                Params.Year=[];
                var lstItem = [];
                var tableBodyRow = [];
                data.forEach(element => {
                    Params.Year.push(element["YEAR"]);
                    Params.Item.push(element["Item"]);
                });
                Params.Max = 100;
                Params.Year = [...new Set(Params.Year)];
                Params.Item = [...new Set(Params.Item)];
                Params.Year.sort();
                Params.Data =  Params.Year.map(function(year,iItem){
                    var newData={};
                    newData.category=year;
                    lstItem.push(year+"年");
                    Params.Item.forEach(item=>{
                        var findRate = data.find(each=>each.YEAR == year && each.Item == item);
                        if(findRate!==undefined)
                            newData[item] = findRate.RATE;
                        else
                            newData[item] = 0;
                    });
                    newData.color=Params.Color;
                    return newData;
                });
                tableBodyRow =  Params.Item.map(function(item,iItem){
                    var newData={};
                    newData.title = item+','+Params.Color[iItem];
                    Params.Year.forEach((year)=>{
                            var findRate = data.find(each=>each.YEAR == year && each.Item == item);
                            if(findRate!==undefined)
                                newData["Y"+year] = Math.round(findRate.RATE*100)/100+"%";
                            else
                                newData["Y"+year] = '0%';
                    });
                    return newData;
                });
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"LineSeries");
                setTimeout(() => {
                            var aaTable = new ChartTable(chartID+"Table");
                            const firstLength=tableBodyRow[0].title.split(',')[0].length+15;
                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow,firstLength);
                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                        }, 100);
            })
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
}};
