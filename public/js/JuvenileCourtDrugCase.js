function getJuvenileCourtDrugCase(id,div){
    const chartID = id + (div.indexOf('popup') > -1 ? "_popup" : "");
    var breadcrumbtitle=document.getElementById("breadcrumbtitle");
    var title=document.getElementById("title");
    var bannerhref = document.getElementById("bannerhref");
    var source  = document.getElementById("source");
    var selectYear = document.getElementById("selectYear");
    var selectMonth = document.getElementById("selectMonth");
    var selectStartYear = document.getElementById("selectStartYear");
    var selectEndYear = document.getElementById("selectEndYear");
    var sYear = moment().format('YYYY')-2;
    var eYear = moment().format('YYYY');
    var sDate = moment().format('YYYY')+"/01";
    var eDate = moment().format('YYYY/MM');
    var reg = /^\d+$/;
    if(selectYear && selectMonth){
        if(selectYear.value.match(reg) && selectMonth.value.match(reg)){
            sDate =(parseInt(selectYear.value)+1911).toString()+"/01";
            eDate =(parseInt(selectYear.value)+1911).toString() +"/"+ selectMonth.value;
        }
    };
    if(selectStartYear && selectEndYear){
        if(selectStartYear.value.match(reg) && selectEndYear.value.match(reg)){
            sYear = (parseInt(selectStartYear.value)+1911).toString()
            eYear = (parseInt(selectEndYear.value)+1911).toString()
        }
    };
    function CommentText (Params,unit){
        return  "註：" + Params.Year + "年" + Params.month + "月在案人數總計" + Params.Total + unit;
    };
    function getTextCss(font_size) {
        var scale = 1;
        if (font_size < 8)
            scale = font_size / 8;
        else if(font_size>12)
            font_size = 12;
        return {
            "font-size": font_size + 'px',
            "transform-origin": "0",
            "white-space": "nowrap",
            "transform": "scale(" + scale + ")"
        };
    };
    function ChartTable_FitSize(div,offset) {
        const id = $("#"+div+ " div[name='mChart']").attr("id");
        const divHeight = $("#"+div).parents(".main-card").height();
        var Chart = am4core.registry.baseSprites.find(function (chartObj) {
            return chartObj.htmlContainer.id === id;
        });
        var Table = getChartTable(id+"Table");
        if(Table!==undefined){
            const firstLength=$("#"+chartID+"Table").find("tbody").find("tr:nth-child(2)").find("td:first").text()+2;
            $.FitTableOnResize(Table, Chart,firstLength);
            $("#"+id).css("height",(divHeight-Table.Height-offset)+'px');
        }
        else
            $("#"+id).css("height",(divHeight-offset)+'px');
    }
    var chartParentName = ".card-item";
    if(title && breadcrumbtitle && source && bannerhref)
        chartParentName = ".largechart";
    else if(document.getElementsByClassName("content-index").length>0)
        chartParentName = ".main-card"
    switch(id){
//一、人數統計
    case"DrugCourtPersonByMonth":
        $.getDrugCourtPersonByMonth(sDate,eDate,function(data){
            am4core.ready(function () {
                const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                if($("#"+div).html()==""){
                    $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                    <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                }
                $("#"+chartID).height(divHeight-40);
                var Params = {};
                var lstItem = [];
                Params.ChartType = "Group";
                var tableBodyRow = [];
                Params.Data = [];
                Params.Color=["#7030a0","#0dd3c4"];
                Params.Name = eDate;
                Params.Title = "一、人數統計";
                Params.fonSize=12;
                Params.Total = 0;
                Params.Year=[];
                Params.TableTitle = ["曝險少年(毒品)","違反毒品危害防制條例"]
                Params.valuekey=["Not_Penalty_Num","Penalty_Num"];
                // Params.Year = sDate.substring(0,4)-1911;
                Params.Month = eDate.split('/')[1].replace('0','');
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市少年法庭毒品案件統計分析";
                        bannerhref.href="JuvenileCourtDrugCase";
                        source.innerHTML="來源：臺灣高雄少年及家事法院";
                        Params.fonSize=18;
                    }
                if(data.length>0){

                    data.forEach((element,index)=>{
                        Params.Year.push(element["CHINA_Date"]);
                        // Params.Data.push({category:Params.TableTitle[index],count:element["Count"],color:Params.fill[index]})
                        Params.Total+=parseInt(element["Not_Penalty_Num"]);
                        Params.Total+=parseInt(element["Penalty_Num"]);
                    });
                    Params.Year = [...new Set(Params.Year)];
                    Params.Data = Params.Year.map((element,index)=>{
                        lstItem.push("人數");
                        var newData={};
                        newData.category = element;
                        data.filter(each=>{return each.CHINA_Date == element}).forEach((eachD,i)=>{
                            newData[Params.TableTitle[0]] = eachD.Not_Penalty_Num;
                            newData[Params.TableTitle[1]] = eachD.Penalty_Num;
                        })
                        newData.color=Params.Color;
                        return newData;

                    })
                    tableBodyRow = Params.TableTitle.map((element,index)=>{
                        var newData={};
                        newData.title = element+','+Params.Color[index];
                        Params.Year.map((eachYear)=>{
                            data.filter(each=>{return each.CHINA_Date == eachYear}).forEach((eachD,i)=>{
                                newData["i"+i] = eachD[Params.valuekey[index]];
                            })
                            return newData;
                        })
                        return newData;
                    })
                    tableBodyRow.push({title:"合計,#fff",i0:Params.Total.toString()});
                    document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月累計在案人數總計"+Params.Total+"人";
                    $("#"+chartID+"Text").css("fontSize",Params.fonSize+"px");
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"ColumnSeries");
                                setTimeout(() => {
                                    var aaTable = new ChartTable(chartID+"Table");
                                    const firstLength=tableBodyRow[1].title.split(',')[0].length+30;
                                    $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow,firstLength);
                                    $("#"+chartID).height(divHeight-aaTable.Height-40);
                                    $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"DrugCourtPersonByYear":
        $.getDrugCourtPersonByYear(sYear,eYear,function(data){
                const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                    if($("#"+div).html()==""){
                        $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                        <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                    }
            am4core.ready(function () {
                var Params = {};
                var lstItem = [];
                Params.ChartType = "Line";
                Params.Data = [];
                Params.Color="#7030a0";
                Params.Title = "一、人數統計";
                Params.fonSize=12;
                tableBodyRow={title:"合計,"+Params.Color};
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市少年法庭毒品案件統計分析";
                        bannerhref.href="JuvenileCourtDrugCase";
                        source.innerHTML="來源：臺灣高雄少年及家事法院";
                        Params.fonSize=18;
                    }
                Params.Title2 = {
                        title: "單位：人數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach((element,index) => {
                        Params.Data.push({category:element["Year"],count:element["Total"]});
                        lstItem.push(element["Year"]+"年");
                        tableBodyRow["value" + index] = element["Total"];
                    });
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"LineSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                            aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                            aaTable.dataSource=[tableBodyRow];
                                            aaTable.columnSource=[""].concat(lstItem);
                                            aaTable.bind();
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                            }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
//二、件數統計
    case"DrugCourtCaseByMonth":
        $.getDrugCourtCaseByMonth(sDate,eDate,function(data){
                am4core.ready(function () {
                    const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
                    if($("#"+div).html()==""){
                        $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                        <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
                    }
                    $("#"+chartID).height(divHeight-40);
                    var Params = {};
                    var lstItem = [];
                    Params.ChartType = "Group";
                    var tableBodyRow = [];
                    Params.Data = [];
                    Params.Color=["#7030a0","#0dd3c4"];
                    Params.Name = eDate;
                    Params.Title = "二、件數統計";
                    Params.fonSize=12;
                    Params.Total = 0;
                    Params.Year=[];
                    Params.TableTitle = ["曝險少年(毒品)","違反毒品危害防制條例"]
                    Params.valuekey=["Not_Penalty_Case","Penalty_Case"];
                    // Params.Year = sDate.substring(0,4)-1911;
                    Params.Month = eDate.split('/')[1].replace('0','');
                    if(title && breadcrumbtitle && source && bannerhref){
                            title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                            bannerhref.innerHTML = "本市少年法庭毒品案件統計分析";
                            bannerhref.href="JuvenileCourtDrugCase";
                            source.innerHTML="來源：臺灣高雄少年及家事法院";
                            Params.fonSize=18;
                        }
                    if(data.length>0){
                        data.forEach((element,index)=>{
                            Params.Year.push(element["CHINA_Date"]);
                            // Params.Data.push({category:Params.TableTitle[index],count:element["Count"],color:Params.fill[index]})
                            Params.Total+=parseInt( element["Not_Penalty_Case"]);
                            Params.Total+=parseInt( element["Penalty_Case"]);
                        });
                        Params.Year = [...new Set(Params.Year)];
                        Params.Data = Params.Year.map((element,index)=>{
                            lstItem.push("件數");
                            var newData={};
                            newData.category = element;
                            data.filter(each=>{return each.CHINA_Date == element}).forEach((eachD,i)=>{
                                newData[Params.TableTitle[0]] = eachD.Not_Penalty_Case;
                                newData[Params.TableTitle[1]] = eachD.Penalty_Case;
                            })
                            newData.color=Params.Color;
                            return newData;

                        })
                        tableBodyRow = Params.TableTitle.map((element,index)=>{
                            var newData={};
                            newData.title = element+','+Params.Color[index];
                            Params.Year.map((eachYear)=>{
                                data.filter(each=>{return each.CHINA_Date == eachYear}).forEach((eachD,i)=>{
                                    newData["i"+i] = eachD[Params.valuekey[index]];
                                })
                                return newData;
                            })
                            return newData;
                        })
                        tableBodyRow.push({title:"合計,#fff",i0:Params.Total.toString()});
                        document.getElementById(chartID+"Text").innerHTML = "註：" + Params.Month + "月累計在案人數總計"+Params.Total+"人";
                        $("#"+chartID+"Text").css("fontSize",Params.fonSize+"px");
                // ChartTable_FitSize("chart2",0);
                var newChart = amchartfunc(chartID,Params,Params.ChartType,"ColumnSeries");
                                setTimeout(() => {
                                            var aaTable = new ChartTable(chartID+"Table");
                                            const firstLength=tableBodyRow[1].title.split(',')[0].length+30;
                                            $.FitTable(aaTable, newChart, [""].concat(lstItem), tableBodyRow,firstLength);
                                            $("#"+chartID).height(divHeight-aaTable.Height-40);
                                            $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                        }, 100);
                             }
            }); // end am4core.ready()
        })
        $(window).on('resize',function(){
            var chart = am4core.registry.baseSprites.find(function(chartObj) {
                return chartObj.htmlContainer.id === chartID;
            });
            setTimeout(() => {
                var aaTable = getChartTable(chartID+"Table");
                aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
                aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
                aaTable.setWidth();
                $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
            }, 100);
        });
    break;
    case"DrugCourtCaseByYear":
    $.getDrugCourtCaseByYear(sYear,eYear,function(data){
        const divHeight = $("#"+div).parents(chartParentName).height()<150?150:$("#"+div).parents(chartParentName).height()-(div.indexOf('popup')>-1?80:0);;;
            if($("#"+div).html()==""){
                $("#"+div).html('<div name="mChart" id="'+chartID+'" style="height:'+(divHeight - 100)+'px;width:100%;"></div>\
                <div name="mTable" id="'+chartID+'Table" style="margin-top:-16px"></div>');
            }
        am4core.ready(function () {
            var Params = {};
                var lstItem = [];
                Params.ChartType = "Line";
                Params.Data = [];
                Params.Color="#7030a0";
                Params.Title = "二、件數統計";
                Params.fonSize=12;
                tableBodyRow={title:"合計,"+Params.Color};
                if(title && breadcrumbtitle && source && bannerhref){
                        title.innerHTML = breadcrumbtitle.innerHTML = Params.Title;
                        bannerhref.innerHTML = "本市少年法庭毒品案件統計分析";
                        bannerhref.href="JuvenileCourtDrugCase";
                        source.innerHTML="來源：臺灣高雄少年及家事法院";
                        Params.fonSize=18;
                    }
                Params.Title2 = {
                        title: "單位：件數",
                        fonSize: Params.fonSize,
                        color: "#000000",
                        align: "right",
                        valign: "top",
                        dy: 0
                };
                if(data.length>0){
                    data.forEach((element,index) => {
                        Params.Data.push({category:element["Year"],count:element["Total"]});
                        lstItem.push(element["Year"]+"年");
                        tableBodyRow["value" + index] = element["Total"];
                    });
            var newChart = amchartfunc(chartID,Params,Params.ChartType,"LineSeries");
                            setTimeout(() => {
                                        var aaTable = new ChartTable(chartID+"Table");
                                        aaTable.headerWidth=newChart.leftAxesContainer.contentWidth+14;
                                        aaTable.bodyWidth=newChart.plotContainer.contentWidth+1;
                                        aaTable.dataSource=[tableBodyRow];
                                        aaTable.columnSource=[""].concat(lstItem);
                                        aaTable.bind();
                                        $("#"+chartID).height(divHeight-aaTable.Height-40);
                                        $("#" + chartID + "Text").css(getTextCss(newChart.contentWidth / 35));
                                    }, 100);
                        }
        }); // end am4core.ready()
    })
    $(window).on('resize',function(){
        var chart = am4core.registry.baseSprites.find(function(chartObj) {
            return chartObj.htmlContainer.id === chartID;
        });
        setTimeout(() => {
            var aaTable = getChartTable(chartID+"Table");
            aaTable.headerWidth=chart.leftAxesContainer.contentWidth+14;
            aaTable.bodyWidth=chart.plotContainer.contentWidth+1;
            aaTable.setWidth();
            $("#" + chartID + "Text").css(getTextCss(chart.contentWidth / 35));
        }, 100);
    });
    break;
}};
