// menu
$(function () {
    $("#menuBtn").on("click", SHOWSHOW);

    function SHOWSHOW() {
        $(".nav").attr({
            style: " display: flex;"
        });
        $("#xx").on("click", HIDEHIDE);
        $(".js-btn-index").on("click", HIDEHIDE);
        // $("#menu li a").on("click", HIDEHIDE);
    }

    function HIDEHIDE() {
        $(".nav").hide();
        return false;
    }

    $(window).on("resize", CLEARSTYLE);

    function CLEARSTYLE() {
        if ($(window).innerWidth() > 767) {
            $("#menuBtn").attr("style", "");
            $(".nav").attr("style", "");
        }
    }
});
// Menu thisscroll
$(function () {
    $(".js-btn-index").click(function goTop() {
        $("html,body").animate({
            scrollTop: 166
        }, 300);
        return false;
    });
});

//slick
$(".slick").slick({
    arrows: true,
    dots: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: false,
    pauseOnHover: true,
    autoplaySpeed: 5000,
    speed: 1000,
    responsive: [{
            breakpoint: 1300,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

//popup
// $(function() {
//   for (let i = 1; i < $(".popupBox").length + 1; i++) {
//     $(".js-popupBtn-open-" + i).click(function() {
//       $(".js-popupBox-" + i).attr({ style: " display: flex;" });
//       return false;
//     });
//   }

//   $(".js-popupBtn-close").click(function() {
//     $(".popupBox").hide();
//     return false;
//   });
// });



//people-name-popup
$(function () {
    $(".js-people-name").on("click", function () {
        $(".people-popupBox").toggleClass("hidden");
    });
});
//case_Page
$(function () {
    $(".js-case_Page").on("click", function () {
        window.location.href = "CasePage";
    });
});

//content-page
$(function () {
    $(window).on("resize", function () {
        if ($(window).innerWidth() < 768) {
            var headerHeight = $(".header").height();
            $(".content-page").attr({
                style: "margin-top:" + headerHeight + "px"
            });
        } else {
            $(".content-page").attr("style", "");
        }
    });
});

// $(document).ready(function () {
//     bsCustomFileInput.init();
// });
//KSarea_page_ownTeacher
// $(function() {
//   $(".js-KSarea_page_ownTeacher").on("click", function() {
//     window.location.href = "KSareaPageOwnTeacher";
//   });
// });
//KSarea_page_ownUser
// $(function() {
//   $(".js-KSarea_page_ownUser").on("click", function() {
//     window.location.href = "KSareaPageOwnUser";
//   });
// });

//stick-header
// $(function() {
//   $(window).scroll(function() {

//     if ($(window).scrollTop() > 0) {
//       $(".header").addClass("fixed");
//     } else {
//       $(".header").removeClass("fixed");
//     }
//   });
// });
