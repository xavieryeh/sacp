function makeGenogram2(div,params) {

    function init() {
        // console.log('init');
        go.Diagram.licenseKey = "YourKeyHere";
        var $ = go.GraphObject.make;
        myDiagram =
            $(go.Diagram, div,
                {
                    // initialAutoScale: go.Diagram.Uniform, // 自動縮放
                    "undoManager.isEnabled": true,
                    isReadOnly: true, // 不允許拖動節點
                    allowVerticalScroll: false, // 不允許垂直拖動
                    // allowHorizontalScroll: false, // 不允許水平拖動
                    // when a node is selected, draw a big yellow circle behind it
                    nodeSelectionAdornmentTemplate:
                        $(go.Adornment, "Auto",
                            {layerName: "Grid"},  // the predefined layer that is behind everything else
                            $(go.Shape, "Circle", {fill: "#c1cee3", stroke: null}),
                            $(go.Placeholder, {margin: 2})
                        ),
                    layout:  // use a custom layout, defined below
                        $(GenogramLayout, {direction: 90, layerSpacing: 30, columnSpacing: 10})
                });

        // determine the color for each attribute shape
        function attrFill(a) {
            switch (a) {
                case "A": return "#00af54"; // green
                case "B": return "#0848df"; // blue
                // case "B": return "#fcf384"; // gold fcf384
                case "C": return "#d4071c"; // red
                // case "D": return "#70bdc2"; // cyan
                case "E": return "#ffbe06"; // orange f27935
                // case "F": return "#e69aaf"; // pink
                // case "G": return "#08488f"; // blue
                // case "H": return "#866310"; // brown
                // case "I": return "#9270c2"; // purple
                // case "J": return "#a3cf62"; // chartreuse
                // case "K": return "#91a4c2"; // lightgray bluish
                // case "L": return "#af70c2"; // magenta
                case "S": return "#d4071c"; // red
                default: return "transparent";
            }
        }

        // determine the geometry for each attribute shape in a male;
        // except for the slash these are all squares at each of the four corners of the overall square
        var tlsq = go.Geometry.parse("F M1 1 l19 0 0 19 -19 0z");
        var trsq = go.Geometry.parse("F M20 1 l19 0 0 19 -19 0z");
        var brsq = go.Geometry.parse("F M20 20 l19 0 0 19 -19 0z");
        var blsq = go.Geometry.parse("F M1 20 l19 0 0 19 -19 0z");
        var slash = go.Geometry.parse("F M38 0 L40 0 40 2 2 40 0 40 0 38z");

        function maleGeometry(a) {
            switch (a) {
                case "A": return brsq;
                case "B": return brsq;
                case "C": return tlsq;
                // case "D": return trsq;
                case "E": return tlsq;
                // case "F": return trsq;
                // case "G": return brsq;
                // case "H": return brsq;
                // case "I": return brsq;
                // case "J": return blsq;
                // case "K": return blsq;
                // case "L": return blsq;
                case "S": return slash;
                default: return tlsq;
            }
        }

        // determine the geometry for each attribute shape in a female;
        // except for the slash these are all pie shapes at each of the four quadrants of the overall circle
        var tlarc = go.Geometry.parse("F M20 20 B 180 90 20 20 19 19 z");
        var trarc = go.Geometry.parse("F M20 20 B 270 90 20 20 19 19 z");
        var brarc = go.Geometry.parse("F M20 20 B 0 90 20 20 19 19 z");
        var blarc = go.Geometry.parse("F M20 20 B 90 90 20 20 19 19 z");

        function femaleGeometry(a) {
            switch (a) {
                case "A": return brarc;
                case "B": return brarc;
                case "C": return tlarc;
                // case "D": return trarc;
                case "E": return tlarc;
                // case "F": return trarc;
                // case "G": return brarc;
                // case "H": return brarc;
                // case "I": return brarc;
                // case "J": return blarc;
                // case "K": return blarc;
                // case "L": return blarc;
                case "S": return slash;
                default: return tlarc;
            }
        }


        // two different node templates, one for each sex,
        // named by the category value in the node data object
        myDiagram.nodeTemplateMap.add("M",  // male
            $(go.Node, "Vertical",
                {locationSpot: go.Spot.Center, locationObjectName: "ICON", selectionObjectName: "ICON"},
                $(go.Panel,
                    {name: "ICON"},
                    // 方塊底色
                    $(go.Shape, "Square",
                        {width: 40, height: 40, strokeWidth: 2, fill: "white", stroke: "#919191", portId: ""}),
                    $(go.Panel,
                        { // for each attribute show a Shape at a particular place in the overall square
                            itemTemplate:
                                $(go.Panel,
                                    $(go.Shape,
                                        {stroke: null, strokeWidth: 0},
                                        new go.Binding("fill", "", attrFill),
                                        new go.Binding("geometry", "", maleGeometry)),
                                ),
                            margin: 1
                        },
                        new go.Binding("itemArray", "a")
                    ),
                    $(go.Panel, "Table",
                        // 佔位用, 之後可擴充四個角落文字, 重點要指定寬度
                        $(go.TextBlock, {row: 0, column: 0, width: 20, text: '', textAlign: 'center'}),
                        $(go.TextBlock, // margin:t,r,b,l
                            {row: 0, column: 1, textAlign: 'center', width: 20, margin: new go.Margin(6,0,0,0), text: ''},
                            new go.Binding("text", "tr")),
                        $(go.TextBlock,
                            {row: 1, column: 0, textAlign: 'center', width: 20, margin: new go.Margin(5,0,0,0), text: ''},
                            new go.Binding("text", "bl")),
                    ),
                ),
                $(go.TextBlock,
                    {textAlign: "center", margin: new go.Margin(2), maxSize: new go.Size(80, NaN)},
                    new go.Binding("text", "n"))
            ));

        myDiagram.nodeTemplateMap.add("F",  // female
            $(go.Node, "Vertical",
                {locationSpot: go.Spot.Center, locationObjectName: "ICON", selectionObjectName: "ICON"},
                $(go.Panel,
                    {name: "ICON"},
                    $(go.Shape, "Circle",
                        {width: 40, height: 40, strokeWidth: 2, fill: "white", stroke: "#a1a1a1", portId: ""}),
                    $(go.Panel,
                        { // for each attribute show a Shape at a particular place in the overall circle
                            itemTemplate:
                                $(go.Panel,
                                    $(go.Shape,
                                        {stroke: null, strokeWidth: 0},
                                        new go.Binding("fill", "", attrFill),
                                        new go.Binding("geometry", "", femaleGeometry))
                                ),
                            margin: 1
                        },
                        new go.Binding("itemArray", "a")
                    ),
                    $(go.Panel, "Table",
                        // 佔位用, 之後可擴充四個角落文字, 重點要指定寬度
                        $(go.TextBlock, {row: 0, column: 0, width: 20, text: '', textAlign: 'center'}),
                        $(go.TextBlock, // margin:t,r,b,l
                            // 女生圓形, width 稍微往左移動
                            {row: 0, column: 1, textAlign: 'center', width: 14, margin: new go.Margin(6,0,0,0), text: ''},
                            new go.Binding("text", "tr")),
                        $(go.TextBlock,
                            // 女生圓形, margin 稍微往右上移動
                            {row: 1, column: 0, textAlign: 'center', width: 20, margin: new go.Margin(4,0,0,2), text: ''},
                            new go.Binding("text", "bl")),
                    ),
                ),
                $(go.TextBlock,
                    {textAlign: "center", margin: new go.Margin(2), maxSize: new go.Size(80, NaN)},
                    new go.Binding("text", "n"))
            ));

        // the representation of each label node -- nothing shows on a Marriage Link
        myDiagram.nodeTemplateMap.add("LinkLabel",
            $(go.Node, {selectable: false, width: 1, height: 1, fromEndSegmentLength: 20}));


        myDiagram.linkTemplate =  // for parent-child relationships
            $(go.Link,
                {
                    routing: go.Link.Orthogonal, corner: 5,
                    layerName: "Background", selectable: false,
                    fromSpot: go.Spot.Bottom, toSpot: go.Spot.Top
                },
                $(go.Shape, {stroke: "#424242", strokeWidth: 2})
            );

        myDiagram.linkTemplateMap.add("Marriage",  // for marriage relationships
            $(go.Link,
                {selectable: false},
                $(go.Shape, {strokeWidth: 2.5, stroke: "#5d8cc1" /* blue */})
            ));

        myDiagram.linkTemplateMap.add("同住",  // for 同住 relationships
            $(go.Link,
                {
                    routing: go.Link.Orthogonal, corner: 5,
                    layerName: "Background", selectable: false,
                    fromSpot: go.Spot.Bottom, toSpot: go.Spot.Top
                },
                $(go.Shape, {stroke: "#424242", strokeWidth: 2, strokeDashArray: [4,2]})
            ));


        // n: name, s: sex, m: mother, f: father, ux: wife, vir: husband, a: attributes/markers
        // console.log('setupDiagram', params);
        setupDiagram(myDiagram, params.data, params.focus_id);
        // setupDiagram(myDiagram, [
        //         {key: 0, n: "父", s: "M", m: -10, f: -11, ux: 1, a: ["A", "C", "F", "K"], tr: 'A', bl: '1'},
        //         {key: 1, n: "母", s: "F", m: -12, f: -13, a: ["A", "H", "K"], tr: 'B', bl: '2'},
        //         // { key: 30, n: "岳父", s: "M", m: -14, f: -15, ux: 31, a: ["C", "F", "K"] },
        //         // { key: 31, n: "岳母", s: "F", m: -16, f: -17, a: ["H", "K"] },
        //         {key: 29, n: "同住人", s: "M", m: 0, f: 1, r:'同住', a: ["A", "C", "H"], tr: 'C', bl: '3'},
        //         {key: 24, n: "1兄", s: "M", m: 1, f: 0, ux: 25, a: ["A", "C", "H", "L"], tr: 'D', bl: '4'},
        //         {key: 25, n: "1兄嫂", s: "F", a: ["A", "C"], tr: 'E', bl: '5'},
        //         {key: 4, n: "個案", s: "M", m: 1, f: 0, ux: 5, a: ["A", "E", "H"], tr: 'F', bl: '6'},
        //         {key: 5, n: "配偶", s: "F", m: 31, f: 30, a: ["B", "H", "L"], tr: 'G', bl: '7'},
        //         {key: 2, n: "1弟", s: "M", m: 1, f: 0, ux: 3, a: ["B","C", "H", "L"], tr: 'H', bl: '8'},
        //         {key: 3, n: "1弟媳", s: "F", a: ["A", "C"], tr: 'I', bl: '1'},
        //         // { key: 7, n: "大姨", s: "F", m: 31, f: 30, a: ["C", "I"] },
        //         // { key: 8, n: "大姨1", s: "F", m: 31, f: 30, vir: 9, a: ["E"] },
        //         // { key: 9, n: "襟兄1", s: "M", a: ["H"] },
        //         // { key: 10, n: "Ellie", s: "F", m: 3, f: 2, a: ["E", "G"] },
        //         // { key: 11, n: "Dan", s: "M", m: 3, f: 2, a: ["J"] },
        //         {key: 12, n: "1媳婦", s: "F", vir: 13, a: ["A", "J"], tr: 'J', bl: '2'},
        //         {key: 13, n: "1兒子", s: "M", m: 5, f: 4, a: ["A", "H"], tr: 'K', bl: '3'},
        //         {key: 14, n: "1女兒", s: "F", m: 5, f: 4, a: ["A", "E", "G"], tr: 'L', bl: '4'},
        //         {key: 23, n: "1女婿", s: "M", ux: 14, a: ["B","E", "G"], tr: 'M', bl: '5'},
        //         // { key: 15, n: "Evan", s: "M", m: 8, f: 9, a: ["F", "H"] },
        //         // { key: 16, n: "Ethan", s: "M", m: 8, f: 9, a: ["D", "K"] },
        //         // { key: 17, n: "Eve", s: "F", vir: 16, a: ["F", "L"] },
        //         // { key: 18, n: "Emily", s: "F", m: 8, f: 9 },
        //         // { key: 19, n: "Fred", s: "M", m: 17, f: 16, a: [] },
        //         // { key: 20, n: "Faith", s: "F", m: 17, f: 16, a: ["L"] },
        //         {key: 21, n: "孫女", s: "F", m: 12, f: 13, a: ["B","H"], tr: 'N', bl: '6'},
        //         {key: 22, n: "孫子", s: "M", m: 12, f: 13, a: ["B","H"], tr: 'O', bl: '7'},
        //         {key: 6, n: "1妹", s: "F", m: 1, f: 0, vir: 26, a: ["B","C"], tr: 'P', bl: '8'},
        //         {key: 26, n: "1妹婿", s: "M", a: ["A","C"], tr: 'P', bl: '1'},
        //         {key: 27, n: "外孫女", s: "F", m: 14, f: 23, a: ["A", "H"], tr: 'P', bl: '2'},
        //         {key: 28, n: "外孫子", s: "M", m: 14, f: 23, a: ["A", "H"], tr: 'P', bl: '3'},
        //         {key: 29, n: "單親1", s: "M", f:28, a: ["A", "H"], tr: 'P', bl: '3'},
        //         {key: 30, n: "單親2", s: "M", m:28, a: ["A", "H"], tr: 'P', bl: '3'},
        //
        //         // "父"'s ancestors
        //         {key: -10, n: "祖父", s: "M", m: -33, f: -32, ux: -11, a: ["A", "S"]},
        //         {key: -11, n: "祖母", s: "F", a: ["B","E", "S"]},
        //         // { key: -32, n: "Paternal Great", s: "M", ux: -33, a: ["F", "H", "S"] },
        //         // { key: -33, n: "Paternal Great", s: "F", a: ["S"] },
        //         // { key: -40, n: "Great Uncle", s: "M", m: -33, f: -32, a: ["F", "H", "S"] },
        //         // { key: -41, n: "Great Aunt", s: "F", m: -33, f: -32, a: ["I", "S"] },
        //         // { key: -20, n: "伯父", s: "M", m: -11, f: -10, a: ["A", "S"] },
        //
        //         // "母"'s ancestors
        //         // { key: -12, n: "外祖父", s: "M", ux: -13, a: ["D", "L", "S"] },
        //         // { key: -13, n: "外祖母", s: "F", m: -31, f: -30, a: ["H", "S"] },
        //         // { key: -21, n: "Aunt", s: "F", m: -13, f: -12, a: ["C", "I"] },
        //         // { key: -22, n: "Uncle", s: "M", ux: -21 },
        //         // { key: -23, n: "Cousin", s: "M", m: -21, f: -22 },
        //         // { key: -30, n: "Maternal Great", s: "M", ux: -31, a: ["D", "J", "S"] },
        //         // { key: -31, n: "Maternal Great", s: "F", m: -50, f: -51, a: ["H", "L", "S"] },
        //         // { key: -42, n: "Great Uncle", s: "M", m: -30, f: -31, a: ["C", "J", "S"] },
        //         // { key: -43, n: "Great Aunt", s: "F", m: -30, f: -31, a: ["E", "G", "S"] },
        //         // { key: -50, n: "Maternal Great Great", s: "F", ux: -51, a: ["D", "I", "S"] },
        //         // { key: -51, n: "Maternal Great Great", s: "M", a: ["H", "S"] },
        //
        //         // "岳父"'s ancestors
        //         // { key: -14, n: "太岳父", s: "M", ux: -15, a: ["D", "L", "S"] },
        //         // { key: -15, n: "太岳母", s: "F", a: ["H", "S"] },
        //
        //         // "岳母"'s ancestors
        //         // { key: -16, n: "外太岳父", s: "M", ux: -17, a: ["D", "L", "S"] },
        //         // { key: -17, n: "外太岳母", s: "F", a: ["H", "S"] },
        //     ],
        //     4 /* focus on this person */);
    }


    // create and initialize the Diagram.model given an array of node data representing people
    function setupDiagram(diagram, array, focusId) {
        diagram.model =
            go.GraphObject.make(go.GraphLinksModel,
                { // declare support for link label nodes
                    linkLabelKeysProperty: "labelKeys",
                    // this property determines which template is used
                    nodeCategoryProperty: "s",
                    // if a node data object is copied, copy its data.a Array
                    copiesArrays: true,
                    // create all of the nodes for people
                    nodeDataArray: array
                });
        setupMarriages(diagram);
        setupParents(diagram);

        var node = diagram.findNodeForKey(focusId);
        if (node !== null) {
            diagram.select(node);
            // remove any spouse for the person under focus:
            //node.linksConnected.each(function(l) {
            //  if (!l.isLabeledLink) return;
            //  l.opacity = 0;
            //  var spouse = l.getOtherNode(node);
            //  spouse.opacity = 0;
            //  spouse.pickable = false;
            //});
        }
    }

    function findMarriage(diagram, a, b) {  // A and B are node keys
        var nodeA = diagram.findNodeForKey(a);
        var nodeB = diagram.findNodeForKey(b);
        if (nodeA !== null && nodeB !== null) {
            var it = nodeA.findLinksBetween(nodeB);  // in either direction
            while (it.next()) {
                var link = it.value;
                // Link.data.category === "Marriage" means it's a marriage relationship
                if (link.data !== null && link.data.category === "Marriage") return link;
            }
        }
        return null;
    }

    // now process the node data to determine marriages
    function setupMarriages(diagram) {
        var model = diagram.model;
        var nodeDataArray = model.nodeDataArray;
        for (var i = 0; i < nodeDataArray.length; i++) {
            var data = nodeDataArray[i];
            var key = data.key;
            var uxs = data.ux;
            if (uxs !== undefined) {
                if (typeof uxs === "number") uxs = [uxs];
                for (var j = 0; j < uxs.length; j++) {
                    var wife = uxs[j];
                    if (key === wife) {
                        // or warn no reflexive marriages
                        continue;
                    }
                    var link = findMarriage(diagram, key, wife);
                    if (link === null) {
                        // add a label node for the marriage link
                        var mlab = {s: "LinkLabel"};
                        model.addNodeData(mlab);
                        // add the marriage link itself, also referring to the label node
                        var mdata = {from: key, to: wife, labelKeys: [mlab.key], category: "Marriage"};
                        model.addLinkData(mdata);
                    }
                }
            }
            var virs = data.vir;
            if (virs !== undefined) {
                if (typeof virs === "number") virs = [virs];
                for (var j = 0; j < virs.length; j++) {
                    var husband = virs[j];
                    if (key === husband) {
                        // or warn no reflexive marriages
                        continue;
                    }
                    var link = findMarriage(diagram, key, husband);
                    if (link === null) {
                        // add a label node for the marriage link
                        var mlab = {s: "LinkLabel"};
                        model.addNodeData(mlab);
                        // add the marriage link itself, also referring to the label node
                        var mdata = {from: key, to: husband, labelKeys: [mlab.key], category: "Marriage"};
                        model.addLinkData(mdata);
                    }
                }
            }
        }
    }

    // process parent-child relationships once all marriages are known
    function setupParents(diagram) {
        var model = diagram.model;
        var nodeDataArray = model.nodeDataArray;
        for (var i = 0; i < nodeDataArray.length; i++) {
            var data = nodeDataArray[i];
            var key = data.key;
            var mother = data.m;
            var father = data.f;
            if (mother !== undefined && father !== undefined) {
                var link = findMarriage(diagram, mother, father);
                if (link === null) {
                    // or warn no known mother or no known father or no known marriage between them
                    if (window.console) window.console.log("unknown marriage: " + mother + " & " + father);
                    continue;
                }
                var mdata = link.data;
                var mlabkey = mdata.labelKeys[0];
                var cdata = {from: mlabkey, to: key, category: data.r};
                // console.log('cdata1', cdata);
                myDiagram.model.addLinkData(cdata);
            }
            else if(father !== undefined || mother !== undefined) {
                var cdata = {from: father || mother, to: key, category: data.r};
                myDiagram.model.addLinkData(cdata);
            }
        }
    }


    // A custom layout that shows the two families related to a person's parents
    function GenogramLayout() {
        go.LayeredDigraphLayout.call(this);
        // this.initializeOption = go.LayeredDigraphLayout.InitDepthFirstIn; // 不要指定會照順序畫
        this.spouseSpacing = 30;  // minimum space between spouses
    }

    go.Diagram.inherit(GenogramLayout, go.LayeredDigraphLayout);

    GenogramLayout.prototype.makeNetwork = function (coll) {
        // generate LayoutEdges for each parent-child Link
        var net = this.createNetwork();
        if (coll instanceof go.Diagram) {
            this.add(net, coll.nodes, true);
            this.add(net, coll.links, true);
        } else if (coll instanceof go.Group) {
            this.add(net, coll.memberParts, false);
        } else if (coll.iterator) {
            this.add(net, coll.iterator, false);
        }
        return net;
    };

    // internal method for creating LayeredDigraphNetwork where husband/wife pairs are represented
    // by a single LayeredDigraphVertex corresponding to the label Node on the marriage Link
    GenogramLayout.prototype.add = function (net, coll, nonmemberonly) {
        var multiSpousePeople = new go.Set();
        // consider all Nodes in the given collection
        var it = coll.iterator;
        while (it.next()) {
            var node = it.value;
            if (!(node instanceof go.Node)) continue;
            if (!node.isLayoutPositioned || !node.isVisible()) continue;
            if (nonmemberonly && node.containingGroup !== null) continue;
            // if it's an unmarried Node, or if it's a Link Label Node, create a LayoutVertex for it
            if (node.isLinkLabel) {
                // get marriage Link
                var link = node.labeledLink;
                var spouseA = link.fromNode;
                var spouseB = link.toNode;
                // create vertex representing both husband and wife
                var vertex = net.addNode(node);
                // now define the vertex size to be big enough to hold both spouses
                vertex.width = spouseA.actualBounds.width + this.spouseSpacing + spouseB.actualBounds.width;
                vertex.height = Math.max(spouseA.actualBounds.height, spouseB.actualBounds.height);
                vertex.focus = new go.Point(spouseA.actualBounds.width + this.spouseSpacing / 2, vertex.height / 2);
            } else {
                // don't add a vertex for any married person!
                // instead, code above adds label node for marriage link
                // assume a marriage Link has a label Node
                var marriages = 0;
                node.linksConnected.each(function (l) {
                    if (l.isLabeledLink) marriages++;
                });
                if (marriages === 0) {
                    var vertex = net.addNode(node);
                } else if (marriages > 1) {
                    multiSpousePeople.add(node);
                }
            }
        }
        // now do all Links
        it.reset();
        while (it.next()) {
            var link = it.value;
            if (!(link instanceof go.Link)) continue;
            if (!link.isLayoutPositioned || !link.isVisible()) continue;
            if (nonmemberonly && link.containingGroup !== null) continue;
            // if it's a parent-child link, add a LayoutEdge for it
            if (!link.isLabeledLink) {
                var parent = net.findVertex(link.fromNode);  // should be a label node
                var child = net.findVertex(link.toNode);
                if (child !== null) {  // an unmarried child
                    net.linkVertexes(parent, child, link);
                } else {  // a married child
                    link.toNode.linksConnected.each(function (l) {
                        if (!l.isLabeledLink) return;  // if it has no label node, it's a parent-child link
                        // found the Marriage Link, now get its label Node
                        var mlab = l.labelNodes.first();
                        // parent-child link should connect with the label node,
                        // so the LayoutEdge should connect with the LayoutVertex representing the label node
                        var mlabvert = net.findVertex(mlab);
                        if (mlabvert !== null) {
                            net.linkVertexes(parent, mlabvert, link);
                        }
                    });
                }
            }
        }

        while (multiSpousePeople.count > 0) {
            // find all collections of people that are indirectly married to each other
            var node = multiSpousePeople.first();
            var cohort = new go.Set();
            this.extendCohort(cohort, node);
            // then encourage them all to be the same generation by connecting them all with a common vertex
            var dummyvert = net.createVertex();
            net.addVertex(dummyvert);
            var marriages = new go.Set();
            cohort.each(function (n) {
                n.linksConnected.each(function (l) {
                    marriages.add(l);
                })
            });
            marriages.each(function (link) {
                // find the vertex for the marriage link (i.e. for the label node)
                var mlab = link.labelNodes.first()
                var v = net.findVertex(mlab);
                if (v !== null) {
                    net.linkVertexes(dummyvert, v, null);
                }
            });
            // done with these people, now see if there are any other multiple-married people
            multiSpousePeople.removeAll(cohort);
        }
    };

    // collect all of the people indirectly married with a person
    GenogramLayout.prototype.extendCohort = function (coll, node) {
        if (coll.has(node)) return;
        coll.add(node);
        var lay = this;
        node.linksConnected.each(function (l) {
            if (l.isLabeledLink) {  // if it's a marriage link, continue with both spouses
                lay.extendCohort(coll, l.fromNode);
                lay.extendCohort(coll, l.toNode);
            }
        });
    };

    GenogramLayout.prototype.assignLayers = function () {
        go.LayeredDigraphLayout.prototype.assignLayers.call(this);
        var horiz = this.direction == 0.0 || this.direction == 180.0;
        // for every vertex, record the maximum vertex width or height for the vertex's layer
        var maxsizes = [];
        this.network.vertexes.each(function (v) {
            var lay = v.layer;
            var max = maxsizes[lay];
            if (max === undefined) max = 0;
            var sz = (horiz ? v.width : v.height);
            if (sz > max) maxsizes[lay] = sz;
        });
        // now make sure every vertex has the maximum width or height according to which layer it is in,
        // and aligned on the left (if horizontal) or the top (if vertical)
        this.network.vertexes.each(function (v) {
            var lay = v.layer;
            var max = maxsizes[lay];
            if (horiz) {
                v.focus = new go.Point(0, v.height / 2);
                v.width = max;
            } else {
                v.focus = new go.Point(v.width / 2, 0);
                v.height = max;
            }
        });
        // from now on, the LayeredDigraphLayout will think that the Node is bigger than it really is
        // (other than the ones that are the widest or tallest in their respective layer).
    };

    GenogramLayout.prototype.commitNodes = function () {
        go.LayeredDigraphLayout.prototype.commitNodes.call(this);
        // position regular nodes
        this.network.vertexes.each(function (v) {
            if (v.node !== null && !v.node.isLinkLabel) {
                v.node.position = new go.Point(v.x, v.y);
            }
        });
        // position the spouses of each marriage vertex
        var layout = this;
        this.network.vertexes.each(function (v) {
            if (v.node === null) return;
            if (!v.node.isLinkLabel) return;
            var labnode = v.node;
            var lablink = labnode.labeledLink;
            // In case the spouses are not actually moved, we need to have the marriage link
            // position the label node, because LayoutVertex.commit() was called above on these vertexes.
            // Alternatively we could override LayoutVetex.commit to be a no-op for label node vertexes.
            lablink.invalidateRoute();
            var spouseA = lablink.fromNode;
            var spouseB = lablink.toNode;
            // prefer fathers on the left, mothers on the right
            if (spouseA.data.s === "F") {  // sex is female
                var temp = spouseA;
                spouseA = spouseB;
                spouseB = temp;
            }
            // see if the parents are on the desired sides, to avoid a link crossing
            var aParentsNode = layout.findParentsMarriageLabelNode(spouseA);
            var bParentsNode = layout.findParentsMarriageLabelNode(spouseB);
            if (aParentsNode !== null && bParentsNode !== null && aParentsNode.position.x > bParentsNode.position.x) {
                // swap the spouses
                var temp = spouseA;
                spouseA = spouseB;
                spouseB = temp;
            }
            spouseA.position = new go.Point(v.x, v.y);
            spouseB.position = new go.Point(v.x + spouseA.actualBounds.width + layout.spouseSpacing, v.y);
            if (spouseA.opacity === 0) {
                var pos = new go.Point(v.centerX - spouseA.actualBounds.width / 2, v.y);
                spouseA.position = pos;
                spouseB.position = pos;
            } else if (spouseB.opacity === 0) {
                var pos = new go.Point(v.centerX - spouseB.actualBounds.width / 2, v.y);
                spouseA.position = pos;
                spouseB.position = pos;
            }
        });
        // position only-child nodes to be under the marriage label node
        this.network.vertexes.each(function (v) {
            if (v.node === null || v.node.linksConnected.count > 1) return;
            var mnode = layout.findParentsMarriageLabelNode(v.node);
            if (mnode !== null && mnode.linksConnected.count === 1) {  // if only one child
                var mvert = layout.network.findVertex(mnode);
                var newbnds = v.node.actualBounds.copy();
                newbnds.x = mvert.centerX - v.node.actualBounds.width / 2;
                // see if there's any empty space at the horizontal mid-point in that layer
                var overlaps = layout.diagram.findObjectsIn(newbnds, function (x) {
                    return x.part;
                }, function (p) {
                    return p !== v.node;
                }, true);
                if (overlaps.count === 0) {
                    v.node.move(newbnds.position);
                }
            }
        });
    };

    GenogramLayout.prototype.findParentsMarriageLabelNode = function (node) {
        var it = node.findNodesInto();
        while (it.next()) {
            var n = it.value;
            if (n.isLinkLabel) return n;
        }
        return null;
    };
    // end GenogramLayout class

    // window.addEventListener('DOMContentLoaded', init);
    init();
}
