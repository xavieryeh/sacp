<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // TODO 刪除機制(unique)
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('email')->unique(); // FIXME 不能 unique,因為 softDelete
            $table->string('department', 100);
            $table->string('roles', 200);
            $table->boolean('is_enabled');
            $table->timestamp('last_login_at')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('password_1')->nullable();
            $table->string('password_2')->nullable();
            $table->string('password_3')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });

        $user = new User();
        $user->name='管理員';
        $user->department='毒防局';
        $user->email='admin@dsacp.kcg.gov.tw';
        $user->roles='系統管理者';
        $user->password = Hash::make('!QAZ2wsx#EDC');
        $user->is_enabled = true;
        $user->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
