@extends('layouts.index')

@section('title', 'Banner')

@section('breadcrumb')
    <!-- breadcrumb start -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">首頁</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                壹、本市查獲毒品概況分析
            </li>
        </ol>
    </nav>
    <!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                壹、本市查獲毒品概況分析
            </h3>
            <div class="main-card-contenet">
                <div class="row banner-itemBox">
                    <div class="col-12 col-md-6">
                        <p class="main-card-titleSub titleSubDeco">月報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、查獲施用毒品(刑罰)初犯人數同期比較
                                </p>
                                {{-- <div class="card-item-pic" style="width:420px;height:330px"> --}}
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'SelectCrimialMonth']) !!}">
                                        <div id="chart1"></div>
                                    </a>
                                    <p class="CommentText" id="SelectCrimialMonthText"></p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                  二、查獲施用毒品(行政罰)初犯人數同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'SelectAdministrativeMonth']) !!}">
                                        <div id="chart2"></div>
                                    </a>
                                    <p class="CommentText" id="SelectAdministrativeMonthText"></p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                            </div>
                            <div class="card-item" >
                                <p class="card-item-title">
                                  三、查獲毒品上游(製造、運輸、販賣)人數同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'SelectPersonMonth']) !!}">
                                        <div id="chart3"></div>
                                    </a>
                                    <p class="CommentText" id="SelectPersonMonthText"></p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    四、各級毒品查獲重量
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'SelectWeightMonth']) !!}">
                                        <div id="chart4"></div>
                                    </a>
                                    <p class="CommentText" id="SelectWeightMonthText">&nbsp;</p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    五、各級毒品查獲人數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'SelectNumMonth']) !!}">
                                        <div id="chart5"></div>
                                    </a>
                                    <p class="CommentText" id="SelectNumMonthText">&nbsp;</p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    六、各級毒品查獲件數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'SelectCaseMonth']) !!}">
                                        <div id="chart6"></div>
                                    </a>
                                    <p class="CommentText" id="SelectCaseMonthText">&nbsp;</p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    七、各級毒品查獲重量同期比較
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPage',['id'=>'SelectWeightContrastMonth']) !!}">
                                        <div id="chart7"></div>
                                    </a>
                                    <p class="CommentText" id="SelectWeightContrastMonthText"></p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    八、各級毒品查獲人數同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'SelectPeosonContrastMonth']) !!}">
                                        <div id="chart8"></div>
                                    </a>
                                    <p class="CommentText" id="SelectPeosonContrastMonthText"></p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    九、各級毒品查獲件數同期比較
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPage',['id'=>'SelectCaseContrastMonth']) !!}">
                                        <div id="chart9"></div>
                                    </a>
                                    <p class="CommentText" id="SelectCaseContrastMonthText"></p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-6 col-line-left">
                            <p class="main-card-titleSub titleSubDeco">年報</p>
                            <div class="card-itemBox">
                              <div class="card-item">
                                <p class="card-item-title">
                                  一、查獲施用毒品(刑罰)初犯人數同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'SelectCrimialYear']) !!}">
                                        <div id="chartY1"></div>
                                    </a>
                                    <p class="CommentText" id="SelectCrimialYearText">&nbsp;</p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                  二、查獲施用毒品(行政罰)初犯人數同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'SelectAdministrativeYear']) !!}">
                                        <div id="chartY2"></div>
                                    </a>
                                    <p class="CommentText" id="SelectAdministrativeYearText">&nbsp;</p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                  三、查獲毒品上游(製造、運輸、販賣)人數同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'SelectPersonYear']) !!}">
                                        <div id="chartY3"></div>
                                    </a>
                                    <p class="CommentText" id="SelectPersonYearText">&nbsp;</p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    四、各級毒品查獲重量同期比較
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPageforYear',['id'=>'SelectWeightContrastYear']) !!}">
                                        <div id="chartY4"></div>
                                    </a>
                                    <p class="CommentText" id="SelectWeightContrastYearText">&nbsp;</p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    五、各級毒品查獲人數同期比較
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPageforYear',['id'=>'SelectPeosonContrastYear']) !!}">
                                        <div id="chartY5"></div>
                                    </a>
                                    <p class="CommentText" id="SelectPeosonContrastYearText">&nbsp;</p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    六、各級毒品查獲件數同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'SelectCaseContrastYear']) !!}">
                                        <div id="chartY6"></div>
                                    </a>
                                    <p class="CommentText" id="SelectCaseContrastYearText">&nbsp;</p>
                                    <p class="main-card-info">來源 : 高雄市政府警察局</p>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->


@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/PoisonAnalysis.js')}}"></script>
<script>
am4core.addLicense("{{config('custom.ChartsPN')}}");
$.Log("壹、本市查獲毒品概況分析","進入頁面");
getPoisonAnalysischart("SelectCrimialMonth","chart1");
getPoisonAnalysischart("SelectCrimialYear","chartY1");
getPoisonAnalysischart("SelectAdministrativeMonth","chart2");
getPoisonAnalysischart("SelectAdministrativeYear","chartY2");
getPoisonAnalysischart("SelectPersonMonth","chart3");
getPoisonAnalysischart("SelectPersonYear","chartY3");
getPoisonAnalysischart("SelectWeightMonth","chart4");
getPoisonAnalysischart("SelectNumMonth","chart5");
getPoisonAnalysischart("SelectCaseMonth","chart6");
getPoisonAnalysischart("SelectWeightContrastMonth","chart7");
getPoisonAnalysischart("SelectWeightContrastYear","chartY4");
getPoisonAnalysischart("SelectPeosonContrastMonth","chart8")
;getPoisonAnalysischart("SelectPeosonContrastYear","chartY5");
getPoisonAnalysischart("SelectCaseContrastMonth","chart9");
getPoisonAnalysischart("SelectCaseContrastYear","chartY6");
</script>
@endsection
