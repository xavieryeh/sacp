@extends('layouts.index')

@section('title', 'Banner')

@section('breadcrumb')
    <!-- breadcrumb start -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">首頁</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                捌、本市非春暉(中輟、離校)概況分析
            </li>
        </ol>
    </nav>
    <!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                捌、本市非春暉(中輟、離校)概況分析
            </h3>
            <div class="main-card-contenet">
                <div class="row banner-itemBox">
                    <div class="col-12 col-md-6">
                        <p class="main-card-titleSub titleSubDeco">月報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、性別統計
                                </p>
                                <div class="card-item-pic">
                                    <a  href="{!! route('bannerPage',['id'=>'TeenagerInterruptDrugGenderByMonth']) !!}">
                                        <div id="chart1"></div>
                                        {{-- <div id="DrugGenderByMonth" style="height:220px;"></div> --}}
                                        {{-- <div id="DrugGenderByMonthTable" class="cell-border" style="margin-top:-15px;width:50%;position: relative;"></div> --}}
                                    </a>
                                    <p class="CommentText" id="TeenagerInterruptDrugGenderByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    二、年齡統計
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerInterruptDrugAgeByMonth']) !!}">
                                        <div id="chart2"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerInterruptDrugAgeByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    三、藥物濫用類型統計
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerInterruptDrugAbuseTypeByMonth']) !!}">
                                        <div id="chart3"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerInterruptDrugAbuseTypeByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    四、年齡層與類型交叉分析
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerInterruptDrugAgeTypeByMonth']) !!}">
                                        <div id="chart4"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerInterruptDrugAgeTypeByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    五、個案區域(戶籍地)
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerInterruptDrugAreaByMonth']) !!}">
                                        <div id="chart5"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerInterruptDrugAreaByMonthText"></p>
                                    <p></p>
                                    <p class="main-card-info">來源：高雄市政府教育局、教育部高雄市聯絡處</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-line-left">
                        <p class="main-card-titleSub titleSubDeco">年報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、性別同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerInterruptDrugGenderByYear']) !!}">
                                        <div id="chartY1"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerInterruptDrugGenderByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    二、年齡層同期比較
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerInterruptDrugAgeByYear']) !!}">
                                        <div id="chartY2"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerInterruptDrugAgeByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    三、藥物濫用類型同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerInterruptDrugAbuseTypeByYear']) !!}">
                                        <div id="chartY3"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerInterruptDrugAbuseTypeByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    四、年齡層與類型交叉同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerInterruptDrugAgeTypeByYear']) !!}">
                                        <div id="chartY4"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerInterruptDrugAgeTypeByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    五、個案區域(戶籍地)
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerInterruptDrugAreaByYear']) !!}">
                                        <div id="chartY5"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerInterruptDrugAreaByYearText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->


@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/TeenagerInterruptDrugCase.js')}}"></script>
<script>
am4core.addLicense("{{config('custom.ChartsPN')}}");
$.Log("捌、本市非春暉(中輟、離校)概況分析","進入頁面");
getTeenagerInterruptDrugCase("TeenagerInterruptDrugGenderByMonth","chart1");
getTeenagerInterruptDrugCase("TeenagerInterruptDrugGenderByYear","chartY1");
getTeenagerInterruptDrugCase("TeenagerInterruptDrugAgeByMonth","chart2");
getTeenagerInterruptDrugCase("TeenagerInterruptDrugAgeByYear","chartY2");
getTeenagerInterruptDrugCase("TeenagerInterruptDrugAbuseTypeByMonth","chart3");
getTeenagerInterruptDrugCase("TeenagerInterruptDrugAbuseTypeByYear","chartY3");
getTeenagerInterruptDrugCase("TeenagerInterruptDrugAgeTypeByMonth","chart4");
getTeenagerInterruptDrugCase("TeenagerInterruptDrugAgeTypeByYear","chartY4");
getTeenagerInterruptDrugCase("TeenagerInterruptDrugAreaByMonth","chart5");
getTeenagerInterruptDrugCase("TeenagerInterruptDrugAreaByYear","chartY5");
</script>
@endsection
