@extends('layouts.index')

@section('title', 'Banner Page')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item">
            <a id="bannerhref" href="">月報或年報</a>
        </li>
        <li id="breadcrumbtitle" class="breadcrumb-item active" aria-current="page">

        </li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox" >
        <div class="main-card">
            <h3 class="main-card-title">
                <form action="" id="title">
            </h3>
            <div class="main-card-contenet" style="display:flex;flex-direction:column;">
                <div  id="downloadcahrt" class="largechart" style="width:1050px;height:650px;align-self:center;">
                    <div class="card-item-pic" style="width: 1000px;height:600px;margin:0 auto">
                        <div id="chart_large"></div>
                        {{-- <div id="{{$id}}" style="height: 70%"></div>
                        <div id="{{$id}}Table" class="cell-border" style="margin-top:-15px"></div> --}}

                    </div>
                    <div style="margin-top:15px;">
                        <p class="CommentText" id="{{$id}}Text"></p>
                        <p id="source" class="main-card-info"></p>
                    </div>
                </div>
                <form>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <select class="form-control" id="selectStartYear">
                                    <option>請選擇年度</option>
                                    <?php
                                    $optionlist = array();
                                    for($i=date("Y")-10;$i<=date("Y")+10;$i++){
                                        array_push($optionlist,$i-1911);
                                    }
                                    foreach ($optionlist as $key => $value) {
                                        echo "<option>". $value."</option>";
                                    }
                                    ?>

                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <select class="form-control" id="selectEndYear">
                                    <option>請選擇年度</option>
                                    <?php
                                    $optionlist = array();
                                    for($i=date("Y")-10;$i<=date("Y")+10;$i++){
                                        array_push($optionlist,$i-1911);
                                    }
                                    foreach ($optionlist as $key => $value) {
                                        echo "<option>". $value."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex justify-content-center">
                        <input type="button" onclick="download()" class="btn btn-primary btn-single" value="下載">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/PoisonAnalysis.js')}}"></script>
<script type="text/javascript" src="{{asset('js/DrugRecidivismRateChart.js')}}"></script>
<script type="text/javascript" src="{{asset('js/TaiwanDrugAnalysis.js')}}"></script>
<script type="text/javascript" src="{{asset('js/DrugTypeAnalysis.js')}}"></script>
<script type="text/javascript" src="{{asset('js/DrugCase.js')}}"></script>
<script type="text/javascript" src="{{asset('js/TeenagerDrugCase.js')}}"></script>
<script type="text/javascript" src="{{asset('js/TeenagerChunhuiDrugCase.js')}}"></script>
<script type="text/javascript" src="{{asset('js/TeenagerInterruptDrugCase.js')}}"></script>
<script type="text/javascript" src="{{asset('js/JuvenileCourtDrugCase.js')}}"></script>
<script type="text/javascript" src="{{asset('js/SpecificArea.js')}}"></script>
<script>
am4core.addLicense("{{config('custom.ChartsPN')}}");
getPoisonAnalysischart("{{$id}}","chart_large");
getDrugRecidivismRateChartid("{{$id}}","chart_large");
getTaiwanDrugAnalysischart("{{$id}}","chart_large");
getDrugTypeAnalysischart("{{$id}}","chart_large");
getDrugCase("{{$id}}","chart_large");
getTeenagerDrugCase("{{$id}}","chart_large");
getTeenagerChunhuiDrugCase("{{$id}}","chart_large");
getTeenagerInterruptDrugCase("{{$id}}","chart_large");
getJuvenileCourtDrugCase("{{$id}}","chart_large");
getSpecificArea("{{$id}}","chart_large");
$("#selectStartYear").on('change',function(){
        changeduring();
    });
$("#selectEndYear").change(function(){
        changeduring();
});
setTimeout(() => {
    $.Log(title.innerHTML,"點擊放大");
}, 1000);
function changeduring(){
    getPoisonAnalysischart("{{$id}}","chart_large");
    getDrugRecidivismRateChartid("{{$id}}","chart_large");
    getTaiwanDrugAnalysischart("{{$id}}","chart_large");
    getDrugTypeAnalysischart("{{$id}}","chart_large");
    getDrugCase("{{$id}}","chart_large");
    getTeenagerDrugCase("{{$id}}","chart_large");
    getTeenagerChunhuiDrugCase("{{$id}}","chart_large");
    getTeenagerInterruptDrugCase("{{$id}}","chart_large");
    getJuvenileCourtDrugCase("{{$id}}","chart_large");
    getSpecificArea("{{$id}}","chart_large");
    var reg = /^\d+$/;
    if (document.getElementById("selectStartYear").value.match(reg) && document.getElementById("selectEndYear").value.match(reg)) {
            $.Log(title.innerHTML,"調整時間");
        }
}
function download(){
    $.Log(title.innerHTML,"下載圖片");
    domtoimage.toBlob(document.getElementById('downloadcahrt'),{bgcolor:"#fff",height:700})
    .then(function (blob) {
        window.saveAs(blob, 'my-node.png');
    });
}
</script>
@endsection
