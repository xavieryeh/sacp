@extends('layouts.index')

@section('title', '個案管理')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('index')}}">首頁</a></li>
        <li class="breadcrumb-item active">
            <a id="breadcrumb_KSarea">{{$areaName}}</a>
            {{-- <a id="bannerhref" href=""></a> --}}
        </li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                個案管理
            </h3>
            <div class="main-card-contenet">
                <div class="row align-items-center">
                    <div class="col-12 col-md-4">
                        <div class="chart" id="chart1" style="height: 300px"></div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="chart" id="chart2" style="height: 300px"></div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="chart" id="chart3" style="height: 300px"></div>
                    </div>
                </div>
                <ul class="KSarea-list">
                    @foreach ($Villages as $Village)
                        <li><a href="{{route('KSarea.getKSareaPage')}}?areaNo={{$areaNo}}_{{$Village->District_Code}}">{{$Village->District_Name}}</a></li>
                    @endforeach
                </ul>
                <div class="btnBox">
                    <a href="index" class="btn btn-primary">
                        返回主畫面
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->

@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/KSarea.js')}}"></script>
<script>
    var self = {};
    self.ChartsPN = "{{config('custom.ChartsPN')}}";
    self.areaName = "{{$areaName}}";

    var KSarea = new KSarea(self);
</script>
@endsection
