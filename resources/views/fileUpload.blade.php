@extends('layouts.index')

@section('title', '檔案上傳')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item"><a href="upload">檔案管理</a></li>
        <li class="breadcrumb-item active" aria-current="page">檔案上傳</li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                檔案列表
            </h3>
            <div class="main-card-contenet">
                <p class="main-card-memo">請選擇以下項目進入詳細列表</p>
                <ul class="linkList">
                    <li><a href="{{route('Upload.getUploadPage')}}?Type=LNI">地方網絡資料</a></li>
                    <li><a href="{{route('Upload.getUploadPage')}}?Type=CCMS">中央資料</a></li>
                    <li><a href="{{route('Upload.getUploadPage')}}?Type=CCMS1">中央案管系統</a></li>
                    <li><a href="{{route('Upload.getUploadPage')}}?Type=CPCMS">兒少保案管系統</a></li>
                    <li><a href="{{route('Upload.getUploadPage')}}?Type=LTAFL">三四級講習</a></li>
                    <li><a href="{{route('Upload.getUploadPage')}}?Type=VR">訪視記錄</a></li>
            {{--    <li><a href="{{route('Upload.getUploadPage')}}?Type=CPCMS2">春暉中斷</a></li>--}}
                    <li><a href="{{route('Upload.getUploadPage')}}?Type=CPCMS3">監所銜接輔導</a></li>
                    <li><a href="{{route('Upload.getUploadPage')}}?Type=CPCMS4">特定營業場所</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>
</style>
@endsection

@section('custom-script')
<script>
    $.Log("檔案管理","檔案上傳頁面載入");
</script>
@endsection
