<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>高雄市政府毒品防制局智慧毒防</title>
    <link rel="icon" href="images/favicon.ico" />

    <meta name="keywords" content="關鍵字,關鍵字" />
    <meta name="description" content="網站描述" />
    <meta property="og:description" content="網站描述" />
    <meta property="og:title" content="高雄市政府毒品防制局智慧毒防" />
    <meta property="og:site_name" content="高雄市政府毒品防制局智慧毒防" />
    <meta property="og:image" content="images/ogimg.jpg" />

    @include('includes.styles')
    @yield('custom-style')
</head>

<body>
        <!-- content start -->
        <div class="content">
            @yield('content')
            <!-- footer start -->
            <div class="footer">
                <p class="copyright">
                    高雄市政府毒品防制局版權所有 <br />
                    Copyright © All Rights Reserved.
                </p>
            </div>
            <!-- footer end -->
        </div>
        <!-- content end -->

    @include('includes.scripts')
    @yield('custom-script')
</body>

</html>
