<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>高雄市政府毒品防制局智慧毒防</title>
    <link rel="icon" href="{{asset('asset/images/favicon.ico')}}" />

    <meta name="keywords" content="關鍵字,關鍵字" />
    <meta name="description" content="網站描述" />
    <meta property="og:description" content="網站描述" />
    <meta property="og:title" content="高雄市政府毒品防制局智慧毒防" />
    <meta property="og:site_name" content="高雄市政府毒品防制局智慧毒防" />
    <meta property="og:image" content="images/ogimg.jpg" />

    @include('includes.styles')
    @yield('custom-style')
</head>

<body>
    <div class="wrapper">
        <!-- header start -->
        <div class="header">
            <div class="nav-top">
                <a href="{{route('index')}}" class="logo"><img src="{{asset('asset/images/logo.jpg')}}" alt="" /></a>
                <div class="people">
                    <div class="people-name js-people-name">{{Auth::user()->name}}</div>
                    <div class="people-popupBox hidden">
                        <div class="people-popup">
                            <div class="people-name js-people-name">{{Auth::user()->name}}</div>
                            <div class="people-info">
                                <p>{{Auth::user()->name}}</p>
                                <p>{{Auth::user()->email}}</p>
                                <div class="people-popup-btn">
                                    <a class="btn btn-outline-info" href="{{route('member.change_password')}}">變更密碼</a>
                                    <form method="post" action="{{route('logout')}}">
                                        @csrf
                                        <button class="btn btn-outline-info" type="submit">登出</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="menuBtn" id="menuBtn"></div>
            </div>

            <!-- nav start -->
            <div class="nav">
                <div class="xx" id="xx"></div>
                <div class="nav_content">
                    <ul class="menu" id="menu">
                        <li><a href="{{route('index')}}">主畫面</a></li>
                        <li><a href="{{route('Case.getCase')}}">個案管理</a></li>
                        {{--<li><a href="#">特營管理</a></li>--}}
                        <li><a href="{{route('upload')}}">檔案管理</a></li>
                        <li><a href="{{route('Report.getReport')}}">產出報表</a></li>
                        <li><a href="{{route('system.index')}}">系統管理</a></li>
                    </ul>
                </div>
            </div>
            <!-- nav end -->
            @yield('breadcrumb')
        </div>
        <!-- header end -->

        <!-- mainKV start -->

        <!-- mainKV end -->

        <!-- content start -->
        <div class="content">
            @yield('content')
            <!-- other start -->
            <section class="other">
                <!-- slickBox start -->
                <div class="slickBox">
                    <!-- slick start -->
                    <div class="slick slick-body">
                        <div class="slick-item">
                            <a href="KaohsiungPoisonAnalysis">壹、本市查獲 <br />毒品概況分析</a>
                        </div>
                        <div class="slick-item">
                            <a href="KaohsiungAndTaiwanDrugRecidivismRateAnalysis">貳、本市及全國藥癮個案 <br />再犯率比較分析</a>
                        </div>
                        <div class="slick-item">
                            <a href="TaiwanDrugAnalysis">參、全國藥癮個案 <br />統計分析</a>
                        </div>
                        <div class="slick-item">
                            <a href="DrugTypeAnalysis">肆、本市藥癮個案 <br />收案類型分析</a>
                        </div>
                        <div class="slick-item">
                            <a href="DrugCase">伍、本市18歲(含)以上 <br />藥癮個案概況分析</a>
                        </div>
                        <div class="slick-item">
                            <a href="TeenagerDrugCase">陸、本市17 歲(含)以下 <br />藥癮個案概況分析</a>
                        </div>
                        <div class="slick-item">
                            <a href="TeenagerChunhuiDrugCase">柒、本市學生 <br />春暉專案概況分析</a>
                        </div>
                        <div class="slick-item">
                            <a href="TeenagerInterruptDrugCase">捌、本市非春暉(中輟、離校) <br />概況分析</a>
                        </div>
                        <div class="slick-item">
                            <a href="JuvenileCourtDrugCase">玖、本市少年法庭 <br />毒品案件統計分析</a>
                        </div>
                        <div class="slick-item">
                            <a href="SpecificArea">拾、本市特定營業場所 <br />列管概況分析</a>
                        </div>
                        <div class="slick-item">
                            <a href="{{route('Analyze.index')}}">拾壹、國內外 <br />情勢分析</a>
                        </div>
                    </div>
                    <!-- slick end -->
                </div>
                <!-- slickBox end -->
            </section>
            <!-- other end -->
            <!-- footer start -->
            <div class="footer">
                <p class="copyright">
                    高雄市政府毒品防制局版權所有 <br />
                    Copyright © All Rights Reserved.
                </p>
            </div>
            <!-- footer end -->
        </div>
        <!-- content end -->

    </div>

    @include('includes.scripts')
    @yield('custom-script')
</body>

</html>
