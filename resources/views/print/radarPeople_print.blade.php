@extends('layouts.print')

@section('title', '個案管理')

@section('content')
    <!-- main start -->
    <section class="block block-centerY">
        <!-- main-cardBox start -->
        <div class="main-card" id="main_cardBox">
            <h3 class="main-card-title">
                雷達圖與文字雲
            </h3>
            <div class="main-card-contenet">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="chart" id="chart1" style="height: 300px"></div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="chart" id="chart2" style="height: 300px"></div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="chart" id="chart3" style="height: 300px"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main-cardBox end -->
    </section>
    <!-- main end -->

@endsection

@section('custom-style')
    <style>

    </style>
@endsection

@section('custom-script')
{{--    <script type="text/javascript" src="{{asset('js/CasePage.js')}}"></script>--}}
    <script>
        var self = {};
        self.ChartsPN = "{{config('custom.ChartsPN')}}";
        self.caseNo = "{{$caseNo}}";
        function CasePage(self){
            makeWCChart();
            // 只載入雷達圖 降低附載
            function makeWCChart() {
                am4core.ready(function () {
                    // Themes begin
                    // am4core.unuseAllThemes();
                    // am4core.useTheme(am4themes_material);
                    // am4core.unuseTheme(am4themes_animated);
                    am4core.addLicense(self.ChartsPN);
                    // Themes end
                    const id = "chart3";
                    // Create chart instance
                    var chart = am4core.registry.baseSprites.find(function (chartObj) {
                        return chartObj.htmlContainer.id === id;
                    });

                    // $file = file('database/wordcloud.txt');
                    $.PostAJAX("Case_casePageOwnTeacherProtectionFactor", {
                        caseNo: self.caseNo
                    }, response => {
                        if (chart === undefined) {
                            var Params = {};
                            Params.ChartType = "WordCloud";
                            Params.Data = response.chart3.map(function (each) {
                                return {
                                    category: each.category,
                                    count: each.count
                                };
                            });
                            MakeAMChart(id, Params);
                        } else {
                            var NewChartData = response.chart3.map(function (each) {
                                return {
                                    category: each.category,
                                    count: each.count,
                                };
                            });

                            chart.data = NewChartData;
                            chart.invalidateRawData();
                        }

                        var chart3 = [{
                            id: "chart1",
                            title: "風險因子",
                            color: "#E98440"
                        }, {
                            id: "chart2",
                            title: "保護因子",
                            color: "#3FBEEC"
                        }];
                        chart3.forEach((subChart, iSub) => {
                            var sub_chart = am4core.registry.baseSprites.find(function (
                                chartObj) {
                                return chartObj.htmlContainer.id === subChart.id;
                            });
                            if (sub_chart === undefined) {
                                var Params = {};
                                Params.ChartType = "Radar";
                                Params.Title = {
                                    title: subChart.title,
                                    fonSize: 15,
                                    color: subChart.color,
                                    align: "center",
                                    valign: "top",
                                    dy: -10
                                };
                                Params.FontSize = 10;
                                Params.name = subChart.title;
                                Params.color = subChart.color;
                                Params.Data = response[subChart.id].map(function (each, i) {
                                    return {
                                        category: each.category,
                                        count: each.count
                                    };
                                });
                                MakeAMChart(subChart.id, Params);
                            } else {
                                var NewChartData = response[subChart.id].map(function (each, i) {
                                    return {
                                        category: each.category,
                                        count: each.count
                                    };
                                });

                                sub_chart.data = NewChartData;
                                sub_chart.invalidateRawData();
                            }
                        });
                    });
                }); // end am4core.ready()
            }
        }
        var CasePage = new CasePage(self);

        $(document).ready(function () {
            setTimeout(function () {
                download();
            },1000*5) ; // 雷達圖5秒
        });

        {{-- 檔案下載 列印 --}}
        function download(){
            var div_id = 'main_cardBox' ;
            domtoimage.toBlob(document.getElementById(div_id)).then(function (blob) {
                window.saveAs(blob, 'people-radar.png');
                // 3秒後確認
                setTimeout(function(){
                    var c = confirm('是否下載完成?');
                    if(c){
                        window.close();
                    }else{
                        // 重新呼叫
                        download();
                    }
                },1000*3);
            });
        }
    </script>
@endsection
