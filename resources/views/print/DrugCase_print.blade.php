@extends('layouts.print')

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox" id="main">
        <div class="main-card">
            <h3 class="main-card-title">
                本市18歲(含)以上藥癮個案概況分析
            </h3>
            <div class="main-card-contenet">
                <div class="row banner-itemBox">
                    <div class="col-12 col-md-6">
                        <p class="main-card-titleSub titleSubDeco">月報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、性別統計
                                </p>
                                <div class="card-item-pic">
                                    <a  href="{!! route('bannerPage',['id'=>'DrugGenderByMonth']) !!}">
                                        <div id="chart1"></div>
                                        {{-- <div id="DrugGenderByMonth" style="height:220px;"></div> --}}
                                        {{-- <div id="DrugGenderByMonthTable" class="cell-border" style="margin-top:-15px;width:50%;position: relative;"></div> --}}
                                    </a>
                                    <p class="CommentText" id="DrugGenderByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    二、年齡層統計
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'DrugAgeByMonth']) !!}">
                                        <div id="chart2"></div>
                                    </a>
                                    <p class="CommentText" id="DrugAgeByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    三、藥物濫用類型統計
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'DrugAbuseTypeByMonth']) !!}">
                                        <div id="chart3"></div>
                                    </a>
                                    <p class="CommentText" id="DrugAbuseTypeByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    四、年齡層與類型交叉分析
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'DrugAgeTypeByMonth']) !!}">
                                        <div id="chart4"></div>
                                    </a>
                                    <p class="CommentText" id="DrugAgeTypeByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    五、個案區域(戶籍地)未校正數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'DrugAreaByMonth']) !!}">
                                        <div id="chart5"></div>
                                    </a>
                                    <p class="CommentText" id="DrugAreaByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    六、個案區域(戶籍地)校正數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'DrugCorrectionAreaByMonth']) !!}">
                                        <div id="chart6"></div>
                                    </a>
                                    <p class="CommentText" id="DrugCorrectionAreaByMonthText"></p>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    七、本市藥癮個案列管追蹤輔導數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'DrugCoachByMonth']) !!}">
                                        <div id="chart7"></div>
                                    </a>
                                    <p class="CommentText" id="DrugCoachByMonthText"></p>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    八、本市藥癮個案轉介服務數
                                </p>
                                <div class="card-item-pic">
                                    <a  href="{!! route('bannerPage',['id'=>'DrugServiceByMonth']) !!}">
                                        <div id="chart8"></div>
                                    </a>
                                    <p class="CommentText" id="DrugServiceByMonthText"></p>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    九、監所銜接輔導數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'DrugCouplingByMonth']) !!}">
                                        <div id="chart9"></div>
                                    </a>
                                    <p class="CommentText" id="DrugCouplingByMonthText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    十、醫療戒治累計數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'DrugMedicalByMonth']) !!}">
                                        <div id="chart10"></div>
                                    </a>
                                    <p class="CommentText" id="DrugMedicalByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府衛生局</p>
                                </div>
                            </div>

                            <div class="card-item">
                                <p class="card-item-title">
                                    十一、24小時專線服務類型累計數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'Drug24HoursByMonth']) !!}">
                                        <div id="chart11"></div>
                                    </a>
                                    <p class="CommentText" id="Drug24HoursByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府衛生局</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-line-left">
                        <p class="main-card-titleSub titleSubDeco">年報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、性別同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'DrugGenderByYear']) !!}">
                                        <div id="chartY1"></div>
                                    </a>
                                    <p class="CommentText" id="DrugGenderByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    二、年齡層同期比較
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPageforYear',['id'=>'DrugAgeByYear']) !!}">
                                        <div id="chartY2"></div>
                                    </a>
                                    <p class="CommentText" id="DrugAgeByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    三、藥物濫用類型同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'DrugAbuseTypeByYear']) !!}">
                                        <div id="chartY3"></div>
                                    </a>
                                    <p class="CommentText" id="DrugAbuseTypeByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    四、年齡層與類型交叉同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'DrugAgeTypeByYear']) !!}">
                                        <div id="chartY4"></div>
                                    </a>
                                    <p class="CommentText" id="DrugAgeTypeByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    五、個案區域(戶籍地)未校正數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'DrugAreabyYear']) !!}">
                                        <div id="chartY5"></div>
                                    </a>
                                    <p class="CommentText" id="DrugAreabyYearText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    六、個案區域(戶籍地)校正數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'DrugCorrectionAreaByYear']) !!}">
                                        <div id="chartY6"></div>
                                    </a>
                                    <p class="CommentText" id="DrugCorrectionAreaByYearText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    七、本市藥癮個案列管追蹤輔導數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'DrugCoachByYear']) !!}">
                                        <div id="chartY7"></div>
                                    </a>
                                    <p class="CommentText" id="DrugCoachByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    八、本市藥癮個案轉介服務數
                                </p>
                                <div class="card-item-pic">
                                    <a  href="{!! route('bannerPage',['id'=>'DrugServiceByYear']) !!}">
                                        <div id="chartY8"></div>
                                    </a>
                                    <p class="CommentText" id="DrugServiceByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    九、監所銜接輔導數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'DrugCouplingByYear']) !!}">
                                        <div id="chartY9"></div>
                                    </a>
                                    <p class="CommentText" id="DrugCouplingByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    十、醫療戒治累計數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'DrugMedicalByYear']) !!}">
                                        <div id="chartY10"></div>
                                    </a>
                                    <p class="CommentText" id="DrugMedicalByYearText"></p>
                                    <p class="main-card-info">來源：高雄市政府衛生局</p>
                                </div>
                            </div>

                            <div class="card-item">
                                <p class="card-item-title">
                                    十一、24小時專線服務類型累計數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'Drug24HoursByYear']) !!}">
                                        <div id="chartY11"></div>
                                    </a>
                                    <p class="CommentText" id="Drug24HoursByYearText"></p>
                                    <p class="main-card-info">來源：高雄市政府衛生局</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->

<input type="hidden" id="selectYear" value="{{$selectYear}}">
<input type="hidden" id="selectMonth" value="{{$selectMonth}}">
<input type="hidden" id="selectStartYear" value="{{($selectYear-2)}}">
<input type="hidden" id="selectEndYear" value="{{$selectYear}}">
@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/DrugCase.js')}}"></script>
<script>
am4core.addLicense("{{config('custom.ChartsPN')}}");
getDrugCase("DrugGenderByMonth","chart1");
getDrugCase("DrugGenderByYear","chartY1");
getDrugCase("DrugAgeByMonth","chart2");
getDrugCase("DrugAgeByYear","chartY2");
getDrugCase("DrugAbuseTypeByMonth","chart3");
getDrugCase("DrugAbuseTypeByYear","chartY3");
getDrugCase("DrugAgeTypeByMonth","chart4");
getDrugCase("DrugAgeTypeByYear","chartY4");
getDrugCase("DrugAreaByMonth","chart5");
getDrugCase("DrugAreabyYear","chartY5");
getDrugCase("DrugCorrectionAreaByMonth","chart6");
getDrugCase("DrugCorrectionAreaByYear","chartY6");
getDrugCase("DrugCoachByMonth","chart7");
getDrugCase("DrugCoachByYear","chartY7");
getDrugCase("DrugServiceByMonth","chart8");
getDrugCase("DrugServiceByYear","chartY8");
getDrugCase("DrugCouplingByMonth","chart9");
getDrugCase("DrugCouplingByYear","chartY9");
getDrugCase("DrugMedicalByMonth","chart10");
getDrugCase("DrugMedicalByYear","chartY10");
getDrugCase("Drug24HoursByMonth","chart11");
getDrugCase("Drug24HoursByYear","chartY11");

{{-- 檔案下載 列印 --}}
function download(){
    domtoimage.toBlob(document.getElementById('main')).then(function (blob) {
        window.saveAs(blob, 'my-DrugCase.png');
        // alert('假裝列印');
        setInterval(function(){
            var c = confirm('是否下載完成?');
            if(c){
                window.close();
            }else{
                // 強制執行下載
                domtoimage.toBlob(document.getElementById('main')).then(function (blob) {
                    window.saveAs(blob, 'my-DrugCase.png');
                });
            }
        },1000*3); //確認是否完成下載
    });
}

$(document).ready(function () {
    setTimeout(function () {
        download();
    },1000*10) ; // 5秒後下載
});
</script>
@endsection
