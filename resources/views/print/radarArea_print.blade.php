@extends('layouts.print')

@section('title', '個案管理')

@section('content')
    <!-- main start -->
    <section class="block block-centerY">
        <!-- main-cardBox start -->
        <div class="main-cardBox" id="main_cardBox">
            <div class="main-card">
                <h3 class="main-card-title">
                    個案管理
                </h3>
                <div class="main-card-contenet">
                    <div class="row align-items-center">
                        <div class="col-12 col-md-4">
                            <div class="chart" id="chart1" style="height: 300px"></div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="chart" id="chart2" style="height: 300px"></div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="chart" id="chart3" style="height: 300px"></div>
                        </div>
                    </div>
                    <ul class="KSarea-list">
                        @foreach ($Villages as $Village)
                            <li><a href="{{route('KSarea.getKSareaPage')}}?areaNo={{$areaNo}}_{{$Village->District_Code}}">{{$Village->District_Name}}</a></li>
                        @endforeach
                    </ul>

                </div>
            </div>
        </div>
        <!-- main-cardBox end -->
    </section>
    <!-- main end -->

@endsection

@section('custom-style')
    <style>

    </style>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{asset('js/KSarea.js')}}"></script>
    <script>
        var self = {};
        self.ChartsPN = "{{config('custom.ChartsPN')}}";
        self.areaName = "{{$areaName}}";

        var KSarea = new KSarea(self);

        $(document).ready(function () {
            setTimeout(function () {
                download();
            },1000*5) ; // 雷達圖5秒
        });

        {{-- 檔案下載 列印 --}}
        function download(){
            var div_id = 'main_cardBox' ;
            domtoimage.toBlob(document.getElementById(div_id)).then(function (blob) {
                window.saveAs(blob, 'area-radar.png');
                // 3秒後確認
                setTimeout(function(){
                    var c = confirm('是否下載完成?');
                    if(c){
                        window.close();
                    }else{
                        // 重新呼叫
                        download();
                    }
                },1000*3);
            });
        }
    </script>
@endsection
