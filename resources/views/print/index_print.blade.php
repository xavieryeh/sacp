@extends('layouts.print')

@section('title', 'Home Page')

@section('content')
    <div class="content-index">
        <!-- title start -->
        <section class="title">
            <h1>智慧毒防 <span style="color: yellow">數位治理 X 大數據</span></h1>

        </section>
        <!-- title end -->
        <!-- main start -->
        <section class="main" id="main">
            <!-- main-cardBox start -->
            <div class="main-cardBox">
                <div class="main-cardBoxItem">
                    <div class="main-card js-popupBtn-open-1">
                        {{-- <div class="main-card-btn"></div> --}}
                        <h3 id="chartTitle1" class="main-card-title main-card-title-s"></h3>
                        <div class="main-card-contenet">
                            <div class="chart" id=chart1></div>
                            <div id="chartText1"></div>
                            {{-- <img src="images/chart_1.jpg" alt="" /> --}}
                        </div>
                        <p id="chartSource1" class="main-card-info"></p>
                    </div>
                </div>
                <div class="main-cardBoxItem">
                    <div class="main-card js-popupBtn-open-2">
                        {{-- <div class="main-card-btn"></div> --}}
                        <h3 id="chartTitle2" class="main-card-title main-card-title-s"></h3>
                        <div class="main-card-contenet">
                            <div class="chart" id=chart2></div>
                            <div id="chartText2"></div>
                            {{-- <img src="images/chart_2.jpg" alt="" /> --}}
                        </div>
                        <p id="chartSource2" class="main-card-info"></p>
                    </div>
                </div>
                <div class="main-cardBoxItem">
                    <div class="main-card js-popupBtn-open-3">
                        {{-- <div class="main-card-btn js-popupBtn-open-3"></div> --}}
                        <h3 id="chartTitle3" class="main-card-title main-card-title-s"></h3>
                        <div class="main-card-contenet">
                            <div class="chart" id=chart3></div>
                            <div id="chartText3"></div>
                            {{-- <img src="images/chart_3.jpg" alt="" /> --}}
                        </div>
                        <p id="chartSource3" class="main-card-info"></p>
                    </div>
                </div>
            </div>
            <!-- main-cardBox end -->

            <!-- main-map start -->
            <div class="main-map">
                <!-- map-notice starr -->
                <div class="map-notice">
                    <div class="map-notice-top">
                        {{-- <h2 class="map-notice-title">毒品防制局</h2> --}}
                        <div class="map-notice-itemBox">
                            <div class="map-notice-item">
                                <p class="map-notice-item-top-title">109年個案總數</p>
                                <p id="caseCount" class="map-notice-item-top-number"></p>
                            </div>
                            <div class="map-notice-item">
                                <p class="map-notice-item-top-title">本月新增</p>
                                <p id="newCount" class="map-notice-item-top-number"></p>
                            </div>
                        </div>
                    </div>

                    <div class="map-notice-bottom">
                        <div class="map-notice-itemBox">
                            <div class="map-notice-item">
                                <p class="map-notice-item-bottom-title">行政區個案人數</p>
                                <div id="noticeQuantity" class="map-notice-item-bottom-quantity">
                                    {{-- <li>1</li>
                                <li>50</li>
                                <li>100</li>
                                <li>150</li>
                                <li>200</li> --}}
                                </div>
                            </div>
                            {{-- <div class="map-notice-item">
                                <p class="map-notice-item-bottom-title">本月新增</p>
                                <div class="todayMark"></div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <!-- map-notice end -->

                <!-- map-area start -->
                <div class="map-area">
                    {{-- <div class="todayMarkBox"></div> --}}
                </div>
                <!-- map-area end -->

                <!-- map-list start -->
                <ol id="mapList" class="map-list"></ol>
                <!-- map-list end -->
            </div>
            <!-- main-map end -->

            <!-- main-cardBox start -->
            <div class="main-cardBox">
                <div class="main-cardBoxItem">
                    <div class="main-card js-popupBtn-open-4">
                        {{-- <div class="main-card-btn"></div> --}}
                        <div class="main-card-contenet">
                            <div class="chart" id=chart4></div>
                            {{-- <img src="images/chart_4.jpg" alt="" /> --}}
                        </div>
                    </div>
                </div>
                <div class="main-cardBoxItem">
                    <div class="main-card js-popupBtn-open-5">
                        {{-- <div class="main-card-btn"></div> --}}
                        <h3 id="chartTitle5" class="main-card-title main-card-title-s"></h3>
                        <div class="main-card-contenet">
                            <div class="chart" id=chart5></div>
                            <div id="chartText5"></div>
                            {{-- <img src="images/chart_5.jpg" alt="" /> --}}
                        </div>
                        <p id="chartSource5" class="main-card-info"></p>
                    </div>
                </div>
                <div class="main-cardBoxItem">
                    <div class="main-card js-popupBtn-open-6">
                        {{-- <div class="main-card-btn"></div> --}}
                        <h3 id="chartTitle6" class="main-card-title main-card-title-s"></h3>
                        <div class="main-card-contenet">
                            <div class="chart" id=chart6></div>
                            <div id="chartText6"></div>
                            {{-- <img src="images/chart_6.jpg" alt="" /> --}}
                        </div>
                        <p id="chartSource6" class="main-card-info"></p>
                    </div>
                </div>
            </div>
            <!-- main-cardBox end -->
        </section>
        <!-- main end -->

        <!-- popup start -->
        <div class="popupBox js-popupBox-1">
            <div class="popup">
                <div class="main-card">
                    <div class="main-card-btn js-popupBtn-close"></div>
                    <h3 id="chartTitle1_popup" class="main-card-title"></h3>
                    <div class="main-card-contenet">
                        <div id=chart1_popup></div>
                        <div id="chartText1_popup"></div>
                        {{-- <img src="images/chart_1.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource1_popup" class="main-card-info"></p>
                </div>
            </div>
        </div>
        <!-- popup end -->

        <!-- popup start -->
        <div class="popupBox js-popupBox-2">
            <div class="popup">
                <div class="main-card">
                    <div class="main-card-btn js-popupBtn-close"></div>
                    <h3 id="chartTitle2_popup" class="main-card-title"></h3>
                    <div class="main-card-contenet">
                        <div id=chart2_popup></div>
                        <div id="chartText2_popup"></div>
                        {{-- <img src="images/chart_2.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource2_popup" class="main-card-info"></p>
                </div>
            </div>
        </div>
        <!-- popup end -->

        <!-- popup start -->
        <div class="popupBox js-popupBox-3">
            <div class="popup">
                <div class="main-card">
                    <div class="main-card-btn js-popupBtn-close"></div>
                    <h3 id="chartTitle3_popup" class="main-card-title"></h3>
                    <div class="main-card-contenet">
                        <div id=chart3_popup></div>
                        <div id="chartText3_popup"></div>
                        {{-- <img src="images/chart_3.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource3_popup" class="main-card-info"></p>
                </div>
            </div>
        </div>
        <!-- popup end -->

        <!-- popup start -->
        <div class="popupBox js-popupBox-4">
            <div class="popup" id="popupBox_4">
                <div class="main-card">
                    <div class="main-card-btn js-popupBtn-close"></div>
                    <h3 class="main-card-title">

                    </h3>
                    <div class="main-card-contenet">
                        <div id=chart4_popup></div>
                        {{-- <img src="images/chart_4.jpg" alt="" /> --}}
                    </div>
                </div>
            </div>
        </div>
        <!-- popup end -->

        <!-- popup start -->
        <div class="popupBox js-popupBox-5">
            <div class="popup">
                <div class="main-card">
                    <div class="main-card-btn js-popupBtn-close"></div>
                    <h3 id="chartTitle5_popup" class="main-card-title"></h3>
                    <div class="main-card-contenet">
                        <div id=chart5_popup></div>
                        <div id="chartText5_popup"></div>
                        {{-- <img src="images/chart_5.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource5_popup" class="main-card-info"></p>
                </div>
            </div>
        </div>
        <!-- popup end -->

        <!-- popup start -->
        <div class="popupBox js-popupBox-6">
            <div class="popup">
                <div class="main-card">
                    <div class="main-card-btn js-popupBtn-close"></div>
                    <h3 id="chartTitle6_popup" class="main-card-title"></h3>
                    <div class="main-card-contenet">
                        <div id=chart6_popup></div>
                        <div id="chartText6_popup"></div>
                        {{-- <img src="images/chart_6.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource6_popup" class="main-card-info"></p>
                </div>
            </div>
        </div>
        <!-- popup end -->
    </div>
    @if(($action??false)=='home')
    <input type="hidden" id="selectYear" value="{{$selectYear}}">
    <input type="hidden" id="selectMonth" value="{{$selectMonth}}">
    @elseif(($action??false)=='radar')
    <input type="hidden" id="selectStartYear" value="{{($selectStartYear)}}">
    <input type="hidden" id="selectEndYear" value="{{$selectEndYear}}">
    <input type="hidden" id="selectStartMonth" value="{{$selectStartMonth}}">
    <input type="hidden" id="selectEndMonth" value="{{$selectEndMonth}}">
    @endif
@endsection

@section('custom-style')
<style>
    .chart {
        display: block;
    }

    .map-block {
        display: flex;
        justify-content: center;
    }

    .chart3-sub-block {
        display: flex;
        justify-content: space-between;
        height: 50%;
    }

    .chart3-sub-block_popup {
        display: flex;
        justify-content: center;
        height: 50%;
    }

    .PieTable {
        height: 220px;
        display: flex;
        justify-content: center;
        align-items: flex-start;
    }

    .PieTable .chart {
        width: 60%;
    }

    .PieTable .cTable {
        /* width:30%; */
        align-self: flex-end;
    }

    .PieTable_popup {
        display: flex;
        justify-content: center;
        align-items: flex-start;
    }

    .PieTable_popup .chart {
        width: 60%;
    }

    .PieTable_popup .cTable {
        /* width:30%; */
        align-self: flex-end;
    }

</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/PoisonAnalysis.js')}}"></script>
<script type="text/javascript" src="{{asset('js/DrugRecidivismRateChart.js')}}"></script>
<script type="text/javascript" src="{{asset('js/TaiwanDrugAnalysis.js')}}"></script>
<script type="text/javascript" src="{{asset('js/DrugTypeAnalysis.js')}}"></script>
<script type="text/javascript" src="{{asset('js/DrugCase.js')}}"></script>
@if(($action??false)=='home')
<script type="text/javascript" src="{{asset('js/Index.js')}}"></script>
@endif
<script>
    var self = {};
    self.ChartsPN = "{{config('custom.ChartsPN')}}";
    self.KSareaHref = "{{route('KSarea.getKSarea')}}";
    {{-- 只呼叫雷達圖 降低附載--}}
            @if(($action??false)=='radar')
            function IndexPage(self) {
                var eventText = "";
                self.init = function () {
                    // 文字雲、雷達圖
                    self.makeWCChart("chart4");
                    $(".js-popupBtn-open-4").click(function () {
                        $.Log("主畫面", "右上小圖放大");
                        eventText = "右上小圖";
                        $(".js-popupBox-4").attr({
                            style: " display: flex;"
                        });
                        setTimeout(() => {
                            self.makeWCChart("chart4_popup");
                        }, 100);
                        return false;
                    });
                    $(".js-popupBtn-close").click(function () {
                        $.Log("主畫面", eventText + "放大視窗關閉");
                        $(".popupBox").hide();
                        return false;
                    });
                }
                self.makeWCChart = function(div) {
                    am4core.ready(function () {
                        // <div class="chart" id="WCChart" style="height: 50%"></div>
                        $("#"+div).css('height','100%');
                        // const divHeight = $("#"+div).parent().height()<300?300:$("#"+div).parent().height()-(div.indexOf('popup')>-1?80:0);
                        const id = div+"_WC";
                        if($("#"+div).html()==""){
                            $("#"+div).html('<div name="mChart" id="'+id+'" style="height:50%;width:100%;"></div>\
                    <div class="'+(div.indexOf('popup')>-1?'chart3-sub-block_popup':'chart3-sub-block')+'">\
                        <div name="mChart" id="'+id+'_sub1" style="height:100%;width:100%"></div>\
                        <div name="mChart" id="'+id+'_sub2" style="height:100%;width:100%"></div>\
                    </div>');
                        }
                        // Create chart instance
                        var chart = am4core.registry.baseSprites.find(function (chartObj) {
                            return chartObj.htmlContainer.id === id;
                        });

                        // $file = file('database/wordcloud.txt');
                        $.PostAJAX("Index_wordCloud", {
                            selectStartYear:$('#selectStartYear').val(),
                            selectStartMonth:$('#selectStartMonth').val(),
                            selectEndYear:$('#selectEndYear').val(),
                            selectEndMonth:$('#selectEndMonth').val(),
                        }, response => {
                            if (chart === undefined) {
                                var Params = {};
                                Params.ChartType = "WordCloud";
                                Params.Data = response.wordCloud.map(function (each) {
                                    return {
                                        category: each.category,
                                        count: each.count
                                    };
                                });
                                MakeAMChart(id, Params);
                            } else {
                                // var NewChartData = response.wordCloud.map(function (each) {
                                //     return {
                                //         category: each.category,
                                //         count: each.count,
                                //     };
                                // });

                                // chart.data = NewChartData;
                                // chart.invalidateRawData();
                            }

                            var chart3 = [{
                                id: id+'_sub1',
                                title: "風險因子",
                                color: "#E98440"
                            }, {
                                id: id+'_sub2',
                                title: "保護因子",
                                color: "#3FBEEC"
                            }]
                            chart3.forEach((subChart, iSub) => {
                                var sub_chart = am4core.registry.baseSprites.find(function (
                                    chartObj) {
                                    return chartObj.htmlContainer.id === subChart.id;
                                });
                                if (sub_chart === undefined) {
                                    var Params = {};
                                    Params.ChartType = "Radar";
                                    // Params.Title = {
                                    //     title: subChart.title,
                                    //     fonSize: 10,
                                    //     color: subChart.color,
                                    //     align: "center",
                                    //     valign: "top",
                                    //     dy: -20
                                    // };
                                    Params.FontSize = 8;
                                    Params.name = subChart.title;
                                    Params.color = subChart.color;
                                    Params.Data = response["wordCloudSub" + (iSub + 1)
                                        .toString()].map(function (each, i) {
                                        return {
                                            category: each.category,
                                            count: each.count
                                        };
                                    });
                                    MakeAMChart(subChart.id, Params);
                                } else {
                                    var NewChartData = response["wordCloudSub" + (iSub + 1)
                                        .toString()].map(function (each, i) {
                                        return {
                                            category: each.category,
                                            count: each.count
                                        };
                                    });

                                    sub_chart.data = NewChartData;
                                    sub_chart.invalidateRawData();
                                }
                            });
                        });
                    }); // end am4core.ready()
                }
                return self;
            }
            @endif
    var IndexPage = new IndexPage(self);
    IndexPage.init();

    $(document).ready(function () {
        @if(($action??false)=='radar')
        $(".js-popupBtn-open-4").click();
        @endif

        setTimeout(function () {
            download();
        },1000*{{$action=='radar'?5:($action=='home'?10:0)}}) ; // 雷達圖5秒 首頁10
    });

    {{-- 檔案下載 列印 --}}
    function download(){
        var div_id = '{{$action=='radar'?'popupBox_4':($action=='home'?'main':'')}}' ;
        domtoimage.toBlob(document.getElementById(div_id)).then(function (blob) {
            window.saveAs(blob, '{{$action=='radar'?'all-radar':($action=='home'?'my-index':'')}}.png');
            // 3秒後確認
            setTimeout(function(){
                var c = confirm('是否下載完成?');
                if(c){
                    window.close();
                }else{
                    // 重新呼叫
                    download();
                }
            },1000*3);
        });
    }
</script>
@endsection
