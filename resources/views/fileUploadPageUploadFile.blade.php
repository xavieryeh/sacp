@extends('layouts.index')

@section('title', '檔案上傳')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item"><a href="upload">檔案管理</a></li>
        <li class="breadcrumb-item"><a href="fileUpload">檔案上傳</a></li>
        <li class="breadcrumb-item"><a href="{{route('Upload.getUploadPage')}}?Type={{$Type}}">{{$Title}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">檔案上傳</li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                上傳檔案
            </h3>
            <div class="main-card-contenet">
                <!-- form start -->
                <form>
                    <div class="form-group">
                        <label class="sr-only" for="selFileType">請選擇欲上傳檔案類別</label>
                        <select id="selFileType" class="form-control">
                            <option value="LNI" @if ($Type == "LNI") selected @endif>地方網絡資料</option>
                            <option value="CCMS" @if ($Type == "CCMS") selected @endif>中央資料</option>
                            <option value="CCMS1" @if ($Type == "CCMS1") selected @endif>中央案管系統</option>
                            <option value="CPCMS" @if ($Type == "CPCMS") selected @endif>兒少保案管系統</option>
                            <option value="LTAFL" @if ($Type == "LTAFL") selected @endif>三四級講習</option>
                            <option value="VR" @if ($Type == "VR") selected @endif>訪視記錄</option>
                            <option value="CPCMS2" @if ($Type == "CPCMS2") selected @endif>春暉中斷</option>
                            <option value="CPCMS3" @if ($Type == "CPCMS3") selected @endif>監所銜接輔導</option>
                            <option value="CPCMS4" @if ($Type == "CPCMS4") selected @endif>特定營業場所</option>
                        </select>
                    </div>
                    <div id="dataGroup" class="form-group">
                        <label class="sr-only" for="selData">請選擇資料類別</label>
                        <select id="selData" class="form-control">

                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="input_2">請選擇檔案種類</label>
                        <select id="input_2" class="form-control" disabled>
                            <option>年度資料</option>
                            <option selected>月資料</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="selYear">請選擇年度</label>
                        <select id="selYear" class="form-control">
                            <option value="" selected>請選擇年度</option>
                            @foreach ($yearlist as $year)
                                <option value="{{$year}}">{{$year}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="selMonth">請選擇月份</label>
                        <select id="selMonth" class="form-control">
                            <option value="" selected>請選擇月份</option>
                            @foreach ($monthlist as $month)
                                <option value="{{$month}}">{{$month}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="custom-file">
                        <input type="file" style="opacity: 0;" class="custom-file-input" id="customFile"  />
                        <label class="custom-file-label" for="customFile"></label>
                    </div>
                    <!-- btnBox start -->
                    <div class="btnBox">
                        <button id="btnFileUpload" type="button" class="btn btn-primary btn-small">
                            送出檔案上傳
                        </button>
                    </div>
                    <!-- btnBox end -->
                </form>
                <!-- form end -->
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>
    .custom-file-input{
        color:black;
    }
</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/bs-custom-file-input.js')}}"></script>
<script type="text/javascript" src="{{asset('js/UploadPageUploadFile.js')}}"></script>
<script>
    var self={};
    var UploadPageUploadFile = new UploadPageUploadFile(self);
    UploadPageUploadFile.init();

    $(function () {
        bsCustomFileInput.init();
    });
</script>
@endsection
