@extends('layouts.index')

@section('title', 'Banner')

@section('breadcrumb')
    <!-- breadcrumb start -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">首頁</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                參、全國藥癮個案統計分析
            </li>
        </ol>
    </nav>
    <!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                參、全國藥癮個案統計分析
            </h3>
            <div class="main-card-contenet">
                <div class="row banner-itemBox">
                    <div class="col-12 col-md-6">
                        <p class="main-card-titleSub titleSubDeco">月報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、全國藥癮個案統計(未校正數)
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPage',['id'=>'SelectLocationeTubeMonth']) !!}">
                                        <div id="chart1"></div>
                                    </a>
                                    <p class="CommentText" id="SelectLocationeTubeMonthText"></p>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    二、全國藥癮個案統計(校正數)
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'SelectCorrectionLocationeTubeMonth']) !!}">
                                        <div id="chart2"></div>
                                    </a>
                                    <p class="CommentText" id="SelectCorrectionLocationeTubeMonthText"></p>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-6 col-line-left">
                            <p class="main-card-titleSub titleSubDeco">年報</p>
                            <div class="card-itemBox">
                              <div class="card-item" >
                                <p class="card-item-title">
                                    一、全國藥癮個案統計(未校正數)
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'SelectLocationeTubebyYear']) !!}">
                                        <div id="chartY1"></div>
                                    </a>
                                    <p class="CommentText" id="SelectLocationeTubebyYearText"></p>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    二、全國藥癮個案統計(校正數)
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'SelectCorrectionLocationeTubebyYear']) !!}">
                                        <div id="chartY2"></div>
                                      </a>
                                      <p class="CommentText" id="SelectCorrectionLocationeTubebyYearText"></p>
                                      <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->

@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/TaiwanDrugAnalysis.js')}}"></script>
<script>
am4core.addLicense("{{config('custom.ChartsPN')}}");
$.Log("參、全國藥癮個案統計分析","進入頁面");
getTaiwanDrugAnalysischart("SelectLocationeTubeMonth","chart1");
getTaiwanDrugAnalysischart("SelectLocationeTubebyYear","chartY1");
getTaiwanDrugAnalysischart("SelectCorrectionLocationeTubeMonth","chart2");
getTaiwanDrugAnalysischart("SelectCorrectionLocationeTubebyYear","chartY2");

</script>
@endsection
