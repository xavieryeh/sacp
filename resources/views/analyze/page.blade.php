@extends('layouts.index')

@section('title', 'Banner Page')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item">
            <a  href="{{route('Analyze.index')}}">國內外情勢分析</a>
        </li>
        <li id="breadcrumbtitle" class="breadcrumb-item active" aria-current="page">
            {{$title??'月報或年報'}}
        </li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox" >
        <div class="main-card">
            <h3 class="main-card-title">
                國內外情勢分析 - {{$title}}
            </h3>
            <div class="main-card-contenet" >

                <div  id="downloadcahrt" class="largechart" style="width:1050px;height:650px">
                        <div  class="card-item-pic" style="width: 1000px;height:600px;margin:0 auto;" >
                        <div id="content">
                            文字敘述
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 @if($type=='month') col-md-6 @endif">
                        <div class="form-group">
                            <select class="form-control" id="selectYear">
{{--                                <option>請選擇年度</option>--}}
                                @for($y=$max_year??110;$y>$min_year??110 ;$y--)
                                    <option value="{{$y}}" @if(($now_year??'0')==$y) selected @endif>{{$y}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    @if($type=='month')
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <select class="form-control" id="selectMonth">
{{--                                <option>請選擇月份</option>--}}
                                @for($m =1 ;$m<13;$m++)
                                    <option value="{{$m}}" @if(($now_month??'1')==$m) selected @endif>{{$m}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="d-flex justify-content-center">
                    <button type="button" onclick="getNewConent()" class="btn btn-primary btn-single">查詢</button>
                    &nbsp;
                    <input type="button" onclick="download()" class="btn btn-primary btn-single" value="下載">
                </div>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')

<script>

    $(function () {
        // 預選年度為
        $.ajaxSetup({
            // post,put,delete 需要
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}',
            },
        });
        getNewConent();
    });
    function getNewConent() {
        //form_data
        var form_data = {
            'type' : '{{$type??'year'}}',
            'year' : $('#selectYear').val(),
            @if(($type??false)==='month')
            'month' : $('#selectMonth').val(),
            @endif
        };
        $.ajax({
            method: 'post',
            url: '{{ $data_url??'#' }}',
            data: form_data,
            success:function(res){
                console.log(res);
                if(res.status){
                    $('#content').html(res.response.content);
                }else{
                    $('#content').html('查無資料');
                }
            }
        });
    }

    function download(){
        domtoimage.toBlob(document.getElementById('downloadcahrt'),{bgcolor:"#fff",height:700,style:{'color': 'red', 'writing-mode': 'vertical-lr' }})
        .then(function (blob) {
            window.saveAs(blob, 'my-node.png');
        });
    }

</script>
@endsection
