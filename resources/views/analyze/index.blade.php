@extends('layouts.index')

@section('title', 'Banner')

@section('breadcrumb')
    <!-- breadcrumb start -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">首頁</a></li>
            <li class="breadcrumb-item active" aria-current="page">拾壹、國內外情勢分析</li>
        </ol>
    </nav>
    <!-- breadcrumb end -->
@endsection

@section('content')
    <!-- main start -->
    <section class="block block-centerY">
        <!-- main-cardBox start -->
        <div class="main-cardBox">
            <div class="main-card">
                <h3 class="main-card-title">
                    拾壹、國內外情勢分析
                </h3>
                <div class="main-card-contenet">
                    <div class="row banner-itemBox">
                        <div class="col-12 col-md-6">
                            <p class="main-card-titleSub titleSubDeco">月報</p>
                            <div class="card-itemBox">
                                <div class="card-item">
                                    <p class="card-item-title">
                                        {{$last_month??'最新年份月報'}}
                                    </p>
                                    <div class="card-item-pic">
                                        <a href="{{route('Analyze.page',['type'=>'month'])}}">{{$month_comment??'月份內容'}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-line-left">
                            <p class="main-card-titleSub titleSubDeco">年報</p>
                            <div class="card-itemBox">
                                <div class="card-item">
                                    <p class="card-item-title">
                                        {{$last_year??'最新年報'}}
                                    </p>
                                    <div class="card-item-pic">
                                        <a href="{{route('Analyze.page',['type'=>'year'])}}">{{$year_comment??'年報內容'}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main-cardBox end -->
    </section>
    <!-- main end -->
@endsection

@section('custom-style')
    <style>

    </style>
@endsection

@section('custom-script')
    <script>

    </script>
@endsection
