@extends('layouts.index')

@section('title', '國內外情勢分析')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item"><a href="upload">檔案管理</a></li>
        <li class="breadcrumb-item"><a href="{{route('Upload.FileAnalyze')}}">國內外情勢分析</a></li>
        <li class="breadcrumb-item active" aria-current="page">資料上傳</li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                資料上傳
            </h3>
            <div class="main-card-contenet">
                <!-- form start -->
                <form>
                    <div class="form-group">
                        <label class="sr-only" for="selType">請選擇資料種類</label>
                        <select id="selType" class="form-control">
                            <option value="年報">年度資料</option>
                            <option value="月報" selected>月資料</option>
                        </select>
                    </div>
                    <div id="monthDiv">
                        <div class="form-group">
                            <label class="sr-only" for="selectYear">請選擇年度</label>
                            <select id="selectYear" class="form-control">
                                <option value="" selected>請選擇年度</option>
                                @foreach ($yearlist as $year)
                                    <option value="{{$year}}">{{$year}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="selectMonth">請選擇月份</label>
                            <select id="selectMonth" class="form-control">
                                <option value="" selected>請選擇月份</option>
                                @foreach ($monthlist as $month)
                                    <option value="{{$month}}">{{$month}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div id="yearDiv">
                        <div class="form-group">
                            <label class="sr-only" for="selectStartYear">請選擇年度</label>
                            <select id="selectStartYear" class="form-control">
                                <option value="" selected>請選擇年度</option>
                                @foreach ($yearlist as $year)
                                    <option value="{{$year}}">{{$year}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="selectEndYear">請選擇年度</label>
                            <select id="selectEndYear" class="form-control">
                                <option value="" selected>請選擇年度</option>
                                @foreach ($yearlist as $year)
                                    <option value="{{$year}}">{{$year}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="txtMessage">請輸入文字</label>
                        <textarea class="form-control" id="txtMessage" rows="3" placeholder="請輸入文字"></textarea>
                    </div>
                    <!-- btnBox start -->
                    <div class="btnBox">
                        <button id="btnFileUpload" type="button" class="btn btn-primary btn-small">
                            資料上傳
                        </button>
                    </div>
                    <!-- btnBox end -->
                </form>
                <!-- form end -->
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>
</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/FileAnalyzeUpload.js')}}"></script>
<script>
    var self={};
    var FileAnalyzeUpload = new FileAnalyzeUpload(self);
    FileAnalyzeUpload.init();
</script>
@endsection
