@extends('layouts.index')

@section('title', '個案管理')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item active" aria-current="page">個案管理</li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <div class="main-card-contenet">
                <div class="row align-items-center">
                    <div class="col-12 col-md" style="min-width: 400px">
                        <div class="chart" id="chart1" style="height: 300px"></div>
                    </div>
                    <div class="col-12 col-md" style="min-width: 400px">
                        <div class="chart" id="chart2" style="height: 300px"></div>
                    </div>
                    <div class="col-12 col-md" style="min-width: 300px">
                        <div class="chart" id="chart3" style="height: 300px"></div>
                    </div>
                </div>

                <!-- table start -->
                <div class="table-responsive table-KSarea_page">
                    <table class="table table-hover table-bordered table-case">
                        <thead>
                            <tr>
                                <th scope="col">
                                    個案 <br class="br_phone" />(去識別化)
                                </th>
                                <th scope="col">來源別</th>
                                <th scope="col">性別</th>
                                <th scope="col">年齡</th>
                                <th scope="col">毒品<br class="br_phone" />級數</th>
                                <th scope="col">列管<br class="br_phone" />(Y/N)</th>
                                <th scope="col">初案/再案<br class="br_phone" />(1/2)</th>
                                <th scope="col">類型</th>
                            </tr>
                        </thead>
                        <tbody id=tBody>
                            {{-- <tr class="js-case_Page">
                                <td date-title="個案(去識別化)" scope="row">001</td>
                                <td date-title="來源別">A</td>
                                <td date-title="性別">男</td>
                                <td date-title="年齡">20</td>
                                <td date-title="毒品級數">一</td>
                                <td date-title="狀態">列管</td>
                                <td date-title="初案/再案">初案</td>
                                <td date-title="類型">少年法庭裁定保護管束</td>
                            </tr>
                            <tr class="js-case_Page">
                                <td date-title="個案(去識別化)" scope="row">002</td>
                                <td date-title="來源別">B</td>
                                <td date-title="性別">女</td>
                                <td date-title="年齡">20</td>
                                <td date-title="毒品級數">混用</td>
                                <td date-title="狀態">非列管</td>
                                <td date-title="初案/再案">再案</td>
                                <td date-title="類型">緩起訴</td>
                            </tr>
                            <tr class="js-case_Page">
                                <td date-title="個案(去識別化)" scope="row">003</td>
                                <td date-title="來源別">A</td>
                                <td date-title="性別">男</td>
                                <td date-title="年齡">20</td>
                                <td date-title="毒品級數">一</td>
                                <td date-title="狀態">列管</td>
                                <td date-title="初案/再案">初案</td>
                                <td date-title="類型">少年法庭裁定保護管束</td>
                            </tr>
                            <tr class="js-case_Page">
                                <td date-title="個案(去識別化)" scope="row">004</td>
                                <td date-title="來源別">A</td>
                                <td date-title="性別">男</td>
                                <td date-title="年齡">20</td>
                                <td date-title="毒品級數">一</td>
                                <td date-title="狀態">列管</td>
                                <td date-title="初案/再案">初案</td>
                                <td date-title="類型">少年法庭裁定保護管束</td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>
                <!-- table end -->

                <!-- tableFoot start -->
                <div class="tableFoot">
                    <div class="table-quantity">
                        <P>顯示</P>
                        <form class="form-inline">
                            <div class="col-auto">
                                <label class="sr-only" for="selShowCount">State</label>
                                <select id="selShowCount" class="form-control form-control-sm">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                            </div>
                        </form>
                        <P>項結果，共<span id="sTotalCount"></span>筆</P>
                    </div>
                    <div class="table-pagination">
                        <nav aria-label="Page navigation example">
                            <ul id="ulPageList" class="pagination"></ul>
                        </nav>
                    </div>
                </div>
                <!-- tableFoot end -->
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>
</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset_v('js/Case.js')}}"></script>
<script>
    var self = {};
    self.Cases = {!! json_encode($Cases, JSON_HEX_TAG) !!};
    self.ChartsPN = "{{config('custom.ChartsPN')}}";
    self.route = "{{route('Case.getCasePage')}}";
    self.Manager = "{{$Manager}}";

    var Case = new Case(self);
</script>
@endsection
