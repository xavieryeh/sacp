@extends('layouts.index')

@section('title', 'Banner')

@section('breadcrumb')
    <!-- breadcrumb start -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">首頁</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                肆、本市藥癮個案收案類型分析
            </li>
        </ol>
    </nav>
    <!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                肆、本市藥癮個案收案類型分析
            </h3>
            <div class="main-card-contenet">
                <div class="row banner-itemBox">
                    <div class="col-12 col-md-6">
                        <p class="main-card-titleSub titleSubDeco">月報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    本市藥癮個案收案類型分析
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'DrugTypebyMonth']) !!}">
                                        <div id="chart1"></div>
                                        {{-- <div id="DrugTypebyMonth" style="height:420px"></div> --}}
                                        {{--<div id="DrugTypebyMonthTable" class="cell-border" style="margin-top:-15px"></div>--}}
                                    </a>
                                    <p class="CommentText" id="DrugTypebyMonthText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-line-left">
                        <p class="main-card-titleSub titleSubDeco">年報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    本市藥癮個案收案類型分析
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'DrugTypebyYear']) !!}">
                                        <div id="chartY1"></div>
                                    </a>
                                    <p class="CommentText" id="DrugTypebyYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->


@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/DrugTypeAnalysis.js')}}"></script>
<script>
am4core.addLicense("{{config('custom.ChartsPN')}}");
$.Log("肆、本市藥癮個案收案類型分析","進入頁面");
getDrugTypeAnalysischart("DrugTypebyMonth","chart1");
getDrugTypeAnalysischart("DrugTypebyYear","chartY1");
</script>
@endsection
