@extends('layouts.index')

@section('title', 'Banner')

@section('breadcrumb')
    <!-- breadcrumb start -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">首頁</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                玖、本市少年法庭毒品案件統計分析
            </li>
        </ol>
    </nav>
    <!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                玖、本市少年法庭毒品案件統計分析
            </h3>
            <div class="main-card-contenet">
                <div class="row banner-itemBox">
                    <div class="col-12 col-md-6">
                        <p class="main-card-titleSub titleSubDeco">月報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、人數統計
                                </p>
                                <div class="card-item-pic">
                                    <a  href="{!! route('bannerPage',['id'=>'DrugCourtPersonByMonth']) !!}">
                                        <div id="chart1"></div>
                                        {{-- <div id="DrugGenderByMonth" style="height:220px;"></div> --}}
                                        {{-- <div id="DrugGenderByMonthTable" class="cell-border" style="margin-top:-15px;width:50%;position: relative;"></div> --}}
                                    </a>
                                    <p class="CommentText" id="DrugCourtPersonByMonthText"></p>
                                    <p class="main-card-info">來源：臺灣高雄少年及家事法院</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    二、件數統計
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'DrugCourtCaseByMonth']) !!}">
                                        <div id="chart2"></div>
                                    </a>
                                    <p class="CommentText" id="DrugCourtCaseByMonthText"></p>
                                    <p class="main-card-info">來源：臺灣高雄少年及家事法院</p>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-line-left">
                        <p class="main-card-titleSub titleSubDeco">年報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、人數統計
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'DrugCourtPersonByYear']) !!}">
                                        <div id="chartY1"></div>
                                    </a>
                                    <p class="CommentText" id="DrugCourtPersonByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：臺灣高雄少年及家事法院</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    二、件數統計
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPageforYear',['id'=>'DrugCourtCaseByYear']) !!}">
                                        <div id="chartY2"></div>
                                    </a>
                                    <p class="CommentText" id="DrugCourtCaseByYearYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：臺灣高雄少年及家事法院</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->


@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/JuvenileCourtDrugCase.js')}}"></script>
<script>
am4core.addLicense("{{config('custom.ChartsPN')}}");
$.Log("玖、本市少年法庭毒品案件統計分析","進入頁面");
getJuvenileCourtDrugCase("DrugCourtPersonByMonth","chart1");
getJuvenileCourtDrugCase("DrugCourtPersonByYear","chartY1");
getJuvenileCourtDrugCase("DrugCourtCaseByMonth","chart2");
getJuvenileCourtDrugCase("DrugCourtCaseByYear","chartY2");
</script>
@endsection
