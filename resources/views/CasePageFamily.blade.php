@extends('layouts.index')

@section('title', '編輯家系圖')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('index')}}">首頁</a></li>
        <li class="breadcrumb-item"><a href="{{route('Case.getCase')}}?Manager=XXX">個案管理</a></li>
        <li class="breadcrumb-item"><a href="{{route('Case.getCasePage')}}?caseNo={{$caseNo}}">{{$caseNo}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">編輯家系圖</li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <div class="main-card-contenet">
                <!-- form start -->
                <form id="addForm" data-parsley-validate enctype="multipart/form-data">
                    <input type="hidden" name="db_id" id="db_id"><!-- for 編輯使用 -->
                    <div class="form-row">
                        <input name="caseNo" type="hidden" value="{{$caseNo}}">
                        <div class="form-group col-xl-2 col-lg-4">
                            <label for="selRelative">親等</label>
                            <select id="selRelative" name="relative" class="form-control" required="">
                                <option value="" selected disabled>請選擇</option>
                                @foreach ($RelativeList as $Relative)
                                <option value="{{$Relative->Relative}}">{{$Relative->Relative}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xl-2 col-lg-4">
                            <label for="selTitle">稱謂</label>
                            <select id="selTitle" name="title" class="form-control" required="">
                                <option value="" selected disabled>請選擇</option>
                                @foreach ($TitleList as $Title)
                                <option value="{{$Title->Title}}">{{$Title->Title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xl-2 col-lg-4">
                            <label for="iName">姓名</label>
                            <input type="text" name="name" class="form-control" id="iName" autocomplete="off"
                                required0="" />
                        </div>
                        <div class="form-group col-xl-2 col-lg-4">
                            <label for="iGender">性別</label>
                            <select id="iGender" name="gender" class="form-control" required="">
                                <option value="" selected disabled>請選擇</option>
                                <option value="男">男</option>
                                <option value="女">女</option>
                            </select>
                        </div>
                        <div class="form-group col-xl-2 col-lg-4">
                            <label for="iBirth">出生年月日</label>
                            <input type="text" name="birth" class="form-control" id="iBirth"
                                   placeholder="YYYMMDD"
                                   data-parsley-pattern="^[01][0-9]{2}[01][0-9][0-3][0-9]$" data-parsley-error-message="請填上正確格式"
                                   autocomplete="off"/>
                        </div>
                        <div class="form-group col-xl-2 col-lg-4">
                            <label for="iID">身分編號</label>
                            <input type="text" name="id" class="form-control" id="iID" autocomplete="off" required0=""
                                data-parsley-pattern0="^[A-Z]{1}[1-2]{1}[0-9]{8}$" data-parsley-error-message0="請填上正確格式"
                                onkeyup0="value=value.toUpperCase()"
                                onbeforepaste0="clipboardData.setData('text',clipboardData.getData('text').toUpperCase()" />
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-xl-2  col-lg-4">
                            <label for="iProfession">職業</label>
                            <input type="text" name="profession" class="form-control" id="iProfession"
                                autocomplete="off" />
                        </div>
                        <div class="form-group col-xl-4  col-lg-8">
                            <label for="iDrugRecoder">涉毒紀錄</label>
                            <input type="text" name="drugRecoder" class="form-control" id="iDrugRecoder"
                                autocomplete="off" />
                        </div>
                        <div class="form-group col-xl-2  col-lg-4">
                            <label for="iCohabit">是否同住</label>
                            <select id="iCohabit" name="cohabit" class="form-control" required="">
                                <option value="" selected disabled>請選擇</option>
                                <option value="是">是</option>
                                <option value="否">否</option>
                            </select>
                        </div>
                        <div class="form-group col-xl-2  col-lg-4">
                            <label for="iProblem">家庭問題</label>
                            <select id="iProblem" name="problem[]" class="form-control" multiple required0="">
                                <option value="" selected>無</option>
                                @foreach ($ProblemList as $Problem)
                                {{--<option value="{{$Problem->Color}}">{{$Problem->Message}}</option>--}}
                                <option value="{{$Problem->Message}}">{{$Problem->Message}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xl-2  col-lg-4">
                            <label for="iStructure">家庭結構</label>
                            <select id="iStructure" name="structure[]" class="form-control" multiple required0="">
                                <option value="" selected>無</option>
                                @foreach ($StructureList as $Structure)
                                {{--<option value="{{$Structure->Color}}">{{$Structure->Message}}</option>--}}
                                <option value="{{$Structure->Message}}">{{$Structure->Message}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        {{--<div class="form-group col-xl-2  col-lg-4">--}}
                        {{--    <label for="iRelatives">是否親屬</label>--}}
                        {{--    <select id="iRelatives" name="relatives" class="form-control" required="">--}}
                        {{--        <option value="" selected>請選擇是否親屬</option>--}}
                        {{--        <option value="是">是</option>--}}
                        {{--        <option value="否">否</option>--}}
                        {{--    </select>--}}
                        {{--</div>--}}
                        <div class="form-group col-xl-2  col-lg-4">
                            <label for="isFirst">初犯/再犯</label>
                            <select id="isFirst" name="初犯再犯" class="form-control" required="">
                                <option value="無">無</option>
                                <option value="初犯">初犯</option>
                                <option value="再犯">再犯</option>
                            </select>
                        </div>
                        <div class="form-group col-xl-2  col-lg-4">
                            <label for="iAlive">是否存歿</label>
                            <select id="iAlive" name="alive" class="form-control" required="">
                                <option value="" selected disabled>請選擇</option>
                                <option value="是">存</option>
                                <option value="否">歿</option>
                            </select>
                        </div>
                        <div class="form-group col-xl-4 col-lg-8">
                            <label for="iRemark">備註</label>
                            <input type="text" name="remark" class="form-control" id="iRemark" autocomplete="off" />
                        </div>
                    </div>
                    <!-- btnBox start -->
                    <div class="btnBox">
                        <button id="btnAdd" type="button" class="btn btn-primary btn-small">
                            新增 / 儲存
                        </button>
                        <button id="btnCancel" type="button" class="btn btn-gray btn-small">
                            取消
                        </button>
                    </div>
                    <!-- btnBox end -->
                </form>
                <!-- form end -->
            </div>
        </div>
        <div class="main-card">
            <div class="main-card-contenet">
                <!-- btnBox start -->
                {{-- <div class="btnBox justify-content-end">
                    <button type="submit" class="btn btn-primary btn-xs" data-toggle="modal"
                        data-target="#exampleModal">
                        新增
                    </button>
                </div> --}}
                <!-- btnBox end -->
                <!-- table start -->
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-case">
                        <thead>
                            <tr>
                                <th scope="col">親等</th>
                                <th scope="col">稱謂</th>
                                <th scope="col">姓名</th>
                                <th scope="col">性別</th>
                                <th scope="col">出生年月日</th>
                                <th scope="col">身分編號</th>
                                <th scope="col">職業</th>
                                <th scope="col">涉毒紀錄</th>
                                <th scope="col">是否同住</th>
                                <th scope="col">家庭問題</th>
                                <th scope="col">家庭結構</th>
                                {{--<th scope="col">是否親屬</th>--}}
                                <th scope="col">初犯/再犯</th>
                                <th scope="col">是否存歿</th>
                                <th scope="col">備註</th>
                                <th scope="col">功能</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($families as $family)
                            <tr class="">
                                <td date-title="親等" scope="row">{{$family->親等}}</td>
                                <td date-title="稱謂">{{$family->稱謂}}</td>
                                <td date-title="姓名">{{$family->姓名}}</td>
                                <td date-title="性別">{{$family->性別}}</td>
                                <td date-title="出生年月日">{{$family->出生年月日}}</td>
                                <td date-title="身分編號">{{$family->身份證字號}}</td>
                                <td date-title="職業">{{$family->職業}}</td>
                                <td date-title="涉毒紀錄">{{$family->涉毒紀錄}}</td>
                                <td date-title="是否同住">{{$family->是否同住}}</td>
                                <td date-title="家庭問題">{{$family->家庭問題}}</td>
                                <td date-title="家庭結構">{{$family->家庭結構}}</td>
                                {{--<td date-title="是否親屬">{{$family->Relatives}}</td>--}}
                                <td date-title="初犯/再犯">{{$family->初犯再犯}}</td>
                                <td date-title="是否存歿">{{$family->是否在世=='是'?'存':'歿'}}</td>
                                <td date-title="備註">{{$family->備註}}</td>
                                <td date-title="功能">
                                    <!-- btnBox start -->
                                    {{--<div class="btnBox">--}}
                                        <button onclick="CasePageFamily.onEdit('{{$family->ID}}')" type="buttom" class="btn btn-primary btn-small">
                                            編輯
                                        </button>
                                        <button onclick="CasePageFamily.onDelete('{{$family->姓名}}',{{$family->ID}})" type="buttom" class="btn btn-danger btn-small">
                                            刪除
                                        </button>
                                    {{--</div>--}}
                                    <!-- btnBox end -->
                                </td>
                            </tr>
                            @endforeach
                            {{-- <tr class="">
                                <td date-title="親等" scope="row">直系血親尊親屬</td>
                                <td date-title="稱謂">個案</td>
                                <td date-title="姓名">林小明</td>
                                <td date-title="性別">男</td>
                                <td date-title="出生年月日">1988/07/21</td>
                                <td date-title="身分編號">A12345678</td>
                                <td date-title="職業">無業</td>
                                <td date-title="涉毒紀錄">文字文字</td>
                                <td date-title="是否同住">是</td>
                                <td date-title="備註"></td>
                                <td date-title="功能">
                                    <!-- btnBox start -->
                                    <div class="btnBox">
                                        <button type="submit" href="CasePage" class="btn btn-primary btn-small">
                                            新增
                                        </button>
                                        <button type="submit" href="CasePage" class="btn btn-danger btn-small">
                                            刪除
                                        </button>
                                    </div>
                                    <!-- btnBox end -->
                                </td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>
                <!-- table end -->
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
    <!-- btnBox start -->
    <div class="btnBox">
        <a href="{{route('Case.getCasePage')}}?caseNo={{$caseNo}}" class="btn btn-dark btn-small">
            返回個案頁面
        </a>
    </div>
    <!-- btnBox end -->
    <!-- Modal start-->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content main-card">
                <div class="modal-header"></div>
                <div class="modal-body">
                    <p>您是否確定要新增/儲存?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-gray" onclick="$('#addModal').modal('hide');">取消</button>
                    <button id="btnAddConfirm" type="button" class="btn btn-primary">確定</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="delModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content main-card">
                <div class="modal-header"></div>
                <div class="modal-body">
                    <p>您是否確定要刪除[<span id="delKey"></span>]?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-gray" onclick="$('#delModal').modal('hide');">取消</button>
                    <button id="btnDelConfirm" type="button" class="btn btn-primary">確定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal end -->
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>
</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset_v('js/CasePageFamily.js')}}"></script>
<script>

    var self = {};
    {{--self.Familys = {!! json_encode($Familys, JSON_HEX_TAG) !!};--}}
    self.Familys = {!! json_encode($families, JSON_HEX_TAG) !!};
    self.ProblemList =  {!! json_encode($ProblemList, JSON_HEX_TAG) !!};
    self.StructureList = {!! json_encode($StructureList, JSON_HEX_TAG) !!};
    self.caseNo = "{{$caseNo}}";
    self.addUrl = "{{route('Case.casePageFamilyAddDate')}}";
    self.reLoadUrl = "{{route('Case.getCasePageFamily')}}?caseNo={{$caseNo}}";
    var CasePageFamily = new CasePageFamily(self);

</script>
@endsection
