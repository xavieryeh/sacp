@extends('layouts.index')

@section('title', 'Banner')

@section('breadcrumb')
    <!-- breadcrumb start -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">首頁</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                拾、本市特定營業場所列管概況分析
            </li>
        </ol>
    </nav>
    <!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                拾、本市特定營業場所列管概況分析
            </h3>
            <div class="main-card-contenet">
                <div class="row banner-itemBox">
                    <div class="col-12 col-md-6">
                        <p class="main-card-titleSub titleSubDeco">月報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、本市列管特定營業場所業別比例分析
                                </p>
                                <div class="card-item-pic">
                                    <a  href="{!! route('bannerPage',['id'=>'SpecificAreaBusinessIndustryByMonth']) !!}">
                                        <div id="chart1"></div>
                                    </a>
                                    <p class="CommentText" id="SpecificAreaBusinessIndustryByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    二、本市列管特定營業場所業別與行政區交叉分析
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'SpecificAreaAdministrativeByMonth']) !!}">
                                        <div id="chart2"></div>
                                    </a>
                                    <p class="CommentText" id="SpecificAreaAdministrativeByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    三、本市特定營業場所業別列管率
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'SpecificIndustryByMonth']) !!}">
                                        <div id="chart3"></div>
                                    </a>
                                    <p class="CommentText" id="SpecificIndustryByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    四、本市特定營業場所月列管率
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'SpecificAreaBusinessByMonth']) !!}">
                                        <div id="chart4"></div>
                                    </a>
                                    <p class="CommentText" id="SpecificAreaBusinessByMonthText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    五、本市毒危條例規範六大業別場所業者主動通報率
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'SixBusinessByMonth']) !!}">
                                        <div id="chart5"></div>
                                    </a>
                                    <p class="CommentText" id="SixBusinessByMonthText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-line-left">
                        <p class="main-card-titleSub titleSubDeco">年報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、本市列管特定營業場所業別比例分析
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'SpecificAreaBusinessIndustryByYear']) !!}">
                                        <div id="chartY1"></div>
                                    </a>
                                    <p class="CommentText" id="SpecificAreaBusinessIndustryByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    二、本市列管特定營業場所業別與行政區交叉分析
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPageforYear',['id'=>'SpecificAreaAdministrativeByYear']) !!}">
                                        <div id="chartY2"></div>
                                    </a>
                                    <p class="CommentText" id="SpecificAreaAdministrativeByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    三、本市特定營業場所業別列管率
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'SpecificIndustryByYear']) !!}">
                                        <div id="chartY3"></div>
                                    </a>
                                    <p class="CommentText" id="SpecificIndustryByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    四、本市特定營業場所總列管率
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'SpecificAreaBusinessByYear']) !!}">
                                        <div id="chartY4"></div>
                                    </a>
                                    <p class="CommentText" id="SpecificAreaBusinessByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    五、本市毒危條例規範六大業別場所業者主動通報率
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'SixBusinessByYear']) !!}">
                                        <div id="chartY5"></div>
                                    </a>
                                    <p class="CommentText" id="SixBusinessByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->


@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/SpecificArea.js')}}"></script>
<script>
am4core.addLicense("{{config('custom.ChartsPN')}}");
$.Log("拾、本市特定營業場所列管概況分析","進入頁面");
getSpecificArea("SpecificAreaBusinessIndustryByMonth","chart1");
getSpecificArea("SpecificAreaBusinessIndustryByYear","chartY1");
getSpecificArea("SpecificAreaAdministrativeByMonth","chart2");
getSpecificArea("SpecificAreaAdministrativeByYear","chartY2");
getSpecificArea("SpecificIndustryByMonth","chart3");
getSpecificArea("SpecificIndustryByYear","chartY3");
getSpecificArea("SpecificAreaBusinessByMonth","chart4");
getSpecificArea("SpecificAreaBusinessByYear","chartY4");
getSpecificArea("SixBusinessByMonth","chart5");
getSpecificArea("SixBusinessByYear","chartY5");
</script>
@endsection
