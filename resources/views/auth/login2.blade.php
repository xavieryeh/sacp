<!DOCTYPE html>
<html lang="zh-Hant-TW">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta
            name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0"
    />

    <title>高雄市政府毒品防制局智慧毒防</title>
    <link rel="icon" href="{{asset('asset/images/favicon.ico')}}" />

    <meta name="keywords" content="關鍵字,關鍵字" />
    <meta name="description" content="網站描述" />
    <meta property="og:description" content="網站描述" />
    <meta property="og:title" content="高雄市政府毒品防制局智慧毒防" />
    <meta property="og:site_name" content="高雄市政府毒品防制局智慧毒防" />
    <meta property="og:image" content="asset/images/ogimg.jpg" />

    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/normalize.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/slick/slick.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/slick/slick-theme.css')}}" />
    <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css"
    />
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/share.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/media-query.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom.css')}}" />
</head>

<body>
<div class="wrapper">
    <!-- content start -->
    <div class="content content-page content-login">
        <!-- main start -->
        <section class="block block-centerY">
            <!-- main-cardBox start -->
            <div class="main-cardBox">
                <div class="main-card">
                    <div class="login_logo"><img src="{{asset('asset/images/logo.jpg')}}" alt="" /></div>
                    <div class="main-card-contenet">
                        <form method="post">
                            @csrf
                            <div class="form-group">
                                <label class="sr-only"
                                       for="inlineFormInputGroup1">帳號</label                                >
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">帳號</div>
                                    </div>
                                    <input
                                            name="email"
                                            type="text"
                                            class="form-control"
                                            id="inlineFormInputGroup1"
                                            placeholder="請輸入帳號"
                                    />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="sr-only"
                                       for="inlineFormInputGroup2">密碼</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">密碼</div>
                                    </div>
                                    <input
                                            type="password"
                                            name="password"
                                            class="form-control"
                                            id="inlineFormInputGroup2"
                                            placeholder="請輸入密碼"
                                    />
                                </div>
                                @error('email')
                                <div class="alert-danger">{{$message}}</div>
                                @enderror
                                <small
                                        id="passwordHelpInline"
                                        class="text-muted text-right"
                                >
                                    {{--<a href="member.html">忘記密碼?</a>--}}
                                </small>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md">
                                    <label class="sr-only"
                                           for="inlineFormInputGroup3">驗證碼</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">驗證碼</div>
                                        </div>
                                        <input
                                                type="text"
                                                class="form-control"
                                                name="captcha"
                                                id="inlineFormInputGroup3"
                                                placeholder="請輸入驗證碼"
                                        />
                                    </div>
                                </div>
                                <div class="form-group col-md-auto">
                                    <img id="img_captcha" src="{{captcha_src('flat')}}" alt="captcha"/>
                                </div>
                                <div class="form-group col-md-auto">
                                    <button class="btn btn-light" onclick="change_captcha()"
                                            type="button" title="更新">
                                        <i class="fas fa-redo"></i></button>
                                </div>
                                @error('captcha')
                                <div class="form-group col-md-auto alert-danger">{{$message}}</div>
                                @enderror
                            </div>

                            <!-- btnBox start -->
                            <div class="btnBox">
                                <button type="submit" class="btn btn-primary">
                                    登入
                                </button>
                            </div>
                            <!-- btnBox end -->
                        </form>
                    </div>
                </div>
            </div>
            <!-- main-cardBox end -->
        </section>
        <!-- main end -->
    </div>
    <!-- content end -->
</div>

<!-- Modal start-->
<div
        class="modal fade"
        id="exampleModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
>
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content main-card">
            <div class="modal-header"></div>
            <div class="modal-body">
                <p>您是否確定要新增?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-gray" data-dismiss="modal">
                    取消
                </button>
                <button type="button" class="btn btn-primary">確定</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal end -->

<!--
  jquery
  ************************************************************
-->
<script src="{{asset('asset/js/jquery-3.6.0.min.js')}}"></script>
<script src="{{asset('asset/js/popper.min.js')}}"></script>
<script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
<script src="{{asset('asset/js/slick.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

<script src="{{asset('asset/js/module.js')}}"></script>
<script>
    function change_captcha() {
        var img = document.getElementById('img_captcha');
        var src = img.src.split('?');
        // console.log('src', src);
        img.src = src[0] + '?' + Math.random();
    }
</script>
</body>
</html>
