@extends('layouts.index')

@section('title', '該里個案管理')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item"><a id="breadcrumb_KSarea"
                href="{{route('KSarea.getKSarea')}}?areaNo={{explode('_',$areaNo)[0]}}">{{$areaName}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$villageName}}</li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="KSarea_page">
    <div class="row">
        <div class="col-xs-12 col-md-9">
            <!-- main-cardBox start -->
            <div class="main-cardBox main-table">
                <div class="main-card">
                    <div class="main-card-contenet">
                        <form class="form-inline form-KSareaSeasch">
                            <div class="form-row align-items-center">
                                <div class="row align-items-center">
                                    <div class="col-auto">
                                        <label class="sr-only" for="iAgeStart">ageStart</label>
                                        <input id="iAgeStart" type="text" class="form-control mb-2 input-age" placeholder="請輸入年齡"
                                        onkeyup="value=value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))"/>
                                    </div>
                                    <span class="mb-2">~</span>
                                    <div class="col-auto">
                                        <label class="sr-only" for="iAgeEnd">ageEnd</label>
                                        <input id="iAgeEnd" type="text" class="form-control mb-2 input-age" placeholder="請輸入年齡"
                                        onkeyup="value=value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))"/>
                                    </div>
                                </div>
                                <div class="col-auto col-select">
                                    <label class="sr-only" for="iLevel">State</label>
                                    <select id="iLevel" class="form-control mb-2">
                                        <option value="-1" selected>請選擇毒品級數</option>
                                        <option value="一">一級</option>
                                        <option value="二">二級</option>
                                        <option value="三">三級</option>
                                        <option value="四">四級</option>
                                        <option value="混用">混用</option>
                                    </select>
                                </div>
                                <div class="col-auto col-keyWords">
                                    <label class="sr-only" for="iKeyWord">keywords</label>
                                    <input id="iKeyWord" type="text" class="form-control mb-2 input-keyWords" placeholder="請輸入關鍵字" />
                                </div>
                                <div class="col-auto">
                                    <button id="btnSearch" type="button" class="btn btn-primary mb-2">
                                        搜尋
                                    </button>
                                </div>
                            </div>
                        </form>

                        <!-- table start -->
                        <div class="table-responsive table-KSarea_page">
                            <table class="table table-hover table-bordered table-case">
                                <thead>
                                    <tr>
                                        <th scope="col">
                                            個案 <br class="br_phone" />(去識別化)
                                        </th>
                                        <th scope="col">來源別</th>
                                        <th scope="col">性別</th>
                                        <th scope="col">年齡</th>
                                        <th scope="col">
                                            毒品 <br class="br_phone" />級數
                                        </th>
                                        <th scope="col">列管<br class="br_phone" />(Y/N)</th>
                                        <th scope="col">
                                            初案/ 再案 <br class="br_phone" />(1/2)
                                        </th>
                                        <th scope="col">類型</th>
                                    </tr>
                                </thead>
                                <tbody id=tBody>
                                    {{-- @foreach ($Cases as $Case)
                                    <tr class="js-KSarea_page_ownTeacher">
                                        <td date-title="個案(去識別化)" scope="row"><a
                                                style="text-decoration:underline;color:#0080FF"
                                                href="{{route('KSarea.getKSareaPageOwnTeacher')}}?areaNo={{$areaNo}}_{{$Case->Case_ID}}">{{$Case->Case_ID}}</a>
                                        </td>
                                        <td date-title="來源別">{{$Case->SourceNo}}</td>
                                        <td date-title="性別">{{$Case->Gender}}</td>
                                        <td date-title="年齡">{{$Case->File_Age}}</td>
                                        <td date-title="毒品級數">{{$Case->Level}}</td>
                                        <td date-title="狀態">{{$Case->States}}</td>
                                        <td date-title="初案/再案">{{$Case->CaseType}}</td>
                                        <td date-title="類型">{{$Case->Manage_Type}}</td>
                                    </tr>
                                    @endforeach --}}
                                    {{-- <tr class="js-KSarea_page_ownTeacher">
                                        <td date-title="個案(去識別化)" scope="row">002</td>
                                        <td date-title="來源別">B</td>
                                        <td date-title="性別">女</td>
                                        <td date-title="年齡">20</td>
                                        <td date-title="毒品級數">混用</td>
                                        <td date-title="狀態">非列管</td>
                                        <td date-title="初案/再案">再案</td>
                                        <td date-title="類型">緩起訴</td>
                                    </tr>
                                    <tr class="js-KSarea_page_ownUser">
                                        <td date-title="個案(去識別化)" scope="row">003</td>
                                        <td date-title="來源別">A</td>
                                        <td date-title="性別">男</td>
                                        <td date-title="年齡">20</td>
                                        <td date-title="毒品級數">一</td>
                                        <td date-title="狀態">列管</td>
                                        <td date-title="初案/再案">初案</td>
                                        <td date-title="類型">少年法庭裁定保護管束</td>
                                    </tr>
                                    <tr class="js-KSarea_page_ownUser">
                                        <td date-title="個案(去識別化)" scope="row">004</td>
                                        <td date-title="來源別">A</td>
                                        <td date-title="性別">男</td>
                                        <td date-title="年齡">20</td>
                                        <td date-title="毒品級數">一</td>
                                        <td date-title="狀態">列管</td>
                                        <td date-title="初案/再案">初案</td>
                                        <td date-title="類型">少年法庭裁定保護管束</td>
                                    </tr> --}}
                                </tbody>
                            </table>
                        </div>
                        <!-- table end -->
                        <!-- tableFoot start -->
                        <div class="tableFoot">
                            {{-- <form class="form-inline"> --}}
                                <div class="table-quantity">
                                    <P>顯示</P>
                                    <input name="areaNo" type="hidden" value="{{$areaNo}}">
                                    <div class="col-auto">
                                        <label class="sr-only" for="selShowCount">State</label>
                                        <select id="selShowCount" class="form-control form-control-sm">
                                            <option value="5">5</option>
                                            <option value="10">10</option>
                                            <option value="15">15</option>
                                            <option value="20">20</option>
                                            <option value="25">25</option>
                                        </select>
                                    </div>
                                    <P>項結果，共<span id="sTotalCount"></span>筆</P>
                                </div>
                                <div class="table-pagination">
                                    <nav aria-label="Page navigation example">
                                        <ul id="ulPageList" class="pagination"></ul>
                                    </nav>
                                </div>
                            {{-- </form> --}}
                        </div>
                        <!-- tableFoot end -->
                    </div>

                <!-- btnBox start -->
                <div class="btnBox" style="margin:0 45%">
                    <a href="{{route('KSarea.getKSarea')}}?areaNo={{explode('_',$areaNo)[0]}}"
                        class="btn btn-dark btn-small">
                        返回
                    </a>
		</div>
                </div>
                <!-- btnBox end -->
            </div>
            <!-- main-cardBox end -->
        </div>
        <div class="col-xs-12 col-md-3">
            <!-- main-cardBox start -->
            <div class="main-cardBox">
                <div class="main-card">
                    <div class="main-card-btn js-popupBtn-open-1"></div>
                    <h3 class="main-card-title main-card-title-m">
                        該里個案年齡分佈
                    </h3>
                    <div class="main-card-contenet">
                        <div id=chart1></div>
                        {{-- <img src="asset/images/KSarea_page_pic1.jpg" alt="" /> --}}
                    </div>
                </div>
                <div class="main-card">
                    <div class="main-card-btn js-popupBtn-open-2"></div>
                    <h3 class="main-card-title main-card-title-m">
                        各級毒品人數統計
                    </h3>
                    <div class="main-card-contenet">
                        <div id=chart2></div>
                    </div>
                </div>
                <div class="main-card">
                    <div class="main-card-btn js-popupBtn-open-3"></div>
                    <h3 class="main-card-title main-card-title-m">
                        年齡與類型交叉分析
                    </h3>
                    <div class="main-card-contenet">
                        <div id=chart3></div>
                    </div>
                </div>
                <div class="main-card">
                    <div class="main-card-btn js-popupBtn-open-4"></div>
                    <h3 class="main-card-title main-card-title-m">
                        性別統計分析
                    </h3>
                    <div class="main-card-contenet">
                        <div id=chart4></div>
                        {{-- <img src="asset/images/KSarea_page_pic4.jpg" alt="" /> --}}
                    </div>
                </div>
                <div class="main-card">
                    <div class="main-card-btn js-popupBtn-open-5"></div>
                    <h3 class="main-card-title main-card-title-m">
                        收案來源分析
                    </h3>
                    <div class="main-card-contenet">
                        <div id=chart5></div>
                        {{-- <img src="asset/images/KSarea_page_pic5.jpg" alt="" /> --}}
                    </div>
                </div>
                <div class="main-card">
                    <div class="main-card-btn js-popupBtn-open-6"></div>
                    <h3 class="main-card-title main-card-title-m">
                        初案/再案統計
                    </h3>
                    <div class="main-card-contenet">
                        <div id=chart6></div>
                        {{-- <img src="asset/images/KSarea_page_pic6.jpg" alt="" /> --}}
                    </div>
                </div>
            </div>
            <!-- main-cardBox end -->
        </div>
    </div>
</section>
<!-- main end -->
<!-- popup start -->
<div class="popupBox js-popupBox-1">
    <div class="popup">
        <div class="main-card">
            <div class="main-card-btn js-popupBtn-close"></div>
            <h3 class="main-card-title">
                該里個案年齡分佈
            </h3>
            <div class="main-card-contenet">
                {{-- <img src="images/chart_1.jpg" alt="" /> --}}
                <div id="chart1_popup"></div>
            </div>
        </div>
    </div>
</div>
<!-- popup end -->

<!-- popup start -->
<div class="popupBox js-popupBox-2">
    <div class="popup">
        <div class="main-card">
            <div class="main-card-btn js-popupBtn-close"></div>
            <h3 class="main-card-title">
                各級毒品人數統計
            </h3>
            <div class="main-card-contenet">
                <div id=chart2_popup></div>
                {{-- <img src="images/chart_2.jpg" alt="" /> --}}
            </div>
        </div>
    </div>
</div>
<!-- popup end -->

<!-- popup start -->
<div class="popupBox js-popupBox-3">
    <div class="popup">
        <div class="main-card">
            <div class="main-card-btn js-popupBtn-close"></div>
            <h3 class="main-card-title">
                年齡與類型交叉分析
            </h3>
            <div class="main-card-contenet">
                <div id=chart3_popup></div>
                {{-- <img src="images/chart_3.jpg" alt="" /> --}}
            </div>
        </div>
    </div>
</div>
<!-- popup end -->

<!-- popup start -->
<div class="popupBox js-popupBox-4">
    <div class="popup">
        <div class="main-card">
            <div class="main-card-btn js-popupBtn-close"></div>
            <h3 class="main-card-title">
                性別統計分析
            </h3>
            <div class="main-card-contenet">
                <div id=chart4_popup></div>
                {{-- <img src="images/chart_4.jpg" alt="" /> --}}
            </div>
        </div>
    </div>
</div>
<!-- popup end -->

<!-- popup start -->
<div class="popupBox js-popupBox-5">
    <div class="popup">
        <div class="main-card">
            <div class="main-card-btn js-popupBtn-close"></div>
            <h3 class="main-card-title">
                收案來源分析
            </h3>
            <div class="main-card-contenet">
                <div id=chart5_popup></div>
                {{-- <img src="images/chart_5.jpg" alt="" /> --}}
            </div>
        </div>
    </div>
</div>
<!-- popup end -->

<!-- popup start -->
<div class="popupBox js-popupBox-6">
    <div class="popup">
        <div class="main-card">
            <div class="main-card-btn js-popupBtn-close"></div>
            <h3 class="main-card-title">
                初案/再案統計
            </h3>
            <div class="main-card-contenet">
                <div id=chart6_popup></div>
                {{-- <img src="images/chart_6.jpg" alt="" /> --}}
            </div>
        </div>
    </div>
</div>
<!-- popup end -->
@endsection

@section('custom-style')
<style>
    .PieTable {
        height: 220px;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .PieTable .chart {
        width: 60%;
    }

    .PieTable .cTable {
        /* width:30%; */
        align-self: flex-end;
    }

    .PieTable_popup{
        display: flex;
        justify-content: center;
        align-items: flex-start;
    }

    .PieTable_popup .chart {
        width: 60%;
    }

    .PieTable_popup .cTable {
        /* width:30%; */
        align-self: flex-end;
    }
</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/KSareaPage.js')}}"></script>
<script>
    var self = {};
    self.Cases = {!! json_encode($Cases, JSON_HEX_TAG) !!};
    self.areaNo = "{{$areaNo}}";
    self.areaName = "{{$areaName}}";
    self.villageName = "{{$villageName}}";
    self.route = "{{route('KSarea.getKSareaPageOwnTeacher')}}";
    self.ChartsPN = "{{config('custom.ChartsPN')}}";

    var kSareaPage = new KSareaPage(self);
    kSareaPage.makeCADChart("chart1");
    kSareaPage.makeDSChart("chart2");
    kSareaPage.makeATCAChart("chart3");
    kSareaPage.makeGSAChart("chart4");
    kSareaPage.makeSAAChart("chart5");
    kSareaPage.makeCSChart("chart6");

    for (let i = 1; i < $(".popupBox").length + 1; i++) {
        $(".js-popupBtn-open-" + i).click(function() {
            $.Log("主畫面","行政區"+self.areaName+"-"+self.villageName+"個案管理小圖放大");
            $(".js-popupBox-" + i).attr({ style: " display: flex;" });
            if(i==1){
                setTimeout(() => {
                    self.makeCADChart("chart1_popup");
                }, 100);
            }
            else if(i==2){
                setTimeout(() => {
                    self.makeDSChart("chart2_popup");
                }, 100);
            }
            else if(i==3){
                setTimeout(() => {
                    self.makeATCAChart("chart3_popup");
                }, 100);
            }
            else if(i==4){
                setTimeout(() => {
                    self.makeGSAChart("chart4_popup");
                }, 100);
            }
            else if(i==5){
                setTimeout(() => {
                    self.makeSAAChart("chart5_popup");
                }, 100);
            }
            else if(i==6){
                setTimeout(() => {
                    self.makeCSChart("chart6_popup");
                }, 100);
            }
            return false;
        });
    }

    $(".js-popupBtn-close").click(function() {
        $.Log("主畫面","行政區"+self.areaName+"-"+self.villageName+"個案管理小圖放大關閉");
        $(".popupBox").hide();
        return false;
    });
</script>
@endsection
