@extends('layouts.index')

@section('title', '{{$Title}}')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item"><a href="upload">檔案管理</a></li>
        <li class="breadcrumb-item"><a href="fileUpload">檔案上傳</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$Title}}</li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <div class="main-card-contenet">
                <form class="form-inline form-KSareaSeasch">
                    <div class="form-row form-seachOutput">
                        <div class="col-md">
                            <div class="form-row align-items-center">
                                <div class="row align-items-center">
                                    <div class="col-auto">
                                        <label class="sr-only" for="iAgeStart">ageStart</label>
                                        <input type="text" class="form-control mb-2 input-age" id="iAgeStart" placeholder="請輸入年齡"
                                         onkeyup="value=value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))"/>
                                    </div>
                                    <span class="mb-2">~</span>
                                    <div class="col-auto">
                                        <label class="sr-only" for="iAgeEnd">ageEnd</label>
                                        <input type="text" class="form-control mb-2 input-age" id="iAgeEnd" placeholder="請輸入年齡"
                                        onkeyup="value=value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))"/>
                                    </div>
                                </div>
                                <div class="col-auto col-select">
                                    <label class="sr-only" for="iLevel">State</label>
                                    <select id="iLevel" class="form-control mb-2">
                                        <option value="-1" selected>請選擇毒品級數</option>
                                        <option value="一">一級</option>
                                        <option value="二">二級</option>
                                        <option value="三">三級</option>
                                        <option value="四">四級</option>
                                        <option value="混用">混用</option>
                                    </select>
                                </div>
                                <div class="col-auto">
                                    <label class="sr-only" for="iKeyWord">keywords</label>
                                    <input type="text" class="form-control mb-2 input-keyWords" id="iKeyWord"
                                        placeholder="請輸入關鍵字" />
                                </div>
                                <div class="col-auto">
                                    <button id="searchData" type="button" class="btn btn-gray mb-2">
                                        搜尋
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-auto">
                            <a href="{{route('Upload.getUploadPageUploadFile')}}?Type={{$Type}}" class="btn btn-primary btn-output mb-2">上傳檔案</a>
                        </div>
                    </div>
                </form>

                <!-- table start -->
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-case">
                        <thead>
                            <tr>
                                <th scope="col">
                                    個案 <br class="br_phone" />(去識別化)
                                </th>
                                <th scope="col">來源別</th>
                                <th scope="col">性別</th>
                                <th scope="col">年齡</th>
                                <th scope="col">毒品<br class="br_phone" />級數</th>
                                <th scope="col">列管<br class="br_phone" />(Y/N)</th>
                                <th scope="col">初案/再案<br class="br_phone" />(1/2)</th>
                                <th scope="col">類型</th>
                                <th scope="col">功能</th>
                            </tr>
                        </thead>
                        <tbody id="tBody">
                            {{-- <tr>
                                <td date-title="個案(去識別化)" scope="row">001</td>
                                <td date-title="來源別">A</td>
                                <td date-title="性別">男</td>
                                <td date-title="年齡">20</td>
                                <td date-title="毒品級數">一</td>
                                <td date-title="狀態">列管</td>
                                <td date-title="初案/再案">初案</td>
                                <td date-title="類型">少年法庭裁定保護管束</td>
                                <td date-title="功能">
                                    <!-- btnBox start -->
                                    <div class="btnBox">
                                        <button type="submit" href="file_upload_page_uploadFile.html"
                                            class="btn btn-danger btn-small">
                                            刪除
                                        </button>
                                    </div>
                                    <!-- btnBox end -->
                                </td>
                            </tr>
                            <tr>
                                <td date-title="個案(去識別化)" scope="row">002</td>
                                <td date-title="來源別">B</td>
                                <td date-title="性別">女</td>
                                <td date-title="年齡">20</td>
                                <td date-title="毒品級數">混用</td>
                                <td date-title="狀態">非列管</td>
                                <td date-title="初案/再案">再案</td>
                                <td date-title="類型">緩起訴</td>
                                <td date-title="功能">
                                    <!-- btnBox start -->
                                    <div class="btnBox">
                                        <button type="submit" href="file_upload_page_uploadFile.html"
                                            class="btn btn-danger btn-small">
                                            刪除
                                        </button>
                                    </div>
                                    <!-- btnBox end -->
                                </td>
                            </tr>
                            <tr>
                                <td date-title="個案(去識別化)" scope="row">003</td>
                                <td date-title="來源別">A</td>
                                <td date-title="性別">男</td>
                                <td date-title="年齡">20</td>
                                <td date-title="毒品級數">一</td>
                                <td date-title="狀態">列管</td>
                                <td date-title="初案/再案">初案</td>
                                <td date-title="類型">少年法庭裁定保護管束</td>
                                <td date-title="功能">
                                    <!-- btnBox start -->
                                    <div class="btnBox">
                                        <button type="submit" href="file_upload_page_uploadFile.html"
                                            class="btn btn-danger btn-small">
                                            刪除
                                        </button>
                                    </div>
                                    <!-- btnBox end -->
                                </td>
                            </tr>
                            <tr>
                                <td date-title="個案(去識別化)" scope="row">004</td>
                                <td date-title="來源別">A</td>
                                <td date-title="性別">男</td>
                                <td date-title="年齡">20</td>
                                <td date-title="毒品級數">一</td>
                                <td date-title="狀態">列管</td>
                                <td date-title="初案/再案">初案</td>
                                <td date-title="類型">少年法庭裁定保護管束</td>
                                <td date-title="功能">
                                    <!-- btnBox start -->
                                    <div class="btnBox">
                                        <button type="submit" href="file_upload_page_uploadFile.html"
                                            class="btn btn-danger btn-small">
                                            刪除
                                        </button>
                                    </div>
                                    <!-- btnBox end -->
                                </td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>
                <!-- table end -->

                <!-- tableFoot start -->
                <div class="tableFoot">
                    {{-- <form class="form-inline"> --}}
                        <div class="table-quantity">
                            <P>顯示</P>
                            <div class="col-auto">
                                <label class="sr-only" for="selShowCount">State</label>
                                <select id="selShowCount" class="form-control form-control-sm">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                    <option value="25">25</option>
                                </select>
                            </div>
                            <P>項結果，共<span id="sTotalCount"></span>筆</P>
                        </div>
                        <div class="table-pagination">
                            <nav aria-label="Page navigation example">
                                <ul id="ulPageList" class="pagination"></ul>
                            </nav>
                        </div>
                    {{-- </form> --}}
                </div>
                <!-- tableFoot end -->
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
    <!-- Modal start-->
    <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="delModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content main-card">
                <input id="delKeyValue" type="hidden">
                <div class="modal-header"></div>
                <div class="modal-body">
                    <p>您是否確定要刪除<span id="delKey"></span>的資料?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-gray" onclick="$('#delModal').modal('hide');">取消</button>
                    <button id="btnDelConfirm" type="button" class="btn btn-primary">確定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal end -->
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>
</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/UploadPage3.js')}}"></script>
<script>
    var self={};
    self.Datas = {!! json_encode($Datas, JSON_HEX_TAG) !!};
    self.Type = "{{$Type}}";
    var UploadPage = new UploadPage(self);
    UploadPage.init();
</script>
@endsection
