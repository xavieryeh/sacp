@extends('layouts.index')

@section('title', 'Banner')

@section('breadcrumb')
    <!-- breadcrumb start -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">首頁</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                貳、本市及全國藥癮個案再犯率比較分析
            </li>
        </ol>
    </nav>
    <!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                貳、本市及全國藥癮個案再犯率比較分析
            </h3>
            <div class="main-card-contenet">
                <div class="row banner-itemBox">
                    <div class="col-12 col-md-6">
                        <p class="main-card-titleSub titleSubDeco">月報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、列管期間內施用毒品再犯率
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPage',['id'=>'SelectRecidivesmRateMonth']) !!}">
                                       <div id="chart1"></div>
                                    </a>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    二、列管期滿結案後半年內施用毒品再犯率
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPage',['id'=>'SelectRecidivesmRateHalfYearMonth']) !!}">
                                        <div id="chart2"></div>
                                    </a>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    三、列管期滿結案後一年內施用毒品再犯率
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPage',['id'=>'SelectRecidivesmRateOneYearMonth']) !!}">
                                        <div id="chart3"></div>
                                    </a>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    四、列管期滿結案後二年內施用毒品再犯率
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'SelectRecidivesmRateTwoYearMonth']) !!}">
                                        <div id="chart4"></div>
                                    </a>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-6 col-line-left">
                            <p class="main-card-titleSub titleSubDeco">年報</p>
                            <div class="card-itemBox">
                              <div class="card-item">
                                <p class="card-item-title">
                                    一、列管期間內施用毒品再犯率
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPageforYear',['id'=>'SelectRecidivesmRateYear']) !!}">
                                        <div id="chartY1"></div>
                                    </a>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    二、列管期滿結案後半年內施用毒品再犯率
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'SelectRecidivesmRateHalfYearbyYear']) !!}">
                                        <div id="chartY2"></div>
                                      </a>
                                      <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    三、列管期滿結案後一年內施用毒品再犯率
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPageforYear',['id'=>'SelectRecidivesmRateOneYearbyYear']) !!}">
                                        <div id="chartY3"></div>
                                    </a>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                              </div>
                              <div class="card-item">
                                <p class="card-item-title">
                                    四、列管期滿結案後二年內施用毒品再犯率
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPageforYear',['id'=>'SelectRecidivesmRateTwoYearbyYear']) !!}">
                                        <div id="chartY4"></div>
                                    </a>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/DrugRecidivismRateChart.js')}}"></script>
<script>
am4core.addLicense("{{config('custom.ChartsPN')}}");
$.Log("貳、本市及全國藥癮個案再犯率比較分析","進入頁面");
getDrugRecidivismRateChartid("SelectRecidivesmRateMonth","chart1");
getDrugRecidivismRateChartid("SelectRecidivesmRateYear","chartY1");
getDrugRecidivismRateChartid("SelectRecidivesmRateHalfYearMonth","chart2");
getDrugRecidivismRateChartid("SelectRecidivesmRateHalfYearbyYear","chartY2");
getDrugRecidivismRateChartid("SelectRecidivesmRateOneYearMonth","chart3");
getDrugRecidivismRateChartid("SelectRecidivesmRateOneYearbyYear","chartY3");
getDrugRecidivismRateChartid("SelectRecidivesmRateTwoYearMonth","chart4");
getDrugRecidivismRateChartid("SelectRecidivesmRateTwoYearbyYear","chartY4");
</script>
@endsection
