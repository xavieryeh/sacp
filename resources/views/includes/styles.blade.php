<!-- App css -->
<link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}" crossorigin="anonymous">

{{-- custom css --}}
<link rel="stylesheet" type="text/css" href="{{asset('asset/css/normalize.css')}}" crossorigin="anonymous"/>
<link rel="stylesheet" type="text/css" href="{{asset('asset/css/slick/slick.css')}}" crossorigin="anonymous"/>
<link rel="stylesheet" type="text/css" href="{{asset('asset/css/slick/slick-theme.css')}}" crossorigin="anonymous"/>
<link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css"
    crossorigin="anonymous"
    />

<link rel="stylesheet" type="text/css" href="{{asset('asset/css/share.css')}}?{{filemtime('asset/css/share.css')}}" crossorigin="anonymous"/>
<link rel="stylesheet" type="text/css" href="{{asset('asset/css/media-query.css')}}" crossorigin="anonymous"/>
<link rel="stylesheet" href="{{asset('asset/css/all.css')}}" crossorigin="anonymous"/>

<!-- App css -->
<link rel="stylesheet" href="{{asset('css/app.css')}}" crossorigin="anonymous"/>

<!-- webpack -->
<link rel="stylesheet" href="{{asset('css/custom.css')}}"/>
