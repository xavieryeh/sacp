{{-- jquery --}}
<script type="text/javascript" src="{{asset('asset/js/jquery-3.6.0.min.js')}}"></script>

{{-- moment --}}
<script type="text/javascript" src="{{asset('asset/js/moment.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/moment-with-locales.js')}}"></script>

{{-- bootstrap --}}
<script type="text/javascript" src="{{asset('asset/js/bootstrap.min.js')}}"></script>

{{-- app --}}
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
<script type="text/javascript" src="{{asset_v('js/amCharts.js')}}"></script>
<script type="text/javascript" src="{{asset('js/ChartTable.js')}}"></script>
<script type="text/javascript" src="{{asset('js/amchartfunc.js')}}"></script>
<script type="text/javascript" src="{{asset('js/postajax.js')}}"></script>
<script type="text/javascript" src="{{asset('js/database.js')}}"></script>
<script type="text/javascript" src="{{asset('js/module.js')}}"></script>

{{-- axios --}}
<script type="text/javascript" src="{{asset('asset/js/axios.min.js')}}"></script>

{{-- domtoimg --}}
<script type="text/javascript" src="{{asset('asset/js/domtoimg.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/FileSaver.js')}}"></script>

{{-- amchart --}}
<script type="text/javascript" src="{{asset('asset/js/amchart/4/core.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/amchart/4/charts.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/amchart/4/plugins/wordCloud.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/amchart/4/themes/material.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/amchart/4/themes/animated.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/amchart/4/themes/kelly.js')}}"></script>
<script type="text/javascript" src="https://cdn.amcharts.com/lib/5/index.js"></script>
<script type="text/javascript" src="{{asset('asset/js/amchart/5/xy.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/amchart/5/themes/Animated.js')}}"></script>

{{-- custom --}}
<script type="text/javascript" src="{{asset('asset/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/slick.js')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script type="text/javascript" src="{{asset('asset/js/module.js')}}"></script>

{{-- go.js sample --}}
<script type="text/javascript" src="{{asset('asset/js/go.js')}}"></script>
<script type="text/javascript" src="{{asset_v('asset/js/Genogram2.js')}}"></script>

{{-- parsley.js --}}
<script type="text/javascript" src="{{asset('asset/js/parsleyjs/dist/parsley.min.js')}}"></script>
