@extends('layouts.index')

@section('title', '{{$Title}}')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item"><a href="upload">檔案管理</a></li>
        <li class="breadcrumb-item"><a href="fileUpload">檔案上傳</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$Title}}</li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <div class="main-card-contenet">
                <form class="form-inline form-system">
                    <div class="form-row">
                        <div class="col-md">
                            <div class="form-row form-keyword">
                                <div class="form-group">
                                    <label class="sr-only" for="iKeyWord">關鍵字</label>
                                    <input type="text" class="form-control mb-2 mr-sm-2" id="iKeyWord"
                                        placeholder="請輸入關鍵字 " />
                                </div>
                                <div class="form-group">
                                    <button id="searchData" type="button" class="btn btn-gray mb-2">
                                        搜尋
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-auto">
                            <a href="{{route('Upload.getUploadPageUploadFile')}}?Type={{$Type}}" class="btn btn-primary mb-2">上傳檔案</a>
                        </div>
                    </div>
                </form>

                <!-- table start -->
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-case">
                        <thead>
                            <tr>
                                <th scope="col">檔案種類</th>
                                <th scope="col">檔案時間</th>
                                <th scope="col">上傳日期</th>
                                <th scope="col">上傳人員</th>
                                <th scope="col">功能</th>
                            </tr>
                        </thead>
                        <tbody id="tBody">
                            {{-- @foreach ($Datas as $Data)
                            <tr class="">
                                <td date-title="檔案種類">{{$Data->File_type}}</td>
                                <td date-title="檔案時間">{{$Data->File_Date}}</td>
                                <td date-title="上傳日期">{{$Data->UpLoad_Date}}</td>
                                <td date-title="上傳人員">{{$Data->UserName}}</td>
                                <td date-title="功能">
                                    <!-- btnBox start -->
                                    <div class="btnBox">
                                        <button type="submit" href="file_upload_page_uploadFile.html"
                                            class="btn btn-danger btn-small">
                                            刪除
                                        </button>
                                    </div>
                                    <!-- btnBox end -->
                                </td>
                            </tr>
                            @endforeach --}}
                        </tbody>
                    </table>
                </div>
                <!-- table end -->
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
    <!-- Modal start-->
    <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="delModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content main-card">
                <input id="delKeyValue" type="hidden">
                <div class="modal-header"></div>
                <div class="modal-body">
                    <p>您是否確定要刪除<span id="delKey"></span>的資料?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-gray" onclick="$('#delModal').modal('hide');">取消</button>
                    <button id="btnDelConfirm" type="button" class="btn btn-primary">確定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal end -->
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>
</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/UploadPage2.js')}}"></script>
<script>
    var self={};
    self.Datas = {!! json_encode($Datas, JSON_HEX_TAG) !!};
    self.Type = "{{$Type}}";
    var UploadPage = new UploadPage(self);    
    UploadPage.init();
</script>
@endsection
