@extends('layouts.index')

@section('title', '個案基本資料')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item"><a href="{{route('Case.getCase')}}?Manager=XXX">個案管理</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$caseNo}}</li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                個案基本資料
            </h3>
            <div class="main-card-contenet">
                <!-- table start -->
                <div class="table-responsive table-KSarea_page_own">
                    <table class="table table-bordered table-case">
                        <tbody>
                            <tr>
                                <th scope="col">來源編號</th>
                                <th scope="col">個案案號</th>
                                <th scope="col">身分編號</th>
                                <th scope="col">姓名</th>
                                <th scope="col">個管師/ <br />社工</th>
                                <th scope="col">類型</th>
                                <th scope="col">接收輔導日</th>
                                <th scope="col">毒品級數</th>
                                <th scope="col">性別</th>
                            </tr>
                            @if($Cases)
                            <tr>
                                <td date-title="來源編號" scope="row">{{$Cases->SourceNo}}</td>
                                <td date-title="個案案號">{{$Cases->Case_ID}}</td>
                                <td date-title="身分編號">{{$Cases->Person_ID}}</td>
                                <td date-title="姓名">{{$Cases->Person_Name}}</td>
                                <td date-title="個管師/社工">{{$Cases->Manager}}</td>
                                <td date-title="類型">{{$Cases->Manage_Type}}</td>
                                <td date-title="接收輔導日">{{$Cases->Counseling_Day_CHINA}}</td>
                                <td date-title="毒品級數">{{$Cases->Level}}</td>
                                <td date-title="性別">{{$Cases->Gender}}</td>
                            </tr>
                            <tr>
                                <th scope="col">出生年月日</th>
                                <th scope="col">進案年齡</th>
                                <th scope="col">實際年齡</th>
                                <th scope="col">婚姻狀況</th>
                                <th scope="col">學歷</th>
                                <th scope="col" colspan="2">戶籍地址</th>
                                <th scope="col" colspan="2">居住地</th>
                            </tr>
                            <tr>
                                <td date-title="出生年月日" scope="col">{{$Cases->Birthday_CHINA}}</td>
                                <td date-title="進案年齡" scope="col">{{$Cases->Entry_Age}}</td>
                                <td date-title="實際年齡" scope="col">{{$Cases->File_Age}}</td>
                                <td date-title="婚姻狀況" scope="col">{{$Cases->Marriage}}</td>
                                <td date-title="學歷" scope="col">{{$Cases->Education}}</td>
                                <td date-title="戶籍地址" scope="col" colspan="2">
                                    {{$Cases->Residence_Address}}
                                </td>
                                <td date-title="居住地" scope="col" colspan="2">
                                    {{$Cases->Live_Address}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="col">就業</th>
                                <th scope="col">在學</th>
                                <th scope="col">工作類型</th>
                                <th scope="col">結案日期</th>
                                <th scope="col" colspan="3">結案原因</th>
                                <th scope="col">家戶有吸毒者</th>
                                <th scope="col">多次施用</th>
                            </tr>
                            <tr>
                                <td date-title="就業" scope="col">{{$Cases->Employment_Status}}</td>
                                <td date-title="在學" scope="col">{{$Cases->Learning}}</td>
                                <td date-title="工作類型" scope="col">{{$Cases->WorkType}}</td>
                                <td date-title="結案日期" scope="col">{{$Cases->Closing_Date_CHINA}}</td>
                                <td date-title="結案原因" scope="col" colspan="3">{{$Cases->Closing_Reason}}</td>
                                <td date-title="家戶有吸毒者" scope="col">{{$Cases->Family_Drug_Addict}}</td>
                                <td date-title="多次施用" scope="col">{{$Cases->Multiple}}</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- table end -->
            </div>
        </div>
        <div class="main-card">
            <h3 class="main-card-title">
                雷達圖與文字雲
            </h3>
            <div class="main-card-contenet">
                <div class="row">
                    <div class="col-12 col-md" style="min-width: 400px">
                        <div class="chart" id="chart1" style="height: 300px"></div>
                    </div>
                    <div class="col-12 col-md" style="min-width: 400px">
                        <div class="chart" id="chart2" style="height: 300px"></div>
                    </div>
                    <div class="col-12 col-md" style="min-width: 300px">
                        <div class="chart" id="chart3" style="height: 300px"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card">
            <h3 class="main-card-title">
                家系圖
            </h3>
            <div class="main-card-contenet">
                <div class="Genogram-chart">
                    <img src="asset/images/FamilyDescription.png" alt="" />
                    <div id="myDiagramDiv"
                        style="background-color: #ffffff; border: solid 1px rgb(255, 255, 255); width:100%; height:600px;">
                    </div>
                </div>
            </div>
            <!-- btnBox start -->
            <div class="btnBox">
                <a href="{{route('Case.getCasePageFamily')}}?caseNo={{$caseNo}}" class="btn btn-primary btn-small">
                    編輯家系圖資料
                </a>
            </div>
            <!-- btnBox end -->
        </div>
        <div class="main-card">
            <h3 class="main-card-title">
                歷次訪視記錄
            </h3>
            <div class="main-card-contenet">
                <!-- table start -->
                <div class="table-responsive table-KSarea_page_own table-KSarea_page_ownVisit">
                    <table class="table table-bordered table-case">
                        <thead>
                            <tr>
                                <th scope="col" colspan="9">個案輔導追蹤紀錄</th>
                            </tr>
                            <tr>
                                <th scope="col">追蹤輔導 <br />日期</th>
                                <th scope="col">個案 <br />管理師</th>
                                <th scope="col">訪查方式</th>
                                <th scope="col">受訪者</th>
                                <th scope="col">受訪者 <br />關係</th>
                                <th scope="col">社會救助 <br />需求</th>
                                <th scope="col">就業扶助 <br />需求</th>
                                <th scope="col">戒治服務 <br />需求</th>
                                <th scope="col">就學服務 <br />需求</th>
                            </tr>
                            <tr>
                                <th scope="col">追輔建檔<br />日期</th>
                                <th scope="col" colspan="8">輔導內容</th>
                            </tr>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col" colspan="8">處遇計畫</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Visits as $Visit)
                            <tr class="table-itemLine table-itemLinNo">
                                <td date-title="追蹤輔導日期" scope="row">
                                    {{$Visit->Track_Counseling_Date}}
                                </td>
                                <td date-title="個案管理師">{{$Visit->Case_Manager}}</td>
                                <td date-title="訪查方式">{{$Visit->Interview}}</td>
                                <td date-title="受訪者">{{$Visit->Respondents}}</td>
                                <td date-title="受訪者關係">{{$Visit->Interviewee_Relationship}}</td>
                                <td date-title="社會救助需求">{{$Visit->Social_Assistance}}</td>
                                <td date-title="就業扶助需求">{{$Visit->Employment_Assistance}}</td>
                                <td date-title="戒治服務需求">{{$Visit->Abstinence_Service}}</td>
                                <td date-title="就學服務需求">{{$Visit->School_Service}}</td>
                            </tr>
                            <tr>
                                <td date-title="追輔建檔日期" scope="col">
                                    {{$Visit->Follow_File_Creation_Date}}
                                </td>
                                <td date-title="輔導內容" scope="col" colspan="8">
                                    <p style="white-space: pre-line">{{$Visit->Counseling_Content}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td date-title="" scope="col"></td>
                                <td date-title="處遇計畫" scope="col" colspan="8">
                                    <p style="white-space: pre-line">{{ $Visit->Treatment_Plan }}</p>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- table end -->
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
    <!-- btnBox start -->
    <div class="btnBox">
        <a href="{{route('Case.getCase')}}?Manager=XXX" class="btn btn-dark btn-small">
            返回
        </a>
    </div>
    <!-- btnBox end -->
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>
    .Genogram-chart {
        display: flex;
        align-items: flex-start;
    }

    .Genogram-chart img {
        width: 30%;
    }
</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset_v('js/CasePage.js')}}"></script>
<script>
    var self = {};
    self.ChartsPN = "{{config('custom.ChartsPN')}}";
    self.caseNo = "{{$caseNo}}";

    var CasePage = new CasePage(self);

</script>
@endsection
