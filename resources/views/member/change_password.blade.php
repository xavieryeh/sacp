@extends('layouts.index')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('index')}}">首頁</a></li>
            <li class="breadcrumb-item"><a href="{{route('member.change_password')}}">個人資料</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                密碼變更
            </li>
        </ol>
    </nav>
@endsection

@section('content')
    <section class="block block-centerY">
        <!-- main-cardBox start -->
        <div class="main-cardBox">
            <div class="main-card">
                <h3 class="main-card-title">
                    密碼變更
                </h3>
                <div class="main-card-contenet">
                    <!-- form start -->
                    <form method="post">
                        @csrf
                        <div class="form-group">
                            <input
                                    type="password"
                                    name="current_password"
                                    class="form-control"
                                    id="formGroupExampleInput1"
                                    placeholder="輸入您的舊密碼"
                            />
                            @error('current_password')
                            <div class="alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input
                                    type="password"
                                    name="password"
                                    class="form-control"
                                    id="formGroupExampleInput2"
                                    placeholder="輸入您的新密碼"
                            />
                            @error('password')
                            <div class="alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input
                                    type="password"
                                    name="password_confirmation"
                                    class="form-control"
                                    id="formGroupExampleInput3"
                                    placeholder="再次確認您的密碼"
                            />
                        </div>
                        <div>提醒：密碼必須10碼以上,包含英文大小寫、數字、符號</div>
                        <!-- btnBox start -->
                        <div class="btnBox">
                            <button
                                    type="submit"
                                    class="btn btn-primary btn-small"
                                    data-toggle="modal"
                                    data-target="#exampleModal"
                            >
                                更新密碼
                            </button>
                        </div>
                        <!-- btnBox end -->
                    </form>
                    <!-- form end -->
                </div>
            </div>
        </div>
        <!-- main-cardBox end -->
    </section>
@endsection