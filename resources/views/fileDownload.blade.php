@extends('layouts.index')

@section('title', '檔案下載')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item"><a href="upload">檔案管理</a></li>
        <li class="breadcrumb-item active" aria-current="page">檔案下載</li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')

<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->

    <div class="main-cardBox ">
        <div class="main-card">
            <h3 class="main-card-title">
                檔案下載
            </h3>
            <div class="main-card-contenet">
                <!-- form start -->
                <form action="{{$form_action??''}}" target="_blank" id="form_list" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="sr-only" for="type_name">請選擇檔案種類</label>
                        <select id="type_name" name="type_name" class="form-control">
                                <option value="" selected disabled> -- 請選擇檔案種類 -- </option>
                            @foreach($downTypes??[] as $type_val => $type_name )
                                <option value="{{$type_val}}">{{$type_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- 三四級講習 lecture select group -->
                    <div class="group_level_1 group_lecture" style="display: none">
                        <div class="form-group">
                            <label class="sr-only" for="lecture_type">選擇年報或月報</label>
                            <select id="lecture_type" name="lecture_type" class="form-control">
                                <option value="" selected> -- 請選擇年報或月報 --</option>
                                <option value="year">年報</option>
                                <option value="month">月報</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="lecture_year">選擇年度</label>
                            <select id="lecture_year" name="lecture_year" class="form-control">
                                <option value="" selected> -- 請選擇年度 --</option>
                                @for($star_year = $now_year ;$star_year>=$min_year ;$star_year--)
                                    <option value="{{$star_year}}">{{$star_year}}</option>
                                @endfor
                                {{-- <option>...</option>--}}
                            </select>
                        </div>
                        <div class="form-group lecture_type_month" style="display: none;">
                            <label class="sr-only" for="lecture_month">選擇月份</label>
                            <select id="lecture_month" name="lecture_month" class="form-control">
                                <option  value="" selected>請選擇月份</option>
                                <option>...</option>
                            </select>
                        </div>
                    </div>

                    <!-- 主畫面 home_screen select group -->
                    <div class="group_level_1 group_home"  style="display: none">
                        <div class="form-group form-row align-items-center">
                            <div class="col-6 col-md">
                                <label class="sr-only" for="home_year">請選擇年度</label>
                                <select id="home_year" name="home_year" class="form-control">
                                    <option value="" selected> -- 請選擇年度 --</option>
                                    @for($star_year = $now_year ;$star_year>=$min_year ;$star_year--)
                                        <option value="{{$star_year}}">{{$star_year}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-6 col-md">
                                <label class="sr-only" for="home_month">請選擇月份</label>
                                <select id="home_month" name="home_month" class="form-control">
                                    <option value="" selected disabled> -- 請選擇月份 --</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <!-- 文字雲&雷達圖 radar select group -->
                    <div class="group_level_1 group_radar "  style="display: none">
                        <div class="form-group">
                            <label class="sr-only" for="radar_sub_type">選擇範圍</label>
                            <select id="radar_sub_type" name="radar_sub_type"  class="form-control">
                                <option value="" selected disabled> -- 請選擇範圍 -- </option>
                                <option value="all">全市</option>
                                <option value="area">區</option>
                                <option value="people">個案</option>
                            </select>
                        </div>
                        <div class="form-group group_level_2 group_area" style="display: none">
                            <label class="sr-only" for="area_id">選擇行政區</label>
                            <select id="area_id"  name="area_id" class="form-control">
                                <option value="" selected disabled>請選擇行政區</option>
                                @foreach($maps as $map)
                                    <option value="{{$map->ID}}">{{$map->City_Name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group group_level_2 group_people" style="display: none">
                            <label class="sr-only" for="case_people">選擇案號</label>
                            <input type="text" class="form-control "   name="case_people" id="case_people"  placeholder="請輸入案號">
                        </div>

                        <div class="form-row align-items-center">
                            <div class="col-6 col-md">
                                <label class="sr-only" for="radar_start_year">請選擇年度</label>
                                <select id="radar_start_year" name="radar_start_year" class="form-control">
                                    <option value="" selected> -- 請選擇年度 --</option>
                                    @for($star_year = $now_year ;$star_year>=$min_year ;$star_year--)
                                        <option value="{{$star_year}}">{{$star_year}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-6 col-md">
                                <label class="sr-only" for="radar_start_month">請選擇月份</label>
                                <select id="radar_start_month" name="radar_start_month" class="form-control">
                                    <option selected>請選擇月份</option>
                                    <option>...</option>
                                </select>
                            </div>
                            <div class="col-12 col-md-auto">
                                <span>~</span>
                            </div>
                            <div class="col-6 col-md">
                                <label class="sr-only" for="radar_end_year">請選擇年度</label>
                                <select id="radar_end_year" name="radar_end_year" class="form-control">
                                    <option value="" selected> -- 請選擇年度 --</option>
                                    @for($star_year = $now_year ;$star_year>=$min_year ;$star_year--)
                                        <option value="{{$star_year}}">{{$star_year}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-6 col-md">
                                <label class="sr-only" for="radar_end_month">請選擇月份</label>
                                <select id="radar_end_month" name="radar_end_month" class="form-control">
                                    <option selected>請選擇月份</option>
                                    <option>...</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- btnBox start -->
                    <span id="exportProcess"> </span>
                    <div class="btnBox">
                        <button type="button" onclick="checkData()" id="btnExport" class="btn btn-primary btn-small"> 檔案匯出 </button>
                        <span id="exportProcess"></span>
                    </div>
                    <!-- btnBox end -->
                </form>
                <!-- form end -->
            </div>
        </div>
    </div>
    <!-- 放文字雲 個案 & 區域 -->

    <!-- main-cardBox end -->
    <div class="main-cardBox  z_index_-99" id="chartGroup"  style="/* z-index: -99; */">
        {{--  77777 --}}
    </div>


</section>
<!-- main end -->

<!-- home map -->
<div class="content-index my_hide z_index_-99" >
    <!-- main start -->
    <section class="main" id="main_map">
        <!-- main-cardBox start -->
        <div class="main-cardBox">
            <div class="main-cardBoxItem">
                <div class="main-card js-popupBtn-open-1">
                    {{-- <div class="main-card-btn"></div> --}}
                    <h3 id="chartTitle1" class="main-card-title main-card-title-s"></h3>
                    <div class="main-card-contenet">
                        <div class="chart" id=chart1></div>
                        <div id="chartText1"></div>
                        {{-- <img src="images/chart_1.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource1" class="main-card-info"></p>
                </div>
            </div>
            <div class="main-cardBoxItem">
                <div class="main-card js-popupBtn-open-2">
                    {{-- <div class="main-card-btn"></div> --}}
                    <h3 id="chartTitle2" class="main-card-title main-card-title-s"></h3>
                    <div class="main-card-contenet">
                        <div class="chart" id=chart2></div>
                        <div id="chartText2"></div>
                        {{-- <img src="images/chart_2.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource2" class="main-card-info"></p>
                </div>
            </div>
            <div class="main-cardBoxItem">
                <div class="main-card js-popupBtn-open-3">
                    {{-- <div class="main-card-btn js-popupBtn-open-3"></div> --}}
                    <h3 id="chartTitle3" class="main-card-title main-card-title-s"></h3>
                    <div class="main-card-contenet">
                        <div class="chart" id=chart3></div>
                        <div id="chartText3"></div>
                        {{-- <img src="images/chart_3.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource3" class="main-card-info"></p>
                </div>
            </div>
        </div>
        <!-- main-cardBox end -->

        <!-- main-map start -->
        <div class="main-map">
            <!-- map-notice starr -->
            <div class="map-notice">
                <div class="map-notice-top">
                    {{-- <h2 class="map-notice-title">毒品防制局</h2> --}}
                    <div class="map-notice-itemBox">
                        <div class="map-notice-item">
                            <p class="map-notice-item-top-title">109年個案總數</p>
                            <p id="caseCount" class="map-notice-item-top-number"></p>
                        </div>
                        <div class="map-notice-item">
                            <p class="map-notice-item-top-title">本月新增</p>
                            <p id="newCount" class="map-notice-item-top-number"></p>
                        </div>
                    </div>
                </div>

                <div class="map-notice-bottom">
                    <div class="map-notice-itemBox">
                        <div class="map-notice-item">
                            <p class="map-notice-item-bottom-title">行政區個案人數</p>
                            <div id="noticeQuantity" class="map-notice-item-bottom-quantity">
                                {{-- <li>1</li>
                            <li>50</li>
                            <li>100</li>
                            <li>150</li>
                            <li>200</li> --}}
                            </div>
                        </div>
                        {{-- <div class="map-notice-item">
                            <p class="map-notice-item-bottom-title">本月新增</p>
                            <div class="todayMark"></div>
                        </div> --}}
                    </div>
                </div>
            </div>
            <!-- map-notice end -->

            <!-- map-area start -->
            <div class="map-area">
                {{-- <div class="todayMarkBox"></div> --}}
            </div>
            <!-- map-area end -->

            <!-- map-list start -->
            <ol id="mapList" class="map-list"></ol>
            <!-- map-list end -->
        </div>
        <!-- main-map end -->

        <!-- main-cardBox start -->
        <div class="main-cardBox">
            <div class="main-cardBoxItem">
                <div class="main-card js-popupBtn-open-4">
                    {{-- <div class="main-card-btn"></div> --}}
                    <div class="main-card-contenet">
                        <div class="chart" id=chart4></div>
                        {{-- <img src="images/chart_4.jpg" alt="" /> --}}
                    </div>
                </div>
            </div>
            <div class="main-cardBoxItem">
                <div class="main-card js-popupBtn-open-5">
                    {{-- <div class="main-card-btn"></div> --}}
                    <h3 id="chartTitle5" class="main-card-title main-card-title-s"></h3>
                    <div class="main-card-contenet">
                        <div class="chart" id=chart5></div>
                        <div id="chartText5"></div>
                        {{-- <img src="images/chart_5.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource5" class="main-card-info"></p>
                </div>
            </div>
            <div class="main-cardBoxItem">
                <div class="main-card js-popupBtn-open-6">
                    {{-- <div class="main-card-btn"></div> --}}
                    <h3 id="chartTitle6" class="main-card-title main-card-title-s"></h3>
                    <div class="main-card-contenet">
                        <div class="chart" id=chart6></div>
                        <div id="chartText6"></div>
                        {{-- <img src="images/chart_6.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource6" class="main-card-info"></p>
                </div>
            </div>
        </div>
        <!-- main-cardBox end -->
    </section>
    <!-- main end -->

    <!-- popup start -->
    <div class="popupBox js-popupBox-1">
        <div class="popup">
            <div class="main-card">
                <div class="main-card-btn js-popupBtn-close"></div>
                <h3 id="chartTitle1_popup" class="main-card-title"></h3>
                <div class="main-card-contenet">
                    <div id=chart1_popup></div>
                    <div id="chartText1_popup"></div>
                    {{-- <img src="images/chart_1.jpg" alt="" /> --}}
                </div>
                <p id="chartSource1_popup" class="main-card-info"></p>
            </div>
        </div>
    </div>
    <!-- popup end -->

    <!-- popup start -->
    <div class="popupBox js-popupBox-2">
        <div class="popup">
            <div class="main-card">
                <div class="main-card-btn js-popupBtn-close"></div>
                <h3 id="chartTitle2_popup" class="main-card-title"></h3>
                <div class="main-card-contenet">
                    <div id=chart2_popup></div>
                    <div id="chartText2_popup"></div>
                    {{-- <img src="images/chart_2.jpg" alt="" /> --}}
                </div>
                <p id="chartSource2_popup" class="main-card-info"></p>
            </div>
        </div>
    </div>
    <!-- popup end -->

    <!-- popup start -->
    <div class="popupBox js-popupBox-3">
        <div class="popup">
            <div class="main-card">
                <div class="main-card-btn js-popupBtn-close"></div>
                <h3 id="chartTitle3_popup" class="main-card-title"></h3>
                <div class="main-card-contenet">
                    <div id=chart3_popup></div>
                    <div id="chartText3_popup"></div>
                    {{-- <img src="images/chart_3.jpg" alt="" /> --}}
                </div>
                <p id="chartSource3_popup" class="main-card-info"></p>
            </div>
        </div>
    </div>
    <!-- popup end -->

    <!-- popup start -->
    <div class="popupBox js-popupBox-4 z_index_-99" >
        <div class="popup" id="popupBox_4" >
            <div class="main-card">
                <div class="main-card-btn js-popupBtn-close"></div>
                <h3 class="main-card-title">

                </h3>
                <div class="main-card-contenet">
                    <div id=chart4_popup></div>
                    {{-- <img src="images/chart_4.jpg" alt="" /> --}}
                </div>
            </div>
        </div>
    </div>
    <!-- popup end -->

    <!-- popup start -->
    <div class="popupBox js-popupBox-5">
        <div class="popup">
            <div class="main-card">
                <div class="main-card-btn js-popupBtn-close"></div>
                <h3 id="chartTitle5_popup" class="main-card-title"></h3>
                <div class="main-card-contenet">
                    <div id=chart5_popup></div>
                    <div id="chartText5_popup"></div>
                    {{-- <img src="images/chart_5.jpg" alt="" /> --}}
                </div>
                <p id="chartSource5_popup" class="main-card-info"></p>
            </div>
        </div>
    </div>
    <!-- popup end -->

    <!-- popup start -->
    <div class="popupBox js-popupBox-6">
        <div class="popup">
            <div class="main-card">
                <div class="main-card-btn js-popupBtn-close"></div>
                <h3 id="chartTitle6_popup" class="main-card-title"></h3>
                <div class="main-card-contenet">
                    <div id=chart6_popup></div>
                    <div id="chartText6_popup"></div>
                    {{-- <img src="images/chart_6.jpg" alt="" /> --}}
                </div>
                <p id="chartSource6_popup" class="main-card-info"></p>
            </div>
        </div>
    </div>
    <!-- popup end -->
</div>

    <input type="hidden" id="selectYear" value="">
    <input type="hidden" id="selectMonth" value="">
    <input type="hidden" id="selectStartYear" value="">
    <input type="hidden" id="selectEndYear" value="">
    <input type="hidden" id="selectStartMonth" value="">
    <input type="hidden" id="selectEndMonth" value="">
@endsection

@section('custom-style')
    <style>
        .my_hide{
            position:absolute;
            top:-20000px;
        }
        .z_index_-99{
            z-index: -99;
        }
        .chart {
            display: block;
        }

        .map-block {
            display: flex;
            justify-content: center;
        }

        .chart3-sub-block {
            display: flex;
            justify-content: space-between;
            height: 50%;
        }

        .chart3-sub-block_popup {
            display: flex;
            justify-content: center;
            height: 50%;
        }

        .PieTable {
            height: 220px;
            display: flex;
            justify-content: center;
            align-items: flex-start;
        }

        .PieTable .chart {
            width: 60%;
        }

        .PieTable .cTable {
            /* width:30%; */
            align-self: flex-end;
        }

        .PieTable_popup {
            display: flex;
            justify-content: center;
            align-items: flex-start;
        }

        .PieTable_popup .chart {
            width: 60%;
        }

        .PieTable_popup .cTable {
            /* width:30%; */
            align-self: flex-end;
        }

    </style>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{asset('js/PoisonAnalysis.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/DrugRecidivismRateChart.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/TaiwanDrugAnalysis.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/DrugTypeAnalysis.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/DrugCase.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/Index.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/FileDownload.js').'?'.filemtime('js/FileDownload.js')}}"></script>

    <script>
    var now_year = '{{$now_year??110}}';
    var now_month = '{{$now_month??12}}';
    var chartInfo =[];
    var self = {};
    self.ChartsPN = "{{config('custom.ChartsPN')}}";
    self.KSareaHref = "{{route('KSarea.getKSarea')}}";
    self.ReportCharts = {!! json_encode($ReportCharts, JSON_HEX_TAG) !!};
    // var Report = new Report(self);
    $(function () {
        typeName();
        $.ajaxSetup({
            // post,put,delete 需要
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}',
            },
        });
    });
    var IndexPage = new IndexPage(self);
    IndexPage.init();
    function typeName() {
        $('#type_name').change(function () {
            var now_type = $('#type_name').val();
            $('.group_level_1').hide();
            switch (now_type) {
                case 'lecture': {{-- 講習 --}}
                    $('.group_lecture').show();
                    break;
                case 'home': {{-- 主畫面 --}}
                    $('.group_home').show();
                    break;
                case 'radar': {{-- 文字雲雷達圖 --}}
                    $('.group_radar').show();
                    break;
                default:
                    break;
            }
        });
        var year_months = [
            'lecture_', // 講習
            'home_',  // 首頁
            'radar_start_', //文字&雷達
            'radar_end_', //文字&雷達
        ];
        for (var i in year_months){
            var year_name = '#'+year_months[i]+'year';
            var month_name = '#'+year_months[i]+'month';
            $(year_name).on('change',function (){
                var _year = $(year_name).val();
                var _month = $(month_name);
                _month.empty();
                var max_month = 12;
                if(_year == now_year){
                    // lecture_month
                    max_month = now_month;
                }
                if(year_months[i]=='lecture_'){
                    _month.append($('<option>',{value:'',text: '-- 請選擇月報 --'}));
                }else{
                    _month.append($('<option>',{value:'',text: '-- 請選擇月份 --'}));
                }
                for (var m = max_month ;m>0;m--){
                    var m_s = (m<10?'0':'')+m;
                    _month.append($('<option>',{value:m_s,text:m_s}));
                }
            }(year_name,month_name));
        }
        //文字&雷達 radar_sub_type
        $('#radar_sub_type').change(function () {
            var now_type = $('#radar_sub_type').val();
            $('.group_level_2').hide();
            switch (now_type) {
                case 'area': {{-- 區域 --}}
                $('.group_area').show();
                    break;
                case 'people': {{-- 個案 --}}
                $('.group_people').show();
                    break;
                default:
                    break;
            }
        });

        $('#lecture_type').change(function () {
            var now_type = $('#lecture_type').val();
            switch (now_type) {
                case 'year': {{-- 年報 --}}
                $('.lecture_type_month').hide();
                    break;
                case 'month': {{-- 月報 --}}
                $('.lecture_type_month').show();
                    break;
                default:
                    $('.lecture_type_month').hide();
                    break;
            }
        });
    }

    function checkData() {
        $('#chartGroup').html("");
        var now_type = $('#type_name').val();
        var c_data =[];
        c_data['type'] = now_type;
        switch (now_type) {
            {{-- 講習 --}}
            case 'lecture':
                c_data['lecture_type'] = $('#lecture_type').val();
                c_data['lecture_year'] = $('#lecture_year').val();
                c_data['lecture_month'] = $('#lecture_month').val();
                if(c_data['lecture_type']=='year'){
                    if(c_data['lecture_year']=='' ){
                        alert('請確認年份是否皆有選擇');
                        return;
                    }
                }else{
                    if(c_data['lecture_year']=='' || c_data['lecture_month']==''){
                        alert('請確認年份與月份是否皆有選擇');
                        return;
                    }
                }
                break;
            {{-- 主畫面 --}}
            case 'home':
                c_data['home_year'] = $('#home_year').val();
                c_data['home_month'] = $('#home_month').val();
                if(c_data['home_year']=='' || c_data['home_month']==''){
                    alert('請確認年度與月份是否皆有選擇');
                    return;
                }
                break;
            {{-- 文字雲雷達圖 --}}
            case 'radar':
                c_data['radar_sub_type'] = $('#radar_sub_type').val();
                if(!c_data){
                    alert('請選擇範圍');
                    return;
                }
                // 區
                if(c_data['radar_sub_type']=='area'){
                    c_data['area_id'] = $('#area_id').val();
                    if(c_data['area_id']==''){
                        alert('請選擇行政區');
                        return;
                    }
                }

                // 個案
                if(c_data['radar_sub_type']=='people'){
                    c_data['case_people'] = $('#case_people').val();
                    if(c_data['case_people']==''){
                        alert('請輸入個案案號');
                        return;
                    }
                }
                c_data['radar_start_year'] = $('#radar_start_year').val();
                c_data['radar_start_month'] = $('#radar_start_month').val();
                c_data['radar_end_year'] = $('#radar_end_year').val();
                c_data['radar_end_month'] = $('#radar_end_month').val();
                if(c_data['radar_start_year']=='' || c_data['radar_start_month']==''){
                    alert('請確認起始年分與起始月份是否皆有選擇');
                    return;
                }
                if(c_data['radar_end_year']=='' || c_data['radar_end_month']==''){
                    alert('請確認起始年分與起始月份是否皆有選擇');
                    return;
                }
                break;
            default:
                alert('請選擇檔案類型');
                return;
                break;
        }
        var form_data = $('#form_list').serialize();

        // console.log('get data' ,form_data);
        $.ajax({
            method: 'post',
            url: '{{ $check_url??'#' }}',
            data: form_data,
            success:function(res){
                console.log('my_info OK', res);
                if(res.status){
                    $('#btnExport').attr('disabled',true);
                    var datas = res.response;
                    // 迴圈處理
                    chartInfo = [];
                    var chartDiv = '';
                    for (var i in datas){
                        var data = datas[i];
                        $('#exportProcess').html('檔案生成中，請稍後...');
                        console.log(i,data,333,data['Type']);
                        // 迴圈呼叫執行資料,
                        switch (data['Type']) {
                            case '年報':
                            case '月報':
                                FileDownload();
                                break;
                            case '主畫面':
                                window[data.Function_Name](); // 呼叫作圖
                                // download_png('main_map','home');
                                break;
                            case '文字雲與雷達圖-全市':
                                window[data.Function_Name](); // 呼叫作圖
                                break;
                            case '文字雲與雷達圖-區域':
                                self.areaName = $('#area_id').find('option:selected').html();
                                chartDiv =
                                    '<div class="largechart" style="width:550px;height:350px;position:fixed;">\
                                        <div id="download_area" class="card-item-pic" style="width: 500px;height:250px;margin:0 auto;padding-right:40px;padding-left:20px;" >\
                                            <div class="row">\
                                                <div class="col-12 col-md-4">\
                                                    <div class="chart" id="chart1" style="height: 300px"></div>\
                                                </div>\
                                                <div class="col-12 col-md-4">\
                                                    <div class="chart" id="chart2" style="height: 300px"></div>\
                                                </div>\
                                                <div class="col-12 col-md-4">\
                                                    <div class="chart" id="chart3" style="height: 300px"></div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                        <div style="margin-top:15px;">\
                                            <p class="CommentText" id="'+data.Function_ID+'Text"></p>\
                                        </div>\
                                    </div>';
                                $("#chartGroup").append(chartDiv);

                                // self.caseNo = $('#case_people').val();
                                // 呼叫作圖
                                console.log(data.Function_Name);
                                //download_
                                chartInfo.push({
                                    banner:data.Banner_Name,
                                    title:data.Chart_Name,
                                    source:data.Source_Name,
                                    downID:'download_area',
                                    textID:data.Function_ID+'Text',
                                    type:data.Type
                                });

                                window[data.Function_Name](); // 呼叫作圖
                                break;
                            case '文字雲與雷達圖-個案':
                                self.caseNo = $('#case_people').val();
                                chartDiv =
                                    '<div class="largechart" style="width:550px;height:350px;position:fixed;">\
                                        <div id="download_people" class="card-item-pic" style="width: 500px;height:250px;margin:0 auto;padding-right:40px;padding-left:20px;" >\
                                            <div class="row">\
                                                <div class="col-12 col-md-4">\
                                                    <div class="chart" id="chart1" style="height: 300px"></div>\
                                                </div>\
                                                <div class="col-12 col-md-4">\
                                                    <div class="chart" id="chart2" style="height: 300px"></div>\
                                                </div>\
                                                <div class="col-12 col-md-4">\
                                                    <div class="chart" id="chart3" style="height: 300px"></div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                        <div style="margin-top:15px;">\
                                            <p class="CommentText" id="'+data.Function_ID+'Text"></p>\
                                        </div>\
                                    </div>';
                                $("#chartGroup").append(chartDiv);

                                // self.caseNo = $('#case_people').val();
                                // 呼叫作圖
                                console.log(data.Function_Name);
                                //download_
                                chartInfo.push({
                                    banner:data.Banner_Name,
                                    title:data.Chart_Name,
                                    source:data.Source_Name,
                                    downID:'download_people',
                                    textID:data.Function_ID+'Text',
                                    type:data.Type
                                });

                                window[data.Function_Name](); // 呼叫作圖
                                break;
                            default:
                                alert('為定義');
                                return;
                                chartDiv =
                                    '<div class="largechart" style="width:550px;height:170px;position:fixed;">\
                                        <div id="download'+data.Function_ID+'" class="card-item-pic" style="width: 500px;height:250px;margin:0 auto;padding-right:40px;padding-left:20px;" >\
                    <div id="largeChart'+data.Chart_ID+'"></div>\
                </div>\
                <div style="margin-top:15px;">\
                    <p class="CommentText" id="'+data.Function_ID+'Text"></p>\
                </div>\
            </div>';
                                $("#chartGroup").append(chartDiv);

                                // 呼叫作圖
                                console.log(data.Function_Name);

                                window[data.Function_Name](data.Function_ID,'largeChart'+data.Chart_ID);
                                chartInfo.push({
                                    banner:data.Banner_Name,
                                    title:data.Chart_Name,
                                    source:data.Source_Name,
                                    downID:'download'+data.Function_ID,
                                    textID:data.Function_ID+'Text',
                                    type:data.Type
                                });
                                break;
                        }

                    }
                    return;
                }else{
                    alert(res.error);
                }
            }
        });
    }

    // 個人 呼叫
    function makeWCPeopleChart() {
        am4core.ready(function () {
            // Themes begin
            // am4core.unuseAllThemes();
            // am4core.useTheme(am4themes_material);
            // am4core.unuseTheme(am4themes_animated);
            am4core.addLicense(self.ChartsPN);
            // Themes end
            const id = "chart3";
            // Create chart instance
            var chart = am4core.registry.baseSprites.find(function (chartObj) {
                return chartObj.htmlContainer.id === id;
            });

            // $file = file('database/wordcloud.txt');
            $.PostAJAX("Case_casePageOwnTeacherProtectionFactor", {
                caseNo: $('#case_people').val()
            }, response => {
                if (chart === undefined) {
                    var Params = {};
                    Params.ChartType = "WordCloud";
                    Params.Data = response.chart3.map(function (each) {
                        return {
                            category: each.category,
                            count: each.count
                        };
                    });
                    MakeAMChart(id, Params);
                } else {
                    var NewChartData = response.chart3.map(function (each) {
                        return {
                            category: each.category,
                            count: each.count,
                        };
                    });

                    chart.data = NewChartData;
                    chart.invalidateRawData();
                }

                var chart3 = [{
                    id: "chart1",
                    title: "風險因子",
                    color: "#E98440"
                }, {
                    id: "chart2",
                    title: "保護因子",
                    color: "#3FBEEC"
                }];
                chart3.forEach((subChart, iSub) => {
                    var sub_chart = am4core.registry.baseSprites.find(function (chartObj) {
                        return chartObj.htmlContainer.id === subChart.id;
                    });
                    if (sub_chart === undefined) {
                        var Params = {};
                        Params.ChartType = "Radar";
                        Params.Title = {
                            title: subChart.title,
                            fonSize: 15,
                            color: subChart.color,
                            align: "center",
                            valign: "top",
                            dy: -10
                        };
                        Params.FontSize = 10;
                        Params.name = subChart.title;
                        Params.color = subChart.color;
                        Params.Data = response[subChart.id].map(function (each, i) {
                            return {
                                category: each.category,
                                count: each.count
                            };
                        });
                        MakeAMChart(subChart.id, Params);
                    } else {
                        var NewChartData = response[subChart.id].map(function (each, i) {
                            return {
                                category: each.category,
                                count: each.count
                            };
                        });
                        sub_chart.data = NewChartData;
                        sub_chart.invalidateRawData();
                    }
                });
                download_png('download_people','radar_people')
                // download_people();
            });
        }); // end am4core.ready()
        // function download_people(){
        //     console.log('download_people');
            // //延遲200ms確保產生完整圖表
            // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            // var formData = new FormData();
            // var chartFinish = [];
            // setTimeout(() => {
            //     var downID='download_people';
            //     domtoimage.toBlob(document.getElementById(downID),{bgcolor:"#fff",height:$("#"+downID).height()})
            //         .then(function (blob) {
            //             formData.append(downID, blob, downID +".jpg");
            //             chartFinish.push(chart.downID);
            //             nowExcute = false;
            //             // 全數處理完，準備匯出報表
            //             // if(chartInfo.length == chartFinish.length){
            //                 $("#exportProcess").text("正在產生報表...");
            //                 // clearInterval(checkTimer);
            //                 // chartInfo.forEach(chart=>{chart.subText= $('#'+downID).text();});
            //                 chartInfo.forEach(chart=>{chart.subText= '';}); // 文字混亂暫時不顯示
            //
            //                 formData.append('chartInfo', JSON.stringify(chartInfo));
            //                 axios({
            //                     url: 'Report_exportWord',
            //                     method: 'post',
            //                     headers: {
            //                         "X-CSRF-TOKEN": CSRF_TOKEN
            //                     },
            //                     data: formData,
            //                     responseType: 'blob'
            //                 })
            //                     .then((response) => {
            //                         $("#exportProcess").text("完成");
            //                         $("#btnExport").prop('disabled', false);
            //                         const url = window.URL.createObjectURL(new Blob([response.data]));
            //                         const link = document.createElement('a');
            //                         link.href = url;
            //                         link.setAttribute('download', '檔案下載-文字雲雷達圖-個案.odf');
            //                         document.body.appendChild(link);
            //                         link.click();
            //                         document.body.removeChild(link);
            //                     });
            //             // }
            //         });
            // }, 1000*1);
        // }
    }

    // 區域 呼叫
    function makeWCAreaChart() {
        am4core.ready(function () {
            // Themes begin
            // am4core.unuseAllThemes();
            // am4core.useTheme(am4themes_material);
            // am4core.unuseTheme(am4themes_animated);
            am4core.addLicense(self.ChartsPN);
            // Themes end
            const id = "chart3";
            // Create chart instance
            var chart = am4core.registry.baseSprites.find(function (chartObj) {
                return chartObj.htmlContainer.id === id;
            });

            // $file = file('database/wordcloud.txt');
            $.PostAJAX("KSarea_areaProtectionFactor", {areaName:self.areaName}, response => {
                if (chart === undefined) {
                    var Params = {};
                    Params.ChartType = "WordCloud";
                    Params.Data = response.chart3.map(function (each) {
                        return {
                            category: each.category,
                            count: each.count
                        };
                    });
                    MakeAMChart(id, Params);
                } else {
                    var NewChartData = response.chart3.map(function (each) {
                        return {
                            category: each.category,
                            count: each.count,
                        };
                    });

                    chart.data = NewChartData;
                    chart.invalidateRawData();
                }

                var chart3 = [{
                    id: "chart1",
                    title: "風險因子",
                    color: "#E98440"
                }, {
                    id: "chart2",
                    title: "保護因子",
                    color: "#3FBEEC"
                }];
                chart3.forEach((subChart, iSub) => {
                    var sub_chart = am4core.registry.baseSprites.find(function (
                        chartObj) {
                        return chartObj.htmlContainer.id === subChart.id;
                    });
                    if (sub_chart === undefined) {
                        var Params = {};
                        Params.ChartType = "Radar";
                        Params.Title = {
                            title: subChart.title,
                            fonSize: 15,
                            color: subChart.color,
                            align: "center",
                            valign: "top",
                            dy: -10
                        };
                        Params.FontSize = 10;
                        Params.name = subChart.title;
                        Params.color = subChart.color;
                        Params.Data = response[subChart.id].map(function (each, i) {
                            return {
                                category: each.category,
                                count: each.count
                            };
                        });
                        MakeAMChart(subChart.id, Params);
                    } else {
                        var NewChartData = response[subChart.id].map(function (each, i) {
                            return {
                                category: each.category,
                                count: each.count
                            };
                        });

                        sub_chart.data = NewChartData;
                        sub_chart.invalidateRawData();
                    }
                });
                download_png('download_area','radar_area');
            });
        }); // end am4core.ready()
    }

    function download_png(div_id,file_name) {
        $('#exportProcess').html('下載中，請稍後...');
        setTimeout(function(){
            // var div_id = 'download_people';
            domtoimage.toBlob(document.getElementById(div_id)).then(function (blob) {
                window.saveAs(blob, file_name+'.png');
                $('#exportProcess').html('完成');
                $('#btnExport').attr('disabled',false);
            });
        }, 1000 * 5);
    }

    function makeWCCityChart() {
        //main_map
        // console.log('popupBox_4');
        $(".js-popupBtn-open-4").click();
        download_png('popupBox_4','radar_city');
    }
    function makeWCHomeChart() {
        // console.log('makeWCHomeChart');
        //main_map
        download_png('main_map','home');
    }
</script>
@endsection
