@extends('layouts.index')

@section('title', '國內外情勢分析')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item"><a href="upload">檔案管理</a></li>
        <li class="breadcrumb-item active" aria-current="page">國內外情勢分析</li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <div class="main-card-contenet">
                <form class="form-inline form-system">
                    <div class="form-row">
                        <div class="col-md">
                            <div class="form-row form-keyword">
                                <div class="form-group">
                                    <label class="sr-only" for="iKeyWord">關鍵字</label>
                                    <input type="text" class="form-control mb-2 mr-sm-2" id="iKeyWord"
                                        placeholder="請輸入關鍵字 " />
                                </div>
                                <div class="form-group">
                                    <button id="searchData" type="button" class="btn btn-gray mb-2">
                                        搜尋
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-auto">
                            <a href="{{route('Upload.FileAnalyzeUpload')}}" class="btn btn-primary mb-2">資料上傳</a>
                        </div>
                    </div>
                </form>

                <!-- table start -->
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-case">
                        <thead>
                            <tr>
                                <th scope="col">資料種類</th>
                                <th scope="col">資料時間</th>
                                <th scope="col">上傳日期</th>
                                <th scope="col">上傳人員</th>
                                <th scope="col">功能</th>
                            </tr>
                        </thead>
                        <tbody id="tBody">
                            {{-- <tr class="">
                                <td date-title="檔案種類">年度資料</td>
                                <td date-title="檔案時間">110/12</td>
                                <td date-title="上傳日期">2021/11/11 12:11:55</td>
                                <td date-title="上傳人員">小薇</td>
                                <td date-title="功能">
                                    <!-- btnBox start -->
                                    <div class="btnBox">
                                        <button type="submit" href="case_Page.html" class="btn btn-danger btn-small">
                                            刪除
                                        </button>
                                    </div>
                                    <!-- btnBox end -->
                                </td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>
                <!-- table end -->
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
    <!-- Modal start-->
    <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="delModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content main-card">
                <input id="delKeyValue" type="hidden">
                <div class="modal-header"></div>
                <div class="modal-body">
                    <p>您是否確定要刪除<span id="delKey"></span>的資料?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-gray" onclick="$('#delModal').modal('hide');">取消</button>
                    <button id="btnDelConfirm" type="button" class="btn btn-primary">確定</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal end -->
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>
</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/FileAnalyze.js')}}"></script>
<script>
    var self={};
    self.Datas = {!! json_encode($Datas, JSON_HEX_TAG) !!};
    var FileAnalyze = new FileAnalyze(self);    
    FileAnalyze.init();
</script>
@endsection
