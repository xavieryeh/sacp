@extends('layouts.index')

@section('title', 'Excel Upload')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item active"  aria-current="page">檔案管理</li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                檔案管理
            </h3>
            <div class="main-card-contenet">
                <p class="main-card-memo">請選擇以下項目進入詳細列表</p>
                <ul class="linkList">
                    <li><a href="fileUpload">檔案上傳</a></li>
                    <li><a href="{{route('fileDownload')}}">檔案下載</a></li>
                    <li><a href="{{route('Upload.FileAnalyze')}}">國內外情勢分析</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->
{{-- <button id="btnUpload" type="button" class="btn btn-primary">再犯率分析匯入Excel</button>
<input multiple type="file" name="file_upload" hidden />
<button id="btnDrugcaseUpload" type="button" class="btn btn-primary">全國各縣市藥癮個案匯入Excel</button>
<input multiple type="file" name="Drugcasefile_upload" hidden />
<button id="btnAllPeopleUpload" type="button" class="btn btn-primary">全國人口匯入Excel</button>
<input multiple type="file" name="AllPeoplefile_upload" hidden />
<button id="btnCatchUpload" type="button" class="btn btn-primary">追輔數匯入Excel</button>
<input multiple type="file" name="Catch_upload" hidden />
<button id="btnServiceUpload" type="button" class="btn btn-primary">轉介服務數匯入Excel</button>
<input multiple type="file" name="Service_upload" hidden />
<button id="btn24HoursUpload" type="button" class="btn btn-primary">24小時專線服務匯入Excel</button>
<input multiple type="file" name="24Hours_upload" hidden />
<button id="btnDrugUpload" type="button" class="btn btn-primary">各級毒品數量及初犯匯入Excel</button>
<input multiple type="file" name="Drug_upload" hidden />
<button id="btnPopulationUpload" type="button" class="btn btn-primary">高雄各區人口數匯入Excel</button>
<input multiple type="file" name="Population_upload" hidden />
<button id="btnMDTreatmentUpload" type="button" class="btn btn-primary">醫療戒治使用人數匯入Excel</button>
<input multiple type="file" name="MDTreatment_upload" hidden />
<button id="btnJuvenile_CourtUpload" type="button" class="btn btn-primary">少家院統計匯入Excel</button>
<input multiple type="file" name="Juvenile_Court_upload" hidden />
<button id="btnCoupling_CounselingUpload" type="button" class="btn btn-primary">銜接輔導統計匯入Excel</button>
<input multiple type="file" name="Coupling_Counseling_upload" hidden />
<button id="btnDrugListUpload" type="button" class="btn btn-primary">中央案管系統匯入Excel</button>
<input multiple type="file" name="DrugList_upload" hidden />
<button id="btnDrugListTeenagerUpload" type="button" class="btn btn-primary">兒少保案管系統匯入Excel</button>
<input multiple type="file" name="DrugListTeenager_upload" hidden />
<button id="btn3_4_LectureUpload" type="button" class="btn btn-primary">三四級講習匯入Excel</button>
<input multiple type="file" name="3_4_Lecture_upload" hidden />
<button id="btnDRUGS_LIST_TEENAGER_InterruptUpload" type="button" class="btn btn-primary">春暉中斷總表匯入Excel</button>
<input multiple type="file" name="DRUGS_LIST_TEENAGER_Interrupt_upload" hidden />
<button id="btnSpecific_BusinessuptUpload" type="button" class="btn btn-primary">特地營業場所匯入Excel</button>
<input multiple type="file" name="Specific_Business_upload" hidden />
<button id="btnViewRecorduptUpload" type="button" class="btn btn-primary">訪視紀錄匯入Excel</button>
<input multiple type="file" name="ViewRecord_upload" hidden /> --}}
@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script>
    // $('#btnUpload').on('click', function (e) {
    //     $("input:file[name='file_upload']").click();
    // });
    // $("input:file[name='file_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_uploadFiles',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             // const url = window.URL.createObjectURL(new Blob([response.data]));
    //             // const link = document.createElement('a');
    //             // link.href = url;
    //             // link.setAttribute('download', 'Month Report.xlsx');
    //             // document.body.appendChild(link);
    //             // link.click();
    //             // document.body.removeChild(link);
    //             // console.log(response);
    //             // alert(response);
    //         });
    // });
    // $('#btnDrugcaseUpload').on('click', function (e) {
    //     $("input:file[name='Drugcasefile_upload']").click();
    // });
    // $("input:file[name='Drugcasefile_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_DrugcaseFiles',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //         });
    // });
    // $('#btnAllPeopleUpload').on('click', function (e) {
    //     $("input:file[name='AllPeoplefile_upload']").click();
    // });
    // $("input:file[name='AllPeoplefile_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_AllPeopleFiles',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             window.location.href = window.location.href;
    //         });
    // });
    // $('#btnCatchUpload').on('click', function (e) {
    //     $("input:file[name='Catch_upload']").click();
    // });
    // $("input:file[name='Catch_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_CatchListFiles',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             window.location.href = window.location.href;
    //         });
    // });
    // $('#btnServiceUpload').on('click', function (e) {
    //     $("input:file[name='Service_upload']").click();
    // });
    // $("input:file[name='Service_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_ServiceListFiles',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             window.location.href = window.location.href;
    //         });
    // });

    // $('#btn24HoursUpload').on('click', function (e) {
    //     $("input:file[name='24Hours_upload']").click();
    // });
    // $("input:file[name='24Hours_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_24HoursFiles',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             window.location.href = window.location.href;
    //         });
    // });

    // $('#btnDrugUpload').on('click', function (e) {
    //     $("input:file[name='Drug_upload']").click();
    // });
    // $("input:file[name='Drug_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_DrugFiles',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             window.location.href = window.location.href;
    //         });
    // });
    // $('#btnPopulationUpload').on('click', function (e) {
    //     $("input:file[name='Population_upload']").click();
    // });
    // $("input:file[name='Population_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_PopulationFiles',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             window.location.href = window.location.href;
    //         });
    // });

    // $('#btnMDTreatmentUpload').on('click', function (e) {
    //     $("input:file[name='MDTreatment_upload']").click();
    // });
    // $("input:file[name='MDTreatment_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_MDTreatmentFiles',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             window.location.href = window.location.href;
    //         });
    // });
    // $('#btnJuvenile_CourtUpload').on('click', function (e) {
    //     $("input:file[name='Juvenile_Court_upload']").click();
    // });
    // $("input:file[name='Juvenile_Court_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_Juvenile_CourtFiles',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             window.location.href = window.location.href;
    //         });
    // });
    // $('#btnCoupling_CounselingUpload').on('click', function (e) {
    //     $("input:file[name='Coupling_Counseling_upload']").click();
    // });
    // $("input:file[name='Coupling_Counseling_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_Coupling_Counseling',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             window.location.href = window.location.href;
    //         });
    // });
    // //中央案管系統
    // $('#btnDrugListUpload').on('click', function (e) {
    //     $("input:file[name='DrugList_upload']").click();
    // });
    // $("input:file[name='DrugList_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_DrugList',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             // window.location.href = window.location.href;
    //         });
    // });
    // //兒少保案管系統
    // $('#btnDrugListTeenagerUpload').on('click', function (e) {
    //     $("input:file[name='DrugListTeenager_upload']").click();
    // });
    // $("input:file[name='DrugListTeenager_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_DrugListTeenager',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             // window.location.href = window.location.href;
    //         });
    // });

    // //三、四級講習系統
    // $('#btn3_4_LectureUpload').on('click', function (e) {
    //     $("input:file[name='3_4_Lecture_upload']").click();
    // });
    // $("input:file[name='3_4_Lecture_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_3_4_Lecture',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             // window.location.href = window.location.href;
    //         });
    // });
    // //春暉中斷總表
    // $('#btnDRUGS_LIST_TEENAGER_InterruptUpload').on('click', function (e) {
    //     $("input:file[name='DRUGS_LIST_TEENAGER_Interrupt_upload']").click();
    // });
    // $("input:file[name='DRUGS_LIST_TEENAGER_Interrupt_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_DRUGS_LIST_TEENAGER_Interrupt',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             // window.location.href = window.location.href;
    //         });
    // });
    // //特定營業場所
    // $('#btnSpecific_BusinessuptUpload').on('click', function (e) {
    //     $("input:file[name='Specific_Business_upload']").click();
    // });
    // $("input:file[name='Specific_Business_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_Specific_Business',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             // window.location.href = window.location.href;
    //         });
    // });
    // //訪視紀錄
    // $('#btnViewRecorduptUpload').on('click', function (e) {
    //     $("input:file[name='ViewRecord_upload']").click();
    // });
    // $("input:file[name='ViewRecord_upload']").on('change', function (e) {
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     var formData = new FormData();
    //     if (event.target.files) {
    //         var fileList = event.target.files;
    //         fileList.forEach(function (file) {
    //             formData.append('Files', file, file.name);
    //         });
    //     };

    //     axios({
    //             url: 'Upload_View_Record',
    //             method: 'post',
    //             headers: {
    //                 "X-CSRF-TOKEN": CSRF_TOKEN
    //             },
    //             data: formData,
    //             // responseType: 'blob'
    //             responseType: 'text'
    //         })
    //         .then((response) => {
    //             alert(response.data);
    //             // window.location.href = window.location.href;
    //         });
    // });

</script>
@endsection
