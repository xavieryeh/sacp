@extends('layouts.index')

@section('title', 'Banner')

@section('breadcrumb')
    <!-- breadcrumb start -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">首頁</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                陸、本市17歲(含)以下藥癮個案概況分析
            </li>
        </ol>
    </nav>
    <!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                陸、本市17歲(含)以下藥癮個案概況分析
            </h3>
            <div class="main-card-contenet">
                <div class="row banner-itemBox">
                    <div class="col-12 col-md-6">
                        <p class="main-card-titleSub titleSubDeco">月報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、性別統計
                                </p>
                                <div class="card-item-pic">
                                    <a  href="{!! route('bannerPage',['id'=>'TeenagerDrugGenderByMonth']) !!}">
                                        <div id="chart1"></div>
                                        {{-- <div id="DrugGenderByMonth" style="height:220px;"></div> --}}
                                        {{-- <div id="DrugGenderByMonthTable" class="cell-border" style="margin-top:-15px;width:50%;position: relative;"></div> --}}
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugGenderByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    二、年齡統計
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerDrugAgeByMonth']) !!}">
                                        <div id="chart2"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugAgeByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    三、藥物濫用類型統計
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerDrugAbuseTypeByMonth']) !!}">
                                        <div id="chart3"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugAbuseTypeByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    四、年齡層與類型交叉分析
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerDrugAgeTypeByMonth']) !!}">
                                        <div id="chart4"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugAgeTypeByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    五、個案區域(戶籍地)未校正數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerDrugAreaByMonth']) !!}">
                                        <div id="chart5"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugAreaByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    六、個案區域(戶籍地)校正數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerDrugCorrectionAreaByMonth']) !!}">
                                        <div id="chart6"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugCorrectionAreaByMonthText"></p>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                            </div>
                            {{-- <div class="card-item">
                                <p class="card-item-title">
                                    七、本市學生春暉專案概況分析
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'DrugTeenagerChunhuiSexByMonth']) !!}">
                                        <div id="chart7"></div>
                                    </a>
                                    <p class="CommentText" id="DrugTeenagerChunhuiSexByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府教育局、教育部高雄市聯絡處</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    八、本市藥癮個案轉介服務數
                                </p>
                                <div class="card-item-pic">
                                    <a  href="{!! route('bannerPage',['id'=>'TeenagerDrugServiceByMonth']) !!}">
                                        <div id="chart8"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugServiceByMonthText"></p>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    九、監所銜接輔導數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerDrugCouplingByMonth']) !!}">
                                        <div id="chart9"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugCouplingByMonthText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    十、醫療戒治累計數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerDrugMedicalByMonth']) !!}">
                                        <div id="chart10"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugMedicalByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府衛生局</p>
                                </div>
                            </div>

                            <div class="card-item">
                                <p class="card-item-title">
                                    十一、24小時專線服務類型累計數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerDrug24HoursByMonth']) !!}">
                                        <div id="chart11"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrug24HoursByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府衛生局</p>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-line-left">
                        <p class="main-card-titleSub titleSubDeco">年報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、性別同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerDrugGenderByYear']) !!}">
                                        <div id="chartY1"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugGenderByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    二、年齡同期比較
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerDrugAgeByYear']) !!}">
                                        <div id="chartY2"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugAgeByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    三、藥物濫用類型同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerDrugAbuseTypeByYear']) !!}">
                                        <div id="chartY3"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugAbuseTypeByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    四、年齡層與類型交叉同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerDrugAgeTypeByYear']) !!}">
                                        <div id="chartY4"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugAgeTypeByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    五、個案區域(戶籍地)未校正數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerDrugAreabyYear']) !!}">
                                        <div id="chartY5"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugAreabyYearText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    六、個案區域(戶籍地)校正數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerDrugCorrectionAreaByYear']) !!}">
                                        <div id="chartY6"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugCorrectionAreaByYearText"></p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            {{-- <div class="card-item">
                                <p class="card-item-title">
                                    七、本市學生春暉專案概況分析
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerDrugCoachByYear']) !!}">
                                        <div id="chartY7"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugCoachByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    八、本市藥癮個案轉介服務數
                                </p>
                                <div class="card-item-pic">
                                    <a  href="{!! route('bannerPage',['id'=>'TeenagerDrugServiceByYear']) !!}">
                                        <div id="chartY8"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugServiceByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：衛福部毒癮單窗服務系統</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    九、監所銜接輔導數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerDrugCouplingByYear']) !!}">
                                        <div id="chartY9"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugCouplingByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府毒品防制局</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    十、醫療戒治累計數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerDrugMedicalByYear']) !!}">
                                        <div id="chartY10"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrugMedicalByYearText"></p>
                                    <p class="main-card-info">來源：高雄市政府衛生局</p>
                                </div>
                            </div>

                            <div class="card-item">
                                <p class="card-item-title">
                                    十一、24小時專線服務類型累計數
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerDrug24HoursByYear']) !!}">
                                        <div id="chartY11"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerDrug24HoursByYearText"></p>
                                    <p class="main-card-info">來源：高雄市政府衛生局</p>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->


@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/TeenagerDrugCase.js')}}"></script>
<script>
am4core.addLicense("{{config('custom.ChartsPN')}}");
$.Log("陸、本市17歲(含)以下藥癮個案概況分析","進入頁面");
getTeenagerDrugCase("TeenagerDrugGenderByMonth","chart1");
getTeenagerDrugCase("TeenagerDrugGenderByYear","chartY1");
getTeenagerDrugCase("TeenagerDrugAgeByMonth","chart2");
getTeenagerDrugCase("TeenagerDrugAgeByYear","chartY2");
getTeenagerDrugCase("TeenagerDrugAbuseTypeByMonth","chart3");
getTeenagerDrugCase("TeenagerDrugAbuseTypeByYear","chartY3");
getTeenagerDrugCase("TeenagerDrugAgeTypeByMonth","chart4");
getTeenagerDrugCase("TeenagerDrugAgeTypeByYear","chartY4");
getTeenagerDrugCase("TeenagerDrugAreaByMonth","chart5");
getTeenagerDrugCase("TeenagerDrugAreabyYear","chartY5");
getTeenagerDrugCase("TeenagerDrugCorrectionAreaByMonth","chart6");
getTeenagerDrugCase("TeenagerDrugCorrectionAreaByYear","chartY6");
// getTeenagerDrugCase("DrugTeenagerChunhuiSexByMonth","chart7");
// getTeenagerDrugCase("TeenagerDrugCoachByYear","chartY7");
// getTeenagerDrugCase("TeenagerDrugServiceByMonth","chart8");
// getTeenagerDrugCase("TeenagerDrugServiceByYear","chartY8");
// getTeenagerDrugCase("TeenagerDrugCouplingByMonth","chart9");
// getTeenagerDrugCase("TeenagerDrugCouplingByYear","chartY9");
// getTeenagerDrugCase("TeenagerDrugMedicalByMonth","chart10");
// getTeenagerDrugCase("TeenagerDrugMedicalByYear","chartY10");
// getTeenagerDrugCase("TeenagerDrug24HoursByMonth","chart11");
// getTeenagerDrugCase("TeenagerDrug24HoursByYear","chartY11");
</script>
@endsection
