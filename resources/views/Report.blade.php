@extends('layouts.index')

@section('title', '產出報表')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item active"  aria-current="page">產出報表</li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                產出報表
            </h3>
            <div class="main-card-contenet">
                <!-- form start -->
                <form id="Form">
                    <div class="form-group">
                        <label class="sr-only" for="reportType">月報</label>
                        <select id="reportType" class="form-control" name="reportType">
                            <option value="月報" selected>月報</option>
                            <option value="年報">年報</option>
                        </select>
                    </div>
                    <div id="monthDiv">
                        <div class="form-group">
                            <label class="sr-only" for="selectYear">請選擇年度</label>
                            <select id="selectYear" class="form-control">
                                <option value="" selected>請選擇年度</option>
                                @foreach ($yearlist as $year)
                                    <option value="{{$year}}">{{$year}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="selectMonth">請選擇月份</label>
                            <select id="selectMonth" class="form-control">
                                <option value="" selected>請選擇月份</option>
                                @foreach ($monthlist as $month)
                                    <option value="{{$month}}">{{$month}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div id="yearDiv">
                        <div class="form-group">
                            <label class="sr-only" for="selectStartYear">請選擇年度</label>
                            <select id="selectStartYear" class="form-control">
                                <option value="" selected>請選擇年度</option>
                                @foreach ($yearlist as $year)
                                    <option value="{{$year}}">{{$year}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="selectEndYear">請選擇年度</label>
                            <select id="selectEndYear" class="form-control">
                                <option value="" selected>請選擇年度</option>
                                @foreach ($yearlist as $year)
                                    <option value="{{$year}}">{{$year}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <!-- btnBox start -->
                    <div class="btnBox">
                        <button id="btnExport" type="button" class="btn btn-primary btn-small">
                            匯出報表
                        </button>
                        <span id="exportProcess"></span>
                    </div>
                    <!-- btnBox end -->
                </form>
                <!-- form end -->
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->

    {{-- 產出charts --}}
    <input name="customchartsPN" type="hidden" value="{{config('custom.ChartsPN')}}">
    <div id="chartGroup" style="z-index: -1;position:fixed;"></div>
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>
    .btnBox{
        display: flex;
        align-items: center;
    }
</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/PoisonAnalysis.js')}}"></script>
<script type="text/javascript" src="{{asset('js/DrugRecidivismRateChart.js')}}"></script>
<script type="text/javascript" src="{{asset('js/TaiwanDrugAnalysis.js')}}"></script>
<script type="text/javascript" src="{{asset('js/DrugTypeAnalysis.js')}}"></script>
<script type="text/javascript" src="{{asset('js/DrugCase.js')}}"></script>
<script type="text/javascript" src="{{asset('js/TeenagerDrugCase.js')}}"></script>
<script type="text/javascript" src="{{asset('js/TeenagerChunhuiDrugCase.js')}}"></script>
<script type="text/javascript" src="{{asset('js/TeenagerInterruptDrugCase.js')}}"></script>
<script type="text/javascript" src="{{asset('js/JuvenileCourtDrugCase.js')}}"></script>
<script type="text/javascript" src="{{asset('js/SpecificArea.js')}}"></script>
<script type="text/javascript" src="{{asset('js/Report.js')}}"></script>
<script>
    var self = {};
    am4core.addLicense("{{config('custom.ChartsPN')}}");
    self.ReportCharts = {!! json_encode($ReportCharts, JSON_HEX_TAG) !!};
    var Report = new Report(self);
    self.init();
</script>
@endsection
