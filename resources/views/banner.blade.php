@extends('layouts.index')

@section('title', 'Banner')

@section('breadcrumb')
    <!-- breadcrumb start -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">首頁</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                月報或年報
            </li>
        </ol>
    </nav>
    <!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                本市查獲毒品概況分析
            </h3>
            <div class="main-card-contenet">
                <div class="row banner-itemBox">
                    <div class="col-12 col-md-6">
                        <p class="main-card-titleSub titleSubDeco">月報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、查獲施用毒品(刑罰)初犯人數同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="bannerPage"><img src="asset/images/bannerMonth.jpg" alt="" /></a>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    二、查獲施用毒品(行政罰)初犯人數同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="bannerPage"><img src="asset/images/bannerMonth.jpg" alt="" /></a>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    三、查獲毒品上游(製造、運輸、販賣)人數同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="bannerPage"><img src="asset/images/bannerMonth.jpg" alt="" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-line-left">
                        <p class="main-card-titleSub titleSubDeco">年報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、查獲施用毒品(刑罰)初犯人數同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="bannerPage"><img src="asset/images/bannerYear.jpg" alt="" /></a>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    二、查獲施用毒品(行政罰)初犯人數同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="bannerPage"><img src="asset/images/bannerYear.jpg" alt="" /></a>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    三、查獲毒品上游(製造、運輸、販賣)人數同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="bannerPage"><img src="asset/images/bannerYear.jpg" alt="" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script>

</script>
@endsection
