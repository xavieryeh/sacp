@extends('layouts.index')

@section('title', 'Home Page')

@section('content')
<div class="content-index">
    <!-- title start -->
    <section class="title">
        <h1>智慧毒防 <span style="color: yellow">數位治理 X 大數據</span></h1>

    </section>
    <!-- title end -->
    <!-- main start -->
    <section class="main">
        <!-- main-cardBox start -->
        <div class="main-cardBox">
            <div class="main-cardBoxItem">
                <div class="main-card js-popupBtn-open-1">
                    {{-- <div class="main-card-btn"></div> --}}
                    <h3 id="chartTitle1" class="main-card-title main-card-title-s"></h3>
                    <div class="main-card-contenet">
                        <div class="chart" id=chart1></div>
                        <div id="chartText1"></div>
                        {{-- <img src="images/chart_1.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource1" class="main-card-info"></p>
                </div>
            </div>
            <div class="main-cardBoxItem">
                <div class="main-card js-popupBtn-open-2">
                    {{-- <div class="main-card-btn"></div> --}}
                    <h3 id="chartTitle2" class="main-card-title main-card-title-s"></h3>
                    <div class="main-card-contenet">
                        <div class="chart" id=chart2></div>
                        <div id="chartText2"></div>
                        {{-- <img src="images/chart_2.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource2" class="main-card-info"></p>
                </div>
            </div>
            <div class="main-cardBoxItem">
                <div class="main-card js-popupBtn-open-3">
                    {{-- <div class="main-card-btn js-popupBtn-open-3"></div> --}}
                    <h3 id="chartTitle3" class="main-card-title main-card-title-s"></h3>
                    <div class="main-card-contenet">
                        <div class="chart" id=chart3></div>
                        <div id="chartText3"></div>
                        {{-- <img src="images/chart_3.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource3" class="main-card-info"></p>
                </div>
            </div>
        </div>
        <!-- main-cardBox end -->

        <!-- main-map start -->
        <div class="main-map">
            <!-- map-notice starr -->
            <div class="map-notice">
                <div class="map-notice-top">
                    {{-- <h2 class="map-notice-title">毒品防制局</h2> --}}
                    <div class="map-notice-itemBox">
                        <div class="map-notice-item">
                            <p class="map-notice-item-top-title">{{date('Y')-1911}}年個案總數</p>
                            <p id="caseCount" class="map-notice-item-top-number"></p>
                        </div>
                        <div class="map-notice-item">
                            <p class="map-notice-item-top-title">本月新增</p>
                            <p id="newCount" class="map-notice-item-top-number"></p>
                        </div>
                    </div>
                </div>

                <div class="map-notice-bottom">
                    <div class="map-notice-itemBox">
                        <div class="map-notice-item">
                            <p class="map-notice-item-bottom-title">行政區個案人數</p>
                            <div id="noticeQuantity" class="map-notice-item-bottom-quantity">
                                {{-- <li>1</li>
                            <li>50</li>
                            <li>100</li>
                            <li>150</li>
                            <li>200</li> --}}
                            </div>
                        </div>
                        {{-- <div class="map-notice-item">
                            <p class="map-notice-item-bottom-title">本月新增</p>
                            <div class="todayMark"></div>
                        </div> --}}
                    </div>
                </div>
            </div>
            <!-- map-notice end -->

            <!-- map-area start -->
            <div class="map-area">
                {{-- <div class="todayMarkBox"></div> --}}
            </div>
            <!-- map-area end -->

            <!-- map-list start -->
            <ol id="mapList" class="map-list"></ol>
            <!-- map-list end -->
        </div>
        <!-- main-map end -->

        <!-- main-cardBox start -->
        <div class="main-cardBox">
            <div class="main-cardBoxItem">
                <div class="main-card js-popupBtn-open-4">
                    {{-- <div class="main-card-btn"></div> --}}
                    <div class="main-card-contenet">
                        <div class="chart" id=chart4></div>
                        {{-- <img src="images/chart_4.jpg" alt="" /> --}}
                    </div>
                </div>
            </div>
            <div class="main-cardBoxItem">
                <div class="main-card js-popupBtn-open-5">
                    {{-- <div class="main-card-btn"></div> --}}
                    <h3 id="chartTitle5" class="main-card-title main-card-title-s"></h3>
                    <div class="main-card-contenet">
                        <div class="chart" id=chart5></div>
                        <div id="chartText5"></div>
                        {{-- <img src="images/chart_5.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource5" class="main-card-info"></p>
                </div>
            </div>
            <div class="main-cardBoxItem">
                <div class="main-card js-popupBtn-open-6">
                    {{-- <div class="main-card-btn"></div> --}}
                    <h3 id="chartTitle6" class="main-card-title main-card-title-s"></h3>
                    <div class="main-card-contenet">
                        <div class="chart" id=chart6></div>
                        <div id="chartText6"></div>
                        {{-- <img src="images/chart_6.jpg" alt="" /> --}}
                    </div>
                    <p id="chartSource6" class="main-card-info"></p>
                </div>
            </div>
        </div>
        <!-- main-cardBox end -->
    </section>
    <!-- main end -->

    <!-- popup start -->
    <div class="popupBox js-popupBox-1">
        <div class="popup">
            <div class="main-card">
                <div class="main-card-btn js-popupBtn-close"></div>
                <h3 id="chartTitle1_popup" class="main-card-title"></h3>
                <div class="main-card-contenet">
                    <div id=chart1_popup></div>
                    <div id="chartText1_popup"></div>
                    {{-- <img src="images/chart_1.jpg" alt="" /> --}}
                </div>
                <p id="chartSource1_popup" class="main-card-info"></p>
            </div>
        </div>
    </div>
    <!-- popup end -->

    <!-- popup start -->
    <div class="popupBox js-popupBox-2">
        <div class="popup">
            <div class="main-card">
                <div class="main-card-btn js-popupBtn-close"></div>
                <h3 id="chartTitle2_popup" class="main-card-title"></h3>
                <div class="main-card-contenet">
                    <div id=chart2_popup></div>
                    <div id="chartText2_popup"></div>
                    {{-- <img src="images/chart_2.jpg" alt="" /> --}}
                </div>
                <p id="chartSource2_popup" class="main-card-info"></p>
            </div>
        </div>
    </div>
    <!-- popup end -->

    <!-- popup start -->
    <div class="popupBox js-popupBox-3">
        <div class="popup">
            <div class="main-card">
                <div class="main-card-btn js-popupBtn-close"></div>
                <h3 id="chartTitle3_popup" class="main-card-title"></h3>
                <div class="main-card-contenet">
                    <div id=chart3_popup></div>
                    <div id="chartText3_popup"></div>
                    {{-- <img src="images/chart_3.jpg" alt="" /> --}}
                </div>
                <p id="chartSource3_popup" class="main-card-info"></p>
            </div>
        </div>
    </div>
    <!-- popup end -->

    <!-- popup start -->
    <div class="popupBox js-popupBox-4">
        <div class="popup">
            <div class="main-card">
                <div class="main-card-btn js-popupBtn-close"></div>
                <h3 class="main-card-title">

                </h3>
                <div class="main-card-contenet">
                    <div id=chart4_popup></div>
                    {{-- <img src="images/chart_4.jpg" alt="" /> --}}
                </div>
            </div>
        </div>
    </div>
    <!-- popup end -->

    <!-- popup start -->
    <div class="popupBox js-popupBox-5">
        <div class="popup">
            <div class="main-card">
                <div class="main-card-btn js-popupBtn-close"></div>
                <h3 id="chartTitle5_popup" class="main-card-title"></h3>
                <div class="main-card-contenet">
                    <div id=chart5_popup></div>
                    <div id="chartText5_popup"></div>
                    {{-- <img src="images/chart_5.jpg" alt="" /> --}}
                </div>
                <p id="chartSource5_popup" class="main-card-info"></p>
            </div>
        </div>
    </div>
    <!-- popup end -->

    <!-- popup start -->
    <div class="popupBox js-popupBox-6">
        <div class="popup">
            <div class="main-card">
                <div class="main-card-btn js-popupBtn-close"></div>
                <h3 id="chartTitle6_popup" class="main-card-title"></h3>
                <div class="main-card-contenet">
                    <div id=chart6_popup></div>
                    <div id="chartText6_popup"></div>
                    {{-- <img src="images/chart_6.jpg" alt="" /> --}}
                </div>
                <p id="chartSource6_popup" class="main-card-info"></p>
            </div>
        </div>
    </div>
    <!-- popup end -->
</div>
@endsection

@section('custom-style')
<style>
    .chart {
        display: block;
    }

    .map-block {
        display: flex;
        justify-content: center;
    }

    .chart3-sub-block {
        display: flex;
        justify-content: space-between;
        height: 50%;
    }

    .chart3-sub-block_popup {
        display: flex;
        justify-content: center;
        height: 50%;
    }

    .PieTable {
        height: 220px;
        display: flex;
        justify-content: center;
        align-items: flex-start;
    }

    .PieTable .chart {
        width: 60%;
    }

    .PieTable .cTable {
        /* width:30%; */
        align-self: flex-end;
    }

    .PieTable_popup {
        display: flex;
        justify-content: center;
        align-items: flex-start;
    }

    .PieTable_popup .chart {
        width: 60%;
    }

    .PieTable_popup .cTable {
        /* width:30%; */
        align-self: flex-end;
    }

</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset_v('js/PoisonAnalysis.js')}}"></script>
<script type="text/javascript" src="{{asset_v('js/DrugRecidivismRateChart.js')}}"></script>
<script type="text/javascript" src="{{asset_v('js/TaiwanDrugAnalysis.js')}}"></script>
<script type="text/javascript" src="{{asset_v('js/DrugTypeAnalysis.js')}}"></script>
<script type="text/javascript" src="{{asset_v('js/DrugCase.js')}}"></script>
<script type="text/javascript" src="{{asset_v('js/Index.js')}}"></script>
<script>
    var self = {};
    self.ChartsPN = "{{config('custom.ChartsPN')}}";
    self.KSareaHref = "{{route('KSarea.getKSarea')}}";

    var IndexPage = new IndexPage(self);
    IndexPage.init();
</script>
@endsection
