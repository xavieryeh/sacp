@extends('layouts.index')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('index')}}">首頁</a></li>
            <li class="breadcrumb-item"><a href="{{route('system.index')}}">系統管理</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                操作檢視記錄
            </li>
        </ol>
    </nav>
@endsection

@section('content')
    <section class="block block-centerY">
        <!-- main-cardBox start -->
        <div class="main-cardBox">
            <div class="main-card">
                <h3 class="main-card-title">
                    操作檢視記錄
                </h3>
                <div class="main-card-contenet">
                    <form class="form-inline form-system">

                        {{--<div class="form-row">--}}
                        {{--    <div class="col">--}}

                        {{--        <div class="form-row form-keyword">--}}
                        {{--            <div class="form-group">--}}
                        {{--                <div class="input-group mb-2 mr-sm-2">--}}
                        {{--                    <div class="input-group-prepend">--}}
                        {{--                        <div class="input-group-text">關鍵字</div>--}}
                        {{--                    </div>--}}
                        {{--                    <select id="inlineFormInputGroupUsername2" class="form-control">--}}
                        {{--                        <option selected>請選擇關鍵字</option>--}}
                        {{--                        <option>---</option>--}}
                        {{--                    </select>--}}
                        {{--                </div>--}}
                        {{--            </div>--}}
                        {{--            <div class="form-group"><button type="submit" class="btn btn-gray mb-2">查詢</button></div>--}}
                        {{--        </div>--}}
                        {{--    </div>--}}
                        {{--</div>--}}
                    </form>

                    <!-- table start -->
                    <div class="table-responsive ">
                        <table class="table table-hover table-bordered table-case">
                            <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">姓名</th>
                                <th scope="col">行為</th>
                                <th scope="col">目標</th>
                                <th scope="col">日期</th>
                                <th scope="col">IP</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($logs as $log)
                            <tr>
                                <td date-title="No." scope="row">{{$log->id}}</td>
                                <td date-title="姓名">{{optional($log->user)->name}}</td>
                                <td date-title="行為">{{$log->func_name}}</td>
                                <td date-title="目標">{{$log->action}}</td>
                                <td date-title="日期">{{$log->created_at}}</td>
                                <td date-title="IP">{{$log->ip}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- table end -->

                    <!-- tableFoot start -->
                    {{--<div class="tableFoot">--}}
                    {{--    <div class="table-quantity">--}}
                    {{--        <P>顯示</P>--}}
                    {{--        <form class="form-inline">--}}
                    {{--            <div class="col-auto">--}}
                    {{--                <label class="sr-only" for="inputState">State</label>--}}
                    {{--                <select id="inputState" class="form-control form-control-sm">--}}
                    {{--                    <option selected>35</option>--}}
                    {{--                    <option>---</option>--}}
                    {{--                </select>--}}
                    {{--            </div>--}}
                    {{--        </form>--}}
                    {{--        <P>項結果，共5432筆</P>--}}
                    {{--    </div>--}}
                    {{--    <div class="table-pagination">--}}
                    {{--        <nav aria-label="Page navigation example">--}}
                    {{--            <ul class="pagination">--}}
                    {{--                <li class="page-item">--}}
                    {{--                    <a class="page-link" href="#">上一項</a>--}}
                    {{--                </li>--}}
                    {{--                <li class="page-item">--}}
                    {{--                    <a class="page-link" href="#">1</a>--}}
                    {{--                </li>--}}
                    {{--                <li class="page-item active" aria-current="page">--}}
                    {{--                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>--}}
                    {{--                </li>--}}
                    {{--                <li class="page-item">--}}
                    {{--                    <a class="page-link" href="#">3</a>--}}
                    {{--                </li>--}}
                    {{--                <li class="page-item">--}}
                    {{--                    <a class="page-link" href="#">下一項</a>--}}
                    {{--                </li>--}}
                    {{--            </ul>--}}
                    {{--        </nav>--}}
                    {{--    </div>--}}
                    {{--</div>--}}
                    <!-- tableFoot end -->
                </div>
            </div>
        </div>
        <!-- main-cardBox end -->
    </section>
@endsection