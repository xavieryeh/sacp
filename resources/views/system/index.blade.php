@extends('layouts.index')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('index')}}">首頁</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                系統管理
            </li>
        </ol>
    </nav>
@endsection

@section('content')
    <section class="block block-centerY">
        <!-- main-cardBox start -->
        <div class="main-cardBox">
            <div class="main-card">
                <h3 class="main-card-title">
                    系統管理
                </h3>
                <div class="main-card-contenet">
                    <p class="main-card-memo">請選擇以下項目進入詳細列表</p>
                    <ul class="linkList">
                        <li><a href="{{route('system.account')}}">帳號管理</a></li>
                        <li><a href="{{route('system.log')}}">操作記錄檢視</a></li>
                        {{--<li><a href="system_setting.html">主畫面小圖設定</a></li>--}}
                    </ul>
                </div>
            </div>
        </div>
        <!-- main-cardBox end -->
    </section>
@endsection