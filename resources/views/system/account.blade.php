@extends('layouts.index')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('index')}}">首頁</a></li>
            <li class="breadcrumb-item"><a href="{{route('system.index')}}">系統管理</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                帳號管理
            </li>
        </ol>
    </nav>
@endsection

@section('content')
    <section class="block block-centerY">
        <!-- main-cardBox start -->
        <div class="main-cardBox">
            <div class="main-card">
                <h3 class="main-card-title">
                    帳號管理
                </h3>
                <div class="main-card-contenet">
                    <form class="form-inline form-system">

                        <div class="form-row">
                            <div class="col-md">

                                {{--<div class="form-row form-keyword">--}}
                                {{--    <div class="form-group">--}}
                                {{--        <div class="input-group mb-2 mr-sm-2">--}}
                                {{--            <div class="input-group-prepend">--}}
                                {{--                <div class="input-group-text">關鍵字</div>--}}
                                {{--            </div>--}}
                                {{--            --}}{{--<select id="inlineFormInputGroupUsername2" class="form-control">--}}
                                {{--            --}}{{--    <option selected>請選擇關鍵字</option>--}}
                                {{--            --}}{{--    <option>---</option>--}}
                                {{--            --}}{{--</select>--}}
                                {{--            <input class="form-control" type="text" name="keyword" placeholder="搜尋姓名、機關、帳號"/>--}}
                                {{--        </div>--}}
                                {{--    </div>--}}
                                {{--    <div class="form-group"><button type="submit" class="btn btn-gray mb-2">查詢</button></div>--}}
                                {{--</div>--}}
                            </div>

                            <div class="col-md-auto">
                                <a href="{{route('system.account_add')}}" class="btn btn-primary mb-2">新增</a>
                            </div>
                        </div>
                    </form>

                    <!-- table start -->
                    <div class="table-responsive ">
                        <table class="table table-hover table-bordered table-case">
                            <thead>
                            <tr>
                                <th scope="col">編號</th>
                                <th scope="col">姓名</th>
                                <th scope="col">機關名稱</th>
                                <th scope="col">權限</th>
                                <th scope="col">信箱(帳號)</th>
                                <th scope="col">建立日期</th>
                                <th scope="col">最後登入日期</th>
                                <th scope="col">啟用</th>
                                <th scope="col">功能</th>

                            </tr>
                            </thead>
                            <tbody>
                            {{--<tr>--}}
                            {{--    <td date-title="編號" scope="row">1</td>--}}
                            {{--    <td date-title="姓名">陳若薇</td>--}}
                            {{--    <td date-title="機關名稱">毒防局</td>--}}
                            {{--    <td date-title="權限">網絡單位使用者</td>--}}
                            {{--    <td date-title="信箱(帳號)">wei@corenet.tw</td>--}}
                            {{--    <td date-title="建立日期">2017/03/15</td>--}}
                            {{--    <td date-title="最後登錄日期">2017/03/15</td>--}}
                            {{--    <td date-title="啟用">--}}
                            {{--        <div class="icon_no"></div>--}}
                            {{--        <div class="icon_ok"></div>--}}
                            {{--    </td>--}}
                            {{--    <td date-title="功能">--}}
                            {{--        <!-- btnBox start -->--}}
                            {{--        <div class="btnBox">--}}
                            {{--            <a href="system_account_new.html" class="btn btn-primary btn-small">--}}
                            {{--                編輯--}}
                            {{--            </a>--}}
                            {{--        </div>--}}
                            {{--        <!-- btnBox end -->--}}
                            {{--    </td>--}}
                            {{--</tr>--}}
                            @foreach($users as $user)
                            <tr>
                                <td date-title="排序" scope="row">{{$user->id}}</td>
                                <td date-title="姓名">{{$user->name}}</td>
                                <td date-title="機關名稱">{{$user->department}}</td>
                                <td date-title="權限">{{implode(',',$user->roles)}}</td>
                                <td date-title="信箱">{{$user->email}}</td>
                                <td date-title="建立日期">{{$user->created_at->format('Y-m-d')}}</td>
                                <td date-title="最後登入日期">{{$user->last_login_at}}</td>
                                <td date-title="啟用">
                                    @if($user->is_enabled)
                                    <div class="icon_ok"></div>
                                    @else
                                    <div class="icon_no"></div>
                                    @endif
                                </td>
                                <td date-title="功能">
                                    <!-- btnBox start -->
                                    {{--<div class="btnBox">--}}
                                        <a href="{{route('system.account_edit',['user'=>$user->id])}}" class="btn btn-primary btn-small text-nowrap">
                                            編輯
                                        </a>
                                        <form method="post" action="{{route('system.account_del',['user'=>$user->id])}}">
                                            @csrf
                                            <button class="btn btn-danger btn-small" onclick="return confirm('確認刪除 [{{$user->email}}]?')">刪除</button>
                                        </form>
                                    {{--</div>--}}
                                    <!-- btnBox end -->
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- table end -->

                    <!-- tableFoot start -->
                    {{--<div class="tableFoot">--}}
                    {{--    <div class="table-quantity">--}}
                    {{--        <P>顯示</P>--}}
                    {{--        <form class="form-inline">--}}
                    {{--            <div class="col-auto">--}}
                    {{--                <label class="sr-only" for="inputState">State</label>--}}
                    {{--                <select id="inputState" class="form-control form-control-sm">--}}
                    {{--                    <option selected>35</option>--}}
                    {{--                    <option>---</option>--}}
                    {{--                </select>--}}
                    {{--            </div>--}}
                    {{--        </form>--}}
                    {{--        <P>項結果，共5432筆</P>--}}
                    {{--    </div>--}}
                    {{--    <div class="table-pagination">--}}
                    {{--        <nav aria-label="Page navigation example">--}}
                    {{--            <ul class="pagination">--}}
                    {{--                <li class="page-item">--}}
                    {{--                    <a class="page-link" href="#">上一項</a>--}}
                    {{--                </li>--}}
                    {{--                <li class="page-item">--}}
                    {{--                    <a class="page-link" href="#">1</a>--}}
                    {{--                </li>--}}
                    {{--                <li class="page-item active" aria-current="page">--}}
                    {{--                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>--}}
                    {{--                </li>--}}
                    {{--                <li class="page-item">--}}
                    {{--                    <a class="page-link" href="#">3</a>--}}
                    {{--                </li>--}}
                    {{--                <li class="page-item">--}}
                    {{--                    <a class="page-link" href="#">下一項</a>--}}
                    {{--                </li>--}}
                    {{--            </ul>--}}
                    {{--        </nav>--}}
                    {{--    </div>--}}
                    {{--</div>--}}
                    <!-- tableFoot end -->
                </div>
            </div>
        </div>
        <!-- main-cardBox end -->
    </section>
@endsection