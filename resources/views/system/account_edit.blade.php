@extends('layouts.index')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('index')}}">首頁</a></li>
            <li class="breadcrumb-item"><a href="{{route('system.index')}}">系統管理</a></li>
            <li class="breadcrumb-item"><a href="{{route('system.account')}}">帳號管理</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                帳號管理
            </li>
        </ol>
    </nav>
@endsection

@section('content')
    <section class="block block-centerY">
        <!-- main-cardBox start -->
        <div class="main-cardBox">
            <div class="main-card">
                <h3 class="main-card-title">
                    帳號管理
                </h3>
                <div class="main-card-contenet">
                    <!-- form start -->
                    <form method="post" action="{{route('system.account_save', ['user'=>$user])}}">
                        @csrf
                        <div class="form-group">
                            <label for="formGroupExampleInput1">信箱(帳號)</label>
                            <input type="text" class="form-control"
                                   maxlength="100"
                                   name="email" value="{{old('email',$user->email)}}"
                                   {{$user->id?'readonly':''}}
                                   id="formGroupExampleInput1" placeholder="請輸入email 最大長度100">
                            @error('email')
                            <div class="alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">姓名</label>
                            <input type="text" class="form-control"
                                   name="name" value="{{old('name',$user->name)}}"
                                   maxlength="20"
                                   id="formGroupExampleInput2" placeholder="請輸入姓名 最大長度20">
                            @error('name')
                            <div class="alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput3">機關名稱</label>
                            <input type="text" class="form-control"
                                   name="department" value="{{old('department',$user->department)}}"
                                   maxlength="20"
                                   id="formGroupExampleInput3" placeholder="請輸入機關名稱 最大長度20">
                            @error('department')
                            <div class="alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput4">密碼</label>
                            <input type="password" class="form-control"
                                   name="password" value=""
                                   id="formGroupExampleInput4" placeholder="{{$user->id?'不修改請留空白':''}}">
                            <div class="">提醒：密碼必須10碼以上,包含英文大小寫、數字、符號</div>
                            @error('password')
                            <div class="alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput4">密碼確認</label>
                            <input type="password" class="form-control"
                                   name="password_confirmation" value=""
                                   id="formGroupExampleInput4" placeholder="{{$user->id?'不修改請留空白':''}}">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput">權限</label>
                            <div class="form-checkBox">
                                @foreach(\App\Models\User::ROLES as $i=>$role)
                                <div class="form-check form-check-inline custom-checkbox">
                                    <input class="form-check-input custom-control-input" type="checkbox" id="inlineCheckbox{{$i}}"
                                           {{in_array($role, old('roles', $user->roles)) ? 'checked':''}}
                                           name="roles[]" value="{{$role}}">
                                    <label class="form-check-label custom-control-label" for="inlineCheckbox{{$i}}">{{$role}}</label>
                                </div>
                                @endforeach
                            </div>
                            @error('roles')
                            <div class="alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput">啟用/停用</label>
                            <div class="radio-beauty-container">
                                <label class="switch-slide">
                                    <input type="checkbox" name="is_enabled" value="1"
                                           id="menu-right" hidden {{old('is_enabled',$user->is_enabled)?'checked':''}}>
                                    <label for="menu-right" class="switch-slide-label"></label>
                                </label>
                            </div>
                        </div>

                    <!-- btnBox start -->
                    <div class="btnBox">
                        <button type="submit" class="btn btn-primary btn-small" data-toggle="modal" data-target="#exampleModal">
                            {{$user->id?'更新':'新增'}}
                        </button>
                    </div>
                    <!-- btnBox end -->

                    </form>
                    <!-- form end -->
                </div>
            </div>
        </div>
        <!-- main-cardBox end -->
    </section>
@endsection