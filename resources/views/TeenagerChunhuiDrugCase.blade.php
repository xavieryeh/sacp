@extends('layouts.index')

@section('title', 'Banner')

@section('breadcrumb')
    <!-- breadcrumb start -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">首頁</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                柒、本市學生春暉專案概況分析
            </li>
        </ol>
    </nav>
    <!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                柒、本市學生春暉專案概況分析
            </h3>
            <div class="main-card-contenet">
                <div class="row banner-itemBox">
                    <div class="col-12 col-md-6">
                        <p class="main-card-titleSub titleSubDeco">月報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、性別統計
                                </p>
                                <div class="card-item-pic">
                                    <a  href="{!! route('bannerPage',['id'=>'TeenagerChunhuiDrugGenderByMonth']) !!}">
                                        <div id="chart1"></div>
                                        {{-- <div id="DrugGenderByMonth" style="height:220px;"></div> --}}
                                        {{-- <div id="DrugGenderByMonthTable" class="cell-border" style="margin-top:-15px;width:50%;position: relative;"></div> --}}
                                    </a>
                                    <p class="CommentText" id="TeenagerChunhuiDrugGenderByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府教育局、教育部高雄市聯絡處</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    二、年齡統計
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerChunhuiDrugAgeByMonth']) !!}">
                                        <div id="chart2"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerChunhuiDrugAgeByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府教育局、教育部高雄市聯絡處</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    三、藥物濫用類型統計
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerChunhuiDrugAbuseTypeByMonth']) !!}">
                                        <div id="chart3"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerChunhuiDrugAbuseTypeByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府教育局、教育部高雄市聯絡處</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    四、年齡層與類型交叉分析
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerChunhuiDrugAgeTypeByMonth']) !!}">
                                        <div id="chart4"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerChunhuiDrugAgeTypeByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府教育局、教育部高雄市聯絡處</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    五、個案區域(戶籍地)
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPage',['id'=>'TeenagerChunhuiDrugAreaByMonth']) !!}">
                                        <div id="chart5"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerChunhuiDrugAreaByMonthText"></p>
                                    <p class="main-card-info">來源：高雄市政府教育局、教育部高雄市聯絡處</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-line-left">
                        <p class="main-card-titleSub titleSubDeco">年報</p>
                        <div class="card-itemBox">
                            <div class="card-item">
                                <p class="card-item-title">
                                    一、性別同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerChunhuiDrugGenderByYear']) !!}">
                                        <div id="chartY1"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerChunhuiDrugGenderByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府教育局、教育部高雄市聯絡處</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    二、年齡同期比較
                                </p>
                                <div class="card-item-pic" >
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerChunhuiDrugAgeByYear']) !!}">
                                        <div id="chartY2"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerChunhuiDrugAgeByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府教育局、教育部高雄市聯絡處</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    三、藥物濫用類型同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerChunhuiDrugAbuseTypeByYear']) !!}">
                                        <div id="chartY3"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerChunhuiDrugAbuseTypeByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府教育局、教育部高雄市聯絡處</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    四、年齡層與類型交叉同期比較
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerChunhuiDrugAgeTypeByYear']) !!}">
                                        <div id="chartY4"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerChunhuiDrugAgeTypeByYearText">&nbsp;</p>
                                    <p class="main-card-info">來源：高雄市政府教育局、教育部高雄市聯絡處</p>
                                </div>
                            </div>
                            <div class="card-item">
                                <p class="card-item-title">
                                    五、個案區域(戶籍地)
                                </p>
                                <div class="card-item-pic">
                                    <a href="{!! route('bannerPageforYear',['id'=>'TeenagerChunhuiDrugAreaByYear']) !!}">
                                        <div id="chartY5"></div>
                                    </a>
                                    <p class="CommentText" id="TeenagerChunhuiDrugAreaByYearText"></p>
                                    <p class="main-card-info">來源：高雄市政府教育局、教育部高雄市聯絡處</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
</section>
<!-- main end -->


@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script type="text/javascript" src="{{asset('js/TeenagerChunhuiDrugCase.js')}}"></script>
<script>
am4core.addLicense("{{config('custom.ChartsPN')}}");
$.Log("柒、本市學生春暉專案概況分析","進入頁面");
getTeenagerChunhuiDrugCase("TeenagerChunhuiDrugGenderByMonth","chart1");
getTeenagerChunhuiDrugCase("TeenagerChunhuiDrugGenderByYear","chartY1");
getTeenagerChunhuiDrugCase("TeenagerChunhuiDrugAgeByMonth","chart2");
getTeenagerChunhuiDrugCase("TeenagerChunhuiDrugAgeByYear","chartY2");
getTeenagerChunhuiDrugCase("TeenagerChunhuiDrugAbuseTypeByMonth","chart3");
getTeenagerChunhuiDrugCase("TeenagerChunhuiDrugAbuseTypeByYear","chartY3");
getTeenagerChunhuiDrugCase("TeenagerChunhuiDrugAgeTypeByMonth","chart4");
getTeenagerChunhuiDrugCase("TeenagerChunhuiDrugAgeTypeByYear","chartY4");
getTeenagerChunhuiDrugCase("TeenagerChunhuiDrugAreaByMonth","chart5");
getTeenagerChunhuiDrugCase("TeenagerChunhuiDrugAreaByYear","chartY5");
</script>
@endsection
