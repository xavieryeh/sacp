@extends('layouts.index')

@section('title', '個案基本資料')

@section('breadcrumb')
<!-- breadcrumb start -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index">首頁</a></li>
        <li class="breadcrumb-item"><a href="KSarea_1.html">茄萣區</a></li>
        <li class="breadcrumb-item">
            <a href="KSarea_page.html">仁和里</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            001
        </li>
    </ol>
</nav>
<!-- breadcrumb end -->
@endsection

@section('content')
<!-- main start -->
<section class="block block-centerY">
    <!-- main-cardBox start -->
    <div class="main-cardBox">
        <div class="main-card">
            <h3 class="main-card-title">
                個案基本資料
            </h3>
            <div class="main-card-contenet">
                <!-- table start -->
                <div class="table-responsive table-KSarea_page_own">
                    <table class="table table-bordered table-case">
                        <tbody>
                            <tr>
                                <th scope="col">來源編號</th>
                                <th scope="col">姓名</th>
                                <th scope="col">類型</th>
                                <th scope="col">毒品級數</th>
                                <th scope="col">性別</th>
                                <th scope="col">出生年月日</th>
                                <th scope="col">婚姻狀況</th>
                                <th scope="col">學歷</th>
                            </tr>
                            <tr>
                                <td date-title="來源編號" scope="row">001</td>
                                <td date-title="姓名">王小明</td>
                                <td date-title="類型"></td>
                                <td date-title="毒品級數">一</td>
                                <td date-title="性別">男</td>
                                <td date-title="出生年月日">1988/05/06</td>
                                <td date-title="婚姻狀況">無</td>
                                <td date-title="學歷">高中</td>
                            </tr>
                            <tr>
                                <th colspan="2" scope="col">戶籍地址</th>
                                <th colspan="2">居住地</th>
                                <th>就業</th>
                                <th>在學</th>
                                <th>工作類型</th>
                                <th>結案日期</th>
                            </tr>
                            <tr>
                                <td date-title="戶籍地址" colspan="2" scope="row">
                                    縣市、鄉鎮區、里
                                </td>
                                <td date-title="居住地" colspan="2">縣市、鄉鎮區</td>
                                <td date-title="就業">無</td>
                                <td date-title="在學">無</td>
                                <td date-title="工作類型"></td>
                                <td date-title="結案日期">2022/10/11</td>
                            </tr>
                            <tr>
                                <th colspan="6" scope="col">結案原因</th>
                                <th>家戶有吸毒者</th>
                                <th>多次施用</th>
                            </tr>
                            <tr>
                                <td date-title="結案原因" colspan="6" scope="row"></td>
                                <td date-title="家戶有吸毒者"></td>
                                <td date-title="多次施用"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- table end -->
            </div>
        </div>
        <div class="main-card">
            <h3 class="main-card-title">
                雷達圖與文字雲
            </h3>
            <div class="main-card-contenet">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <img src="asset/images/KSarea_pic2.jpg" alt="" />
                    </div>
                    <div class="col-12 col-md-4">
                        <img src="asset/images/KSarea_pic3.jpg" alt="" />
                    </div>
                    <div class="col-12 col-md-4">
                        <img src="asset/images/KSarea_pic4.jpg" alt="" />
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card">
            <h3 class="main-card-title">
                家系圖
            </h3>
            <div class="main-card-contenet">
                <div id="myDiagramDiv" style="background-color: #F8F8F8; border: solid 1px black; width:100%; height:600px;"></div>
            </div>
        </div>
    </div>
    <!-- main-cardBox end -->
    <!-- btnBox start -->
    <div class="btnBox">
        <a href="KSarea_page.html" class="btn btn-dark btn-small">
            返回
        </a>
    </div>
    <!-- btnBox end -->
</section>
<!-- main end -->
@endsection

@section('custom-style')
<style>

</style>
@endsection

@section('custom-script')
<script>
    getGenogram();

    //取得家系圖
    function getGenogram() {
        $.PostAJAX("KSarea_getGenogram", {}, response => {
            var Params = {};
            Params.bindingData = response.bindingData;
            Params.Data = [];
            response.Data.forEach(element => {
                var vRelation = response.Relation.filter(f => {
                    return f.Title == element.Title
                })[0];
                var PersonData = {};
                PersonData["key"] = parseInt(element.Key);
                PersonData["n"] = element.Name;
                PersonData["s"] = element.Gender;
                if (vRelation !== undefined) {
                    PersonData["m"] = vRelation.m === undefined ? undefined : (response.Data
                        .filter(f => {
                            return f.Title == vRelation.m
                        }).length == 0 ? undefined : parseInt(response.Data.filter(
                            f => {
                                return f.Title == vRelation.m
                            })[0].Key));
                    PersonData["f"] = vRelation.f === undefined ? undefined : (response.Data
                        .filter(f => {
                            return f.Title == vRelation.f
                        }).length == 0 ? undefined : parseInt(response.Data.filter(
                            f => {
                                return f.Title == vRelation.f
                            })[0].Key));
                    PersonData["ux"] = vRelation.ux === undefined ? undefined : (response
                        .Data.filter(f => {
                            return f.Title == vRelation.ux
                        }).length == 0 ? undefined : parseInt(response.Data.filter(
                            f => {
                                return f.Title == vRelation.ux
                            })[0].Key));
                    PersonData["vir"] = vRelation.vir === undefined ? undefined : (response
                        .Data.filter(f => {
                            return f.Title == vRelation.vir
                        }).length == 0 ? undefined : parseInt(response.Data.filter(
                            f => {
                                return f.Title == vRelation.vir
                            })[0].Key));
                };
                PersonData["coh"] = element.Cohabit;
                PersonData["lv"] = parseInt(vRelation.Level);
                PersonData["a"] = element.DrugRecoder.split(',');
                Params.Data.push(PersonData);
            });
            makeGenogram("myDiagramDiv", Params);
        });
    }

</script>
@endsection
